function this.listcalls(this)
	table.foreach( this.functions, function(key, value)
			print( string.format("%.4x", key) )
			table.foreach(value, print)
			end)
end

function this.addcall(this, address, functionname)
	table.insert( this.stack, address )

	if( this.functions[ address ] == nil ) then
		this.functions[address] = {
			name = functionname,
			calls = 1,
			start = this.ticks,
			maxlength = 0,
			frequency = 0
		}
	else
		f = this.functions[address]
		f.frequency = ((f.frequency * (f.calls - 1)) + (this.ticks - f.start)) / f.calls
		f.calls = f.calls + 1
		f.start = this.ticks
	end
end

function this.endcall(this)
	address = table.remove( this.stack )

	f = this.functions[address]
	if( f ~= nil ) then
		len = this.ticks - f.start
		if( len > f.maxlength ) then
			f.maxlength = len
		end
	end
end

function this.finish(this)
	this:listcalls()
end

function this.call(this, address, push)
	if( push ) then
--		print( "Call ", string.format("%.4x", address), " ", Program:functionName(address) )
		this:addcall(address, Program:functionName(address) )
	end
end

function this.interrupt(this, vector, address)
--	print( "Int ", string.format("%.2d %.4x", vector, address) )
	this:addcall( address, string.format("vector_%.2d", vector) )
end

function this.ret(this, interrupt)
	this:endcall()
--[[
	if( interrupt ) then
		print("Reti")
	else
		print("Ret")
	end
--]]
end

this.functions = {}
this.stack = {}
