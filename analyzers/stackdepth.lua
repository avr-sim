function this.reset(this, type)
	this.depth = 0;
end

function this.update(this)
	if( this.maxdepth < this.depth ) then
		this.maxdepth = this.depth
	end
end

function this.call(this, address, push)
	if( push ) then
		this.depth = this.depth + this.pcBytes
		this:update()
	end
end

function this.interrupt(this, vector, address)
	this.depth = this.depth + this.pcBytes
	this:update()
end

function this.ret(this, interrupt)
	this.depth = this.depth - this.pcBytes
end

function this.push(this, val)
	this.depth = this.depth + 1
	this:update()
end

function this.pop(this, val)
	this.depth = this.depth - 1
end

function this.finish(this)
	print( "Maximum stack depth = ", this.maxdepth )
end

this.depth = 0
this.maxdepth = 0
