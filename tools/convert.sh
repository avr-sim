#!/bin/bash

DIR=../../Partdescriptionfiles

echo > error
ls $DIR | while read i; do
	name=$( (echo ${i%.xml} | tr "A-Z" "a-z") )
	echo $name
	xsltproc convert.xsl $DIR/$i > $name 2>> error
done
