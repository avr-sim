BEGIN { print "<ioregisters>" }
END { print "</ioregisters>" }

function xml(addr, name, comment) {
	if( name != "Reserved" )
		print "\t<ioreg address=\"" addr "\" name=\"" name "\"> <!-- " comment " --></ioreg>"
}

$1 ~ /0x[0-9A-F][0-9A-F]/ && $2 ~ /\(0x[0-9A-F][0-9A-F]\)/ {
	where = match($1, /0x[0-9A-F][0-9A-F]/)
	if( where ) {
		addr = substr($1, where, 4)
		comment = ""
		for(x  = 4; x <= NF; ++x) {
			sub(/[^a-zA-Z0-9]/, "", $x)
			comment = comment " " $x
		}
		xml(addr, $3, comment)
	}
	next
}

$1 ~ /\(?0x[0-9A-F][0-9A-F]\)?/ {
	where = match($1, /0x[0-9A-F][0-9A-F]/)
	if( where ) {
		addr = substr($1, where, 4)
		comment = ""
		for(x  = 3; x <= NF; ++x) {
			sub(/[^a-zA-Z0-9]/, "", $x)
			comment = comment " " $x
		}
		xml(addr, $2, comment)
	}
	next
}
