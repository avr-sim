#!/bin/bash

if [ $# -lt 3 ]; then
	echo "Usage: $0 <pdf> <start> <end>";
	exit 1;
fi

pdf=$1
start=$2
end=$3

pdftotext -f $start -l $end -layout $pdf - | awk -f registers.awk

