#!/bin/bash

if [ $# -lt 2 ]; then
	echo "Usage: $0 <pdf> <page>";
	exit 1;
fi

pdf=$1
page=$2

pdftotext -f $page -l $page -layout $pdf - | awk -f interrupts.awk

