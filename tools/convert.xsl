<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes" doctype-system="device.dtd"/>

	<xsl:template match="AVRPART">
		<device>
			<xsl:apply-templates select="MEMORY"/>
			<xsl:apply-templates select="INTERRUPT_VECTOR"/>
			<xsl:apply-templates select="PACKAGE"/>
			<xsl:apply-templates select="V2"/>
		</device>
	</xsl:template>

	<xsl:template match="MEMORY">
		<memory>
			<flash>
				<xsl:attribute name="size">
					<xsl:value-of select="PROG_FLASH/text()"/>
				</xsl:attribute>
				<xsl:attribute name="page">
					<xsl:value-of select="/AVRPART/PROGRAMMING/FlashPageSize/text()"/>
				</xsl:attribute>
			</flash>

			<iospace>
				<xsl:attribute name="start">
					<xsl:value-of
						select="IO_MEMORY/MEM_START_ADDR/text()"/>
				</xsl:attribute>
				<xsl:attribute name="stop">
					<xsl:value-of
						select="IO_MEMORY/MEM_STOP_ADDR/text()"/>
				</xsl:attribute>
			</iospace>

			<sram>
				<xsl:attribute name="size">
					<xsl:value-of select="INT_SRAM/SIZE/text()"/>
				</xsl:attribute>
			</sram>

			<eram>
				<xsl:attribute name="size">
					<xsl:value-of select="EXT_SRAM/SIZE/text()"/>
				</xsl:attribute>
			</eram>
		</memory>

		<xsl:apply-templates select="IO_MEMORY"/>
	</xsl:template>

	<xsl:template match="IO_MEMORY">
		<ioregisters>
			<xsl:for-each select="*">
				<xsl:sort select="MEM_ADDR/text()"/>
				<xsl:if test="not( contains(name(), 'ADDR') )">
				<ioreg>
					<xsl:attribute name="name">
						<xsl:value-of select="name()"/>
					</xsl:attribute>

					<xsl:attribute name="address">
						<xsl:value-of select="MEM_ADDR/text()"/>
					</xsl:attribute>
				</ioreg>
				</xsl:if>
			</xsl:for-each>
		</ioregisters>
	</xsl:template>

	<xsl:template match="PACKAGE">
		<packages>
			<xsl:for-each select="PDIP | TQFP | MLF">
				<package>
					<xsl:attribute name="name">
						<xsl:value-of select="name()"/>
					</xsl:attribute>

					<xsl:attribute name="pins">
						<xsl:value-of select="NMB_PIN/text()"/>
					</xsl:attribute>

					<xsl:for-each select="*">
						<xsl:if test="starts-with(name(), 'PIN')">
							<pin>
								<xsl:attribute name="id">
									<xsl:value-of select="substring(name(), 4)"/>
								</xsl:attribute>

								<xsl:attribute name="name">
									<xsl:value-of select="NAME/text()"/>
								</xsl:attribute>

								<xsl:value-of select="TEXT/text()"/>
							</pin>
						</xsl:if>
					</xsl:for-each>
				</package>
			</xsl:for-each>
		</packages>
	</xsl:template>

	<xsl:template match="INTERRUPT_VECTOR">
		<interrupts>
		<xsl:attribute name="num">
			<xsl:value-of select="NMB_VECTORS/text()"/>
		</xsl:attribute>

		<xsl:for-each select="*">
			<xsl:if test="starts-with(name(), 'VECTOR')">
				<interrupt>
					<xsl:attribute name="vector">
						<xsl:value-of select="substring(name(), 7)"/>
					</xsl:attribute>

					<xsl:attribute name="address">
						<xsl:value-of select="PROGRAM_ADDRESS/text()"/>
					</xsl:attribute>

					<xsl:attribute name="name">
						<xsl:value-of select="SOURCE/text()"/>
					</xsl:attribute>

					<xsl:value-of select="DEFINITION/text()"/>
				</interrupt>
			</xsl:if>
		</xsl:for-each>
		</interrupts>
	</xsl:template>

	<xsl:template match="module">
		<module>
			<xsl:attribute name="class">
				<xsl:value-of select="@class"/>
			</xsl:attribute>

			<xsl:copy-of select="registers"/>
		</module>
	</xsl:template>

	<xsl:template match="V2">
		<hardware>
			<xsl:comment>Everything after this needs editing!!!</xsl:comment>
			<xsl:apply-templates select="templates/module"/>
		</hardware>
	</xsl:template>

</xsl:stylesheet>
