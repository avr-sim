BEGIN { print "<interrupts>" }
END { print "</interrupts>" }

function xml(vector, addr, name, comment) {
	print "\t<interrupt vector=\"" vector "\" address=\"" addr "\" name=\"" name "\"> <!-- " comment " --></interrupt>"
}

$1 ~ /[0-9]+/ && $2 ~ /0x[0-9A-F]+/ {

	comment = ""
	for(x  = 4; x <= NF; ++x) {
		sub(/[^a-zA-Z0-9]/, "", $x)
		comment = comment " " $x
	}

	xml($1, $2, $3, comment)
	next
}

