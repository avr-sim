# - Try to find BFD
# Once done, this will define
#
#  BFD_FOUND - system has BFD
#  BFD_INCLUDE_DIRS - the BFD include directories
#  BFD_LIBRARIES - link these to use BFD

generate_findpkg(BFD libbfd LIBNAMES bfd INCNAMES bfd.h)
