# - Try to find XML2
# Once done, this will define
#
#  XML2_FOUND - system has XML2
#  XML2_INCLUDE_DIRS - the XML2 include directories
#  XML2_LIBRARIES - link these to use XML2

generate_findpkg(XML2 libxml2 LIBNAMES xml2 INCNAMES libxml/parser.h)
