# should we build static libs?
if (AVRSIM_STATIC)
  set(AVRSIM_LIB_TYPE STATIC)
else ()
  set(AVRSIM_LIB_TYPE SHARED)
endif ()

set(AVRSIM_CONFIG_THREADS 1)

# Create the pkg-config package files on Unix systems
if (UNIX)
  if (AVRSIM_STATIC)
    set(AVRSIM_LIB_SUFFIX "${AVRSIM_LIB_SUFFIX}Static")
  endif ()
  string(TOLOWER "${CMAKE_BUILD_TYPE}" AVRSIM_BUILD_TYPE)
  if (AVRSIM_BUILD_TYPE STREQUAL "debug")
    set(AVRSIM_LIB_SUFFIX "${AVRSIM_LIB_SUFFIX}_d")
  endif ()

  set(AVRSIM_ADDITIONAL_LIBS "")
  set(AVRSIM_CFLAGS "")
  set(AVRSIM_PREFIX_PATH ${CMAKE_INSTALL_PREFIX})
  if (AVRSIM_CONFIG_THREADS GREATER 0)
    set(AVRSIM_CFLAGS "-pthread")
    set(AVRSIM_ADDITIONAL_LIBS "${AVRSIM_ADDITIONAL_LIBS} -lpthread")
  endif ()

endif ()

if (AVRSIM_STANDALONE_BUILD)
  set(CMAKE_USE_RELATIVE_PATHS true)
  set(CMAKE_SUPPRESS_REGENERATION true)
endif()

if (MSVC)
  # Enable intrinsics on MSVC in debug mode
  # Not actually necessary in release mode since /O2 implies /Oi but can't easily add this per build type?
  add_definitions(/Oi)
endif (MSVC)

