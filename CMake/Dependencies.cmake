#######################################################################
# Find all necessary and optional AVRSIM dependencies
#######################################################################

# AVRSIM_DEPENDENCIES_DIR can be used to specify a single base
# folder where the required dependencies may be found.
set(AVRSIM_DEPENDENCIES_DIR "" CACHE PATH "Path to prebuilt AVRSIM dependencies")
include(FindPkgMacros)
include(ParseArguments)
getenv_path(AVRSIM_DEPENDENCIES_DIR)
set(AVRSIM_DEP_SEARCH_PATH
    ${AVRSIM_DEPENDENCIES_DIR}
    ${ENV_AVRSIM_DEPENDENCIES_DIR}
    "${AVRSIM_BINARY_DIR}/Dependencies"
    "${AVRSIM_SOURCE_DIR}/Dependencies"
    "${AVRSIM_BINARY_DIR}/../Dependencies"
    "${AVRSIM_SOURCE_DIR}/../Dependencies"
)

# Set hardcoded path guesses for various platforms
if (UNIX)
  set(AVRSIM_DEP_SEARCH_PATH ${AVRSIM_DEP_SEARCH_PATH} /usr/local)
endif ()

# give guesses as hints to the find_package calls
set(CMAKE_PREFIX_PATH ${CMAKE_PREFIX_PATH} ${AVRSIM_DEP_SEARCH_PATH})
set(CMAKE_FRAMEWORK_PATH ${CMAKE_FRAMEWORK_PATH} ${AVRSIM_DEP_SEARCH_PATH})

MACRO (generate_extract_options PACKAGENAME)
	parse_arguments(${PACKAGENAME} "LIBNAMES;INCNAMES;INCSUFFIXES;LIBSUFFIXES" "" ${ARGN})
	SET( ${PACKAGENAME}_LIBRARY_NAMES ${${PACKAGENAME}_LIBNAMES})
	SET( ${PACKAGENAME}_INCLUDE_NAMES ${${PACKAGENAME}_INCNAMES})
	SET( ${PACKAGENAME}_INC_SUFFIXES ${${PACKAGENAME}_INCSUFFIXES})
	SET( ${PACKAGENAME}_LIB_SUFFIXES ${${PACKAGENAME}_LIBSUFFIXES})
ENDMACRO (generate_extract_options)

macro(generate_findbundle BUNDLENAME BNDLNAME PACKAGES)
	if (NOT ${BUNDLENAME}_FOUND)
		set( ${BUNDLENAME}_LIBRARIES )
		set( ${BUNDLENAME}_FOUND TRUE )
		foreach(pkg ${PACKAGES})
			set(PREFIX "${BUNDLENAME}_${pkg}")
			set( ${PREFIX}_FIND_QUIETLY FALSE )
			generate_findpkg(${pkg} "${BNDLNAME};${pkg}"
				   LIBNAMES "${${PREFIX}_LIBRARY_NAMES}"
				   INCNAMES "${${PREFIX}_INCLUDE_NAMES}"
				   INCSUFFIXES "${${PREFIX}_INCLUDE_SUFFIXES};${BNDLNAME}"
				   LIBSUFFIXES "${${PREFIX}_LIBRARY_SUFFIXES};${BNDLNAME}")

			if( ${pkg}_FOUND )
				set( ${BUNDLENAME}_INCLUDE_DIRS ${${BUNDLENAME}_INCLUDE_DIRS} ${${pkg}_INCLUDE_DIRS} )
				set( ${BUNDLENAME}_LIBRARIES ${${BUNDLENAME}_LIBRARIES} ${${pkg}_LIBRARIES} )
			else( ${pkg}_FOUND )
				set( ${BUNDLENAME}_FOUND FALSE )
			endif( ${pkg}_FOUND )
		endforeach(pkg)

		if( NOT ${BUNDLENAME}_FOUND )
			set( ${BUNDLENAME}_INCLUDE_DIRS )
			set( ${BUNDLENAME}_LIBRARIES )
		endif( NOT ${BUNDLENAME}_FOUND )

	endif (NOT ${BUNDLENAME}_FOUND)
endmacro(generate_findbundle)

# Macro to easily generate a Findxxx.cmake script for an external library that conforms to our directory structures and such.
# PACKAGENAME is the name of the external package you are writing the find script for.
# LIBNAME is the name of the library that has to be searched for and linked with.
# INCNAME is the include file that is used to find the include directory for the package.
macro (generate_findpkg PACKAGENAME PKGNAME)
	generate_extract_options(${PACKAGENAME} ${ARGN})
	findpkg_begin (${PACKAGENAME})

	getenv_path (${PACKAGENAME}_HOME)

	# Construct the search path
	set (${PACKAGENAME}_PREFIX_PATH ${${PACKAGENAME}_HOME} ${ENV_${PACKAGENAME}_HOME})
	create_search_paths (${PACKAGENAME})
	foreach(n ${PKGNAME})
		create_suffixes (${PACKAGENAME} ${n})
	endforeach(n)

	# Redo search if prefix path changed
	clear_if_changed (${PACKAGENAME}_PREFIX_PATH
	  ${PACKAGENAME}_LIBRARY_FWK
	  ${PACKAGENAME}_LIBRARY_REL
	  ${PACKAGENAME}_LIBRARY_DBG
	  ${PACKAGENAME}_INCLUDE_DIR
	)

	get_debug_names (${PACKAGENAME}_LIBRARY_NAMES)

#	use_pkgconfig (${PACKAGENAME}_PKGC ${LIBNAME})

	findpkg_framework (${PACKAGENAME})

	find_path(${PACKAGENAME}_INCLUDE_DIR NAMES ${${PACKAGENAME}_INCLUDE_NAMES} HINTS ${${PACKAGENAME}_INC_SEARCH_PATH} ${${PACKAGENAME}_PKGC_INCLUDE_DIRS} PATH_SUFFIXES ${${PACKAGENAME}_INC_SUFFIXES})
	find_library(${PACKAGENAME}_LIBRARY_REL NAMES ${${PACKAGENAME}_LIBRARY_NAMES} HINTS ${${PACKAGENAME}_LIB_SEARCH_PATH} ${${PACKAGENAME}_PKGC_LIBRARY_DIRS} PATH_SUFFIXES ${${PACKAGENAME}_LIB_SUFFIXES} release relwithdebinfo minsizerel)
	find_library(${PACKAGENAME}_LIBRARY_DBG NAMES ${${PACKAGENAME}_LIBRARY_NAMES_DBG} HINTS ${${PACKAGENAME}_LIB_SEARCH_PATH} ${${PACKAGENAME}_PKGC_LIBRARY_DIRS} PATH_SUFFIXES ${${PACKAGENAME}_LIB_SUFFIXES} debug)
	make_library_set(${PACKAGENAME}_LIBRARY)

	findpkg_finish(${PACKAGENAME})
endmacro (generate_findpkg)

#######################################################################
# Core dependencies
#######################################################################

# Find devil
find_package(Xml2)
macro_log_feature(XML2_FOUND "Xml2" "xml parsing library" "http://" TRUE "" "")

#######################################################################
# Tools
#######################################################################

find_package(Doxygen)
macro_log_feature(DOXYGEN_FOUND "Doxygen" "Tool for building API documentation" "http://doxygen.org" FALSE "" "")

# Display results, terminate if anything required is missing
MACRO_DISPLAY_FEATURE_LOG()

include_directories( ${XML2_INCLUDE_DIRS} )
