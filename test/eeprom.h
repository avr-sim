#ifndef EEPROM_H
#define EEPROM_H

#include <stdint.h>

#ifdef HAVE_SPI
uint8_t spieeprom_read(uint16_t addr);
void spieeprom_write(uint8_t data, uint16_t addr);
#endif

uint8_t internal_eeprom_read8(uint16_t addr);
void internal_eeprom_write8(uint8_t data, uint16_t addr);
#endif
