#ifndef ADIO_H
#define ADIO_H

#include <stdint.h>

extern void init_adc(void);
extern uint16_t analogRead(uint8_t chan);
#endif

