#include <avr/io.h>
#include "adio.h"

//#define ADC_ADLAR
void init_adc(void) {
	// set AD reference to AVCC (5 volts)
#ifdef ADC_ADLAR
	ADMUX = _BV(REFS0)|_BV(ADLAR);
#else
	ADMUX = _BV(REFS0);
#endif

	// 32 divider => 8 MHz / 32 = 250 kHz
	// 250 kHz clock -> 16kHz sampling freq
//	ADCSRA = _BV(ADPS0)|_BV(ADPS2)|_BV(ADEN);

//	// 64 divider => 8 MHz / 64 = 125 kHz
//	// 125 kHz clock -> 8kHz sampling freq
	ADCSRA = _BV(ADPS1)|_BV(ADPS2)|_BV(ADEN);
}

#define analogInChanToBit(P) (P)
uint16_t analogRead(uint8_t chan) {
	uint8_t low, high, ch = analogInChanToBit(chan);

	// the low 4 bits of ADMUX select the ADC channel
	ADMUX = (ADMUX & (unsigned int) 0xf0) | (ch & (unsigned int) 0x0f);

	// start the conversion
	ADCSRA |= _BV(ADSC);

	// ADSC is cleared when the conversion finishes
	while (bit_is_set(ADCSRA, ADSC));

	// we have to read ADCL first, doing so locks both ADCL
	// and ADCH until ADCH is read. reading ADCL second would
	// cause the results of each conversion to be discarded,
	// as ADCL and ADCH would be locked when it completes.
	low = ADCL;
	high = ADCH;

#ifdef ADC_ADLAR
	return (high << 8) | low;
#else
	return (high << 2) | (low >> 6);
#endif
}

