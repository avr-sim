#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include "adio.h"

ISR(SIG_ADC) {
	static volatile uint16_t last = 0;
	if( (ADCSRA & _BV(ADSC)) != 0 ) {
		last = 1;
	}

	last = ADC;
}

int main(void) {
	init_adc();

	int8_t i;
	uint16_t sum = 0;
	for(i = 0; i < 5; ++i)
		sum += analogRead(i);

	if( sum == 0 )
		PORTB |= PB1;

	ADMUX = (ADMUX & ~0x7) | 2;
	ADCSRA |= (_BV(ADFR) | _BV(ADIE));
	sei();  // enable interrupts
	ADCSRA |= _BV(ADSC);

	return 0;
}

void io_init() {
	DDRB = 0xB9;              // spi_clk, spi_in, spi_out, NC, TX, RX, clock in, clock
	PORTB = 0x02;

	DDRC = 0xFF;              // Address
	PORTC = 0x00;

	DDRD = 0xFA;              // NC, NC, NC, NC, NC, CAN_INT, MIDI TX & RX
	PORTD = 0x00;
}

