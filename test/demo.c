#include <inttypes.h>
#include <avr/io.h>

int main(void)
{
    uint8_t cnt;

    DDRB = 0xff;                /* enable port b for output */

    for ( cnt=0xff; cnt > 0; cnt-- )
        PORTB = cnt;

    return 0;
}
