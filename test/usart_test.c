#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include "midi.h"

int main(void) {
	midi_init();

	sei();  // enable interrupts

	sysex_start(0xE);
		sysex_data(0xDE);
		sysex_data(0xAD);
		sysex_data(0xBE);
		sysex_data(0xEF);
	sysex_end();
	return 0;
}

void io_init() {
	DDRA = 0xFF;              // Data
	PORTA = 0x00;

	DDRB = 0xB9;              // spi_clk, spi_in, spi_out, NC, TX, RX, clock in, clock
	PORTB = 0x02;

	DDRC = 0xFF;              // Address
	PORTC = 0x00;

	DDRD = 0xFA;              // NC, NC, NC, NC, NC, CAN_INT, MIDI TX & RX
	PORTD = 0x00;

	DDRE = 0xFF;
	PORTE = 0x00;
}

