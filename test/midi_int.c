#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <stdio.h>
#include "midi.h"

#define false 0
#define true 1

#define MIDI_SYSEX_MAX_LEN 64
volatile uint8_t sysex_buf_len = 0;
volatile uint8_t sysex_buf[MIDI_SYSEX_MAX_LEN];

volatile uint8_t sysex_read = false;
volatile int16_t midisync_clocked = 0;

#define IN_MIDI_Q_SIZE 32
#define OUT_MIDI_Q_SIZE 32

volatile uint8_t in_midi_q[IN_MIDI_Q_SIZE];  // cyclic queue for midi msgs
volatile static uint8_t in_head_idx = 0;
volatile static uint8_t in_tail_idx = 0;
volatile uint8_t out_midi_q[OUT_MIDI_Q_SIZE];// cyclic queue for midi msgs
volatile static uint8_t out_head_idx = 0;
volatile static uint8_t out_tail_idx = 0;

#define MIDI_BAUDRATE 31250UL       // the MIDI spec baudrate

uint8_t EEMEM ee_midi_channel = 0;

void midi_handle_sysex(void);

// interrupt on receive char
ISR(SIG_USART0_RECV) {
	uint8_t c = UDR0;

	if( c == MD_SYSEX_START ) {
		sysex_read = true;
		sysex_buf_len = 0;
	} else if( c == MD_SYSEX_END ) {
		midi_handle_sysex();
		sysex_read = false;
	}

	if( sysex_read ) {
		sysex_buf[ sysex_buf_len++ ] = c;
	} else {
		in_midi_q[in_tail_idx++] = c;    // place at end of q
		in_tail_idx %= IN_MIDI_Q_SIZE;

		if( in_tail_idx == in_head_idx ) {
			// i.e. there are too many msgs in the q
			// drop the oldest msg?
			in_head_idx++;
			in_head_idx %= IN_MIDI_Q_SIZE;
		}
	}
}

ISR(SIG_USART0_DATA) {
	if( out_tail_idx != out_head_idx ) {
		UDR0 = out_midi_q[out_tail_idx++];
		out_tail_idx %= OUT_MIDI_Q_SIZE;

		// output buffer empty
		if( out_tail_idx == out_head_idx )
			UCSR0B &= ~_BV(UDRIE0);
	}
}

ISR(SIG_USART0_TRANS) {

}

void midi_init(void) {
	// read and write, interrupt on recv.
	UCSR0B |= (1<<RXEN0) | (1<<TXEN0)| (1<<RXCIE0) | (1<<TXCIE0);

	/* setup the MIDI UART */
	uint16_t ubbr = (F_CPU / (16 * MIDI_BAUDRATE)) - 1;	// formula in datasheet
	UBRR0L = (uint8_t)ubbr;               // set baudrate
	UBRR0H = (uint8_t)(ubbr>>8);
}

int midi_putchar(uint8_t c) {
	while( (out_head_idx+1)%OUT_MIDI_Q_SIZE == out_tail_idx );

	cli();
	{
		// Add it to the queue
		out_midi_q[out_head_idx++] = c;
		out_head_idx %= OUT_MIDI_Q_SIZE;

		UCSR0B |= _BV(UDRIE0);
	}
	sei();
	return 0;
}

int midi_getch(void) {     // checks if there is a character waiting!
	if( in_head_idx != in_tail_idx )
		return 1;
	return 0;
}

int midi_getchar(void) {
	char c;

	while( in_head_idx == in_tail_idx );

	cli();
	{
		c = in_midi_q[in_head_idx++];
		in_head_idx %= IN_MIDI_Q_SIZE;
	}
	sei();

	return c;
}

void midi_send(uint8_t status, uint8_t chan, uint8_t data, uint8_t data2) {
	midi_putchar( (status & 0xF0)|(chan & 0x0F) );
	midi_putchar( data & 0x7F );
	midi_putchar( data2 & 0x7F );
}

static uint8_t SYSEX_snd_checksum;
void sysex_start(uint8_t command) {
	// Reset checksum
	SYSEX_snd_checksum = 0;

	// Send SYSEX start, manufacturer ID, model number and command
	midi_putchar(MD_SYSEX_START);
	midi_putchar(MD_MF_ID_1);
	midi_putchar(MD_MF_ID_2);
	midi_putchar(MD_MF_ID_3);
	midi_putchar(MD_SYNTH_ID);
	midi_putchar(command);
}

void sysex_data(uint8_t data) {
	// split one byte of data in two
	uint8_t msb = data >> 7;
	uint8_t lsb = data & 0x7f;
	midi_putchar(msb);
	midi_putchar(lsb);

	SYSEX_snd_checksum = (SYSEX_snd_checksum + msb + lsb) & 0x7F;
}

void sysex_end(void) {
	// Send checksum
	midi_putchar(SYSEX_snd_checksum);

	// Send end of SYSEX
	midi_putchar(MD_SYSEX_END);
}

uint8_t calcCRC(void) {
	uint8_t i, crc = 0;
	for(i = 0; i < sysex_buf_len; ++i)
		crc = (crc + sysex_buf[i]) & 0x7F;
	return crc;
}

void midi_handle_sysex(void) {
	uint8_t crc = sysex_buf[ sysex_buf_len-1];
	sysex_buf_len--;

	// check crc
	if( crc != calcCRC() ) {
		sysex_buf_len = 0;
		return;
	}

	if( (sysex_buf_len < 5) ||
			sysex_buf[0] != MD_MF_ID_1 ||
			sysex_buf[1] != MD_MF_ID_2 ||
			sysex_buf[2] != MD_MF_ID_3 ||
			sysex_buf[3] != MD_SYNTH_ID ) {
		sysex_buf_len = 0;
		return;
	}

	//handle_sysex(sysex_buf+4, sysex_buf_len);
}

uint8_t midi_readchannel(void) {
	return eeprom_read_byte( &ee_midi_channel );
}

