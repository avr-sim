#ifndef MIDI_H
#define MIDI_H

#include <stdint.h>

// midi channel messages
#define MIDI_IGNORE 0x0  // ignore running status
#define MIDI_NOTE_ON 0x9
#define MIDI_NOTE_OFF 0x8
#define MIDI_PITCHBEND 0xE
#define MIDI_AFTERTOUCH 0xA
#define MIDI_CHANPRESSURE 0xD
#define MIDI_CONTROLLER 0xB
#define MIDI_PROGRAMCHANGE 0xC

#define MIDI_ALL_NOTES_OFF 123

// system messages
#define MIDI_START 0xFA
#define MIDI_CONTINUE 0xFB
#define MIDI_STOP 0xFC
#define MIDI_CLOCK 0xF8

// SYSEX ID
#define MD_SYSEX_START      0xF0
#define MD_SYSEX_END        0xF7

// MIDI manufacturer ID
#define MD_MF_ID_1          0x00
#define MD_MF_ID_2          0x20
#define MD_MF_ID_3          0x64

// model number
#define MD_SYNTH_ID          0x01

extern int midi_putchar(uint8_t c);
extern int midi_getch(void);
extern int midi_getchar(void);

extern void midi_init(void);
extern uint8_t midi_readchannel(void);
extern void midi_handle(void);

#define midi_note_off(chan, note, velocity) midi_send(MIDI_NOTE_OFF<<4, chan, note, velocity)
#define midi_note_on(chan, note, velocity) midi_send(MIDI_NOTE_ON<<4, chan, note, velocity)
extern void midi_send(uint8_t status, uint8_t chan, uint8_t data, uint8_t data2);

extern uint8_t midi_recv_cmd(void);

extern void midi_stop(void);
extern void midi_notesoff(void);

// Sysex
extern void sysex_start(uint8_t command);
extern void sysex_data(uint8_t data);
extern void sysex_end(void);
#endif

