#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include "midi.h"

volatile int16_t midisync_clocked = 0;

#define MIDI_Q_SIZE 32
volatile uint8_t midi_q[MIDI_Q_SIZE];      // cyclic queue for midi msgs
volatile static uint8_t head_idx = 0;
volatile static uint8_t tail_idx = 0;

#define MIDI_BAUDRATE 31250UL       // the MIDI spec baudrate

// interrupt on receive char
ISR(SIG_USART0_RECV) {
	char c = UDR0;

	midi_q[tail_idx++] = c;    // place at end of q
	tail_idx %= MIDI_Q_SIZE;

	if( tail_idx == head_idx ) {
		// i.e. there are too many msgs in the q
		// drop the oldest msg?
		head_idx++;
		head_idx %= MIDI_Q_SIZE;
	}
}

void midi_init(void) {
	/* setup the MIDI UART */
	uint16_t ubbr = (F_CPU / (16 * MIDI_BAUDRATE)) - 1;	// formula in datasheet
	UCSR0B |= (1<<RXEN0) | (1<<TXEN0)| (1<<RXCIE0);    // read and write, interrupt on recv.
	UBRR0L = (uint8_t)ubbr;               // set baudrate
	UBRR0H = (uint8_t)(ubbr>>8);
}

int midi_putchar(uint8_t c) {
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;
	return 0;
}

int midi_getch(void) {     // checks if there is a character waiting!
	if (head_idx != tail_idx)
		return 1;
	return 0;
}

int midi_getchar(void) {
	char c;

	while (head_idx == tail_idx);

	cli();
	c = midi_q[head_idx++];
	head_idx %= MIDI_Q_SIZE;
	sei();

//	midi_putchar(c);
	return c;
}

static uint8_t SYSEX_snd_checksum;
void sysex_start(uint8_t command) {
	// Reset checksum
	SYSEX_snd_checksum = 0;

	// Send SYSEX start, manufacturer ID, model number and command
	midi_putchar(MD_SYSEX_START);
	midi_putchar(MD_MF_ID_1);
	midi_putchar(MD_MF_ID_2);
	midi_putchar(MD_MF_ID_3);
	midi_putchar(MD_SYNTH_ID);
	midi_putchar(command);
}

void sysex_data(uint8_t data) {
	// split one byte of data in two
	uint8_t msb = data >> 7;
	uint8_t lsb = data & 0x7f;
	midi_putchar(msb);
	midi_putchar(lsb);

	SYSEX_snd_checksum = (SYSEX_snd_checksum + msb + lsb) % 128;
}

void sysex_end(void) {
	// Send checksum
	midi_putchar(SYSEX_snd_checksum);

	// Send end of SYSEX
	midi_putchar(MD_SYSEX_END);
}

