#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include "eeprom.h"

#define SPIEE_CS_PIN PA1
#define SPIEE_CS_PORT PORTA

#define NOP asm("nop")

#ifndef sbi
#define sbi(p,b) (p) |= (1<<(b))
#endif

#ifndef cbi
#define cbi(p,b) (p) &= ~(1<<(b))
#endif

#define SPI_EEPROM_READ 0x3
#define SPI_EEPROM_WRITE 0x2
#define SPI_EEPROM_WREN 0x6
#define SPI_EEPROM_RDSR 0x5
#define SPI_EEPROM_WRSR 0x1

//**************************************************
//         Internal EEPROM
//**************************************************
uint8_t internal_eeprom_read8(uint16_t addr) {
	loop_until_bit_is_clear(EECR, EEWE); // wait for last write to finish
	EEAR = addr;
	sbi(EECR, EERE);        // start EEPROM read
	return EEDR;            // takes only 1 cycle
}

void internal_eeprom_write8(uint8_t data, uint16_t addr) {
	loop_until_bit_is_clear(EECR, EEWE); // wait for last write to finish
	EEAR = addr;
	EEDR = data;
	cli();                // turn off interrupts
	sbi(EECR, EEMWE);     // these instructions must happen within 4 cycles
	sbi(EECR, EEWE);
	sei();                // turn on interrupts again
}

#ifdef HAVE_SPI
//**************************************************
//         Serial Programming Interface EEPROM
//**************************************************
void spieeprom_write(uint8_t data, uint16_t addr) {
  uint8_t status;

  cli();

  do {
    cbi(SPIEE_CS_PORT, SPIEE_CS_PIN); // pull CS low
    NOP; NOP; NOP; NOP;

    SPDR = SPI_EEPROM_RDSR;
    while (!(SPSR & (1<<SPIF)));
    NOP; NOP; NOP; NOP;
    SPDR = 0;
    while (!(SPSR & (1<<SPIF)));
    status = SPDR;
    sbi(SPIEE_CS_PORT, SPIEE_CS_PIN); // pull CS high
    NOP; NOP;NOP; NOP;
  } while ((status & 0x1) != 0);
  /* set the spi write enable latch */

  cbi(SPIEE_CS_PORT, SPIEE_CS_PIN); // pull CS low
  NOP; NOP;


  SPDR = SPI_EEPROM_WREN;           // send command
  while (!(SPSR & (1<<SPIF)));
  NOP; NOP;
  sbi(SPIEE_CS_PORT, SPIEE_CS_PIN); // pull CS low

  NOP; NOP; NOP; NOP;  // wait for write enable latch

  cbi(SPIEE_CS_PORT, SPIEE_CS_PIN); // pull CS low
  NOP; NOP;

  SPDR = SPI_EEPROM_WRITE;           // send command
  while (!(SPSR & (1<<SPIF)));

  SPDR = addr >> 8;                 // send high addr 
  while (!(SPSR & (1<<SPIF)));

  SPDR = addr & 0xFF;               // send low addr
  while (!(SPSR & (1<<SPIF)));

  SPDR = data;               // send data
  while (!(SPSR & (1<<SPIF)));

  NOP;
  NOP;

  sbi(SPIEE_CS_PORT, SPIEE_CS_PIN); // pull CS low  
  sei();
}

uint8_t spieeprom_read(uint16_t addr) {
  uint8_t data;

  cli();

  cbi(SPIEE_CS_PORT, SPIEE_CS_PIN); // pull CS low
  NOP; NOP;

  SPDR = SPI_EEPROM_READ;           // send command
  while (!(SPSR & (1<<SPIF)));

  SPDR = addr >> 8;                 // send high addr 
  while (!(SPSR & (1<<SPIF)));

  SPDR = addr & 0xFF;               // send low addr
  while (!(SPSR & (1<<SPIF)));
  NOP;
  NOP;

  SPDR = 0;
  while (!(SPSR & (1<<SPIF)));
  data = SPDR;

  sbi(SPIEE_CS_PORT, SPIEE_CS_PIN); // pull CS high
  sei();
  return data;
}
#endif

