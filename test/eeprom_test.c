#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include "eeprom.h"
#ifdef HAVE_SPI
#include "spi.h"
#endif

int main(void) {
#ifdef HAVE_SPI
	spi_init();
#endif

	sei();  // enable interrupts

	internal_eeprom_write8(0xDE, 0);
	internal_eeprom_write8(0xAD, 1);

	uint8_t de = internal_eeprom_read8(0);
	uint8_t ad = internal_eeprom_read8(1);

	if( de < ad )
		PORTB |= PB1;

	spieeprom_write(0xBE, 0);
	spieeprom_write(0xEF, 1);
	return 0;
}

void io_init() {
	DDRA = 0xFF;              // Data
	PORTA = 0x00;

	DDRB = 0xB9;              // spi_clk, spi_in, spi_out, NC, TX, RX, clock in, clock
	PORTB = 0x02;

	DDRC = 0xFF;              // Address
	PORTC = 0x00;

	DDRD = 0xFA;              // NC, NC, NC, NC, NC, CAN_INT, MIDI TX & RX
	PORTD = 0x00;

	DDRE = 0xFF;
	PORTE = 0x00;
}

