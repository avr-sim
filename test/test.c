int f(int a, int b) {
	return a + b;
}

int main() {
	int a = 5;
	int b = 6;
	int c = f(a,b);

	if( a > c )
		a++;

	int d = (a < b) ? 0 : 1;
	d++;
	return 0;
}
