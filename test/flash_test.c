#include <avr/io.h>
#include <avr/pgmspace.h>

int8_t blaai[10] PROGMEM = {
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9
};

int main(void) {
	int16_t sum = 0;
	int8_t i;
	for(i = 0; i < 10; ++i) {
		register int8_t tmp asm("r23") =
			pgm_read_byte( &blaai[i] );
		sum += tmp;
	}

	DDRB = sum & 0xff;
	return 0;
}

