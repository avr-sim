<?xml version="1.0"?>
<!DOCTYPE device SYSTEM "device.dtd">
<device>
  <memory>
    <flash size="8192"/>
    <iospace start="$20" stop="$5F"/>
    <sram size="512"/>
    <eram size="65536"/>
  </memory>
  <ioregisters>
    <ioreg name="ACSR" address="$08"/>
    <ioreg name="UBRR" address="$09"/>
    <ioreg name="UCR" address="$0A"/>
    <ioreg name="USR" address="$0B"/>
    <ioreg name="UDR" address="$0C"/>
    <ioreg name="SPCR" address="$0D"/>
    <ioreg name="SPSR" address="$0E"/>
    <ioreg name="SPDR" address="$0F"/>
    <ioreg name="PIND" address="$10"/>
    <ioreg name="DDRD" address="$11"/>
    <ioreg name="PORTD" address="$12"/>
    <ioreg name="PINC" address="$13"/>
    <ioreg name="DDRC" address="$14"/>
    <ioreg name="PORTC" address="$15"/>
    <ioreg name="PINB" address="$16"/>
    <ioreg name="DDRB" address="$17"/>
    <ioreg name="PORTB" address="$18"/>
    <ioreg name="PINA" address="$19"/>
    <ioreg name="DDRA" address="$1A"/>
    <ioreg name="PORTA" address="$1B"/>
    <ioreg name="EECR" address="$1C"/>
    <ioreg name="EEDR" address="$1D"/>
    <ioreg name="EEARL" address="$1E"/>
    <ioreg name="EEARH" address="$1F"/>
    <ioreg name="WDTCR" address="$21"/>
    <ioreg name="ICR1L" address="$24"/>
    <ioreg name="ICR1H" address="$25"/>
    <ioreg name="OCR1BL" address="$28"/>
    <ioreg name="OCR1BH" address="$29"/>
    <ioreg name="OCR1AL" address="$2A"/>
    <ioreg name="OCR1AH" address="$2B"/>
    <ioreg name="TCNT1L" address="$2C"/>
    <ioreg name="TCNT1H" address="$2D"/>
    <ioreg name="TCCR1B" address="$2E"/>
    <ioreg name="TCCR1A" address="$2F"/>
    <ioreg name="TCNT0" address="$32"/>
    <ioreg name="TCCR0" address="$33"/>
    <ioreg name="MCUCR" address="$35"/>
    <ioreg name="TIFR" address="$38"/>
    <ioreg name="TIMSK" address="$39"/>
    <ioreg name="GIFR" address="$3A"/>
    <ioreg name="GIMSK" address="$3B"/>
    <ioreg name="SPL" address="$3D"/>
    <ioreg name="SPH" address="$3E"/>
    <ioreg name="SREG" address="$3F"/>
  </ioregisters>
  <packages>
    <package name="TQFP" pins="44">
      <pin id="1" name="[PB5:MOSI]">MOSI: SPI Master data output, slave data input for SPI channel. When the SPI is enabled as a slave, this pin is configured as an input regardless of the setting of DDB5. When the SPI is enabled as a master, the data direction of this pin is con-trolled by DDB5. When the pin is forced to be an input, the pull-up can still be controlled by the PORTB5 bit. See the description of the SPI port for further details.</pin>
      <pin id="2" name="[PB6:MISO]">MISO: Master data input, slave data output pin for SPI channel. When the SPI is enabled as a master, this pin is configured as an input regardless of the setting of DDB6. When the SPI is enabled as a slave, the data direction of this pin is controlled by DDB6. When the pin is forced to be an input, the pull-up can still be controlled by the PORTB6 bit. See the description of the SPI port for further details.</pin>
      <pin id="3" name="[PB7:SCK]">SCK: Master clock output, slave clock input pin for SPI channel. When the SPI is enabled as a slave, this pin is configured as an input regardless of the setting of DDB7. When the SPI is enabled as a master, the data direction of this pin is con-trolled by DDB7. When the pin is forced to be an input, the pull-up can still be controlled by the PORTB7 bit. See the description of the SPI port for further details.</pin>
      <pin id="4" name="['RESET]"/>
      <pin id="5" name="[PD0:RXD]">Receive Data (data input pin for the UART). When the UART receiver is enabled, this pin is configured as an input, regardless of the value of DDRD0. When the UART forces this pin to be an input, a logical “1” in PORTD0 will turn on the internal pull-up.</pin>
      <pin id="6" name="[NC]"/>
      <pin id="7" name="[PD1:TXD]">Transmit Data (data output pin for the UART). When the UART transmitter is enabled, this pin is configured as an output, regardless of the value of DDRD1.</pin>
      <pin id="8" name="[PD2:INT0]">INT0: External Interrupt source 0. The PD2 pin can serve as an external interrupt source to the MCU. See the interrupt description for further details and how to enable the source.</pin>
      <pin id="9" name="[PD3:INT1]">INT1: External Interrupt source 1. The PD3 pin can serve as an external interrupt source to the MCU. See the interrupt description for further details and how to enable the source.</pin>
      <pin id="10" name="[PD4]"/>
      <pin id="11" name="[PD5:OC1A]">OC1A: Output compare match output. The PD5 pin can serve as an external output when the Timer/Counter1 compare matches. The PD5 pin has to be configured as an output (DDD5 set [one]) to serve this function. See the Timer/Counter1 description for further details and how to enable the output. The OC1A pin is also the output pin for the PWM mode timer function.</pin>
      <pin id="12" name="[PD6:'WR]">WR is the external data memory write control strobe. See “Interface to External SRAM” on page 52 for detailed information.</pin>
      <pin id="13" name="[PD7:'RD]">RD is the external data memory read control strobe. See “Interface to External SRAM” on page 52 for detailed information.</pin>
      <pin id="14" name="[XTAL2]"/>
      <pin id="15" name="[XTAL1]"/>
      <pin id="16" name="[GND]"/>
      <pin id="17" name="[NC]"/>
      <pin id="18" name="[PC0:A8]"/>
      <pin id="19" name="[PC1:A9]"/>
      <pin id="20" name="[PC2:A10]"/>
      <pin id="21" name="[PC3:A11]"/>
      <pin id="22" name="[PC4:A12]"/>
      <pin id="23" name="[PC5:A13]"/>
      <pin id="24" name="[PC6:A14]"/>
      <pin id="25" name="[PC7:A15]"/>
      <pin id="26" name="[OC1B]"/>
      <pin id="27" name="[ALE]"/>
      <pin id="28" name="[NC]"/>
      <pin id="29" name="[ICP]"/>
      <pin id="30" name="[PA7:AD7]"/>
      <pin id="31" name="[PA6:AD6]"/>
      <pin id="32" name="[PA5:AD5]"/>
      <pin id="33" name="[PA4:AD4]"/>
      <pin id="34" name="[PA3:AD3]"/>
      <pin id="35" name="[PA2:AD2]"/>
      <pin id="36" name="[PA1:AD1]"/>
      <pin id="37" name="[PA0:AD0]"/>
      <pin id="38" name="[VCC]"/>
      <pin id="39" name="[NC]"/>
      <pin id="40" name="[PB0:T0]">T0: Timer/Counter0 counter source. See the timer description for further details.</pin>
      <pin id="41" name="[PB1:T1]">T1: Timer/Counter1 counter source. See the timer description for further details</pin>
      <pin id="42" name="[PB2:AIN0]">AIN0: Analog Comparator Positive Input. When configured as an input (DDB2 is cleared [zero]) and with the internal MOS pull-up resistor switched off (PB2 is cleared [zero]), this pin also serves as the positive input of the on-chip Analog Comparator.</pin>
      <pin id="43" name="[PB3:AIN1]">AIN1: Analog Comparator Negative Input. When configured as an input (DDB3 is cleared [zero]) and with the internal MOS pull-up resistor switched off (PB3 is cleared [zero]), this pin also serves as the negative input of the on-chip Analog Comparator.</pin>
      <pin id="44" name="[PB4:'SS]">SS: Slave port select input. When the SPI is enabled as a slave, this pin is configured as an input regardless of the setting of DDB4. As a slave, the SPI is activated when this pin is driven low. When the SPI is enabled as a master, the data direc-tion of this pin is controlled by DDB4. When the pin is forced to be an input, the pull-up can still be controlled by the PORTB4 bit. See the description of the SPI port for further details.</pin>
    </package>
  </packages>
  <interrupts num="13">
    <interrupt vector="1" address="$000" name="RESET">External Reset, Power-on Reset and Watchdog Reset</interrupt>
    <interrupt vector="2" address="$001" name="INT0">External Interrupt Request 0</interrupt>
    <interrupt vector="3" address="$002" name="INT1">External Interrupt Request 1</interrupt>
    <interrupt vector="4" address="$003" name="TIMER1 CAPT">Timer/Counter Capture Event</interrupt>
    <interrupt vector="5" address="$004" name="TIMER1 COMPA">Timer/Counter1 Compare Match A</interrupt>
    <interrupt vector="6" address="$005" name="TIMER1 COMPB">Timer/Counter1 Compare MatchB</interrupt>
    <interrupt vector="7" address="$006" name="TIMER1 OVF">Timer/Counter1 Overflow</interrupt>
    <interrupt vector="8" address="$007" name="TIMER0 OVF">Timer/Counter0 Overflow</interrupt>
    <interrupt vector="9" address="$008" name="SPI,STC">Serial Transfer Complete</interrupt>
    <interrupt vector="10" address="$009" name="UART,RX">UART, Rx Complete</interrupt>
    <interrupt vector="11" address="$00A" name="UART,UDRE">UART Data Register Empty</interrupt>
    <interrupt vector="12" address="$00B" name="UART, TX">UART, Tx Complete</interrupt>
    <interrupt vector="13" address="$00C" name="ANA_COMP">Analog Comparator</interrupt>
  </interrupts>
  <hardware>
<!--Everything after this needs editing!!!-->
    <module class="FUSE">
      <registers name="FUSE" memspace="FUSE">
          <reg size="1" name="LOW" offset="0x00">
            <bitfield name="SPIEN" mask="0x20" text="Serial program downloading (SPI) enabled" icon=""/>
            <bitfield name="FSTRT" mask="0x01" text="Short start-up time selected" icon=""/>
          </reg>
        </registers>
    </module>
    <module class="LOCKBIT">
      <registers name="LOCKBIT" memspace="LOCKBIT">
          <reg size="1" name="LOCKBIT" offset="0x00">
            <bitfield name="LB" mask="0x06" text="Memory Lock" icon="" enum="ENUM_LB"/>
          </reg>
        </registers>
    </module>
    <module class="ANALOG_COMPARATOR">
      <registers name="ANALOG_COMPARATOR" memspace="DATAMEM" text="" icon="io_analo.bmp">
          <reg size="1" name="ACSR" offset="0x28" text="Analog Comparator Control And Status Register" icon="io_analo.bmp">
            <bitfield name="ACD" mask="0x80" text="Analog Comparator Disable" icon=""/>
            <bitfield name="ACO" mask="0x20" text="Analog Comparator Output" icon=""/>
            <bitfield name="ACI" mask="0x10" text="Analog Comparator Interrupt Flag" icon=""/>
            <bitfield name="ACIE" mask="0x08" text="Analog Comparator Interrupt Enable" icon=""/>
            <bitfield name="ACIC" mask="0x04" text="Analog Comparator Input Capture Enable" icon=""/>
            <bitfield name="ACIS" mask="0x03" text="Analog Comparator Interrupt Mode Select bits" icon="" enum="ANALOG_COMP_INTERRUPT"/>
          </reg>
        </registers>
    </module>
    <module class="SPI">
      <registers name="SPI" memspace="DATAMEM" text="" icon="io_com.bmp">
          <reg size="1" name="SPCR" offset="0x2D" text="SPI Control Register" icon="io_flag.bmp">
            <bitfield name="SPIE" mask="0x80" text="SPI Interrupt Enable" icon=""/>
            <bitfield name="SPE" mask="0x40" text="SPI Enable" icon=""/>
            <bitfield name="DORD" mask="0x20" text="Data Order" icon=""/>
            <bitfield name="MSTR" mask="0x10" text="Master/Slave Select" icon=""/>
            <bitfield name="CPOL" mask="0x08" text="Clock polarity" icon=""/>
            <bitfield name="CPHA" mask="0x04" text="Clock Phase" icon=""/>
            <bitfield name="SPR" mask="0x03" text="SPI Clock Rate Selects" icon="" enum="COMM_SCK_RATE"/>
          </reg>
          <reg size="1" name="SPSR" offset="0x2E" text="SPI Status Register" icon="io_flag.bmp">
            <bitfield name="SPIF" mask="0x80" text="SPI Interrupt Flag" icon=""/>
            <bitfield name="WCOL" mask="0x40" text="Write Collision Flag" icon=""/>
          </reg>
          <reg size="1" name="SPDR" offset="0x2F" text="SPI Data Register" icon="io_com.bmp" mask="0xFF"/>
        </registers>
    </module>
    <module class="UART">
      <registers name="UART" memspace="DATAMEM" text="" icon="io_com.bmp">
          <reg size="1" name="UDR" offset="0x2C" text="UART I/O Data Register" icon="io_com.bmp" mask="0xFF"/>
          <reg size="1" name="USR" offset="0x2B" text="UART Status Register" icon="io_flag.bmp">
            <bitfield name="RXC" mask="0x80" text="UART Receive Complete" icon=""/>
            <bitfield name="TXC" mask="0x40" text="UART Transmit Complete" icon=""/>
            <bitfield name="UDRE" mask="0x20" text="UART Data Register Empty" icon=""/>
            <bitfield name="FE" mask="0x10" text="Framing Error" icon=""/>
            <bitfield name="OR" mask="0x08" text="Overrun" icon=""/>
          </reg>
          <reg size="1" name="UCR" offset="0x2A" text="UART Control Register" icon="io_flag.bmp">
            <bitfield name="RXCIE" mask="0x80" text="RX Complete Interrupt Enable" icon=""/>
            <bitfield name="TXCIE" mask="0x40" text="TX Complete Interrupt Enable" icon=""/>
            <bitfield name="UDRIE" mask="0x20" text="UART Data Register Empty Interrupt Enable" icon=""/>
            <bitfield name="RXEN" mask="0x10" text="Receiver Enable" icon=""/>
            <bitfield name="TXEN" mask="0x08" text="Transmitter Enable" icon=""/>
            <bitfield name="CHR9" mask="0x04" text="9-bit Characters" icon=""/>
            <bitfield name="RXB8" mask="0x02" text="Receive Data Bit 8" icon=""/>
            <bitfield name="TXB8" mask="0x01" text="Transmit Data Bit 8" icon=""/>
          </reg>
          <reg size="1" name="UBRR" offset="0x29" text="UART BAUD Rate Register" icon="io_com.bmp" mask="0xFF"/>
        </registers>
    </module>
    <module class="EXTERNAL_INTERRUPT">
      <registers name="EXTERNAL_INTERRUPT" memspace="DATAMEM" text="" icon="io_ext.bmp">
          <reg size="1" name="GIMSK" offset="0x5B" text="General Interrupt Mask Register" icon="io_flag.bmp">
            <bitfield name="INT" mask="0xC0" text="External Interrupt Request 1 Enable" icon=""/>
          </reg>
          <reg size="1" name="GIFR" offset="0x5A" text="General Interrupt Flag register" icon="io_flag.bmp">
            <bitfield name="INTF" mask="0xC0" text="External Interrupt Flags" icon=""/>
          </reg>
        </registers>
    </module>
    <module class="PORTA">
      <registers name="PORTA" memspace="DATAMEM" text="" icon="io_port.bmp">
          <reg size="1" name="PORTA" offset="0x3B" text="Port A Data Register" icon="io_port.bmp" mask="0xFF"/>
          <reg size="1" name="DDRA" offset="0x3A" text="Port A Data Direction Register" icon="io_flag.bmp" mask="0xFF"/>
          <reg size="1" name="PINA" offset="0x39" text="Port A Input Pins" icon="io_port.bmp" mask="0xFF"/>
        </registers>
    </module>
    <module class="PORTB">
      <registers name="PORTB" memspace="DATAMEM" text="" icon="io_port.bmp">
          <reg size="1" name="PORTB" offset="0x38" text="Port B Data Register" icon="io_port.bmp" mask="0xFF"/>
          <reg size="1" name="DDRB" offset="0x37" text="Port B Data Direction Register" icon="io_flag.bmp" mask="0xFF"/>
          <reg size="1" name="PINB" offset="0x36" text="Port B Input Pins" icon="io_port.bmp" mask="0xFF"/>
        </registers>
    </module>
    <module class="PORTC">
      <registers name="PORTC" memspace="DATAMEM" text="" icon="io_port.bmp">
          <reg size="1" name="PORTC" offset="0x35" text="Port C Data Register" icon="io_port.bmp" mask="0xFF"/>
          <reg size="1" name="DDRC" offset="0x34" text="Port C Data Direction Register" icon="io_flag.bmp" mask="0xFF"/>
          <reg size="1" name="PINC" offset="0x33" text="Port C Input Pins" icon="io_port.bmp" mask="0xFF"/>
        </registers>
    </module>
    <module class="PORTD">
      <registers name="PORTD" memspace="DATAMEM" text="" icon="io_port.bmp">
          <reg size="1" name="PORTD" offset="0x32" text="Port D Data Register" icon="io_port.bmp" mask="0xFF"/>
          <reg size="1" name="DDRD" offset="0x31" text="Port D Data Direction Register" icon="io_flag.bmp" mask="0xFF"/>
          <reg size="1" name="PIND" offset="0x30" text="Port D Input Pins" icon="io_port.bmp" mask="0xFF"/>
        </registers>
    </module>
    <module class="TIMER_COUNTER_1">
      <registers name="TIMER_COUNTER_1" memspace="DATAMEM" text="" icon="io_timer.bmp">
          <reg size="1" name="TIMSK" offset="0x59" text="Timer/Counter Interrupt Mask Register" icon="io_flag.bmp">
            <bitfield name="TOIE1" mask="0x80" text="Timer/Counter1 Overflow Interrupt Enable" icon=""/>
            <bitfield name="OCIE1A" mask="0x40" text="Timer/Counter1 Output CompareA Match Interrupt Enable" icon=""/>
            <bitfield name="OCIE1B" mask="0x20" text="Timer/Counter1 Output CompareB Match Interrupt Enable" icon=""/>
            <bitfield name="TICIE1" mask="0x08" text="Timer/Counter1 Input Capture Interrupt Enable" icon=""/>
          </reg>
          <reg size="1" name="TIFR" offset="0x58" text="Timer/Counter Interrupt Flag register" icon="io_flag.bmp">
            <bitfield name="TOV1" mask="0x80" text="Timer/Counter1 Overflow Flag" icon=""/>
            <bitfield name="OCF1A" mask="0x40" text="Output Compare Flag 1A" icon=""/>
            <bitfield name="OCF1B" mask="0x20" text="Output Compare Flag 1B" icon=""/>
            <bitfield name="ICF1" mask="0x08" text="Input Capture Flag 1" icon=""/>
          </reg>
          <reg size="1" name="TCCR1A" offset="0x4F" text="Timer/Counter1 Control Register A" icon="io_flag.bmp">
            <bitfield name="COM1A" mask="0xC0" text="Compare Output Mode 1A, bits" icon=""/>
            <bitfield name="COM1B" mask="0x30" text="Compare Output Mode 1B, bits" icon=""/>
            <bitfield name="PWM1" mask="0x03" text="Pulse Width Modulator Select Bits" icon=""/>
          </reg>
          <reg size="1" name="TCCR1B" offset="0x4E" text="Timer/Counter1 Control Register B" icon="io_flag.bmp">
            <bitfield name="ICNC1" mask="0x80" text="Input Capture 1 Noise Canceler" icon=""/>
            <bitfield name="ICES1" mask="0x40" text="Input Capture 1 Edge Select" icon=""/>
            <bitfield name="CTC1" mask="0x08" text="Clear Timer/Counter1 on Compare Match" icon=""/>
            <bitfield name="CS1" mask="0x07" text="Clock Select1 bits" icon=""/>
          </reg>
          <reg size="2" name="TCNT1" offset="0x4C" text="Timer/Counter1  Bytes" icon="io_timer.bmp" mask="0xFFFF"/>
          <reg size="2" name="OCR1A" offset="0x4A" text="Timer/Counter1 Outbut Compare Register  Bytes" icon="io_timer.bmp" mask="0xFFFF"/>
          <reg size="2" name="OCR1B" offset="0x48" text="Timer/Counter1 Output Compare Register  Bytes" icon="io_timer.bmp" mask="0xFFFF"/>
          <reg size="2" name="ICR1" offset="0x44" text="Timer/Counter1 Input Capture Register  Bytes" icon="io_timer.bmp" mask="0xFFFF"/>
        </registers>
    </module>
    <module class="TIMER_COUNTER_0">
      <registers name="TIMER_COUNTER_0" memspace="DATAMEM" text="" icon="io_timer.bmp">
          <reg size="1" name="TIMSK" offset="0x59" text="Timer/Counter Interrupt Mask Register" icon="io_flag.bmp">
            <bitfield name="TOIE0" mask="0x02" text="Timer/Counter0 Overflow Interrupt Enable" icon=""/>
          </reg>
          <reg size="1" name="TIFR" offset="0x58" text="Timer/Counter Interrupt Flag register" icon="io_flag.bmp">
            <bitfield name="TOV0" mask="0x02" text="Timer/Counter0 Overflow Flag" icon=""/>
          </reg>
          <reg size="1" name="TCCR0" offset="0x53" text="Timer/Counter0 Control Register" icon="io_flag.bmp">
            <bitfield name="CS02" mask="0x04" text="Clock Select0 bit 2" icon="" enum="CLK_SEL_3BIT_EXT"/>
            <bitfield name="CS01" mask="0x02" text="Clock Select0 bit 1" icon=""/>
            <bitfield name="CS00" mask="0x01" text="Clock Select0 bit 0" icon=""/>
          </reg>
          <reg size="1" name="TCNT0" offset="0x52" text="Timer Counter 0" icon="io_timer.bmp" mask="0xFF"/>
        </registers>
    </module>
    <module class="WATCHDOG">
      <registers name="WATCHDOG" memspace="DATAMEM" text="" icon="io_watch.bmp">
          <reg size="1" name="WDTCR" offset="0x41" text="Watchdog Timer Control Register" icon="io_flag.bmp">
            <bitfield name="WDTOE" mask="0x10" text="RW" icon=""/>
            <bitfield name="WDE" mask="0x08" text="Watch Dog Enable" icon=""/>
            <bitfield name="WDP" mask="0x07" text="Watch Dog Timer Prescaler bits" icon="" enum="WDOG_TIMER_PRESCALE_3BITS"/>
          </reg>
        </registers>
    </module>
    <module class="CPU">
      <registers name="CPU" memspace="DATAMEM" text="" icon="io_cpu.com">
          <reg size="1" name="SREG" offset="0x5F" text="Status Register" icon="io_sreg.bmp">
            <bitfield name="I" mask="0x80" text="Global Interrupt Enable" icon=""/>
            <bitfield name="T" mask="0x40" text="Bit Copy Storage" icon=""/>
            <bitfield name="H" mask="0x20" text="Half Carry Flag" icon=""/>
            <bitfield name="S" mask="0x10" text="Sign Bit" icon=""/>
            <bitfield name="V" mask="0x08" text="Two's Complement Overflow Flag" icon=""/>
            <bitfield name="N" mask="0x04" text="Negative Flag" icon=""/>
            <bitfield name="Z" mask="0x02" text="Zero Flag" icon=""/>
            <bitfield name="C" mask="0x01" text="Carry Flag" icon=""/>
          </reg>
          <reg size="2" name="SP" offset="0x5D" text="Stack Pointer " icon="io_sph.bmp" mask="0xFFFF"/>
          <reg size="1" name="MCUCR" offset="0x55" text="MCU Control Register" icon="io_cpu.bmp">
            <bitfield name="SRE" mask="0x80" text="External SRAM Enable" icon=""/>
            <bitfield name="SRW" mask="0x40" text="External SRAM Wait State" icon=""/>
            <bitfield name="SE" mask="0x20" text="Sleep Enable" icon=""/>
            <bitfield name="SM" mask="0x10" text="Sleep Mode" icon=""/>
            <bitfield name="ISC1" mask="0x0C" text="Interrupt Sense Control 1 bits" icon="" enum="INTERRUPT_SENSE_CONTROL"/>
            <bitfield name="ISC0" mask="0x03" text="Interrupt Sense Control 0 bits" icon="" enum="INTERRUPT_SENSE_CONTROL"/>
          </reg>
        </registers>
    </module>
    <module class="EEPROM">
      <registers name="EEPROM" memspace="DATAMEM" text="" icon="io_cpu.bmp">
          <reg size="2" name="EEAR" offset="0x3E" text="EEPROM Read/Write Access  Bytes" icon="io_cpu.bmp" mask="0x01FF"/>
          <reg size="1" name="EEDR" offset="0x3D" text="EEPROM Data Register" icon="io_cpu.bmp" mask="0xFF"/>
          <reg size="1" name="EECR" offset="0x3C" text="EEPROM Control Register" icon="io_flag.bmp">
            <bitfield name="EEMWE" mask="0x04" text="EEPROM Master Write Enable" icon=""/>
            <bitfield name="EEWE" mask="0x02" text="EEPROM Write Enable" icon=""/>
            <bitfield name="EERE" mask="0x01" text="EEPROM Read Enable" icon=""/>
          </reg>
        </registers>
    </module>
  </hardware>
</device>
