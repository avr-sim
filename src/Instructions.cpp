/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Instructions.h"
#include "Core.h"
#include "DecoderHelp.h"
#include "ImplementationException.h"

#define Xl 26
#define Xh 27
#define Yl 28
#define Yh 29
#define Zl 30
#define Zh 31
#define R0 0
#define R1 1
#define EIND 0x10 //TODO

namespace avr {

	namespace op {

		class SReg {
			public:
				SReg(Core *c) : c(c) {
					reg = c->readStatus();
				}
				~SReg() {
					c->writeStatus(reg);
				}

			public:
				union {
				    byte reg;
				    struct {
				        byte C:1;
				        byte Z:1;
				        byte N:1;
				        byte V:1;
				        byte S:1;
				        byte H:1;
				        byte T:1;
				        byte I:1;
				    };
				};

			private:
				Core *c;
		};

		static int get_add_carry( byte res, byte rd, byte rr, int b ) {
		    byte resb = res >> b & 0x1;
		    byte rdb  = rd  >> b & 0x1;
		    byte rrb  = rr  >> b & 0x1;
		    return (rdb & rrb) | (rrb & ~resb) | (~resb & rdb);
		}

		static int get_add_overflow( byte res, byte rd, byte rr ) {
		    byte res7 = res >> 7 & 0x1;
		    byte rd7  = rd  >> 7 & 0x1;
		    byte rr7  = rr  >> 7 & 0x1;
		    return (rd7 & rr7 & ~res7) | (~rd7 & ~rr7 & res7);
		}

		static int get_sub_carry( byte res, byte rd, byte rr, int b ) {
		    byte resb = res >> b & 0x1;
		    byte rdb  = rd  >> b & 0x1;
		    byte rrb  = rr  >> b & 0x1;
		    return (~rdb & rrb) | (rrb & resb) | (resb & ~rdb);
		}

		static int get_sub_overflow( byte res, byte rd, byte rr ) {
		    byte res7 = res >> 7 & 0x1;
		    byte rd7  = rd  >> 7 & 0x1;
		    byte rr7  = rr  >> 7 & 0x1;
		    return (rd7 & ~rr7 & ~res7) | (~rd7 & rr7 & res7);
		}

		static int get_compare_carry( byte res, byte rd, byte rr, int b ) {
		    byte resb = res >> b & 0x1;
		    byte rdb  = rd  >> b & 0x1;
		    byte rrb  = rr  >> b & 0x1;
		    return (~rdb & rrb) | (rrb & resb) | (resb & ~rdb);
		}

		static int get_compare_overflow( byte res, byte rd, byte rr ) {
		    byte res7 = res >> 7 & 0x1;
		    byte rd7  = rd  >> 7 & 0x1;
		    byte rr7  = rr  >> 7 & 0x1;
		    /* The atmel data sheet says the second term is ~rd7 for CP
		     * but that doesn't make any sense. You be the judge. */
		    return (rd7 & ~rr7 & ~res7) | (~rd7 & rr7 & res7);
		}

		static int n_bit_unsigned_to_signed( unsigned int val, int n ) {
		    /* Convert n-bit unsigned value to a signed value. */
		    unsigned int mask;

		    if ( (val & (1 << (n-1))) == 0)
		        return (int)val;

		    /* manually calculate two's complement */
		    mask = (1 << n) - 1;
		    return -1 * ((~val & mask) + 1);
		}

		ADC::ADC(word opcode) :
			Rd( get_rd_5(opcode) ),
			Rr( get_rr_5(opcode) ) {
		}

		int ADC::operator ()(Core *core) const {
			SReg status( core );
		    unsigned char rd = core->readRegister(Rd);
		    unsigned char rr = core->readRegister(Rr);
		    unsigned char res = rd + rr + status.C;

		    status.H = get_add_carry( res, rd, rr, 3 );
		    status.V = get_add_overflow( res, rd, rr );
		    status.N = ((res >> 7) & 0x1);
		    status.S = (status.N ^ status.V);
		    status.Z = ((res & 0xff) == 0);
		    status.C = get_add_carry( res, rd, rr, 7 );

		    core->writeRegister(Rd, res);
		    return 1;   //used clocks
		}

		ADD::ADD(word opcode) :
			Rd( get_rd_5(opcode) ),
			Rr( get_rr_5(opcode) ) {
		}

		int ADD::operator ()(Core *core) const {
			SReg status( core );
		    unsigned char rd = core->readRegister(Rd);
		    unsigned char rr = core->readRegister(Rr);
		    unsigned char res = rd + rr ;

		    status.H = get_add_carry( res, rd, rr, 3 ) ;
		    status.V = get_add_overflow( res, rd, rr ) ;
		    status.N = ((res >> 7) & 0x1)              ;
		    status.S = (status.N ^ status.V)   ;
		    status.Z = ((res & 0xff) == 0)             ;
		    status.C = get_add_carry( res, rd, rr, 7 ) ;

		    core->writeRegister(Rd, res);
		    return 1;   //used clocks
		}

		ADIW::ADIW(word opcode) :
		    Rl( get_rd_2(opcode) ),
		    Rh( get_rd_2(opcode) + 1 ),
		    K( get_K_6(opcode) ) {
		}

		int ADIW::operator()(Core *core) const {
			SReg status( core );
		    word rd = (core->readRegister(Rh) << 8) | core->readRegister(Rl);
		    word res = rd + K;

		    status.V = (((~rd & res) >> 15) & 0x1);
		    status.N = ((res >> 15) & 0x1);
		    status.S = (status.N ^ status.V);
		    status.Z = ((res & 0xffff) == 0);
		    status.C = (((~res & rd) >> 15) & 0x1);

		    core->writeRegister(Rl, res & 0xff);
		    core->writeRegister(Rh, (res >> 8) & 0xff);
		    return 2;
		}

		AND::AND(word opcode) :
			Rd( get_rd_5(opcode) ),
			Rr( get_rr_5(opcode) ) {
		}

	    int AND::operator()(Core *core) const {
	    	SReg status( core );
			byte res = core->readRegister(Rd) & core->readRegister(Rr);

	        status.V = 0;
	        status.N = ((res >> 7) & 0x1)  ;
	        status.S = (status.N ^ status.V);
	        status.Z = ((res & 0xff) == 0);

	        core->writeRegister(Rd, res);
	        return 1;
	    }

		ANDI::ANDI(word opcode) :
		    Rd( get_rd_4(opcode) ),
		    K( get_K_8(opcode) ) {
	    }

		int ANDI::operator()(Core *core) const {
			SReg status( core );
		    byte rd = core->readRegister(Rd);
		    byte res = rd & K;

		    status.V = 0;
		    status.N = ((res >> 7) & 0x1);
		    status.S = (status.N ^ status.V);
		    status.Z = ((res & 0xff) == 0);

		    core->writeRegister(Rd, res);
		    return 1;
		}

		ASR::ASR(word opcode) :
		    Rd( get_rd_5(opcode) ) {
		}

		int ASR::operator()(Core *core) const {
			SReg status( core );
		    byte rd = core->readRegister(Rd);
		    byte res = (rd >> 1) + (rd & 0x80);

		    status.N = ((res >> 7) & 0x1);
		    status.C = (rd & 0x1);
		    status.V = (status.N ^ status.C);
		    status.S = (status.N ^ status.V);
		    status.Z = ((res & 0xff) == 0);

		    core->writeRegister(Rd, res);
		    return 1;
		}

		BCLR::BCLR(word opcode) :
		    K( ~(1<<get_sreg_bit(opcode)) ) {
		}

		int BCLR::operator()(Core *core) const {
			SReg status( core );
		    status.reg = status.reg & K;
		    return 1;
		}

		BLD::BLD(word opcode) :
			Rd( get_rd_5(opcode) ),
		    Kadd(1<<get_reg_bit(opcode)),
		    Kremove(~(1<<get_reg_bit(opcode))) {
		}

		int BLD::operator()(Core *core) const {
			SReg status( core );
		    byte rd  = core->readRegister(Rd);
		    int  T = status.T;
		    byte res;

		    if( T == 0 )
		        res = rd & Kremove;
		    else
		        res = rd | Kadd;

		    core->writeRegister(Rd, res);
		    return 1;
		}

		BRBC::BRBC(word opcode) :
		    bitmask( 1 << get_reg_bit(opcode) ),
		    offset( n_bit_unsigned_to_signed( get_k_7(opcode), 7) ) {
		}

		int BRBC::operator()(Core *core) const {
			SReg status( core );
		    int clks;

		    if( (status.reg & bitmask) == 0 ) {
		        core->jump( offset );
		        clks = 2;
		    } else {
		        clks = 1;
		    }

		    return clks;
		}


		BRBS::BRBS(word opcode) :
		    bitmask( 1 << get_reg_bit(opcode) ),
		    offset( n_bit_unsigned_to_signed( get_k_7(opcode), 7) ) {
		}

		int BRBS::operator()(Core *core) const {
			SReg status( core );
		    int clks;

		    if( (status.reg & bitmask) != 0 ) {
		        core->jump( offset );
		        clks = 2;
		    } else {
		        clks = 1;
		    }

		    return clks;
		}

		BSET::BSET(word opcode) :
			K(1<<get_sreg_bit(opcode)) {
		}

		int BSET::operator()(Core *core) const {
			SReg status( core );
			status.reg = status.reg | K;
		    return 1;
		}

		BST::BST(word opcode) :
			Rd( get_rd_5(opcode) ),
			K( 1 << get_reg_bit(opcode) ) {
		}

		int BST::operator()(Core *core) const {
			SReg status( core );
		    status.T= ((core->readRegister(Rd) & K) != 0 );
		    return 1;
		}

		CALL::CALL(word opcode) :
			KH( get_k_22(opcode) ) {
		}

		int CALL::operator()(Core *core) const {
		    word offset = core->fetchOperand();
		    dword k = (KH<<16) | offset;

		    core->call( k - 1, true );
		    return core->pcBytes() + 2;
		}

		CBI::CBI(word opcode) :
			ioreg( get_A_5(opcode) ),
			K( ~( 1 << get_reg_bit(opcode) ) ) {
		}

		int CBI::operator()(Core *core) const {
		    core->writeIORegister(ioreg, core->readIORegister(ioreg) & K);
		    return 2;
		}

		COM::COM(word opcode) :
		    Rd( get_rd_5(opcode) ) {
		}

		int COM::operator()(Core *core) const {
			SReg status( core );
		    byte rd  = core->readRegister(Rd);
		    byte res = 0xff - rd;

		    status.N = ((res >> 7) & 0x1) ;
		    status.C = 1 ;
		    status.V = 0 ;
		    status.S = (status.N ^ status.V) ;
		    status.Z = ((res & 0xff) == 0) ;

		    core->writeRegister(Rd, res);
		    return 1;
		}

		CP::CP(word opcode) :
			Rd( get_rd_5(opcode) ),
			Rr( get_rr_5(opcode) ) {
		}

		int CP::operator()(Core *core) const {
			SReg status( core );
		    byte rd  = core->readRegister(Rd);
		    byte rr  = core->readRegister(Rr);
		    byte res = rd - rr;

		    status.H = get_compare_carry( res, rd, rr, 3 ) ;
		    status.V = get_compare_overflow( res, rd, rr ) ;
		    status.N = ((res >> 7) & 0x1);
		    status.S = (status.N ^ status.V);
		    status.Z = ((res & 0xff) == 0) ;
		    status.C = get_compare_carry( res, rd, rr, 7 ) ;

		    return 1;
		}

		CPC::CPC(word opcode) :
			Rd( get_rd_5(opcode) ),
			Rr( get_rr_5(opcode) ) {
		}

		int CPC::operator()(Core *core) const {
			SReg status( core );
		    byte rd  = core->readRegister(Rd);
		    byte rr  = core->readRegister(Rr);
		    byte res = rd - rr - status.C;

		    status.H = get_compare_carry( res, rd, rr, 3 ) ;
		    status.V = get_compare_overflow( res, rd, rr ) ;
		    status.N = ((res >> 7) & 0x1) ;
		    status.S = (status.N ^ status.V) ;
		    status.C = get_compare_carry( res, rd, rr, 7 ) ;

		    /* Previous value remains unchanged when result is 0; cleared otherwise */
		    bool Z = ((res & 0xff) == 0);
		    bool prev_Z = status.Z;
		    status.Z = Z && prev_Z ;

		    return 1;
		}


		CPI::CPI(word opcode) :
			Rd( get_rd_4(opcode) ),
			K(get_K_8(opcode)) {
		}

		int CPI::operator()(Core *core) const{
			SReg status( core );
		    byte rd  = core->readRegister(Rd);
		    byte res = rd - K;

		    status.H = get_compare_carry( res, rd, K, 3 ) ;
		    status.V = get_compare_overflow( res, rd, K ) ;
		    status.N = ((res >> 7) & 0x1) ;
		    status.S = (status.N ^ status.V) ;
		    status.Z = ((res & 0xff) == 0) ;
		    status.C = get_compare_carry( res, rd, K, 7 ) ;

		    return 1;
		}

		CPSE::CPSE(word opcode) :
			Rd( get_rd_5(opcode) ),
			Rr( get_rr_5(opcode) ) {
		}

		int CPSE::operator()(Core *core) const {
		    byte rd = core->readRegister(Rd);
		    byte rr = core->readRegister(Rr);

		    int clks = 1;
		    if( rd == rr )
		    	clks += core->skip();

		    return clks;
		}

		DEC::DEC(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int DEC::operator()(Core *core) const {
			SReg status( core );
		    byte res = core->readRegister(Rd) - 1;

		    status.N = ((res >> 7) & 0x1) ;
		    status.V = (res == 0x7f) ;
		    status.S = (status.N ^ status.V) ;
		    status.Z = ((res & 0xff) == 0) ;

		    core->writeRegister(Rd, res);
		    return 1;
		}

		EICALL:: EICALL(word /*opcode*/) :
			eind( EIND ) {
		}

		int  EICALL::operator()(Core *core) const {
			// TODO
			throw util::ImplementationException("Instruction EICALL not fully implemented");
		    int new_pc = core->readRegister(Zl) |
		    			(core->readRegister(Zh)<<8) |
		    			core->readIORegister(eind) << 16;
		    core->call( new_pc, true );
		    return 4;
		}

		EIJMP::EIJMP(word /*opcode*/) :
			eind( EIND ) {
		}

		int EIJMP::operator()(Core *core) const {
			// TODO
			throw util::ImplementationException("Instruction EIJMP not fully implemented");
		    int new_pc = core->readRegister(Zl) |
		    			(core->readRegister(Zh)<<8) |
		    			(core->readIORegister(eind) & 0x3f) << 16;
		    core->call( new_pc, true );
		    return 2;
		}

		ELPM_Z::ELPM_Z(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int ELPM_Z::operator()(Core *core) const {
		    word Z;

		    // TODO
		    throw util::ImplementationException("Instruction ELPM_Z not fully implemented");
#if 0
		    Z = ((core->GetRampz() & 0x3f) << 16) |
		        (Zh << 8) | Zl;
#endif

		    core->writeRegister( Rd, core->readFlash(Z) );
		    return 3;
		}

		ELPM_Z_incr::ELPM_Z_incr(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int ELPM_Z_incr::operator()(Core *core) const {
		    word Z;

		    // TODO
		    throw util::ImplementationException("Instruction ELPM_Z_incr not fully implemented");
#if 0
		    Z = ((core->GetRampz() & 0x3f) << 16) |
		        (Zh << 8) | Zl;
#endif

		    core->writeRegister( Rd, core->readFlash(Z) );

		    /* post increment Z */
		    Z += 1;
#if 0
		    core->SetRampz((Z >> 16) & 0x3f);
#endif
		    core->writeRegister(Zl, Z & 0xff);
		    core->writeRegister(Zh, (Z>>8 & 0xff));
		    return 3;
		}

		ELPM::ELPM(word /*opcode*/) {}

		int ELPM::operator()(Core *core) const {
		    word Z;

		    // TODO
		    throw util::ImplementationException("Instruction ELPM not fully implemented");
#if 0
		    Z = ((core->GetRampz() & 0x3f) << 16) |
		        (Zh << 8) | Zl;
#endif

		    core->writeRegister( R0, core->readFlash(Z) );
		    return 3;
		}

		EOR::EOR(word opcode) :
			Rd( get_rd_5(opcode) ),
			Rr( get_rr_5(opcode) ) {
		}

		int EOR::operator()(Core *core) const {
			SReg status( core );
		    byte rd = core->readRegister(Rd);
		    byte rr = core->readRegister(Rr);

		    byte res = rd ^ rr;

		    status.V = 0 ;
		    status.N = ((res >> 7) & 0x1) ;
		    status.S = (status.N ^ status.V) ;
		    status.Z = ((res & 0xff) == 0 ) ;

		    core->writeRegister(Rd, res);
		    return 1;
		}

		ESPM::ESPM(word /*opcode*/) {}

		int ESPM::operator()(Core * /*core*/) const {
		    return 0;
		}

		FMUL::FMUL(word opcode) :
			Rd( get_rd_3(opcode) ),
			Rr( get_rr_3(opcode) ) {
		}

		int FMUL::operator()(Core *core) const {
			SReg status( core );
		    byte rd =  core->readRegister(Rd);
		    byte rr =  core->readRegister(Rr);

		    word resp = rd * rr;
		    word res = resp << 1;

		    status.Z = ((res & 0xffff) == 0) ;
		    status.C = ((resp >> 15) & 0x1);

		    /* result goes in R1:R0 */
		    core->writeRegister(R0, res & 0xff);
		    core->writeRegister(R1, (res >> 8) & 0xff);
		    return 2;
		}

		FMULS::FMULS(word opcode) :
			Rd( get_rd_3(opcode) ),
			Rr( get_rr_3(opcode) ) {
		}

		int FMULS::operator()(Core *core) const {
			SReg status( core );
		    sbyte rd = core->readRegister(Rd);
		    sbyte rr = core->readRegister(Rr);

		    word resp = rd * rr;
		    word res = resp << 1;

		    status.Z = ((res & 0xffff) == 0) ;
		    status.C = ((resp >> 15) & 0x1) ;

		    /* result goes in R1:R0 */
		    core->writeRegister(R0, res & 0xff);
		    core->writeRegister(R1, (res >> 8) & 0xff);
		    return 2;
		}

		FMULSU::FMULSU(word opcode) :
			Rd( get_rd_3(opcode) ),
			Rr( get_rr_3(opcode) ) {
		}

		int FMULSU::operator()(Core *core) const {
			SReg status( core );
		    sbyte rd = core->readRegister(Rd);
		    sbyte rr = core->readRegister(Rr);

		    word resp = rd * rr;
		    word res = resp << 1;

		    status.Z = ((res & 0xffff) == 0) ;
		    status.C = ((resp >> 15) & 0x1) ;

		    /* result goes in R1:R0 */
		    core->writeRegister(R0, res & 0xff);
		    core->writeRegister(R1, (res >> 8) & 0xff);
		    return 2;
		}

		ICALL::ICALL(word /*opcode*/) {}

		int ICALL::operator()(Core *core) const {
		    /* Z is R31:R30 */
		    int new_pc = (core->readRegister(Zh) << 8) | core->readRegister(Zl);

		    core->call( new_pc - 1, true );
		    return core->pcBytes() + 1;
		}


		IJMP::IJMP(word /*opcode*/) {}

		int IJMP::operator()(Core *core) const {
			int new_pc = (core->readRegister(Zh) << 8) | core->readRegister(Zl);
		    core->call( new_pc - 1, false );

		    return 2;
		}

		IN::IN(word opcode) :
			Rd( get_rd_5(opcode) ),
			ioreg( get_A_6(opcode) ) {
		}

		int IN::operator()(Core *core) const {
			core->writeRegister(Rd, core->readIORegister(ioreg) );
		    return 1;
		}

		INC::INC(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int INC::operator()(Core *core) const {
			SReg status( core );
		    byte rd  = core->readRegister(Rd);
		    byte res = rd + 1;

		    status.N = ((res >> 7) & 0x1) ;
		    status.V = (rd == 0x7f) ;
		    status.S = (status.N ^ status.V) ;
		    status.Z = ((res & 0xff) == 0) ;

		    core->writeRegister(Rd, res);
		    return 1;
		}

		JMP::JMP(word opcode) :
			K( get_k_22(opcode) ) {
		}

		int JMP::operator()(Core *core) const {
		    word offset = core->fetchOperand();
		    core->call( K + offset - 1, false );
		    return 3;
		}

		LDD_Y::LDD_Y(word opcode) :
			Rd( get_rd_5(opcode) ),
			K( get_q(opcode) ) {
		}

		int LDD_Y::operator()(Core *core) const {
		    /* Y is R29:R28 */
		    word Y = ( core->readRegister(Yh) << 8) | core->readRegister(Yl);
		    byte res = core->readByte(Y+K);
		    core->writeRegister(Rd, res);
		    return 2;
		}

		LDD_Z::LDD_Z(word opcode):
			Rd( get_rd_5(opcode) ),
			K( get_q(opcode) ) {
		}

		int LDD_Z::operator()(Core *core) const {
		    /* Z is R31:R30 */
		    word Z = ( core->readRegister(Zh) << 8) | core->readRegister(Zl);
		    byte res = core->readByte(Z+K);
		    core->writeRegister(Rd, res);
		    return 2;
		}

		LDI::LDI(word opcode) :
			Rd( get_rd_4(opcode) ),
			K( get_K_8(opcode) ) {
		}

		int LDI::operator()(Core *core) const {
		    core->writeRegister(Rd, K);
		    return 1;
		}

		LDS::LDS(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int LDS::operator()(Core *core) const {
			/* TODO
			 * The LDS instruction uses the RAMPD register to access memory
			 * above 64K
			 */
		    /* Get data at k in current data segment and put into Rd */
		    word offset = core->fetchOperand();
		    byte res = core->readByte(offset);
		    core->writeRegister(Rd, res);
		    return 2;
		}

		LD_X::LD_X(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int LD_X::operator()(Core *core) const {
		    /* X is R27:R26 */
		    word X = ( core->readRegister(Xh) << 8) | core->readRegister(Xl);
		    byte res = core->readByte(X);
		    core->writeRegister(Rd, res);
		    return 2;
		}

		LD_X_decr::LD_X_decr(word opcode) : LD_X(opcode) {}

		int LD_X_decr::operator()(Core *core) const {
		    /* X is R27:R26 */
		    word X = ( core->readRegister(Xh) << 8) | core->readRegister(Xl);

		    /* Perform pre-decrement */
		    X -= 1;

		    byte res = core->readByte(X);
		    core->writeRegister(Rd, res);

		    core->writeRegister(Xl, X & 0xff);
		    core->writeRegister(Xh, (X>>8) & 0xff);
		    return 2;
		}

		LD_X_incr::LD_X_incr(word opcode) : LD_X(opcode) {}

		int LD_X_incr::operator()(Core *core) const {
		    /* X is R27:R26 */
		    word X = ( core->readRegister(Xh) << 8) | core->readRegister(Xl);

		    byte res = core->readByte(X);
		    core->writeRegister(Rd, res);

		    /* Perform post-increment */
		    X += 1;
		    core->writeRegister(Xl, X & 0xff);
		    core->writeRegister(Xh, (X>>8) & 0xff);
		    return 2;
		}


		LD_Y_decr::LD_Y_decr(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int LD_Y_decr::operator()(Core *core) const {
		    /* Y is R29:R28 */
		    word Y = ( core->readRegister(Yh) << 8) | core->readRegister(Yl);

		    /* Perform pre-decrement */
		    Y -= 1;
		    byte res = core->readByte(Y);
		    core->writeRegister(Rd, res);

		    core->writeRegister(Yl, Y & 0xff);
		    core->writeRegister(Yh, (Y>>8) & 0xff);
		    return 2;
		}


		LD_Y_incr::LD_Y_incr(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int LD_Y_incr::operator()(Core *core) const {
		    /* Y is R29:R28 */
		    word Y = ( core->readRegister(Yh) << 8) | core->readRegister(Yl);

		    byte res = core->readByte(Y);
		    core->writeRegister(Rd, res);

		    /* Post-increment */
		    Y += 1;
		    core->writeRegister(Yl, Y & 0xff);
		    core->writeRegister(Yh, (Y>>8) & 0xff);
		    return 2;
		}

		LD_Z_incr::LD_Z_incr(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int LD_Z_incr::operator()(Core *core) const {
		    /* Z is R31:R30 */
		    word Z = ( core->readRegister(Zh) << 8) | core->readRegister(Zl);

		    byte res = core->readByte(Z);
		    core->writeRegister(Rd, res);

		    /* Perform post-increment */
		    Z += 1;
		    core->writeRegister(Zl, Z & 0xff);
		    core->writeRegister(Zh, (Z>>8) & 0xff);
		    return 2;
		}

		LD_Z_decr::LD_Z_decr(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int LD_Z_decr::operator()(Core *core) const {
		    /* Z is R31:R30 */
		    word Z = ( core->readRegister(Zh) << 8) | core->readRegister(Zl);

		    /* Perform pre-decrement */
		    Z -= 1;

		    byte res = core->readByte(Z);
		    core->writeRegister(Rd, res);

		    core->writeRegister(Zl, Z & 0xff);
		    core->writeRegister(Zh, (Z>>8) & 0xff);
		    return 2;
		}

		LPM_Z::LPM_Z(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int LPM_Z::operator()(Core *core) const {
		    /* Z is R31:R30 */
		    word Z = ( core->readRegister(Zh) << 8) | core->readRegister(Zl);

		    byte data = core->readFlash(Z);
		    core->writeRegister(Rd, data);
		    return 3;
		}

		LPM::LPM(word /*opcode*/) {}

		int LPM::operator()(Core *core) const {
		    /* Z is R31:R30 */
		    word Z = ( core->readRegister(Zh) << 8) | core->readRegister(Zl);

		    byte data = core->readFlash(Z);
		    core->writeRegister(R0, data);
		    return 3;
		}

		LPM_Z_incr::LPM_Z_incr(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int LPM_Z_incr::operator()(Core *core) const {
		    /* Z is R31:R30 */
		    word Z = ( core->readRegister(Zh) << 8) | core->readRegister(Zl);

		    byte data = core->readFlash(Z);
		    core->writeRegister(Rd, data);

		    Z += 1;
		    core->writeRegister(Zl, Z & 0xff);
		    core->writeRegister(Zh, (Z>>8) & 0xff);
		    return 3;
		}

		LSR::LSR(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int LSR::operator()(Core *core) const {
			SReg status( core );
		    byte rd = core->readRegister(Rd);
		    byte res = (rd >> 1) & 0x7f;

		    status.C = (rd & 0x1) ;
		    status.N = (0) ;
		    status.V = (status.N ^ status.C) ;
		    status.S = (status.N ^ status.V) ;
		    status.Z = ((res & 0xff) == 0) ;

		    core->writeRegister(Rd, res);
		    return 1;
		}

		MOV::MOV(word opcode) :
			Rd( get_rd_5(opcode) ),
			Rr( get_rr_5(opcode) ) {
		}

		int MOV::operator()(Core *core) const {
		    core->writeRegister(Rd, core->readRegister(Rr) );
		    return 1;
		}

		MOVW::MOVW(word opcode) :
			Rdl( (get_rd_4(opcode) - 16)<<1 ),
			Rdh( ((get_rd_4(opcode) - 16)<<1) + 1),
			Rrl( (get_rr_4(opcode) - 16)<<1 ),
			Rrh( ((get_rr_4(opcode) - 16)<<1) + 1) {
		}

		int MOVW::operator()(Core *core) const {
			core->writeRegister(Rdl, core->readRegister(Rrl) );
			core->writeRegister(Rdh, core->readRegister(Rrh) );
		    return 1;
		}

		MUL::MUL(word opcode) :
			Rd( get_rd_5(opcode) ),
			Rr( get_rr_5(opcode) ) {
		}

		int MUL::operator()(Core *core) const {
			SReg status( core );
		    byte rd = core->readRegister(Rd);
		    byte rr = core->readRegister(Rr);

		    word res = rd * rr;

		    status.Z = ((res & 0xffff) == 0);
		    status.C = ((res >> 15) & 0x1);

		    /* result goes in R1:R0 */
		    core->writeRegister(R0, res & 0xff);
		    core->writeRegister(R1, (res>>8) & 0xff);
		    return 2;
		}

		MULS::MULS(word opcode) :
			Rd( get_rd_4(opcode) ),
			Rr( get_rr_4(opcode) ) {
		}

		int MULS::operator()(Core *core) const {
			SReg status( core );
		    sbyte rd = (sbyte)core->readRegister(Rd);
		    sbyte rr = (sbyte)core->readRegister(Rr);
		    sword res = rd * rr;

		    status.Z = ((res & 0xffff) == 0) ;
		    status.C = ((res >> 15) & 0x1) ;

		    /* result goes in R1:R0 */
		    core->writeRegister(R0, res & 0xff);
		    core->writeRegister(R1, (res>>8) & 0xff);
		    return 2;
		}

		MULSU::MULSU(word opcode) :
			Rd( get_rd_3(opcode) ),
			Rr( get_rr_3(opcode) ) {
		}

		int MULSU::operator()(Core *core) const {
			SReg status( core );
		    sbyte rd = (sbyte)core->readRegister(Rd);
		    byte rr = core->readRegister(Rr);

		    sword res = rd * rr;

		    status.Z = ((res & 0xffff) == 0);
		    status.C = ((res >> 15) & 0x1);

		    /* result goes in R1:R0 */
		    core->writeRegister(R0, res & 0xff);
		    core->writeRegister(R1, (res>>8) & 0xff);
		    return 2;
		}

		NEG::NEG(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int NEG::operator()(Core *core) const {
			SReg status( core );
		    byte rd  = core->readRegister(Rd);
		    byte res = (0x0 - rd) & 0xff;

		    status.H = (((res >> 3) | (rd >> 3)) & 0x1);
		    status.V = (res == 0x80);
		    status.N = ((res >> 7) & 0x1);
		    status.S = (status.N ^ status.V);
		    status.Z = (res == 0x0);
		    status.C = (res != 0x0);

		    core->writeRegister(Rd, res);
		    return 1;
		}

		NOP::NOP(word /*opcode*/) {}

		int NOP::operator()(Core */*core*/) const {
		    return 1;
		}

		OR::OR(word opcode) :
			Rd( get_rd_5(opcode) ),
			Rr(get_rr_5(opcode) ) {
		}

		int OR::operator()(Core *core) const{
			SReg status( core );
		    byte res = core->readRegister(Rd) | core->readRegister(Rr);

		    status.V = (0);
		    status.N = ((res >> 7) & 0x1);
		    status.S = (status.N ^ status.V);
		    status.Z = (res == 0x0);

		    core->writeRegister(Rd, res);
		    return 1;
		}

		ORI::ORI(word opcode) :
			Rd( get_rd_4(opcode) ),
			K( get_K_8(opcode) ) {
		}

		int ORI::operator()(Core *core) const{
			SReg status( core );
		    byte res = core->readRegister(Rd) | K;

		    status.V = (0);
		    status.N = ((res >> 7) & 0x1);
		    status.S = (status.N ^ status.V);
		    status.Z = (res == 0x0);

		    core->writeRegister(Rd, res);
		    return 1;
		}

		OUT::OUT(word opcode) :
			Rd( get_rd_5(opcode) ),
			ioreg( get_A_6(opcode) ) {
		}

		int OUT::operator()(Core *core) const {
		    core->writeIORegister(ioreg, core->readRegister(Rd));
		    return 1;
		}

		POP::POP(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int POP::operator()(Core *core) const {
			core->writeRegister(Rd, core->pop());
		    return 2;
		}

		PUSH::PUSH(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int PUSH::operator()(Core *core) const {
			core->push( core->readRegister(Rd) );
		    return 2;
		}

		RCALL::RCALL(word opcode) :
			K( n_bit_unsigned_to_signed( get_k_12(opcode), 12 ) ) {
		}

		int RCALL::operator()(Core *core) const {
			int cost = core->pcBytes();
			core->jump(K, true);
		    return cost + 1;
		}

		RET::RET(word /*opcode*/) { }

		int RET::operator()(Core *core) const {
			int cost = core->pcBytes();
			core->ret(false);
		    return cost + 2;
		}

		RETI::RETI(word /*opcode*/) {}

		int RETI::operator()(Core *core) const {
			SReg status( core );
			int cost = core->pcBytes();
			core->ret(true);
			status.I = 1;
		    return cost + 2;
		}

		RJMP::RJMP(word opcode) :
			K( n_bit_unsigned_to_signed( get_k_12(opcode), 12 ) ) {
		}

		int RJMP::operator()(Core *core) const {
			core->jump( K, false );
		    return 2;
		}

		ROR::ROR(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int ROR::operator()(Core *core) const {
			SReg status( core );
		    byte rd = core->readRegister(Rd);

		    byte res = (rd >> 1) | ((( status.C ) << 7) & 0x80);

		    status.C = (rd & 0x1) ;
		    status.N = ((res >> 7) & 0x1) ;
		    status.V = (status.N ^ status.C) ;
		    status.S = (status.N ^ status.V) ;
		    status.Z = (res == 0) ;

		    core->writeRegister(Rd, res);
		    return 1;
		}

		SBC::SBC(word opcode) :
			Rd( get_rd_5(opcode) ),
			Rr( get_rr_5(opcode) ) {
		}

		int SBC::operator()(Core *core) const {
			SReg status( core );
		    byte rd = core->readRegister(Rd);
		    byte rr = core->readRegister(Rr);

		    byte res = rd - rr - ( status.C );

		    status.H = (get_sub_carry( res, rd, rr, 3 )) ;
		    status.V = (get_sub_overflow( res, rd, rr )) ;
		    status.N = ((res >> 7) & 0x1) ;
		    status.S = (status.N ^ status.V) ;
		    status.C = (get_sub_carry( res, rd, rr, 7 )) ;

		    if ((res & 0xff) != 0)
		        status.Z = (0);

		    core->writeRegister(Rd, res);
		    return 1;
		}

		SBCI::SBCI(word opcode) :
			Rd( get_rd_4(opcode) ),
			K( get_K_8(opcode) ) {
		}

		int SBCI::operator()(Core *core) const {
			SReg status( core );
		    byte rd = core->readRegister(Rd);

		    byte res = rd - K - ( status.C );

		    status.H = (get_sub_carry( res, rd, K, 3 )) ;
		    status.V = (get_sub_overflow( res, rd, K )) ;
		    status.N = ((res >> 7) & 0x1) ;
		    status.S = (status.N ^ status.V) ;
		    status.C = (get_sub_carry( res, rd, K, 7 )) ;

		    if ((res & 0xff) != 0)
		        status.Z = 0 ;

		    core->writeRegister(Rd, res);
		    return 1;
		}

		SBI::SBI(word opcode) :
			ioreg(get_A_5(opcode) ),
			K( 1 << get_reg_bit(opcode) ) {
		}

		int SBI::operator()(Core *core) const {
			core->writeIORegister(ioreg, core->readIORegister(ioreg) | K);
		    return 2;
		}

		SBIC::SBIC(word opcode) :
			ioreg( get_A_5(opcode) ),
			K( 1 << get_reg_bit(opcode) ) {
		}

		int SBIC::operator()(Core *core) const {
			int cost = 1;
			if( (core->readIORegister(ioreg) & K) == 0 )
				cost += core->skip();
			return cost;
		}

		SBIS::SBIS(word opcode) :
			ioreg( get_A_5(opcode) ),
			K( 1 << get_reg_bit(opcode) ) {
		}

		int SBIS::operator()(Core *core) const {
			int cost = 1;
			if( (core->readIORegister(ioreg) & K) != 0 )
				cost += core->skip();
			return cost;
		}

		SBIW::SBIW(word opcode) :
			Rl( get_rd_2(opcode) ),
			Rh( get_rd_2(opcode)+1 ),
			K( get_K_6(opcode) ) {
		}

		int SBIW::operator()(Core *core) const {
			SReg status( core );
		    byte rdl = core->readRegister(Rl);
		    byte rdh = core->readRegister(Rh);

		    word rd = (rdh << 8) + rdl;

		    word res = rd - K;

		    status.V = ((rdh >> 7 & 0x1) & ~(res >> 15 & 0x1)) ;
		    status.N = ((res >> 15) & 0x1) ;
		    status.S = (status.N ^ status.V) ;
		    status.Z = ((res & 0xffff) == 0) ;
		    status.C = ((res >> 15 & 0x1) & ~(rdh >> 7 & 0x1)) ;

		    core->writeRegister(Rl, res & 0xff);
		    core->writeRegister(Rh, (res>>8) & 0xff);
		    return 2;
		}

		SBRC::SBRC(word opcode) :
			Rd( get_rd_5(opcode) ),
			K( 1 << get_reg_bit(opcode) ) {
		}

		int SBRC::operator()(Core *core) const {
			int cost = 1;
			if( (core->readRegister(Rd) & K) == 0 )
				cost += core->skip();
			return cost;
		}

		SBRS::SBRS(word opcode) :
			Rd( get_rd_5(opcode) ),
			K( 1 << get_reg_bit(opcode) ) {
		}

		int SBRS::operator()(Core *core) const {
			int cost = 1;
			if( (core->readRegister(Rd) & K) != 0 )
				cost += core->skip();
			return cost;
		}

		SLEEP::SLEEP(word /*opcode*/) {}

		int SLEEP::operator()(Core *core) const {
			core->sleep();
		    return 0;
		}

		SPM::SPM(word /*opcode*/) {
		}

		int SPM::operator()(Core *core) const {
			word Z = ( core->readRegister(Zh) << 8 ) | core->readRegister(Zl);

			word data = ( core->readRegister(R1) << 8 ) | core->readRegister(R0);
			int ret = core->writeFlash(Z, data);
		    return ret;
		}

		STD_Y::STD_Y(word opcode) :
			Rd( get_rd_5(opcode) ),
			K( get_q(opcode) ) {
		}

		int STD_Y::operator()(Core *core) const {
		    /* Y is R29:R28 */
		    word Y = ( core->readRegister(Yh) << 8 ) | core->readRegister(Yl);

		    core->writeByte( Y+K, core->readRegister(Rd) );
		    return 2;
		}

		STD_Z::STD_Z(word opcode) :
			Rd( get_rd_5(opcode) ),
			K( get_q(opcode) ) {
		}

		int STD_Z::operator()(Core *core) const {
		    /* Z is R31:R30 */
			word Z = ( core->readRegister(Zh) << 8 ) | core->readRegister(Zl);

		    core->writeByte( Z+K, core->readRegister(Rd) );
		    return 2;
		}

		STS::STS(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int STS::operator()(Core *core) const {
			word k = core->fetchOperand();
		    core->writeByte( k, core->readRegister(Rd) );
		    return 2;
		}

		ST_X::ST_X(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int ST_X::operator()(Core *core) const {
		    /* X is R27:R26 */
		    word X = ( core->readRegister(Xh) << 8) | core->readRegister(Xl);

		    core->writeByte( X, core->readRegister(Rd) );
		    return 2;
		}

		ST_X_decr::ST_X_decr(word opcode) : ST_X(opcode) {}

		int ST_X_decr::operator()(Core *core) const {
		    /* X is R27:R26 */
		    word X = ( core->readRegister(Xh) << 8) | core->readRegister(Xl);

		    /* Perform pre-decrement */
		    X -= 1;

		    core->writeByte( X, core->readRegister(Rd) );

		    core->writeRegister(Xl, X & 0xff);
		    core->writeRegister(Xh, (X>>8) & 0xff);
		    return 2;
		}

		ST_X_incr::ST_X_incr(word opcode) : ST_X(opcode) {}

		int ST_X_incr::operator()(Core *core) const {
		    /* X is R27:R26 */
		    word X = ( core->readRegister(Xh) << 8) | core->readRegister(Xl);

		    core->writeByte( X, core->readRegister(Rd) );

		    /* Perform post-increment */
		    X += 1;
		    core->writeRegister(Xl, X & 0xff);
		    core->writeRegister(Xh, (X>>8) & 0xff);
		    return 2;
		}

		ST_Y_decr::ST_Y_decr(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int ST_Y_decr::operator()(Core *core) const {
		    /* Y is R29:R28 */
		    word Y = ( core->readRegister(Yh) << 8) | core->readRegister(Yl);

		    /* Perform pre-decrement */
		    Y -= 1;

		    core->writeByte( Y, core->readRegister(Rd) );
		    core->writeRegister(Yl, Y & 0xff);
		    core->writeRegister(Yh, (Y>>8) & 0xff);
		    return 2;
		}

		ST_Y_incr::ST_Y_incr(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int ST_Y_incr::operator()(Core *core) const {
		    /* Y is R29:R28 */
		    word Y = ( core->readRegister(Yh) << 8) | core->readRegister(Yl);

		    core->writeByte( Y, core->readRegister(Rd) );

		    /* Perform post-increment */
		    Y += 1;

		    core->writeRegister(Yl, Y & 0xff);
		    core->writeRegister(Yh, (Y>>8) & 0xff);
		    return 2;
		}

		ST_Z_decr::ST_Z_decr(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int ST_Z_decr::operator()(Core *core) const {
		    /* Z is R31:R30 */
		    word Z = ( core->readRegister(Zh) << 8) | core->readRegister(Zl);

		    /* Perform pre-decrement */
		    Z -= 1;

		    core->writeByte( Z, core->readRegister(Rd) );

		    core->writeRegister(Zl, Z & 0xff);
		    core->writeRegister(Zh, (Z>>8) & 0xff);
		    return 2;
		}

		ST_Z_incr::ST_Z_incr(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int ST_Z_incr::operator()(Core *core) const {
		    /* Z is R31:R30 */
		    word Z = ( core->readRegister(Zh) << 8) | core->readRegister(Zl);

		    core->writeByte( Z, core->readRegister(Rd) );

		    /* Perform post-increment */
		    Z += 1;

		    core->writeRegister(Zl, Z & 0xff);
		    core->writeRegister(Zh, (Z>>8) & 0xff);
		    return 2;
		}

		SUB::SUB(word opcode) :
			Rd( get_rd_5(opcode) ),
			Rr( get_rr_5(opcode) ) {
		}

		int SUB::operator()(Core *core) const {
			SReg status( core );
		    byte rd = core->readRegister(Rd);
		    byte rr = core->readRegister(Rr);

		    byte res = rd - rr;

		    status.H = (get_sub_carry( res, rd, rr, 3 )) ;
		    status.V = (get_sub_overflow( res, rd, rr )) ;
		    status.N = ((res >> 7) & 0x1) ;
		    status.S = (status.N ^ status.V) ;
		    status.Z = ((res & 0xff) == 0) ;
		    status.C = (get_sub_carry( res, rd, rr, 7 )) ;

		    core->writeRegister(Rd, res);
		    return 1;
		}

		SUBI::SUBI(word opcode) :
			Rd( get_rd_4(opcode) ),
			K( get_K_8(opcode) ) {
		}

		int SUBI::operator()(Core *core) const {
			SReg status( core );
		    byte rd = core->readRegister(Rd);

		    byte res = rd - K;

		    status.H = (get_sub_carry( res, rd, K, 3 )) ;
		    status.V = (get_sub_overflow( res, rd, K )) ;
		    status.N = ((res >> 7) & 0x1) ;
		    status.S = (status.N ^ status.V) ;
		    status.Z = ((res & 0xff) == 0) ;
		    status.C = (get_sub_carry( res, rd, K, 7 )) ;

		    core->writeRegister(Rd, res);
		    return 1;
		}

		SWAP::SWAP(word opcode) :
			Rd( get_rd_5(opcode) ) {
		}

		int SWAP::operator()(Core *core) const {
		    byte rd = core->readRegister(Rd);
		    byte res = ((rd << 4) & 0xf0) | ((rd >> 4) & 0x0f);
		    core->writeRegister(Rd, res);
		    return 1;
		}

		WDR::WDR(word /*opcode*/) {}

		int WDR::operator()(Core * /*core*/) const {
			throw util::ImplementationException("Instruction WDR not fully implemented");
		    return 1;
		}

		BREAK::BREAK(word /*opcode*/) {}

		int BREAK::operator()(Core *core) const {
			core->systemBreak();
		    return 1;
		}

		ILLEGAL::ILLEGAL(word opcode) : opcode(opcode) {}

		int ILLEGAL::operator()(Core * /*core*/) const {
			throw IllegalInstruction(opcode);
		}

	}

}
