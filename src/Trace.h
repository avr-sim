#ifndef AVR_TRACE_H
#define AVR_TRACE_H

#include <iostream>
#include <fstream>
#include "Types.h"

namespace avr {
	class Device;
	class DebugInterface;

	/**
	 * @author Tom Haber
	 * @date Apr 25, 2008
	 * @brief Traces almost everything simulated.
	 *
	 * This class traces almost everything simulated: instructions executed,
	 * register reads/writes, clock cycles, break points, instruction skips,
	 * external modules, and a few other miscellaneous things.
	 *
	 * Details
	 *	Each trace takes exactly one 32-bit word. The upper 8-bits define
	 * 	the trace type and the lower 24 are the trace value. For example,
	 *	a register write will get traced with a 32 bit encoded like:
	 *
	 *	TTAAAAVV
	 *
	 * TT - Register write trace type
	 * AAAA - 4-hexdigit address
	 * VV - 2-hexdigit (8-bit) value
	 *
	 * The cycle counter is treated slightly differently. Since it is a
	 * 64-bit object, it has to be split across at least two trace
	 * entries. The upper few bits of the cycle counter aren't
	 * traced.
	 *
	 * This class is based on the trace in gpsim.
	 *
	 * TODO integrate this further with TraceAnalyzer unless real-time tracing is required
	 */
	class Trace {
		public:
			Trace();
			~Trace();

		public:
			/**
			 * Trace raw allows any value to be written to the trace buffer.
			 * This is useful for modules that wish to trace things, but do
			 * not wish to modify the Trace class.
			 */
			void raw(unsigned int ui);

			void register_write(unsigned int address, unsigned char val);
			void register_read(unsigned int address, unsigned char val);
			void opcode_write(unsigned int address, unsigned int opcode);
			void cycle_counter(lword cc);
			void breakpoint(unsigned int bp);
			void interrupt(unsigned int intr);
			void reset(unsigned int type);
			void pc_trace(unsigned int address);

		public:
			/**
			 * return the trace entry at 'index'
			 */
			unsigned int operator [](unsigned int index) const;

			/**
			 * return the trace entry at 'index'
			 */
			unsigned int get(unsigned int index) const;

			/**
			 * inRange - returns true if the trace index i is between the
			 * indices of low and high.
			 * It's assumed that the range does not exceed half of the trace buffer
			 */
			bool inRange(unsigned int i, unsigned int low, unsigned int high) const;

			//* type() - return the trace type at 'index'
			unsigned int type(unsigned int index) const;

			/**
			 * Given an index into the trace buffer, this function determines
			 * if the trace is a cycle counter trace.
			 *
			 * INPUT: index - index into the trace buffer
			 * 		*cycle - a pointer to where the cycle will be decoded
			 * 					if the trace entry is a cycle trace.
			 * RETURN: 0 - trace is not a cycle counter
			 * 		 1 - trace is the high integer of a cycle trace
			 * 		 2 - trace is the low integer of a cycle trace
			 */
			int isCycleTrace(unsigned int index, lword *cycle) const;

		public:
			// When logging is enabled, the entire trace buffer will be copied to a file.
			void enableLogging(const char *fname);
			void disableLogging();

			void list(const char *filename, std::ostream & ostr,
						Device & dev, int verbose = 0);
			void print(std::ostream & ostr, Device & dev, int verbose = 0) const;
			void printFrom(unsigned index, std::ostream & ostr,
							Device & dev, int verbose = 0) const;
			void print(unsigned index, std::ostream & ostr,
							DebugInterface & dbgi, int verbose) const;

		private:
			//* tbi - trace buffer index masking.
			unsigned int tbi(unsigned int index) const;
			void log();

		private:
			enum eTraceTypes {
				NOTHING = 0x3fffffff,
				INTERRUPT = (3<<24),
				RESET = (4<<24),
				REG_WRITE = (5<<24),
				REG_READ = (6<<24),
				OPCODE_WRITE = (7<<24),
				PC_TRACE = (8<<24),
				LAST_TRACE_TYPE = (9<<24),

				TYPE_MASK = (0xff<<24),
				CYCLE_COUNTER_LO = (0x80<<24),
				CYCLE_COUNTER_HI = (0x40<<24)
			};

			static const unsigned int traceBufferSize = 1<<12;
			static const unsigned int traceBufferMask = traceBufferSize - 1;

			unsigned int traceBuffer[traceBufferSize];
			unsigned int traceIndex;

		private:
			std::ofstream fout;
	};

	inline void Trace::raw(unsigned int ui) {
		traceBuffer[traceIndex] = ui;
		log();
		traceIndex = (traceIndex + 1) & traceBufferMask;
	}

	inline void Trace::register_write(unsigned int address, unsigned char val) {
		raw( REG_WRITE | ((address & 0xffff) << 8) | val );
	}

	inline void Trace::register_read(unsigned int address, unsigned char val) {
		raw( REG_READ | ((address & 0xffff) << 8) | val );
	}

	inline void Trace::opcode_write(unsigned int address, unsigned int opcode) {
		raw( OPCODE_WRITE | (address & 0xffffff) );
		raw( OPCODE_WRITE | (opcode & 0xffff) );
	}

	inline void Trace::cycle_counter(lword cc) {
		// The 64 bit cycle counter requires two 32 bit traces.
		raw( (unsigned int)(CYCLE_COUNTER_LO | (cc & 0xffffffff)) );
		raw( (unsigned int)(CYCLE_COUNTER_HI | (cc>>32)| (cc & CYCLE_COUNTER_LO)) );
	}

	inline void Trace::interrupt(unsigned int intr) {
		raw( INTERRUPT | (intr & 0xffff) );
	}

	inline void Trace::reset(unsigned int type) {
		raw( RESET | (type & 0xffff) );
	}

	inline void Trace::pc_trace(unsigned int address) {
		raw( PC_TRACE | (address & 0xffffff) );
	}

	// tbi - trace buffer index masking.
	inline unsigned int Trace::tbi(unsigned int index) const {
		return index & traceBufferMask;
	}

	// get() return the trace entry at 'index'
	inline unsigned int Trace::operator [] (unsigned int index) const {
		return traceBuffer[tbi(index)];
	}

	inline unsigned int Trace::get(unsigned int index) const {
		return traceBuffer[tbi(index)];
	}

	inline void Trace::log() {
		if( fout.is_open() )
			fout.write( (const char*)&traceBuffer[traceIndex], sizeof(unsigned int) );
	}

	inline bool Trace::inRange(unsigned int i, unsigned int low, unsigned int high) const {
		i = tbi(i);

		if( low < high)
			return (i >= low && i <= high);

		// Looks like the range straddles the roll over boundary.
		return (i >= low || i <= high);
	}

}

#endif /*TRACE_H_*/
