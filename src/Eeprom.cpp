/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Eeprom.h"
#include "Bus.h"
#include "Registers.h"
#include <cstring>

#define EERIE	(1<<3)
#define EEMWE	(1<<2)
#define EEWE	(1<<1)
#define EERE	(1<<0)
#define CLEAR	0xf8

namespace avr {

	Eeprom::Eeprom(Bus & bus, unsigned int size, unsigned int rdyVec)
		: Hardware(bus), Memory(size), rdyVec(rdyVec), oldEecr(0) {
	}

	Eeprom::~Eeprom() {
	}

	void Eeprom::writeToAddress(unsigned int addr, unsigned char val) {
		write(addr, &val);
	}

	unsigned char Eeprom::readFromAddress(unsigned int addr) {
	    return readByte(addr);
	}

	bool Eeprom::attachReg(const char *name, IORegister *reg) {
		if( strcmp(name, "eecr") == 0 )
			eecr = reg;
		else if( strcmp(name, "eedr") == 0 ) {
			eedr = reg;
			return true;
		} else if( strcmp(name, "eearl") == 0 )
			eearl = reg;
		else if( strcmp(name, "eearh") == 0 )
			eearh = reg;
		else
			return false;

		reg->registerHW(this);
		return true;
	}

	void Eeprom::regChanged( IORegister *reg ) {
		if( reg == eearh )
		    eear = (eear & 0x00ff) | (*eearh << 8);
		else if( reg == eearl )
			eear = (eear & 0xff00) | (*eearl);
		else if( reg == eecr )
			setEECR( *eecr );
	}

	void Eeprom::step() {
		switch( state ) {
			case READY:
				break;

			case WRITE:
				eecr->set( *eecr & CLEAR );
				writeToAddress( eear, *eedr );

				if( (*eecr & EERIE) != 0 )
					bus.raiseInterrupt(rdyVec);

				state = READY;
				break;

			case READ:
				eecr->set( *eecr & CLEAR );
				*eedr = readFromAddress( eear );

				state = READY;
				break;

			case WRITE_ENABLED:
				eecr->set( *eecr & ~EEMWE );
				state = READY;
				break;
		}
	}

	void Eeprom::setEECR( unsigned char eecr ) {
	    if( ((eecr & EEMWE) != 0) && (state == READY) ) {
	        state = WRITE_ENABLED;
	        bus.reassignBreakDelta(writeEnableCycles, this);
	    } else if( ((eecr & EEWE) != 0) && (state == WRITE_ENABLED) ) {
			state = WRITE;
	        setHoldCycles( writeCycles );
			bus.reassignBreakDelta(writeCycles, this);
		} else if( (eecr & EERE) != 0 ) {
			//EERE EEprom read enable
			if( state == WRITE ) {
				state = READ;
				bus.clearBreak(this);
			} else {
				state = READ;
				setHoldCycles( readCycles );
				bus.reassignBreakDelta(readCycles, this);
			}

			state = READ;
		} else if( (eecr & 0x07) != 0x00 ) {
	        state = READY;
	        bus.clearBreak( this );
	    }

		oldEecr = eecr;
	}

}
