/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "HardwareFactory.h"
#include "Format.h"
#include "RuntimeException.h"
#include "DeviceSettings.h"

#ifdef DEBUG
#	include <iostream>
#endif

#include "Eeprom.h"
#include "Port.h"
#include "Usart.h"
#include "Spi.h"
#include "ADC.h"
#include "Timer8.h"
#include "Timer16.h"
#include "TimerInterrupts.h"

#include <cstring>

namespace avr {

	Hardware *HardwareFactory::build(const char *name,
					HardwareSettings & hws, Bus & bus) {
#ifdef DEBUG
		std::cout << "Building hardware: " << name << std::endl;
#endif

		if( strcmp(name, "eeprom") == 0 )
			return buildEeprom(hws, bus);
		else if( strcmp(name, "hwport") == 0 )
			return buildPort(hws, bus);
		else if( strcmp(name, "timer8") == 0 )
			return buildTimer8(hws, bus);
		else if( strcmp(name, "timer16") == 0 )
			return buildTimer16(hws, bus);
		else if( strcmp(name, "timerirq") == 0 )
			return buildTimerIrq(hws, bus);
		else if( strcmp(name, "usart") == 0 )
			return buildUsart(hws, bus);
		else if( strcmp(name, "spi") == 0 )
			return buildSpi(hws, bus);
		else if( strcmp(name, "adc") == 0 )
			return buildADC(hws, bus);

		throw util::RuntimeException(
				util::format("Trying to build unknown hardware: %s") % name );
	}

	Hardware *HardwareFactory::buildEeprom(HardwareSettings & hws, Bus & bus) {
		unsigned int size = hws.intParam("size", 10);
		unsigned int rdyVec = hws.intParam("rdyVec", 10);
		return new Eeprom(bus, size, rdyVec);
	}

	Hardware *HardwareFactory::buildPort(HardwareSettings & hws, Bus & bus) {
		const std::string & name = hws.strParam("name");
		unsigned int mask = hws.intParamDef("mask", 0xff, 16);
		return new Port(bus, name, mask);
	}

	Hardware *HardwareFactory::buildTimer8(HardwareSettings & hws, Bus & bus) {
		unsigned char tovmask = hws.intParam("tov", 16);

		unsigned int units = hws.intParam("units", 10);
		unsigned char ocfmasks[units];
		if( units >= 1 ) ocfmasks[0] = hws.intParam("ocf0", 16);
		if( units >= 2 ) ocfmasks[1] = hws.intParam("ocf1", 16);
		if( units >= 3 ) ocfmasks[2] = hws.intParam("ocf2", 16);

		return new Timer8(bus, tovmask, units, ocfmasks);
	}

	Hardware *HardwareFactory::buildTimer16(HardwareSettings & hws, Bus & bus) {
		unsigned int tovmask = hws.intParam("tov", 16);

		unsigned int units = hws.intParam("units", 10);
		unsigned char ocfmasks[units];
		if( units >= 1 ) ocfmasks[0] = hws.intParam("ocf0", 16);
		if( units >= 2 ) ocfmasks[1] = hws.intParam("ocf1", 16);

		return new Timer16(bus, tovmask, units, ocfmasks);
	}

	Hardware *HardwareFactory::buildTimerIrq(HardwareSettings & hws, Bus & bus) {
		unsigned int mask = hws.intParamDef("mask", 0xff, 16);
		unsigned int bit0Vec = hws.intParamDef("bit0Vec", 0, 10) - 1;
		unsigned int bit1Vec = hws.intParamDef("bit1Vec", 0, 10) - 1;
		unsigned int bit2Vec = hws.intParamDef("bit2Vec", 0, 10) - 1;
		unsigned int bit3Vec = hws.intParamDef("bit3Vec", 0, 10) - 1;
		unsigned int bit4Vec = hws.intParamDef("bit4Vec", 0, 10) - 1;
		unsigned int bit5Vec = hws.intParamDef("bit5Vec", 0, 10) - 1;
		unsigned int bit6Vec = hws.intParamDef("bit6Vec", 0, 10) - 1;
		unsigned int bit7Vec = hws.intParamDef("bit7Vec", 0, 10) - 1;
		return new TimerInterrupts(bus, mask,
				bit0Vec, bit1Vec, bit2Vec, bit3Vec,
				bit4Vec, bit5Vec, bit6Vec, bit7Vec);
	}

	Hardware *HardwareFactory::buildUsart(HardwareSettings & hws, Bus & bus) {
		const std::string & name = hws.strParam("name");
		unsigned int udreVec = hws.intParam("udreVec", 10) - 1;
		unsigned int rxVec = hws.intParam("rxVec", 10) - 1;
		unsigned int txVec = hws.intParam("txVec", 10) - 1;
		return new Usart(bus, name, udreVec, rxVec, txVec);
	}

	Hardware *HardwareFactory::buildSpi(HardwareSettings & hws, Bus & bus) {
		unsigned int stcVec = hws.intParam("stcVec", 10) - 1;
		return new Spi(bus, stcVec);
	}

	Hardware *HardwareFactory::buildADC(HardwareSettings & hws, Bus & bus) {
		unsigned int ccVec = hws.intParam("ccVec", 10) - 1;
		return new ADC(bus, ccVec);
	}
}
