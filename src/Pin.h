#ifndef AVR_PIN_H
#define AVR_PIN_H

#include "CircuitNode.h"

namespace avr {

	class Pin : public CircuitNode {
		public:
			Pin(float V = 5.0f, float Z = 1e3f);
			~Pin();
			
		public:
			
	};

}

#endif /*AVR_PIN_H*/
