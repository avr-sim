/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptEngine.h"
#include "script/ScriptException.h"
#include "Program.h"

#ifdef DEBUG
#	include <iostream>
#endif

using script::ScriptMethod;
using script::ScriptUtil;
using script::VM;

namespace avr {

	ScriptMethod<ScriptProgram> ScriptProgram::sm_methods[] = {
		ScriptMethod<ScriptProgram>( "functionName", &ScriptProgram::script_functionName ),
		ScriptMethod<ScriptProgram>( "dataName", &ScriptProgram::script_dataName ),
		ScriptMethod<ScriptProgram>( "functionAddress", &ScriptProgram::script_functionAddress ),
		ScriptMethod<ScriptProgram>( "dataAddress", &ScriptProgram::script_dataAddress )
	};

	ScriptProgram::ScriptProgram(script::VM *vm, const Program & program)
			: Scriptable(vm), program(program), methodBase( -1 ) {
		methodBase = ScriptUtil<ScriptProgram,Scriptable>::addMethods( this,
						sm_methods, sizeof(sm_methods)/sizeof(sm_methods[0]) );

		registerClass("Program");
	}

	ScriptProgram::~ScriptProgram() {
		unregisterClass("Program");
	}

	int ScriptProgram::methodCall(int i) {
	    return ScriptUtil<ScriptProgram,Scriptable>::methodCall(this, i, methodBase,
	                    sm_methods, sizeof(sm_methods)/sizeof(sm_methods[0]) );
	}

	int ScriptProgram::script_functionName(const char *funcName) {
	    if ( vm->top() != 1 || vm->getType(1) != VM::Number )
	        throw script::ScriptException("expects a function address");

	    unsigned int addr = (unsigned int)vm->toNumber(1);
	    vm->pushString( program.functionName(addr) );
	    return 1;
	}

	int ScriptProgram::script_dataName(const char *funcName) {
	    if ( vm->top() != 1 || vm->getType(1) != VM::Number )
	        throw script::ScriptException("expects a data address");

	    unsigned int addr = (unsigned int)vm->toNumber(1);
	    vm->pushString( program.dataName(addr) );
	    return 1;
	}

	int ScriptProgram::script_functionAddress(const char *funcName) {
	    if ( vm->top() != 1 || vm->getType(1) != VM::String )
	        throw script::ScriptException("expects a function name");

	    const char *name = vm->toString(1);
	    int addr = program.functionAddress(name);
	    if( addr == -1 )
	    	throw script::ScriptException("Unable to find function address");

	    vm->pushNumber( addr );
	    return 1;
	}

	int ScriptProgram::script_dataAddress(const char *funcName) {
	    if ( vm->top() != 1 || vm->getType(1) != VM::String )
	        throw script::ScriptException("expects a data name");

	    const char *name = vm->toString(1);
	    int addr = program.dataAddress(name);
	    if( addr == -1 )
	    	throw script::ScriptException("Unable to find data address");

	    vm->pushNumber( addr );
	    return 1;
	}

	ScriptEngine::ScriptEngine() : program(0) {
#ifdef DEBUG
		std::cout << "Booting virtual machine" << std::endl;
#endif
	}

	ScriptEngine::~ScriptEngine() {
#ifdef DEBUG
		std::cout << "Unloading virtual machine" << std::endl;
#endif

		if( program != 0 )
			delete program;
	}

	void ScriptEngine::setProgram(const Program & prog) {
		program = new ScriptProgram( this, prog );
	}

	void ScriptEngine::loadConfig(const char *filename) {
		try {
			compileFile(filename);
		} catch( script::ScriptException & ex ) {
			throw ScriptException( ex.message() );
		}
	}

}
