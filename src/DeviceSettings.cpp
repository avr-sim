/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DeviceSettings.h"
#include "Format.h"
#include "Device.h"
#include "Util.h"

#ifdef _WIN32
#   include <io.h>
#   include <direct.h>
#   define WIN32_LEAN_AND_MEAN
#   include <windows.h>
#else
#       include <unistd.h>
#       include <dirent.h>
#endif

#include <iostream>
#include <cstring>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/debugXML.h>

#define SETTINGS_DIR "./devices/"

#ifdef _WIN32
const char separatorChar = '\\';
#else
const char separatorChar = '/';
#endif

/*
 * The amount of separating white space.
 */
#define SPACING 3

/*
 * The longest possible name of a device.
 */
#define MAX_DEVICE_LENGTH 14

namespace avr {

	void DeviceSettings::listAll(unsigned int lineLength /*= 80*/) {
		 unsigned int cols = lineLength / (MAX_DEVICE_LENGTH + SPACING);

		 int count = 0;

#ifdef _WIN32
		std::string filter = SETTINGS_DIR;
		if( filter[filter.length() - 1] != separatorChar )
		filter += File::separatorChar;
		filter += '*';

		_finddata_t fileinfo;
		long h = _findfirst(filter.c_str(), &fileinfo);
		if( h != -1 ) {
			while( true ) {
				const char *name = fileinfo.name;
#else
		DIR *dir = opendir(SETTINGS_DIR);
		if (dir == 0)
			return;

		struct dirent *dp;
		while ( (dp = readdir(dir)) != 0) {
			const char *name = dp->d_name;
#endif

				if( (strcmp(name,".") != 0) && (strcmp(name,"..") != 0) ) {
					int col = count++ % cols;
					if( col == 0 ) std::cout << std::endl;

					int nameLength = strlen(name);
					std::cout << name;

					for(int ws = nameLength; ws < MAX_DEVICE_LENGTH; ++ws)
						std::cout << ' ';
				}

#ifdef _WIN32
				if( _findnext(h,&fileinfo) != 0 )
				break;
			}
			_findclose(h);
		}
#else
		}
		closedir(dir);
#endif

		std::cout << std::endl;
	}

	class XmlHandle {
		public:
			XmlHandle(const char *filename) {
				/*
				 * this initialize the library and check potential ABI mismatches
				 * between the version it was compiled for and the actual shared
				 * library used.
				 */
				LIBXML_TEST_VERSION

				int flags =	XML_PARSE_DTDATTR | /* default DTD attributes */
							XML_PARSE_NOENT | /* substitute entities */
							XML_PARSE_DTDVALID; /* validate with the DTD */

				/*parse the file and get the DOM */
				doc = xmlReadFile(filename, NULL, flags);

				if (doc == 0)
					throw ParseException( util::format("Unable to open %s") % filename );
			}

			~XmlHandle() {
				/*free the document */
				xmlFreeDoc(doc);

				/*
				 * Cleanup function for the XML library.
				 */
				xmlCleanupParser();
			}

			operator xmlDocPtr() {
				return doc;
			}

		private:
			xmlDocPtr doc;
	};

	bool hasAttribute(xmlNode *node, const char *name) {
		return xmlGetNoNsProp(node, reinterpret_cast<const xmlChar*>(name)) != 0;
	}

	char *getAttribute(xmlNode *node, const char *name, bool required = false) {
		char *result = reinterpret_cast<char*>(xmlGetNoNsProp(
				node, reinterpret_cast<const xmlChar*>(name)));
		if( required && (result == 0) )
			throw ParseException(
					util::format("Expected an attribute %s on node %s") % name % node->name );

		return result;
	}

	int getIntAttribute(xmlNode *node, const char *name, int base = 10, bool required = false) {
		xmlChar *result = xmlGetNoNsProp(node,
				reinterpret_cast<const xmlChar*>(name));

		int ret = 0;
		if( result != 0 ) {
			char *str = reinterpret_cast<char*>(result);
			if( str[0] == '$' ) { base = 16; str++; }// Switch base
			ret = strtoul( str, 0, base);
			xmlFree( result );
		} else if( required ) {
			throw ParseException(
					util::format("Expected an attribute %s on node %s") % name % node->name );
		}

		return ret;
	}

	int getIntAttributeDef(xmlNode *node, const char *name, int def = 0, int base = 10) {
		xmlChar *result = xmlGetNoNsProp(node,
				reinterpret_cast<const xmlChar*>(name));

		int ret = 0;
		if( result != 0 ) {
			ret = strtoul( reinterpret_cast<char*>(result), 0, base);
			xmlFree( result );
		} else {
			ret = def;
		}

		return ret;
	}

	void freeAttribute(char *attr) {
		xmlFree( reinterpret_cast<xmlChar*>(attr) );
	}

	bool compare(const xmlChar *a, const char *b) {
		return strcmp( reinterpret_cast<const char*>(a), b ) == 0;
	}

	HardwareSettings::HardwareSettings(XmlHandle & doc, _xmlNode *node)
		: doc(doc), node(node) {

		params = 0;
		reg = 0;
		for(xmlNode *n = node->children; n != 0; n = n->next) {
			if( n->type == XML_ELEMENT_NODE ) {
				if( (params == 0) && compare(n->name, "params") )
					params = n;
				else if( (reg == 0) && compare(n->name, "registers") )
					reg = n->children;
			}
		}

		while( (reg != 0) && (reg->type != XML_ELEMENT_NODE) )
			reg = reg->next;
	}

	_xmlNode *HardwareSettings::findParam(const char *name, bool required) const {
		for(xmlNode *n = params->children; n != 0; n = n->next) {
			if( n->type == XML_ELEMENT_NODE ) {
				xmlChar *result = xmlGetNoNsProp(n,
							reinterpret_cast<const xmlChar*>("name"));
				if( compare(result, name) ) {
					xmlFree(result);
					return n;
				}

				xmlFree(result);
			}
		}

		if( required ) {
			char *hwname = getAttribute(node, "class", true);
			std::string className = hwname;
			freeAttribute(hwname);

			throw ParseException(
				util::format("Expected parameter %s on module %s") % name % className );
		}

		return 0;
	}

	int HardwareSettings::intParam(const char *name, int base /*= 10*/) const {
		xmlNode *node = findParam(name);
		return getIntAttribute(node, "value", base, true);
	}

	int HardwareSettings::intParamDef(const char *name,
				int def /*=0*/, int base /*= 10*/) const {

		xmlNode *node = findParam(name, false);
		if( node == 0 )
			return def;

		return getIntAttributeDef(node, "value", def, base);
	}

	std::string HardwareSettings::strParam(const char *name) const {
		xmlNode *node = findParam(name);
		char *s = getAttribute(node, "value", true);
		std::string str = s;
		freeAttribute(s);
		return str;
	}

	bool HardwareSettings::getBinding(std::string & name, std::string & binding) {
		if( reg == 0 )
			return false;

		char *s;
		s = getAttribute(reg, "name", true);
		name = s;
		freeAttribute(s);

		s = getAttribute(reg, "bind", true);
		binding = s;
		freeAttribute(s);

		do {
			reg = reg->next;
		} while( (reg != 0) && (reg->type != XML_ELEMENT_NODE) );
		return true;
	}

	void DeviceSettings::parseMemory(Device *dev, xmlNode *memory) {
		unsigned int ioSpaceSize = 0;
		unsigned int flashSize = 0;
		unsigned int pageSize = 0;
		unsigned int sramSize = 0;
		unsigned int pcSize = 2;
		unsigned int stackMask = 0xffff;

		for(xmlNode *node = memory->children; node != 0; node = node->next) {
			if( node->type == XML_ELEMENT_NODE ) {
				if( compare(node->name, "stack") ) {
					stackMask = getIntAttribute(node, "mask", 16, true);
				}

				unsigned int *size = 0;
				if( compare(node->name, "flash") ) {
					size = &flashSize;
					pageSize = getIntAttributeDef(node, "page", 128, 10);
				} else if( compare(node->name, "sram") )
					size = &sramSize;
				else if( compare(node->name, "pc") )
					size = &pcSize;
				else if( compare(node->name, "iospace") ) {
					if( ! hasAttribute(node, "size") ) {
						int start = getIntAttribute(node, "start", 16, true);
						int stop = getIntAttribute(node, "stop", 16, true);
						ioSpaceSize = stop - start + 1;
					} else {
						size = &ioSpaceSize;
					}
				}

				if( size != 0 )
					*size = getIntAttribute(node, "size", 10, true);
			}
		}

#ifdef DEBUG
		std::cout << "ioSpaceSize = " << ioSpaceSize << std::endl;
		std::cout << "flashSize = " << flashSize << std::endl;
		std::cout << "pageSize = " << pageSize << std::endl;
		std::cout << "sramSize = " << sramSize << std::endl;
		std::cout << "pcSize = " << pcSize << std::endl;
		std::cout << "stackMask = " << stackMask << std::endl;
#endif

		dev->buildCore(ioSpaceSize, sramSize, flashSize, pageSize, stackMask, pcSize);
	}

	void DeviceSettings::parseRegisters(Device *dev, xmlNode *registers) {
		for(xmlNode *node = registers->children; node != 0; node = node->next) {
			if( node->type == XML_ELEMENT_NODE ) {
				char *n = getAttribute(node, "name", true);
				unsigned int address = getIntAttribute(node, "address", 16, true);
				unsigned char initial = getIntAttributeDef(node, "initial", 0, 16);
				dev->addIOReg(address, std::string(n), initial);
#ifdef DEBUG
				std::cout << "Adding register "<< n
						  << " at address "<< std::hex << address << std::dec
						  << ", initial value = "
						  	<< std::hex << (int)initial << std::dec
						  << std::endl;
#endif

				freeAttribute(n);
			}
		}
	}

	void DeviceSettings::parseInterrupts(Device *dev, xmlNode *interrupts) {
		for(xmlNode *node = interrupts->children; node != 0; node = node->next) {
			if( node->type == XML_ELEMENT_NODE ) {
				unsigned int vector = getIntAttribute(node, "vector", 10, true);
				unsigned int address = getIntAttribute(node, "address", 16, true);
				char *name = getAttribute(node, "name", true);

				dev->addInterrupt(vector - 1, address, name);
#ifdef DEBUG
				std::cout << "Adding interrupt "<< vector
						  << " at address "<< std::hex << address << std::dec
						  << ", name = " << name
						  << std::endl;
#endif

				freeAttribute(name);
			}
		}
	}

	void DeviceSettings::parseHardware(Device *dev, XmlHandle & doc, xmlNode *hardware) {
		for(xmlNode *node = hardware->children; node != 0; node = node->next) {
			if( node->type == XML_ELEMENT_NODE ) {
				char *hwname = getAttribute(node, "class", true);
				HardwareSettings hws( doc, node );
				dev->buildHardware( hwname, hws );
				freeAttribute(hwname);
			}
		}
	}

	void DeviceSettings::parsePackage(Device *dev, xmlNode *package) {
		unsigned int numPins = getIntAttribute(package, "pins", 10, true);
		for(xmlNode *node = package->children; node != 0; node = node->next) {
			if( node->type == XML_ELEMENT_NODE ) {
				char *n = getAttribute(node, "name", true);
				unsigned int id = getIntAttribute(node, "id", 10, true);
				if( id > numPins )
					throw ParseException(
						util::format("Pin %d (%s) goes beyond number of pins (%d)")
							% id % n % numPins );

				if( dev->getPin(id) != 0 )
					throw ParseException(
						util::format("Pin %d (%s) already exists") % id % n );

				dev->addPin(id, n, numPins);
				freeAttribute(n);
			}
		}
	}

	void DeviceSettings::parsePackages(Device *dev, const char *pkgname, xmlNode *packages) {
		bool found = false;
		for(xmlNode *node = packages->children; node != 0 && ! found; node = node->next) {
			if( node->type == XML_ELEMENT_NODE ) {
				if( pkgname == 0 ) { // Just go the first one
					found = true;
				} else {
					char *name = getAttribute(node, "name", true);
					found = strcasecmp(name, pkgname) == 0;
					freeAttribute(name);
				}

				if( found )
					parsePackage(dev, node);
			}
		}

		if( ! found )
			throw ParseException(
				util::format("Requested package %s not found") % pkgname );
	}

	void DeviceSettings::load(Device *dev, const char *devicename) {
		const char *package = strchr(devicename, ':');
		if( package != 0 )
			package++;

		std::string filename( SETTINGS_DIR);
		if( package != 0 )
			filename += std::string(devicename, package - devicename - 1);
		else
			filename += devicename;

		XmlHandle doc(filename.c_str() );

		//xmlDebugDumpDocument(stdout, doc);

		/*Get the root element node */
		xmlNode *root = xmlDocGetRootElement(doc);
		for(xmlNode *node = root->children; node != 0; node = node->next) {
			if( node->type == XML_ELEMENT_NODE ) {
				if( compare(node->name, "memory") )
					parseMemory(dev, node);

				if( compare(node->name, "ioregisters") )
					parseRegisters(dev, node);

				if( compare(node->name, "interrupts") )
					parseInterrupts(dev, node);

				if( compare(node->name, "hardware") )
					parseHardware(dev, doc, node);

				if( compare(node->name, "packages") )
					parsePackages(dev, package, node);
			}
		}
	}
}
