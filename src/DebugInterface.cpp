/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DebugInterface.h"
#include "Core.h"
#include "Device.h"
#include "Instruction.h"
#include "AccessViolation.h"
#include "ImplementationException.h"

#include <algorithm>

#define mmu core.mmu

namespace avr {
	DebugInterface::DebugInterface(Device & dev, Core & core)
		: dev(dev), core(core), breaked(false) {
		core.setDebugInterface(this);
	}

	DebugInterface::~DebugInterface() {
		core.setDebugInterface(0);
	}

	byte DebugInterface::reg(int reg) const {
		return core.readRegister(reg);
	}

	byte DebugInterface::status() const {
		return core.readStatus();
	}

	word DebugInterface::stackPointer() const {
		return core.stack.getSP();
	}

	dword DebugInterface::programCounter() const {
		return (core.PC << 1);
	}

	bool DebugInterface::isRegister(unsigned int offset) const {
		return mmu.isRegister(offset);
	}

	byte DebugInterface::readRegister(unsigned int offset) const {
		return mmu.readByte(offset);
	}

	const unsigned char *DebugInterface::readRam(
			unsigned int offset, unsigned int size) const {

		return mmu.readRam(offset, size);
	}

	const unsigned char *DebugInterface::readFlash(
			unsigned int offset, unsigned int size) const {

		return core.flash.read( offset, size );
	}

	void DebugInterface::writeRam(unsigned char *data,
			unsigned int offset, unsigned int size) {

		mmu.writeRam(data, offset, size);
	}

	void DebugInterface::writeFlash(unsigned char *data,
			unsigned int offset, unsigned int size) {
		core.loadFlash(data, offset, size);
	}

	const unsigned char *DebugInterface::readEeprom(
			unsigned int /*offset*/, unsigned int /*size*/) const {
		throw util::ImplementationException( "readEeprom: not yet implemented" );
	}

	void DebugInterface::writeEeprom(unsigned char * /*data*/,
			unsigned int /*offset*/, unsigned int /*size*/) {
		throw util::ImplementationException( "writeEeprom: not yet implemented" );
	}

	void DebugInterface::setReg(int reg, byte val) {
		core.writeRegister(reg, val);
	}

	void DebugInterface::setStatus(byte val) {
		core.writeStatus( val );
	}

	void DebugInterface::setStackPointer(word val) {
		core.stack.setSP(val);
	}

	void DebugInterface::setProgramCounter(dword val) {
		core.PC = val >> 1;
	}

	void DebugInterface::insertBreak(dword addr) {
		breakPoints.push_back( addr );
	}

	void DebugInterface::removeBreak(dword addr) {
		std::remove(breakPoints.begin(), breakPoints.end(), addr);
	}

	void DebugInterface::deleteAllBreakpoints() {
		breakPoints.erase(breakPoints.begin(), breakPoints.end());
	}

	bool DebugInterface::checkBreak(dword addr) {
		if( breakPoints.end() != std::find(breakPoints.begin(), breakPoints.end(), addr)) {
			if( ! breaked ) {
				breaked = true;
			} else {
				breaked = false;
			}
		} else {
			breaked = false;
		}

		return breaked;
	}

	bool DebugInterface::stepDone() const {
		return (core.cpuCycles == 0);
	}

	const std::string & DebugInterface::registerName(byte addr) const {
		return mmu.registerName(addr);
	}

	void DebugInterface::trace(std::ostream & ostr, dword addr) {
		word opcode = core.flash.readWord( addr );
		Instruction & instr = core.decoder.decode(opcode);
		instr.trace(&core, ostr);
	}

}
