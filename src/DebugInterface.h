#ifndef AVR_DEBUGINTERFACE_H
#define AVR_DEBUGINTERFACE_H

#include "Types.h"
#include "Device.h"
#include <vector>
#include <iostream>

namespace avr {

	class Core;

	/**
	 * @author Tom Haber
	 * @date Apr 29, 2008
	 * @brief Interface for debugging
	 *
	 * This class is used for debugging and tracing functionality.
	 * It has access to some protected parts of the core and can
	 * provide more detailed state information for external use.
	 */
	class DebugInterface {
		public:
			DebugInterface(Device & dev, Core & core);
			~DebugInterface();

		public:
			byte reg(int reg) const;
			byte status() const;
			word stackPointer() const;
			dword programCounter() const;

		public:
			void setReg(int reg, byte val);
			void setStatus(byte val);
			void setStackPointer(word val);
			void setProgramCounter(dword val);

		public:
			void reset();
			void step();
			bool stepDone() const;

		public:
			bool checkBreak(dword addr);
			bool hasBreaked() const { return breaked; }
			void insertBreak(dword addr);
			void removeBreak(dword addr);
			void deleteAllBreakpoints();

		public:
			const std::string & registerName(byte addr) const;
			void trace(std::ostream & ostr, dword addr);

		public:
			bool isRegister(unsigned int offset) const;
			byte readRegister(unsigned int offset) const;
			const unsigned char *readRam(unsigned int offset, unsigned int size) const;
			void writeRam(unsigned char *data, unsigned int offset, unsigned int size);
			const unsigned char *readFlash(unsigned int offset, unsigned int size) const;
			void writeFlash(unsigned char *data, unsigned int offset, unsigned int size);
			const unsigned char *readEeprom(unsigned int offset, unsigned int size) const;
			void writeEeprom(unsigned char *data, unsigned int offset, unsigned int size);

		private:
			Device & dev;
			Core & core;

		private:
			typedef std::vector<dword> Breakpoints;
			Breakpoints breakPoints;
			bool breaked;
	};

	inline void DebugInterface::step() {
		dev.step();
	}

	inline void DebugInterface::reset() {
		dev.reset( SIM_RESET );
	}

}

#endif /*AVR_DEBUGINTERFACE_H*/
