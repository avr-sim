/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Program.h"
#include <iostream>
#include <algorithm>

namespace avr {

	const std::string Program::empty;

	void Program::addSymbolFlash(int addr, const char *name) {
#ifdef DEBUG
		std::cout << "Flash Symbol: " << name << " (" << std::hex << addr << std::dec << ")" << std::endl;
#endif
		flashSymbols.insert( std::make_pair(addr, name) );
	}

	void Program::addSymbolEeprom(int addr, const char *name) {
#ifdef DEBUG
		std::cout << "Eeprom Symbol: " << name << " (" << std::hex << addr << std::dec << ")" << std::endl;
#endif
	}

	void Program::addSymbolRam(int addr, const char *name) {
#ifdef DEBUG
		std::cout << "Ram Symbol: " << name << " (" << std::hex << addr << std::dec << ")" << std::endl;
#endif
		dataSymbols.insert( std::make_pair(addr, name) );
	}

	const std::string & Program::functionName(int addr) const {
		SymTable::const_iterator it = flashSymbols.upper_bound( addr );
		if( it == flashSymbols.end() )
			return empty;

		if( it->first != addr )
			it--;

		return it->second;
	}

	class SymbolFinder {
		public:
			SymbolFinder(const std::string & name) : name(name) {}

		public:
			bool operator() (const std::pair<int, std::string> & p) {
				return p.second == name;
			}

		private:
			const std::string & name;
	};

	int Program::functionAddress(const std::string & name) const {
		SymTable::const_iterator it =
			std::find_if(flashSymbols.begin(), flashSymbols.end(), SymbolFinder(name) );

		if( it == flashSymbols.end() )
			return -1;

		return it->first;
	}

	int Program::dataAddress(const std::string & name) const {
		SymTable::const_iterator it =
			std::find_if(dataSymbols.begin(), dataSymbols.end(), SymbolFinder(name) );

		if( it == dataSymbols.end() )
			return -1;

		return it->first;
	}

	const std::string & Program::dataName(int addr) const {
		SymTable::const_iterator it = dataSymbols.upper_bound( addr );
		if( it == dataSymbols.end() )
			return empty;

		if( it->first != addr )
			it--;

		return it->second;
	}

}
