#ifndef AVR_SCRIPTENGINE_H
#define AVR_SCRIPTENGINE_H

#include "Exception.h"
#include "script/VM.h"
#include "script/Scriptable.h"
#include "script/ScriptUtil.h"

namespace avr {

	class Program;

	//! Thrown if the application or script performs invalid script operation.
	class ScriptException : public util::Exception {
		public:
			ScriptException() : Exception(msg) {}
			ScriptException(const std::string & msg) : Exception(msg) {}
			ScriptException(const char *msg) : Exception(msg) {}
	};

	/**
	 * @author Tom Haber
	 * @date May 17, 2008
	 * @brief Representation of a program in the scripting environment
	 *
	 * Representation of a program in the scripting environment.
	 * Provides two methods for getting the symbols in flash
	 * and ram (data).
	 */
	class ScriptProgram : public script::Scriptable {
		public:
			ScriptProgram(script::VM *vm, const Program & program);
			~ScriptProgram();

		public:
	        /**
	         * When a (C++) method is called from a script, this function
	         * is executed and unique method identifier is passed as parameter.
	         * Derived classes must override this if they add new scriptable methods.
	         * @param vm Script virtual machine executing the method.
	         * @param i Unique identifier (index) of the called method.
	         * @return Number of arguments returned in the script stack.
	         */
	        int methodCall(int i);

		private:
			const Program & program;

			// scripting
			int methodBase;
			static script::ScriptMethod<ScriptProgram> sm_methods[];

		private:
			int script_functionName(const char *funcName);
			int script_dataName(const char *funcName);
			int script_functionAddress(const char *funcName);
			int script_dataAddress(const char *funcName);
	};

	/**
	 * @author Tom Haber
	 * @date May 17, 2008
	 * @brief The scripting environment
	 *
	 * Representation the scripting environment in avr-sim.
	 * Contains the scripting representations of some core classes.
	 */
	class ScriptEngine : public script::VM {
		public:
			ScriptEngine();
			~ScriptEngine();

		public:
			void setProgram(const Program & program);
			void loadConfig(const char *filename);

		private:
			ScriptProgram *program;
	};

}

#endif /*SCRIPTENGINE_H_*/
