/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <iostream>
#include <getopt.h>
#include <errno.h>
#include <cstring>

#ifdef _POSIX_SOURCE
#	include <signal.h>
#	include <setjmp.h>

#	define TIOCGWINSZ
#	include <sys/ioctl.h>
#endif

#include "VerboseInfo.h"
#include "SimulationClock.h"
#include "GdbServer.h"

#include "BfdProgram.h"
#include "HexProgram.h"

#ifdef ENABLE_SCRIPTING
#include "ScriptEngine.h"
#endif

#include "Trace.h"
#include "Device.h"
#include "DeviceSettings.h"

using namespace avr;

#define AVR_SIM_VERSION "0.1"

enum ProgramType {
#ifdef HAVE_LIBBFD
	PROG_BFD,
#endif
	PROG_HEX,
	PROG_NONE
};

ProgramType defaultProgramType =
#ifdef HAVE_LIBBFD
	PROG_BFD; // Default Bfd
#else
	PROG_HEX; // Default Hex
#endif

static void list_supported_devices() {
	unsigned int lineLength = 80;

	{
		const char *p = getenv("COLUMNS");
		if( (p != 0) && (*p != '\0') ) {
			errno = 0;
			unsigned long int tmp_ulong = strtoul(p, NULL, 10);

			if( errno == 0 )
				lineLength = tmp_ulong;
		}
	}

#ifdef TIOCGWINSZ
	{
		struct winsize ws;

		if( (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) != -1)
				&& (0 < ws.ws_col) && (ws.ws_col == (size_t) ws.ws_col) )
		  lineLength = ws.ws_col;
	}
#endif

	DeviceSettings::listAll(lineLength);
}

static void print_usage() {
	std::cout << "AVR-Simulator"<< std::endl
	        << "-u                           run with user interface for external pin handling at port 7777\n"
	        << "-f --file <name>             load file <name> for simulation in simulated target\n"
#ifdef HAVE_LIBBFD
	        << "-b --bfd-file                specified filename is a bfd file (elf,coff)\n"
#endif
	        << "-x --hex-file                specified filename is a hex file\n"
	        << "-d --device <device name>    simulate <device name>\n"
	        << "-a --analyzer <name>         add analyzer <name> (scripts are added using script:<script>)\n"
	        << "-s --scriptconfig <filename> config file for scripts\n"
	        << "-g --gdbserver               running as gdb-server\n"
	        << "-p  <port>                   change <port> for gdb server to port\n"
	        << "-t --trace <file name>       enable trace outputs into <file name>\n"
	        << "-l --listtrace <file name>   list trace outputs from <file name>\n"
	        << "-F --cpufrequence            set the cpu frequence to <Hz>\n"
	        << "-c --clockticks	             set the number of clocks ticks to execute\n"
	        << "-v --verbose                 output some hints to console\n"
	        << "-h --help                    outputs this\n"
	        << std::endl;

	std::cout << "Supported devices:"<< std::endl;
	list_supported_devices();
	std::cout << std::endl;

#ifdef HAVE_LIBBFD
	std::cout << "Bfd file loading is enabled" << std::endl;
#endif
#if defined(HAVE_LUA) && defined(ENABLE_SCRIPTING)
	std::cout << "Lua scripting is enabled" << std::endl;
#endif

	exit(0);
}

static sim::SimulationClock simclock;

#ifdef _POSIX_SOURCE
static void signal_handler(int /*sig*/) {
	simclock.finish();
}

static void enable_signal_handler(int sig = SIGINT) {
	struct sigaction action;
	action.sa_flags = 0;
	action.sa_handler = signal_handler;
	sigemptyset( &action.sa_mask );

	sigaction( sig, &action, 0 );
}
#endif

#ifdef ENABLE_SCRIPTING
struct IsScript {
	bool operator ()(const char *str) {
		const char *scriptPrefix = "script:";
		return strncmp( str, scriptPrefix, strlen(scriptPrefix) ) == 0;
	}
};

static bool HaveScripts(const std::list<const char *> & analyzers) {
	return ( std::find_if(analyzers.begin(), analyzers.end(), IsScript()) != analyzers.end() );
}
#endif

static void addAnalyzers(Device & dev, const std::list<const char *> & names) {
		std::list<const char *>::const_iterator it;
		for(it = names.begin(); it != names.end(); ++it)
		   dev.addAnalyzer( *it );
}

int main(int argc, char *argv[]) {
	const char *devicename = "atmega162";
	const char *filename = 0;
	const char *listtrace = 0;
	const char *tracefile = 0;
	unsigned long long fcpu = 0;
	bool gdbserver = false;
	int gdbserver_port = 1234;
	sim::ClockOffset clockTicks = 0;
	ProgramType programType = PROG_NONE;

	std::list<const char *> analyzers;
	const char *scriptconfig = 0;

	static struct option long_options[] = {
		{ "file", 1, 0, 'f' },
#ifdef HAVE_LIBBFD
		{ "bfd-file", 0, 0, 'b' },
#endif
		{ "hex-file", 0, 0, 'x' },
		{ "device", 1, 0, 'd' },
		{ "analyzer", 1, 0, 'a' },
		{ "scriptconfig", 1, 0, 's' },
		{ "gdb", 0, 0, 'g' },
		{ "gdbserver", 0, 0, 'g' },
		{ "gdbport", 1, 0, 'p' },
		{ "trace", 1, 0, 't' },
		{ "listtrace", 1, 0, 'l' },
		{ "version", 0, 0, 'V' },
		{ "cpufrequency", 1, 0, 'F' },
		{ "clockticks", 1, 0, 'c' },
		{ "verbose", 0, 0, 'v' },
		{ "help", 0, 0, 'h' },
		{ 0, 0, 0, 0 }
	};

	int c;
	while ( (c = getopt_long (argc, argv, "vd:F:f:xba:s:gp:t:l:c:Vh",
	        long_options, 0)) != -1) {

		switch (c ) {
			case 'v':
				info.increaseVerbosity();
				break;

			case 'd':
				devicename = optarg;
				info(INFO) << "Device to simulate "
							<< devicename << std::endl;
				break;

			case 'F':
				fcpu = strtoll(optarg, NULL, 10);
				info(INFO)  << "Running with CPU frequency: "<< fcpu
					        << std::endl;
				break;

			case 'f':
				filename = optarg;
				info(INFO) << "File to load "<< filename << std::endl;
				break;

			case 'x':
				if( programType != PROG_NONE ) {
					std::cerr << "Multiple file types specified!" << std::endl;
					exit(1);
				}

				programType = PROG_HEX;
				info(INFO) << "Program file type is hex" << std::endl;
				break;

#ifdef HAVE_LIBBFD
			case 'b':
				if( programType != PROG_NONE ) {
					std::cerr << "Multiple file types specified!" << std::endl;
					exit(1);
				}

				programType = PROG_BFD;
				info(INFO) << "Program file type is bfd" << std::endl;
				break;
#endif

			case 'a':
				analyzers.push_back( optarg );
				info(INFO) << "Adding analyzer "<< optarg << std::endl;
				break;

			case 's':
				scriptconfig = optarg;
				info(INFO) << "Using configfile "<< scriptconfig << std::endl;
				break;

			case 'g':
				gdbserver = true;
				info(INFO) << "Running as gdb-server"<< std::endl;
				break;

			case 'p':
				gdbserver_port = atoi(optarg);
				info(INFO) << "Running gdb-server on port: "
					        << gdbserver_port << std::endl;
				break;

			case 't':
				tracefile = optarg;
				info(INFO) << "Running in Trace Mode"<< std::endl;
				break;

			case 'l':
				listtrace = optarg;
				info(INFO) << "Listing Trace"<< std::endl;
				break;

			case 'c':
				clockTicks = strtoll(optarg, NULL, 10);
				info(INFO) << "Ending executing after "<< clockTicks
							<< " clock ticks" << std::endl;
				break;

			case 'V': {
					std::cout << "Simulavr++ "<< AVR_SIM_VERSION << std::endl;
					std::cout
					        << "See documentation for copyright and distribution terms"
					        << std::endl;
					std::cout << std::endl;
					exit(0);
				}
				break;

			default:
				print_usage();
		}
	}

#ifdef ENABLE_SCRIPTING
	ScriptEngine *vm = 0;
	if( HaveScripts(analyzers) /*|| scripted peripherals */ )
		vm = new ScriptEngine();
#endif

	if( programType == PROG_NONE )
		programType = defaultProgramType;

	Program *program = 0;
	if( programType == PROG_HEX )
		program = new HexProgram();
#ifdef HAVE_LIBBFD
	else if( programType == PROG_BFD )
		program = new BfdProgram();
#endif

	int ret = EXIT_SUCCESS;
	try {
		//SimulationClock simclock; this is global now for signal_handlers
		Device dev( simclock, devicename );

#ifdef _POSIX_SOURCE
		if( ! gdbserver )
			enable_signal_handler(SIGINT);
#endif

#ifdef ENABLE_SCRIPTING
		if( vm != 0 )
			vm->setProgram( *program );
#endif

		if( filename != 0 ) {
			program->load( filename );
			dev.load( *program );
		}

		if( tracefile != 0 )
			dev.trace( tracefile );

		if( clockTicks != 0 )
			simclock.setMaxTicks( clockTicks );

		if( fcpu != 0 )
			dev.setClockFrequency( fcpu );

		if( analyzers.size() != 0 )
			addAnalyzers( dev, analyzers );

		if( scriptconfig != 0 ) {
#ifdef ENABLE_SCRIPTING
			if( vm != 0 ) {
				vm->loadConfig( scriptconfig );
			} else
				std::cerr << "Warning: scriptconfig specified but no VM" << std::endl;
#else
			std::cerr << "Warning: scriptconfig specified but scripting disabled" << std::endl;
#endif

		}

		if( listtrace != 0 ) {
			Trace tracer;
			tracer.list( listtrace, std::cout, dev);
		} else if( gdbserver ) {
			GdbServer gdb( simclock, gdbserver_port );
			gdb.add( &dev );
			gdb.exec();
		} else {
			simclock.exec();
		}

	} catch( util::Exception & ex ) {
		std::cerr << ex.message() << std::endl;
		ex.printStackTrace( std::cerr );
		ret = EXIT_FAILURE;
	} catch( std::exception & ex ) {
		std::cerr << ex.what() << std::endl;
		ret = EXIT_FAILURE;
	} catch( ... ) {
		std::cerr << "Caught unknown exception" << std::endl;
		ret = EXIT_FAILURE;
	}

	if( program != 0 )
		delete program;

#ifdef ENABLE_SCRIPTING
	if( vm != 0 )
		delete vm;
#endif

	info(INFO) << "Simulation ran for " << simclock.ticks()
					<< " clock ticks" << std::endl;

	return ret;
}
