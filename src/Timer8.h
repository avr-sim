#ifndef AVR_TIMER8_H
#define AVR_TIMER8_H

#include "Types.h"
#include "Hardware.h"

#define MAX_OUTPUTCOMPARE8_UNITS 3

namespace avr {

	/**
	 * @author Tom Haber
	 * @date May 29, 2008
	 * @brief 8-bit Output Compare Unit
	 *
	 * 8-bit Output Compare Unit
	 */
	class OutputCompareUnit8 {
		public:
			OutputCompareUnit8() : ocfmask(0), ocr(0) {}

		public:
			void setOcrRegister(IORegister *ocr) { this->ocr = ocr; }
			bool ocrSet() const { return ocr != 0; }
			void setMask(unsigned char ocfmask) { this->ocfmask = ocfmask; }
			void setCompareMode(unsigned char cm) { this->compareMode = cm; }
			void regChanged(IORegister *reg);

		public:
			bool compare(unsigned char tcnt) const { return tcnt == ocrBuf; }
			void flush();
			unsigned char value() const { return ocrBuf; }
			void forceOutputCompare(IORegister *tifr, unsigned char tcnt);
			void compareMatchOutput(IORegister *tifr, unsigned char tcnt);

		private:
			// For double buffering ocr register
			unsigned char ocrBuf;
			unsigned char compareMode;
			unsigned char ocfmask;

		private:
			IORegister *ocr;
	};

	/**
	 * @author Tom Haber
	 * @date May 2, 2008
	 * @brief 8-bit timers
	 *
	 * 8-bit timers
	 */
	class Timer8 : public Hardware {
		public:
			Timer8(Bus & bus, unsigned char tovmask, int compunits,
					unsigned char ocfmask[]);

		public:
			bool attachReg(const char *name, IORegister *reg);
			bool finishBuild();
			void regChanged( IORegister *reg );
			void step();

		private:
			void setClock(unsigned char tccr);
			void flushOCRn();

		private:
			unsigned char tovmask;
			int direction;

			/* pg. 93 of manual. Block compareMatch for one period after
		     * TCNTn is written to. */
		    bool blockCompareMatch;

			unsigned char timerMode;
			int period;

		private:
			IORegister *tcnt;
			IORegister *tccr;
			IORegister *tifr;

			int nUnits;
			OutputCompareUnit8 units[MAX_OUTPUTCOMPARE8_UNITS];
	};

}

#endif /*AVR_TIMER8_H*/
