/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "TimerInterrupts.h"
#include "Registers.h"
#include "Bus.h"
#include "ImplementationException.h"

#include <cstring>

namespace avr {

	TimerInterrupts::TimerInterrupts(Bus & bus, unsigned char mask,
								unsigned int bit0Vec, unsigned int bit1Vec,
								unsigned int bit2Vec, unsigned int bit3Vec,
								unsigned int bit4Vec, unsigned int bit5Vec,
								unsigned int bit6Vec, unsigned int bit7Vec)
		: Hardware(bus), mask(mask),
			bit0Vec(bit0Vec), bit1Vec(bit1Vec), bit2Vec(bit2Vec), bit3Vec(bit3Vec),
			bit4Vec(bit4Vec), bit5Vec(bit5Vec), bit6Vec(bit6Vec), bit7Vec(bit7Vec) {

		unsigned int vec[] = {
			bit0Vec,
			bit1Vec,
			bit2Vec,
			bit3Vec,
			bit4Vec,
			bit5Vec,
			bit6Vec,
			bit7Vec
		};

		for(int i = 0; i < 8; ++i) {
			if( (mask & (1<<i)) != 0 )
				bus.claimInterrupt(vec[i], this);
		}
	}

	bool TimerInterrupts::attachReg(const char *name, IORegister *reg) {
		if( strcmp(name, "timsk") == 0 )
			timsk = reg;
		else if( strcmp(name, "tifr") == 0 )
			tifr = reg;
		else
			return false;

		reg->registerHW(this);
		return true;
	}

	bool TimerInterrupts::finishBuild() {
		return (timsk != 0) && (tifr != 0);
	}

	void TimerInterrupts::beforeInvokeInterrupt(unsigned int vector) {
		if( vector == bit0Vec ) tifrOld &= ~(1<<0);
		else if( vector == bit0Vec ) tifrOld &= ~(1<<0);
		else if( vector == bit1Vec ) tifrOld &= ~(1<<1);
		else if( vector == bit2Vec ) tifrOld &= ~(1<<2);
		else if( vector == bit3Vec ) tifrOld &= ~(1<<3);
		else if( vector == bit4Vec ) tifrOld &= ~(1<<4);
		else if( vector == bit4Vec ) tifrOld &= ~(1<<5);
		else if( vector == bit6Vec ) tifrOld &= ~(1<<6);
		else if( vector == bit7Vec ) tifrOld &= ~(1<<7);

		tifr->set( tifrOld );
	}

	void TimerInterrupts::checkForNewSetIrq(unsigned char tiac) {
		tiac &= mask;
		if( tiac & (1<<0) ) { bus.raiseInterrupt( bit0Vec ); }
		if( tiac & (1<<1) ) { bus.raiseInterrupt( bit1Vec ); }
		if( tiac & (1<<2) ) { bus.raiseInterrupt( bit2Vec ); }
		if( tiac & (1<<3) ) { bus.raiseInterrupt( bit3Vec ); }
		if( tiac & (1<<4) ) { bus.raiseInterrupt( bit4Vec ); }
		if( tiac & (1<<5) ) { bus.raiseInterrupt( bit5Vec ); }
		if( tiac & (1<<6) ) { bus.raiseInterrupt( bit6Vec ); }
		if( tiac & (1<<7) ) { bus.raiseInterrupt( bit7Vec ); }

		tifr->set( tifrOld );
	}

	void TimerInterrupts::checkForNewClearIrq(unsigned char tiac) {
		tiac &= mask;
		if( tiac & (1<<0) ) { bus.clearInterrupt( bit0Vec ); }
		if( tiac & (1<<1) ) { bus.clearInterrupt( bit1Vec ); }
		if( tiac & (1<<2) ) { bus.clearInterrupt( bit2Vec ); }
		if( tiac & (1<<3) ) { bus.clearInterrupt( bit3Vec ); }
		if( tiac & (1<<4) ) { bus.clearInterrupt( bit4Vec ); }
		if( tiac & (1<<5) ) { bus.clearInterrupt( bit5Vec ); }
		if( tiac & (1<<6) ) { bus.clearInterrupt( bit6Vec ); }
		if( tiac & (1<<7) ) { bus.clearInterrupt( bit7Vec ); }
	}

	void TimerInterrupts::regChanged( IORegister *reg ) {
		unsigned char tiacOld = timskOld & tifrOld;

		if( reg == timsk ) {
		    timskOld = *timsk;
		} else if( reg == tifr ) {
		    tifrOld = *tifr;
		}

		unsigned char tiacNew= timskOld & tifrOld;

	    unsigned char changed = tiacNew ^ tiacOld;
	    unsigned char setnew = changed & tiacNew;
	    unsigned char clearnew = changed & (~tiacNew);

		checkForNewSetIrq(setnew);
		checkForNewClearIrq(clearnew);
	}

}
