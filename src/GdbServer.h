#ifndef AVR_GDBSERVER_H
#define AVR_GDBSERVER_H

#include <vector>
#include "Exception.h"
#include "Types.h"

namespace sim {
	class SimulationClock;
}

namespace avr {

	class Device;
	class DebugInterface;

	/**
	 * @author Tom Haber
	 * @date Apr 27, 2008
	 * @brief gdb communication error
	 *
	 * Thrown when something goes wrong with the gdb communication.
	 */
	class GdbException : public util::Exception {
		public:
			GdbException() : Exception("Gdb Exception") {}
			GdbException(const char * msg) : Exception(msg) {}
			GdbException(const std::string & msg) : Exception(msg) {}
	};

	/**
	 * @author Tom Haber
	 * @date Apr 27, 2008
	 * @brief Gnu debugger interfacing server
	 *
	 * This server can speak the gnu debugger remote debugging language
	 * and is used for remote debugging of the avr chip.
	 *
	 * This is largely based on code from simulavr.
	 *
	 * TODO rewrite, cleanup
	 * TODO implement trace points
	 */
	class GdbServer {
		public:
			GdbServer(sim::SimulationClock & clock, int port = 1234);
			~GdbServer();

		public:
			void add(Device *dev);
			void exec();

		private:
			void openSocket(int port);
			void closeSocket();
			void acceptClient();

		private:
			void handleClient();
			int preParsePacket(bool blocking = false);
			int parsePacket( char *pkt );
			void setBlockingMode( bool blocking );
			int readPacket();
			int readByte();
			void sendPosition(int signo);

			void write( const void *buf, size_t count );
			void sendAck();
			void sendReply( const char *reply );
			void saveLastReply( const char *reply );

		private:
			void queryPacket( char *pkt );
			void readRegisters();
			void writeRegisters( char *pkt );
			void readRegister( char *pkt );
			void writeRegister( char *pkt );
			void readMemory( char *pkt );
			void writeMemory( char *pkt );
			int getSignal( char *pkt );
			void breakPoint( char *pkt );

		private:
			sim::SimulationClock & clock;
			std::vector<DebugInterface*> devices;
			int current;

		private:
			/* A buffer containing a agent expression.  */
			struct AgentExpr {
				std::vector<unsigned char> buf;
				dword scope;
			};

			struct Tracepoint {
				bool enabled;

			    /* Number of times this tracepoint should single-step
			       and collect additional data.  */
			    long step_count;

			    /* Number of times this tracepoint should be hit before
			       disabling/ending.  */
			    int pass_count;

			    /* Count of the number of times this tracepoint was taken, dumped
			       with the info, but not used for anything else.  Useful for
			       seeing how many times you hit a tracepoint prior to the program
			       aborting, so you can back up to just before the abort.  */
			    int hit_count;
			};
			std::vector<Tracepoint> tracePoints;

		private:
			int sock, conn;
			int runMode;
			bool blockingOn;
			char *lastReply;
	};

}

#endif /*AVR_GDBSERVER_H*/
