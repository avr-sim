/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Bus.h"
#include "Hardware.h"
#include "Util.h"

#include "RuntimeException.h"
#include "Format.h"

namespace avr {

	Bus::Bus() {
		irqPending = 0;
		intVectors.resize( intVectorsSize );
	}

	Bus::~Bus() {
		std::for_each( hardware.begin(), hardware.end(), Delete<Hardware>() );
	}

	void Bus::reset() {
		clearAll();

		std::list<Hardware*>::iterator it;
		for(it = hardware.begin(); it != hardware.end(); ++it) {
			(*it)->reset();
		}

		irqPending = 0;
	}

	bool Bus::isHoldingCPU() {
		bool holding = false;

		std::list<Hardware*>::const_iterator it;
		for(it = hardware.begin(); it != hardware.end(); ++it) {
			holding = holding || (*it)->isHoldingCPU();
		}

		return holding;
	}

	void Bus::addInterrupt(unsigned int vector, unsigned int address,
								const char *name) {
		if( vector >= intVectorsSize )
			throw util::RuntimeException(
					util::format("Tried to add not existing interrupt %d") % vector );

		intVectors[vector] = IntVect( address, name );
	}

	void Bus::claimInterrupt(unsigned int vector, Hardware *hw) {
		if( vector >= intVectorsSize )
			throw util::RuntimeException(
					util::format("Hardware tried to claim not existing interrupt %d") % vector );

		if( intVectors[vector].hw != 0 )
			throw util::RuntimeException(
					util::format("Hardware tried to claim already claimed interrupt %d") % vector );

		intVectors[vector].hw = hw;
	}


	void Bus::beforeInvokeInterrupt(unsigned int vector) {
		clearInterrupt(vector);

		if( intVectors[vector].hw != 0 )
			intVectors[vector].hw->beforeInvokeInterrupt(vector);
	}
}
