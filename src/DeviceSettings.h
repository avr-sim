#ifndef AVR_DEVICESETTINGS_H
#define AVR_DEVICESETTINGS_H

#include "Exception.h"
#include <string>

struct _xmlNode;

namespace avr {

	class ParseException : public util::Exception {
		public:
			ParseException() : Exception("Parse Exception") {}
			ParseException(const char * msg) : Exception(msg) {}
			ParseException(const std::string & msg) : Exception(msg) {}
	};

	class Device;
	class XmlHandle;

	/**
	 * @author Tom Haber
	 * @date Apr 26, 2008
	 * @brief Helper for reading Hardware modules from XML
	 *
	 * The HardwareSettings class allows reading Hardware settings
	 * from the xml file without exposing the xml related
	 * bits.
	 */
	class HardwareSettings {
		public:
			HardwareSettings(XmlHandle & doc, _xmlNode *node);

		public:
			int intParam(const char *name, int base = 10) const;
			int intParamDef(const char *name, int def = 0, int base = 10) const;
			std::string strParam(const char *name) const;

		public:
			bool getBinding(std::string & name, std::string & binding);

		private:
			_xmlNode *findParam(const char *name, bool required = true) const;

		private:
			XmlHandle & doc;
			_xmlNode *node;
			_xmlNode *reg;
			_xmlNode *params;
	};

	/**
	 * @author Tom Haber
	 * @date Apr 21, 2008
	 * @brief Reading device settings from xml files
	 *
	 * This class allows reading device settings from xml files.
	 */
	class DeviceSettings {
		public:
			static void listAll(unsigned int lineLength = 80);
			static void load(Device *dev, const char *devicename);

		private:
			static void parseMemory(Device *dev, _xmlNode *memory);
			static void parseRegisters(Device *dev, _xmlNode *registers);
			static void parseInterrupts(Device *dev, _xmlNode *interrupts);
			static void parseHardware(Device *dev, XmlHandle & doc, _xmlNode *hardware);
			static void parsePackages(Device *dev, const char *pkgname, _xmlNode *hardware);
			static void parsePackage(Device *dev, _xmlNode *hardware);
	};

}
#endif /*AVR_DEVICESETTINGS_H*/
