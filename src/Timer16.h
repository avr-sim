#ifndef AVR_TIMER16_H
#define AVR_TIMER16_H

#include "Types.h"
#include "Hardware.h"
#include <string>

#define MAX_OUTPUTCOMPARE16_UNITS 2

namespace avr {

	/**
	 * @author Tom Haber
	 * @date May 29, 2008
	 * @brief 16-bit Output Compare Unit
	 *
	 * 16-bit Output Compare Unit
	 */
	class OutputCompareUnit16 {
		public:
			OutputCompareUnit16()  : ocfmask(0), ocrl(0), ocrh(0) {}

		public:
			void setOcrlRegister(IORegister *ocrl) { this->ocrl = ocrl; }
			void setOcrhRegister(IORegister *ocrh) { this->ocrh = ocrh; }
			bool ocrSet() const { return (ocrl != 0) && (ocrh != 0); }
			void setMask(unsigned char ocfmask) { this->ocfmask = ocfmask; }
			void setCompareMode(unsigned char cm) { this->compareMode = cm; }
			void regChanged(IORegister *reg);

		public:
			void flush();
			word value() const { return ocrBuf; }
			void forceOutputCompare(IORegister *tifr, word tcnt);
			void compareMatchOutput(IORegister *tifr, word tcnt);

		private:
			word ocrBuf;
			unsigned char compareMode;
			unsigned char ocfmask;

		private:
			IORegister *ocrl;
			IORegister *ocrh;
	};

	/**
	 * @author Tom Haber
	 * @date May 2, 2008
	 * @brief 16-bit timers
	 *
	 * 16-bit timers
	 */
	class Timer16 : public Hardware {
		public:
			Timer16(Bus & bus, unsigned char tovmask, int compunits,
					unsigned char ocfmask[]);

		public:
			bool attachReg(const char *name, IORegister *reg);
			void regChanged( IORegister *reg );
			bool finishBuild();
			void step();

		private:
			void flushOCRn();
			void setClock(unsigned char tccr);

		private:
			unsigned char tovmask;
			int direction;

			word tcnt;
			bool blockCompareMatch;
			unsigned char timerMode;
			int period;

			int nUnits;
			OutputCompareUnit16 units[MAX_OUTPUTCOMPARE16_UNITS];

		private:
			IORegister *tcntl;
			IORegister *tcnth;
			IORegister *tccra;
			IORegister *tccrb;
			IORegister *tifr;
	};

}

#endif /*AVR_TIMER16_H*/
