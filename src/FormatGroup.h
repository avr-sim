#ifndef UTIL_FORMAT_GROUP_H
#define UTIL_FORMAT_GROUP_H

namespace util {
	namespace format_interal {
		// empty group, but useful even though.
		struct group0 {
			group0()      {}
		};

		inline std::ostream & operator << ( std::ostream & os, const group0 & ) {
			return os;
		}

		template <class T1>
		struct group1 {
			T1 a1_;

			group1(T1 a1) : a1_(a1) {}
		};

		template <class T1>
		inline std::ostream & operator <<(std::ostream & os, const group1<T1> & x) {
			os << x.a1_;
			return os;
		}

		template <class T1,class T2>
		struct group2 {
			T1 a1_;
			T2 a2_;

			group2(T1 a1,T2 a2) : a1_(a1),a2_(a2) {}
		};

		template <class T1,class T2>
		inline std::ostream & operator <<(std::ostream & os, const group2<T1,T2>& x) {
			os << x.a1_<< x.a2_;
			return os;
		}

		template <class T1,class T2,class T3>
		struct group3 {
			T1 a1_;
			T2 a2_;
			T3 a3_;

			group3(T1 a1,T2 a2,T3 a3) : a1_(a1),a2_(a2),a3_(a3) {}
		};

		template <class T1,class T2,class T3>
		inline std::ostream & operator << (std::ostream & os, const group3<T1,T2,T3>& x) {
			os << x.a1_<< x.a2_<< x.a3_;
			return os;
		}

		template <class T1,class T2,class T3,class T4>
		struct group4 {
			T1 a1_;
			T2 a2_;
			T3 a3_;
			T4 a4_;

			group4(T1 a1,T2 a2,T3 a3,T4 a4) : a1_(a1),a2_(a2),a3_(a3),a4_(a4) {}
		};

		template <class Ch, class Tr, class T1,class T2,class T3,class T4>
		inline std::ostream & operator <<(std::ostream & os, const group4<T1,T2,T3,T4>& x) {
			os << x.a1_<< x.a2_<< x.a3_<< x.a4_;
			return os;
		}

		template <class T1,class T2>
		inline group1<T1> group_head( group2<T1,T2> const& x) {
			return group1<T1> (x.a1_);
		}

		template <class T1,class T2>
		inline group1<T2> group_last( group2<T1,T2> const& x) {
			return group1<T2> (x.a2_);
		}

		template <class T1,class T2,class T3>
		inline group2<T1,T2> group_head( group3<T1,T2,T3> const& x) {
			return group2<T1,T2> (x.a1_,x.a2_);
		}

		template <class T1,class T2,class T3>
		inline group1<T3> group_last( group3<T1,T2,T3> const& x) {
			return group1<T3> (x.a3_);
		}

		template <class T1,class T2,class T3,class T4>
		inline group3<T1,T2,T3> group_head( group4<T1,T2,T3,T4> const& x) {
			return group3<T1,T2,T3> (x.a1_,x.a2_,x.a3_);
		}

		template <class T1,class T2,class T3,class T4>
		inline group1<T4> group_last( group4<T1,T2,T3,T4> const& x) {
			return group1<T4> (x.a4_);
		}
	}

	inline format_interal::group1< format_interal::group0 > group() {
		return format_interal::group1< format_interal::group0 > ( format_interal::group0() );
	}

	template  <class T1, class Var>
	inline format_interal::group1< format_interal::group2<T1, Var const&> >
					group(T1 a1, Var const& var) {
		return format_interal::group1< format_interal::group2<T1, Var const&> >
		( format_interal::group2<T1, Var const&>
		(a1, var)
		);
	}

	template  <class T1,class T2, class Var>
	inline format_interal::group1< format_interal::group3<T1,T2, Var const&> >
				group(T1 a1,T2 a2, Var const& var) {
		return format_interal::group1< format_interal::group3<T1,T2, Var const&> >
		( format_interal::group3<T1,T2, Var const&>
		(a1,a2, var)
		);
	}

	template  <class T1,class T2,class T3, class Var>
	inline format_interal::group1< format_interal::group4<T1,T2,T3, Var const&> >
				group(T1 a1,T2 a2,T3 a3, Var const& var) {
		return format_interal::group1< format_interal::group4<T1,T2,T3, Var const&> >
		( format_interal::group4<T1,T2,T3, Var const&>
		(a1,a2,a3, var)
		);
	}
}
#endif
