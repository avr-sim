/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "TraceAnalyzer.h"
#include "Trace.h"

namespace avr {

	TraceAnalyzer::TraceAnalyzer(Core *core, const char *filename)
			: Analyzer(core) {

		tracer.enableLogging( filename );
	}

	TraceAnalyzer::~TraceAnalyzer() {
		tracer.disableLogging();
	}

	void TraceAnalyzer::reset(unsigned int type) {
		tracer.reset( type );
	}

	void TraceAnalyzer::trace(dword address) {
		tracer.pc_trace( address );
	}

	void TraceAnalyzer::step(lword ticks) {
		tracer.cycle_counter( ticks );
	}

	void TraceAnalyzer::readRegister(unsigned int r, byte val) {
		tracer.register_read( r, val );
	}

	void TraceAnalyzer::writeRegister(unsigned int r, byte val) {
		tracer.register_write( r, val );
	}

	void TraceAnalyzer::interrupt(unsigned int vector, unsigned int /*addr*/) {
		tracer.interrupt( vector );
	}

}
