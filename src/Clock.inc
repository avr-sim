#ifndef SIM_CLOCK_INC
#define SIM_CLOCK_INC

namespace sim {

	template <class Type>
	void Clock<Type>::setBreak(ClockOffset cycle, Type *obj) {
		ClockListIt it = objects.begin();
		while( it != objects.end() ) {
			if( it->first >= cycle )
				break;

			it++;
		}

		objects.insert( it, std::make_pair(cycle, obj) );
	}

	template <class Type>
	void Clock<Type>::setDividedBreak(ClockOffset prescaler, ClockOffset delta,
			Type *obj) {

		const ClockOffset mask = (prescaler - 1);
		const ClockOffset rem = prescaler - (value & mask);
		setBreak( value + rem + (delta - 1) * prescaler, obj );
	}

	template <class Type>
	void Clock<Type>::clearBreak(Type *obj) {
		ClockListIt it = objects.begin();
		while( it != objects.end() ) {
			if( it->second == obj )
				break;

			it++;
		}

		if( it != objects.end() )
			objects.erase( it );
	}

	template <class Type>
	void Clock<Type>::clearBreak(ClockOffset cycle) {
		ClockListIt it = objects.begin();
		while( it != objects.end() ) {
			if( it->first >= cycle )
				break;

			it++;
		}

		if( (it != objects.end()) && (it->first == cycle) )
			objects.erase( it );
	}

	template <class Type>
	void Clock<Type>::reassignBreak(ClockOffset oldCycle, ClockOffset newCycle,
									Type *obj) {

		ClockListIt it = objects.begin();
		while( it != objects.end() ) {
			if( it->first >= oldCycle )
				break;
			else
				it++;
		}

		if( (it != objects.end()) && (it->first == oldCycle) ) {
			objects.erase( it );
			setBreak( newCycle, obj );
		}
	}

	template <class Type>
	void Clock<Type>::reassignBreak(Type *obj, ClockOffset newCycle) {
		clearBreak(obj);
		setBreak( newCycle, obj );
	}

	template <class Type>
	void Clock<Type>::step() {
		value++;

		ClockListIt it = objects.begin();
		while( it != objects.end() ) {
			if( it->first > value )
				break;

			Type *obj = it->second;
			obj->step();
			it++;
		}

		objects.erase( objects.begin(), it );
	}

	template <class Type>
	void Clock<Type>::clearAll() {
		objects.clear();
	}

	template <class Type>
	void Clock<Type>::reassignBreakDelta(ClockOffset delta, Type *obj) {
		clearBreak(obj);
		setBreakDelta(delta, obj);
	}

}
#endif
