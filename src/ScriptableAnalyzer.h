#ifndef AVR_SCRIPTABLEANALYZER_H
#define AVR_SCRIPTABLEANALYZER_H

#include "Analyzer.h"
#include "script/Scriptable.h"
#include "script/ScriptUtil.h"

namespace avr {

	class ScriptEngine;

	/**
	 * @author Tom Haber
	 * @date May 13, 2008
	 * @brief Scriptable Analyzer
	 *
	 * This class passes all core state changes to a script.
	 */
	class ScriptableAnalyzer : public Analyzer, public script::Scriptable {
		public:
			ScriptableAnalyzer(Core *core, ScriptEngine *vm, const char *scriptname);
			~ScriptableAnalyzer();

		public:
			virtual void reset(unsigned int type);
			virtual void trace(dword address);
			virtual void step(lword ticks);

		public:
			virtual void readRegister(unsigned int r, byte val);
			virtual void writeRegister(unsigned int r, byte val);
			virtual void readByte(unsigned int addr, byte val);
			virtual void writeByte(unsigned int addr, byte val);
			virtual void readFlash(unsigned int addr, byte val);
			virtual void writeFlash(unsigned int addr, word data);
			virtual void fetchOperand(word val);

		public:
			virtual void push(byte val);
			virtual void pop(byte val);
			virtual void jump(dword address, bool push);
			virtual void skip();
			virtual void call(dword address, bool push);
			virtual void ret(bool interrupt);

		public:
			virtual void sleep(unsigned int mode);
			virtual void systemBreak();
			virtual void interrupt(unsigned int vector, unsigned int addr);

		public:
	        /**
	         * When a (C++) method is called from a script, this function
	         * is executed and unique method identifier is passed as parameter.
	         * Derived classes must override this if they add new scriptable methods.
	         * @param vm Script virtual machine executing the method.
	         * @param i Unique identifier (index) of the called method.
	         * @return Number of arguments returned in the script stack.
	         */
	        int methodCall(int i);

		private:
			void callHelper(const char *method);
			void callHelper(const char *method, float x);
			void callHelperb(const char *method, bool b);
			void callHelper(const char *method, float x, float y);
			void callHelperb(const char *method, float x, bool val);

		private:
			const char *scriptname;

			// scripting
			int methodBase;
			static script::ScriptMethod<ScriptableAnalyzer> sm_methods[];

		private:
			int script_breakSystem(const char *funcName);
			int script_register(const char *funcName);
			int script_ioregister(const char *funcName);
	};

}

#endif /*AVR_SCRIPTABLEANALYZER_H*/
