#ifndef AVR_VERBOSEINFO_H
#define AVR_VERBOSEINFO_H

#include <iostream>

namespace avr {

	enum Verbosity { NONE = 0, WARN = 1, INFO = 2, DBG = 3 };

	class VerboseInfo;
	class VerboseInfo {
		public:
			VerboseInfo() : verbosity(0) {}

		public:
			void setVerbosity(Verbosity verbosity);
			void setVerbosity(int verbosity);
			void increaseVerbosity();

		public:
			bool shouldOutput(Verbosity vb) const {
				return ((int)vb > verbosity);
			}

		public:
			class Printer {
				public:
					Printer(const VerboseInfo & vi, Verbosity vb,
							std::ostream & ostr)
						: vi(vi), vb(vb), ostr(ostr) {}

				public:
					template <class T>
					Printer & operator <<(const T & c) {
						if( vi.shouldOutput(vb) )
							ostr << c;
						return *this;
					}

				    Printer & operator<<(std::ostream & (*pf)(std::ostream &)) {
						if( vi.shouldOutput(vb) )
							ostr << pf;
						return *this;
					}

				private:
					const VerboseInfo & vi;
					Verbosity vb;
					std::ostream & ostr;
			};

			Printer operator ()(Verbosity vb,
					std::ostream & ostr = std::cout) const {
				return Printer(*this, vb, ostr);
			}

		private:
			int verbosity;
	};

	inline void VerboseInfo::setVerbosity(Verbosity verbosity) {
		this->verbosity = (int)verbosity;
	}

	inline void VerboseInfo::setVerbosity(int verbosity) {
		this->verbosity = verbosity;
	}

	inline void VerboseInfo::increaseVerbosity() {
		this->verbosity++;
	}

	extern VerboseInfo info;
}

#endif /* AVR_VERBOSEINFO_H */
