/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Decoder.h"
#include "Instructions.h"
#include "DecoderHelp.h"

namespace avr {

	Decoder::Decoder() {
		const size_t num_ops = 0x10000;
		lut.resize( num_ops );
		for(size_t i = 0; i < num_ops; i++) {
			lut[i] = lookupOpcode(i);
		}
	}

	Decoder::~Decoder() {
		for(size_t i = 0; i < lut.size(); i++)
			delete lut[i];
		lut.clear();
	}

	bool Decoder::is2WordInstruction( word opcode ) const {
		return lut[opcode]->is2Word();
	}

	Instruction * Decoder::lookupOpcode( word opcode ) {
	    int decode;

	    switch( opcode ) {
	        /* opcodes with no operands */
	        case 0x9519: return new  op::EICALL(opcode);              /* 1001 0101 0001 1001 | EICALL */
	        case 0x9419: return new  op::EIJMP(opcode);               /* 1001 0100 0001 1001 | EIJMP */
	        case 0x95D8: return new  op::ELPM(opcode);                /* 1001 0101 1101 1000 | ELPM */
	        case 0x95F8: return new  op::ESPM(opcode);                /* 1001 0101 1111 1000 | ESPM */
	        case 0x9509: return new  op::ICALL(opcode);               /* 1001 0101 0000 1001 | ICALL */
	        case 0x9409: return new  op::IJMP(opcode);                /* 1001 0100 0000 1001 | IJMP */
	        case 0x95C8: return new  op::LPM(opcode);                 /* 1001 0101 1100 1000 | LPM */
	        case 0x0000: return new  op::NOP(opcode);                 /* 0000 0000 0000 0000 | NOP */
	        case 0x9508: return new  op::RET(opcode);                 /* 1001 0101 0000 1000 | RET */
	        case 0x9518: return new  op::RETI(opcode);                /* 1001 0101 0001 1000 | RETI */
	        case 0x9588: return new  op::SLEEP(opcode);               /* 1001 0101 1000 1000 | SLEEP */
	        case 0x95E8: return new  op::SPM(opcode);                 /* 1001 0101 1110 1000 | SPM */
	        case 0x95A8: return new  op::WDR(opcode);                 /* 1001 0101 1010 1000 | WDR */
	        case 0x9598: return new  op::BREAK(opcode);				/* 1001 0101 1001 1000 | BREAK */
	        default: {
                 /* opcodes with two 5-bit register (Rd and Rr) operands */
                 decode = opcode & ~(mask_Rd_5 | mask_Rr_5);
                 switch ( decode ) {
                     case 0x1C00: return new  op::ADC(opcode);         /* 0001 11rd dddd rrrr | ADC or ROL */

                     case 0x0C00: return new  op::ADD(opcode);         /* 0000 11rd dddd rrrr | ADD or LSL */
                     case 0x2000: return new  op::AND(opcode);         /* 0010 00rd dddd rrrr | AND or TST */
                     case 0x1400: return new  op::CP(opcode);          /* 0001 01rd dddd rrrr | CP */
                     case 0x0400: return new  op::CPC(opcode);         /* 0000 01rd dddd rrrr | CPC */
                     case 0x1000: return new  op::CPSE(opcode);        /* 0001 00rd dddd rrrr | CPSE */
                     case 0x2400: return new  op::EOR(opcode);         /* 0010 01rd dddd rrrr | EOR or CLR */
                     case 0x2C00: return new  op::MOV(opcode);         /* 0010 11rd dddd rrrr | MOV */
                     case 0x9C00: return new  op::MUL(opcode);         /* 1001 11rd dddd rrrr | MUL */
                     case 0x2800: return new  op::OR(opcode);          /* 0010 10rd dddd rrrr | OR */
                     case 0x0800: return new  op::SBC(opcode);         /* 0000 10rd dddd rrrr | SBC */
                     case 0x1800: return new  op::SUB(opcode);         /* 0001 10rd dddd rrrr | SUB */
                 }

                 /* opcode with a single register (Rd) as operand */
                 decode = opcode & ~(mask_Rd_5);
                 switch (decode) {
                     case 0x9405: return new  op::ASR(opcode);         /* 1001 010d dddd 0101 | ASR */
                     case 0x9400: return new  op::COM(opcode);         /* 1001 010d dddd 0000 | COM */
                     case 0x940A: return new  op::DEC(opcode);         /* 1001 010d dddd 1010 | DEC */
                     case 0x9006: return new  op::ELPM_Z(opcode);      /* 1001 000d dddd 0110 | ELPM */
                     case 0x9007: return new  op::ELPM_Z_incr(opcode); /* 1001 000d dddd 0111 | ELPM */
                     case 0x9403: return new  op::INC(opcode);         /* 1001 010d dddd 0011 | INC */
                     case 0x9000: return new  op::LDS(opcode);         /* 1001 000d dddd 0000 | LDS */
                     case 0x900C: return new  op::LD_X(opcode);        /* 1001 000d dddd 1100 | LD */
                     case 0x900E: return new  op::LD_X_decr(opcode);   /* 1001 000d dddd 1110 | LD */
                     case 0x900D: return new  op::LD_X_incr(opcode);   /* 1001 000d dddd 1101 | LD */
                     case 0x900A: return new  op::LD_Y_decr(opcode);   /* 1001 000d dddd 1010 | LD */
                     case 0x9009: return new  op::LD_Y_incr(opcode);   /* 1001 000d dddd 1001 | LD */
                     case 0x9002: return new  op::LD_Z_decr(opcode);   /* 1001 000d dddd 0010 | LD */
                     case 0x9001: return new  op::LD_Z_incr(opcode);   /* 1001 000d dddd 0001 | LD */
                     case 0x9004: return new  op::LPM_Z(opcode);       /* 1001 000d dddd 0100 | LPM */
                     case 0x9005: return new  op::LPM_Z_incr(opcode);  /* 1001 000d dddd 0101 | LPM */
                     case 0x9406: return new  op::LSR(opcode);         /* 1001 010d dddd 0110 | LSR */
                     case 0x9401: return new  op::NEG(opcode);         /* 1001 010d dddd 0001 | NEG */
                     case 0x900F: return new  op::POP(opcode);         /* 1001 000d dddd 1111 | POP */
                     case 0x920F: return new  op::PUSH(opcode);        /* 1001 001d dddd 1111 | PUSH */
                     case 0x9407: return new  op::ROR(opcode);         /* 1001 010d dddd 0111 | ROR */
                     case 0x9200: return new  op::STS(opcode);         /* 1001 001d dddd 0000 | STS */
                     case 0x920C: return new  op::ST_X(opcode);        /* 1001 001d dddd 1100 | ST */
                     case 0x920E: return new  op::ST_X_decr(opcode);   /* 1001 001d dddd 1110 | ST */
                     case 0x920D: return new  op::ST_X_incr(opcode);   /* 1001 001d dddd 1101 | ST */
                     case 0x920A: return new  op::ST_Y_decr(opcode);   /* 1001 001d dddd 1010 | ST */
                     case 0x9209: return new  op::ST_Y_incr(opcode);   /* 1001 001d dddd 1001 | ST */
                     case 0x9202: return new  op::ST_Z_decr(opcode);   /* 1001 001d dddd 0010 | ST */
                     case 0x9201: return new  op::ST_Z_incr(opcode);   /* 1001 001d dddd 0001 | ST */
                     case 0x9402: return new  op::SWAP(opcode);        /* 1001 010d dddd 0010 | SWAP */
                 }

                 /* opcodes with a register (Rd) and a constant data (K) as operands */
                 decode = opcode & ~(mask_Rd_4 | mask_K_8);
                 switch ( decode ) {
                     case 0x7000: return new  op::ANDI(opcode);        /* 0111 KKKK dddd KKKK | CBR or ANDI */
                     case 0x3000: return new  op::CPI(opcode);         /* 0011 KKKK dddd KKKK | CPI */
                     case 0xE000: return new  op::LDI(opcode);         /* 1110 KKKK dddd KKKK | LDI or SER */
                     case 0x6000: return new  op::ORI(opcode);         /* 0110 KKKK dddd KKKK | SBR or ORI */
                     case 0x4000: return new  op::SBCI(opcode);        /* 0100 KKKK dddd KKKK | SBCI */
                     case 0x5000: return new  op::SUBI(opcode);        /* 0101 KKKK dddd KKKK | SUBI */
                 }

                 /* opcodes with a register (Rd) and a register bit number (b) as operands */
                 decode = opcode & ~(mask_Rd_5 | mask_reg_bit);
                 switch ( decode ) {
                     case 0xF800: return new  op::BLD(opcode);         /* 1111 100d dddd 0bbb | BLD */
                     case 0xFA00: return new  op::BST(opcode);         /* 1111 101d dddd 0bbb | BST */
                     case 0xFC00: return new  op::SBRC(opcode);        /* 1111 110d dddd 0bbb | SBRC */
                     case 0xFE00: return new  op::SBRS(opcode);        /* 1111 111d dddd 0bbb | SBRS */
                 }

                 /* opcodes with a relative 7-bit address (k) and a register bit number (b) as operands */
                 decode = opcode & ~(mask_k_7 | mask_reg_bit);
                 switch ( decode ) {
                     case 0xF400: return new  op::BRBC(opcode);        /* 1111 01kk kkkk kbbb | BRBC */
                     case 0xF000: return new  op::BRBS(opcode);        /* 1111 00kk kkkk kbbb | BRBS */
                 }

                 /* opcodes with a 6-bit address displacement (q) and a register (Rd) as operands */
                 decode = opcode & ~(mask_Rd_5 | mask_q_displ);
                 switch ( decode ) {
                     case 0x8008: return new  op::LDD_Y(opcode);       /* 10q0 qq0d dddd 1qqq | LDD */
                     case 0x8000: return new  op::LDD_Z(opcode);       /* 10q0 qq0d dddd 0qqq | LDD */
                     case 0x8208: return new  op::STD_Y(opcode);       /* 10q0 qq1d dddd 1qqq | STD */
                     case 0x8200: return new  op::STD_Z(opcode);       /* 10q0 qq1d dddd 0qqq | STD */
                 }

                 /* opcodes with a absolute 22-bit address (k) operand */
                 decode = opcode & ~(mask_k_22);
                 switch ( decode ) {
                     case 0x940E: return new  op::CALL(opcode);        /* 1001 010k kkkk 111k | CALL */
                     case 0x940C: return new  op::JMP(opcode);         /* 1001 010k kkkk 110k | JMP */
                 }

                 /* opcode with a sreg bit select (s) operand */
                 decode = opcode & ~(mask_sreg_bit);
                 switch ( decode ) {
                     /* BCLR takes place of CL{C,Z,N,V,S,H,T,I} */
                     /* BSET takes place of SE{C,Z,N,V,S,H,T,I} */
                     case 0x9488: return new  op::BCLR(opcode);        /* 1001 0100 1sss 1000 | BCLR */
                     case 0x9408: return new  op::BSET(opcode);        /* 1001 0100 0sss 1000 | BSET */
                 }

                 /* opcodes with a 6-bit constant (K) and a register (Rd) as operands */
                 decode = opcode & ~(mask_K_6 | mask_Rd_2);
                 switch ( decode ) {
                     case 0x9600: return new  op::ADIW(opcode);        /* 1001 0110 KKdd KKKK | ADIW */
                     case 0x9700: return new  op::SBIW(opcode);        /* 1001 0111 KKdd KKKK | SBIW */
                 }

                 /* opcodes with a 5-bit IO Addr (A) and register bit number (b) as operands */
                 decode = opcode & ~(mask_A_5 | mask_reg_bit);
                 switch ( decode ) {
                     case 0x9800: return new  op::CBI(opcode);         /* 1001 1000 AAAA Abbb | CBI */
                     case 0x9A00: return new  op::SBI(opcode);         /* 1001 1010 AAAA Abbb | SBI */
                     case 0x9900: return new  op::SBIC(opcode);        /* 1001 1001 AAAA Abbb | SBIC */
                     case 0x9B00: return new  op::SBIS(opcode);        /* 1001 1011 AAAA Abbb | SBIS */
                 }

                 /* opcodes with a 6-bit IO Addr (A) and register (Rd) as operands */
                 decode = opcode & ~(mask_A_6 | mask_Rd_5);
                 switch ( decode ) {
                     case 0xB000: return new  op::IN(opcode);          /* 1011 0AAd dddd AAAA | IN */
                     case 0xB800: return new  op::OUT(opcode);         /* 1011 1AAd dddd AAAA | OUT */
                 }

                 /* opcodes with a relative 12-bit address (k) operand */
                 decode = opcode & ~(mask_k_12);
                 switch ( decode ) {
                     case 0xD000: return new  op::RCALL(opcode);       /* 1101 kkkk kkkk kkkk | RCALL */
                     case 0xC000: return new  op::RJMP(opcode);        /* 1100 kkkk kkkk kkkk | RJMP */
                 }

                 /* opcodes with two 4-bit register (Rd and Rr) operands */
                 decode = opcode & ~(mask_Rd_4 | mask_Rr_4);
                 switch ( decode ) {
                     case 0x0100: return new  op::MOVW(opcode);        /* 0000 0001 dddd rrrr | MOVW */
                     case 0x0200: return new  op::MULS(opcode);        /* 0000 0010 dddd rrrr | MULS */
                 }

                 /* opcodes with two 3-bit register (Rd and Rr) operands */
                 decode = opcode & ~(mask_Rd_3 | mask_Rr_3);
                 switch ( decode ) {
                     case 0x0300: return new  op::MULSU(opcode);       /* 0000 0011 0ddd 0rrr | MULSU */
                     case 0x0308: return new  op::FMUL(opcode);        /* 0000 0011 0ddd 1rrr | FMUL */
                     case 0x0380: return new  op::FMULS(opcode);       /* 0000 0011 1ddd 0rrr | FMULS */
                     case 0x0388: return new  op::FMULSU(opcode);      /* 0000 0011 1ddd 1rrr | FMULSU */
                 }

             } /* default */
	    } /* first switch */

	    //return NULL;
	    return new op::ILLEGAL(opcode);
	}
}
