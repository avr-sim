#ifndef AVR_CORE_H
#define AVR_CORE_H

#include "Types.h"
#include "Flash.h"
#include "MMU.h"
#include "Stack.h"
#include "Decoder.h"

#include <list>

namespace avr {

	class ERam;
	class Bus;
	class Analyzer;
	class DebugInterface;

	/**
	 * @author Tom Haber
	 * @date Apr 23, 2008
	 * @brief The core of the AVR chip.
	 *
	 * This is the interpreter of the instructions and it contains the state.
	 */
	class Core {
		public:
			Core(Bus & bus, unsigned int ioSpaceSize, unsigned int ramSize,
				unsigned int flashSize, unsigned int pageSize,
				word stackMask, int pcBytes = 2, ERam *eram = 0);
			~Core();

		public:
			void init();
			void loadFlash(unsigned char *data, unsigned int offset, unsigned int size);

			/**
			 * Resets the core
			 */
			void reset(unsigned int type);

			/**
			 * Executes a single cpu cycle.
			 */
			bool step();

			void setDebugInterface(DebugInterface *dbgi);
			bool isStopped() const { return stoppedMode; }

		public:
			void addIOReg(unsigned int address,
					const std::string & name, byte initial = 0);
			void addAnalyzer(Analyzer *analyzer);
			const Register & getR(int r) const { return mmu.getR(r); }
			IORegister & getIoreg(unsigned int offset);
			IORegister *getIoreg(const std::string & name);

		public:
			byte readRegister(unsigned int r) const;
			void writeRegister(unsigned int r, byte val);
			byte readStatus();
			void writeStatus(byte val);
			byte readIORegister(unsigned int r);
			void writeIORegister(unsigned int r, byte val);
			byte readByte(unsigned int addr) const;
			void writeByte(unsigned int addr, byte val);
			byte readFlash(unsigned int addr) const;
			int writeFlash(unsigned int addr, word data);
			word fetchOperand();

		public:
			int pcBytes() const { return pc_bytes; }
			void push(byte val);
			byte pop();
			void jump(sbyte offset, bool push = false);
			int skip();
			void call(dword address, bool push = true);
			void ret(bool interrupt = false);
			void sleep();
			void systemBreak();

		private:
			bool invokeInterrupt();

		private:
			MMU mmu;
			Flash flash;
			Stack stack;
			Decoder decoder;
			Bus & bus;

		private:
			unsigned char SReg;
			dword PC;
			int pc_bytes;
			bool stoppedMode;

			bool justReturnedFromInterrupt;

			enum SleepMode {
			    SLEEP_MODE_IDLE = 0,
			    SLEEP_MODE_ADC_REDUX = 1,
			    SLEEP_MODE_PWR_DOWN = 2,
			    SLEEP_MODE_PWR_SAVE = 3,
			    SLEEP_MODE_reserved1 = 4,
			    SLEEP_MODE_reserved2 = 5,
			    SLEEP_MODE_STANDBY = 6,
			    SLEEP_MODE_EXT_STANDBY = 7,
			    SLEEP_MODE_NONE = 8
			} sleepMode;

		private:
			std::list<Analyzer*> analyzers;

		private:
			DebugInterface *dbgi;
			int cpuCycles;

		friend class DebugInterface;
	};

	inline void Core::loadFlash(unsigned char *data, unsigned int offset, unsigned int size) {
		flash.write(offset, data, size);
	}

	inline void Core::addAnalyzer(Analyzer *analyzer) {
		analyzers.push_back( analyzer );
	}

	inline void Core::setDebugInterface(DebugInterface *dbgi) {
		this->dbgi = dbgi;
	}

	inline void Core::addIOReg(unsigned int address,
				const std::string & name, byte initial) {
		mmu.addIOReg(address, name, initial);
	}

	inline IORegister & Core::getIoreg(unsigned int offset) {
		return mmu.getIoreg(offset);
	}

	inline IORegister *Core::getIoreg(const std::string & name) {
		return mmu.getIoreg(name);
	}

	inline byte Core::readStatus() {
		return readIORegister(SReg);
	}

	inline void Core::writeStatus(byte val) {
		writeIORegister(SReg, val);
	}

}

#endif /*AVR_CORE_H*/
