#ifndef UTIL_FORMAT_EXCEPTIONS_H
#define UTIL_FORMAT_EXCEPTIONS_H

#include <stdexcept>

namespace util {
	class format_error : public std::exception {
	public:
	  format_error() {}
	  virtual const char *what() const throw() {
		return "Format::format_error: format generic failure";
	  }
	};

	class bad_format_string : public format_error {
	public:
	  bad_format_string() {}
	  virtual const char *what() const throw() {
		return "Format::bad_format_string: format-string is ill-formed";
	  }
	};

	class too_few_args : public format_error {
	public:
	  too_few_args() {}
	  virtual const char *what() const throw() {

		return "Format::too_few_args: format-string refered to more arguments than were passed";

	  }

	};

	class too_many_args : public format_error {
	public:
	  too_many_args() {}
	  virtual const char *what() const throw() {
		return "Format::too_many_args: format-string refered to less arguments than were passed";
	  }
	};

	class  out_of_range : public format_error {
	public:
	  out_of_range() {}
	  virtual const char *what() const throw() {
		return "Format::out_of_range: "
		  "tried to refer to an argument (or item) number which is out of range, "
		  "according to the format string.";
	  }
	};
}
#endif
