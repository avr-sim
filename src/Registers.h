#ifndef AVR_REGISTERS_H
#define AVR_REGISTERS_H

#include <vector>
#include <string>
#include "Memory.h"

namespace avr {

	class Hardware;

	/**
	 * @author Tom Haber
	 * @date Apr 23, 2008
	 * @brief A register
	 *
	 * Represents general purpose registers and IORegisters
	 */
	class Register {
		public:
			Register() {}
			Register(word address, const std::string & name);
			~Register() {}

		public:
			/**
			 * Initialize the register
			 * @param address the address of the register (IO address not mem)
			 * @param name the name of the register
			 */
			void init(word address, const std::string & name);

			/**
			 * Changes the register value to \e val.
			 */
	        unsigned char operator =(unsigned char val);

			/**
			 * Returns the register value.
			 */
	        operator unsigned char() const;

			/**
			 * Changes the register value to the value of register \reg.
			 */
	        void operator =(const Register & reg);

		public:
			void set(unsigned char val);
			unsigned char get() const;

		public:
			/**
			 * Returns the IO address of the register
			 */
			word getAddress() const { return address; }

			/**
			 * Returns the name of the register
			 */
			const std::string & getName() const { return name; }

		protected:
			word address;
			std::string name;
			unsigned char val;
	};

	/**
	 * @author Tom Haber
	 * @date Apr 23, 2008
	 * @brief An IOregister
	 *
	 * Adds functionality to Register for IORegisters.
	 * Hardware can be bound to an IORegister and it will be
	 * notified on changes.
	 *
	 * Some registers share an I/O location with another register.
	 * When doing a write access to this I/O location, the high bit
	 * controls which one of the two registers will be written.
	 *
	 * Read access is more complex: the read access is controlled by a
	 * timed sequence. Reading the I/O location once returns the first
	 * register content. If the register location was read in the
	 * previous clock cycle, reading the register in the current cycle
	 * will return the second register content.
	 *
	 * Note that the timed sequence for reading the shared location is
	 * an atomic operation. Interrupts must therefore be controlled
	 * (e.g., by disabling interrupts globally) during the read operation.
	 *
	 * These type of locations have to be taken into account elsewhere!
	 */
	class IORegister : public Register {
		public:
			IORegister(word address, const std::string & name,
					unsigned char initial = 0);

		public:
			/**
			 * Reset the register to its initial value.
			 */
			void reset();

			/**
			 * Returns the register value.
			 */
	        operator unsigned char();

			/**
			 * Changes the register value to \e val
			 */
	        unsigned char operator =(unsigned char val);

	        void operator |=(unsigned char val);
	        void operator &=(unsigned char val);

		public:
			/**
			 * Bind an internal device to this register.
			 * The hardware will be notified on register value changes.
			 */
			void registerHW(Hardware *hw);

		private:
			unsigned char initial;
			Hardware *hw;
	};

	/**
	 * @author Tom Haber
	 * @date Apr 23, 2008
	 * @brief All IORegisters
	 *
	 * Contains all IORegisters and provides an interface for the MMU.
	 */
	class IORegisters {
		public:
			IORegisters(unsigned int count);
			~IORegisters();

		public:
			unsigned int size() const { return regs.size(); }

			/**
			 * Reset all registers to their initial value.
			 */
			void reset();

			/**
			 * Reads a single bytes of raw data from the register memory
			 * at offset \e offset.
			 */
			byte readByte(unsigned int offset) const;

			/**
			 * Writes a single bytes of raw data to the register memory
			 * at offset \e offset.
			 */
			void writeByte(unsigned int offset, byte val);

		public:
			void addReg( word addr, IORegister *reg );
			IORegister & getIoreg(unsigned int offset) const;
			IORegister *getIoreg(const std::string & name, bool required = true) const;

		private:
			std::vector<IORegister *> regs;
	};

	inline Register::Register(word address, const std::string & name) {
		init( address, name );
	}

	inline void Register::init(word address, const std::string & name) {
		this->address = address;
		this->name = name;
	}

	inline IORegister::IORegister(word address,
			const std::string & name, unsigned char initial)
		: Register(address, name), initial(initial), hw(0) {
	}

	inline void IORegister::reset() {
		set( initial );
	}

	inline void Register::set(unsigned char val) {
		this->val = val;
	}

	inline unsigned char Register::get() const {
		return val;
	}

	inline void Register::operator =(const Register & reg) {
	    *this = (unsigned char)reg;
	}

	inline byte IORegisters::readByte(unsigned int offset) const {
		if( regs[offset] != 0 )
			return (byte)*regs[offset];

		return 0;
	}

	inline unsigned char Register::operator =(unsigned char val) {
		unsigned char ret = this->val;
		this->val = val;
		return ret;
	}

	inline Register::operator unsigned char() const {
		return val;
	}

	inline void IORegisters::writeByte(unsigned int offset, byte val) {
		if( regs[offset] != 0 )
			(*regs[offset]) = val;
	}

	inline IORegister & IORegisters::getIoreg(unsigned int offset) const {
		return *regs[offset];
	}

	inline void IORegister::registerHW(Hardware *hw) {
		this->hw = hw;
	}

}

#endif /*AVR_REGISTERS_H*/
