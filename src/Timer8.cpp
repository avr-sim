/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Timer8.h"
#include "Registers.h"
#include "Bus.h"
#include "ImplementationException.h"
#include <cstring>

enum {
    bit_CS0 = 0,                /* timer/counter clock select bit 0 */
    bit_CS1 = 1,
    bit_CS2 = 2,
};

enum {
    mask_CS0 = 1 << bit_CS0,
    mask_CS1 = 1 << bit_CS1,
    mask_CS2 = 1 << bit_CS2,
    mask_CS = (mask_CS0 | mask_CS1 | mask_CS2),
};

enum {
    CS_STOP = 0x00,             /* Stop, the Timer/Counter is stopped */
    CS_CK = 0x01,               /* CK                                 */
    CS_CK_8 = 0x02,             /* CK/8                               */
    CS_CK_64 = 0x03,            /* CK/64                              */
    CS_CK_256 = 0x04,           /* CK/256                             */
    CS_CK_1024 = 0x05,          /* CK/1024                            */
    CS_EXT_FALL = 0x06,         /* External Pin Tn, falling edge      */
    CS_EXT_RISE = 0x07,         /* External Pin Tn, rising edge       */
};

enum {
	bit_WGM1 	= 3,
	bit_WGM0 	= 6,
	bit_FOC		= 7,
	bit_COM0	= 4,
	bit_COM1	= 5
};

enum {
	WGM1	= (1<<bit_WGM1),
	WGM0	= (1<<bit_WGM0),
	FOC		= (1<<bit_FOC),
	COM0	= (1<<bit_COM0),
	COM1	= (1<<bit_COM1),
	mask_WGM = (WGM1|WGM0),
	mask_COM = (COM1|COM0),
};

enum {
	MODE_NORMAL		= 0,
	MODE_PWM		= (WGM0),
	MODE_CTC		= (WGM1),
	MODE_FASTPWM	= (WGM0|WGM1)
};

enum {
	COM_NORMAL	= 0,
	COM_TOGGLE	= COM0,
	COM_CLEAR	= COM1,
	COM_SET		= COM0|COM1
};

namespace avr {

#define overflow() *tifr |= tovmask
#define compareMatch() *tifr |= ocfmask

	void OutputCompareUnit8::flush() {
		ocrBuf = ocr->get();
	}

	void OutputCompareUnit8::regChanged(IORegister *reg) {
		if( reg == ocr )
			ocrBuf = ocr->get();
	}

	void OutputCompareUnit8::forceOutputCompare(IORegister *tifr, unsigned char tcnt) {
		compareMatchOutput( tifr, tcnt );
	}

	void OutputCompareUnit8::compareMatchOutput(IORegister *tifr, unsigned char tcnt) {
        // the non-PWM modes are NORMAL and CTC
        // under NORMAL, there is no pin action for a compare match
        // under CTC, the action is to clear the pin.
        if( tcnt == ocrBuf ) {
            switch( compareMode ) {
                case COM_NORMAL:
                	break;

                case COM_TOGGLE:
                    //outputComparePin.toggle();
                    break;

                case COM_CLEAR:
                    //outputComparePin.low();
                    break;

                case COM_SET:
                    //outputComparePin.high();
                    break;
            }

            compareMatch();
        }
	}


	Timer8::Timer8(Bus & bus, unsigned char tovmask, int compunits,
					unsigned char ocfmask[])
		: Hardware(bus), tovmask(tovmask), direction(1) {
		blockCompareMatch = true;
		timerMode = 0;
		period = 0;

		nUnits = compunits;
		for(int i = 0; i < nUnits; ++i)
			units[i].setMask( ocfmask[i] );
	}

	bool Timer8::attachReg(const char *name, IORegister *reg) {
		if( strcmp(name, "tcnt") == 0 ) {
			tcnt = reg;
		} else if( strcmp(name, "tccr") == 0 ) {
			tccr = reg;
		} else if( strcmp(name, "ocra") == 0 || strcmp(name, "ocr") == 0 ) {
			units[0].setOcrRegister(reg);
		} else if( strcmp(name, "ocrb") == 0 ) {
			units[1].setOcrRegister(reg);
		} else if( strcmp(name, "ocrc") == 0 ) {
			units[2].setOcrRegister(reg);
		} else if( strcmp(name, "tifr") == 0 ) {
			tifr = reg;
			return true;
		} else
			return false;

		reg->registerHW(this);
		return true;
	}

	bool Timer8::finishBuild() {
		bool unitsOk = true;
		for(int i = 0; i < nUnits; ++i)
			unitsOk = unitsOk && units[i].ocrSet();

		return (tcnt != 0) && (tccr != 0) && (tifr != 0) && unitsOk;
	}

	void Timer8::regChanged( IORegister *reg ) {
		if( reg == tccr ) {
			unsigned char val = tccr->get();
			setClock(val);

			unsigned char timerModeNew = val & mask_WGM;
			if( timerMode != timerModeNew )
				direction = +1;
			timerMode = timerModeNew;

			units[0].setCompareMode( val & mask_COM );

			if( val & FOC ) {
				for(int cntr = 0; nUnits; cntr++ )
					units[cntr].forceOutputCompare(tifr, *tcnt);
			}

		} else if( reg == tcnt ) {
			blockCompareMatch = true;
		} else {
			if( (timerMode == MODE_NORMAL) || (timerMode == MODE_CTC) ) {
				for(int i = 0; i < nUnits; ++i)
					units[i].regChanged(reg);
			}
		}
	}

	void Timer8::setClock(unsigned char tccr) {
		byte clk = tccr & mask_CS;
		switch( clk ) {
			case CS_STOP:
				bus.clearBreak(this);
				return;

			case CS_CK:
				period = 1;
				break;

			case CS_CK_8:
				period = 8;
				break;

			case CS_CK_64:
				period = 64;
				break;

			case CS_CK_256:
				period = 256;
				break;

			case CS_CK_1024:
				period = 1024;
				break;

			case CS_EXT_FALL:
			case CS_EXT_RISE:
				throw util::ImplementationException(
						"external timer/counter sources not implemented" );
		}

		bus.setBreakDelta(period, this);
	}

#define TOP 0xff
#define BOTTOM 0x00

	void Timer8::step() {
		byte tcntVal = tcnt->get();
		byte tcntSave = tcntVal;

		bool tov = false;
		switch( timerMode ) {
			case MODE_NORMAL:
				if( tcntVal == TOP ) {
					tov = true;
					tcntVal = 0;
				}
				break;

			case MODE_CTC:
				if( units[0].compare(tcntVal) ) {
					tov = true;
					tcntVal = 0;
				}
				break;

			case MODE_PWM:
				if( (tcntVal == TOP) && (direction == +1) ) {
					direction = -1;
					flushOCRn();
				} else if( (tcntVal == BOTTOM) && (direction == -1) ) {
					direction = +1;
					tov = true;
				}
				break;

			case MODE_FASTPWM:
				if( tcntVal == TOP ) {
					tov = true;
					flushOCRn();
				}
				break;
		}

		if( tov )
			overflow();
		else
			tcntVal += direction;

		if( ! blockCompareMatch )
	        for(int cntr = 0; cntr < nUnits; cntr++ )
	        	units[cntr].compareMatchOutput(tifr, tcntSave);
		blockCompareMatch = false;

		tcnt->set( tcntVal );
		bus.setBreakDelta(period, this);
	}

	void Timer8::flushOCRn() {
		for(int cntr = 0; cntr < nUnits; cntr++ )
			units[cntr].flush();
	}

}
