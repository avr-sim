#ifndef AVR_FLASH_H
#define AVR_FLASH_H

#include "Hardware.h"
#include "Memory.h"
#include <vector>

namespace avr {

	class Instruction;

	/**
	 * @author Tom Haber
	 * @date Apr 21, 2008
	 * @brief Flash memory in the AVR chip
	 *
	 * This class represents the Flash memory in the chip.
	 * It allows read and write operations
	 */
	class Flash : public Hardware, public Memory {
		public:
			Flash(Bus & bus, unsigned int size, int pagesize);
			~Flash();

		public:
			/**
			 * Fill a block of flash memory with the specified value.
			 *
			 * \exception AccessViolation { When the write exceeds
			 * 		amount of memory, this exception is thrown }
			 */
			void fill(unsigned int offset, byte val, unsigned int size = 1);

		public:
			void storeProgramMemory(dword Z, byte r0, byte r1);

		private:
			void pageErase();
			void pageWrite();
			void resetRWW();

		public:
			bool attachReg(const char *name, IORegister *reg);
			bool finishBuild();
			void regChanged( IORegister *reg );
			void step();

		private:
			// For reprogramming..
			IORegister *spmcsr;
			int pagebits;
			int pagemask;

			unsigned char *buffer;
			int pageoffset;
			int pagenum;
			int state;
	};

}

#endif /*AVR_FLASH_H*/
