#ifndef AVR_BFDPROGRAM_H
#define AVR_BFDPROGRAM_H

#include "config.h"

#ifdef HAVE_LIBBFD

#include "Program.h"

struct bfd;
struct bfd_section;

namespace avr {

	/**
	 * @author Tom Haber
	 * @date May 23, 2008
	 * @brief Support for the BFD library
	 *
	 * BfdProgram is an implementation of Program that
	 * reads program files using the BFD library.
	 * BFD allows a number of formats to be read such
	 * as elf, coff, ...
	 */
	class BfdProgram : public Program {
		public:
			BfdProgram();
			BfdProgram(const char *filename);
			~BfdProgram();

		public:
			/**
			 * Opens and loads a file.
			 */
			void load(const char * filename);

			/**
			 * Reads the next available section from the file
			 * and returns the data in \e sec.
			 *
			 * \returns false if no further sections are available.
			 */
			bool readNextSection(Section & sec);

		private:
			void loadSymbols();

		private:
			bfd *abfd;
			bfd_section *sec;
	};

	inline BfdProgram::BfdProgram() : abfd(0), sec(0) {}
	inline BfdProgram::BfdProgram(const char *filename) : abfd(0), sec(0) {
		load( filename );
	}

}
#endif

#endif /*AVR_BFDPROGRAM_H*/
