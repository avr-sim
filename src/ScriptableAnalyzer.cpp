/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptableAnalyzer.h"
#include "ScriptEngine.h"
#include "script/ScriptException.h"
#include "Core.h"

#include "Format.h"
#include <string>
#include <fstream>
#include <iostream>

#define ANALYZER_PATH "./analyzers/:./"

namespace avr {

	static std::string locateScript(const std::string & name) {
		static const std::string path(ANALYZER_PATH);
		static const std::string delimiters(":");
		static const std::string empty;

		size_t i = 0, j;
		do {
			j = path.find_first_of(delimiters, i);

			std::string test;
			if( j != std::string::npos )
				test = path.substr(i, j) + name + ".lua";
			else
				test = path.substr(i) + name + ".lua";

			std::ifstream f( test.c_str() );
			if( f.is_open() )
				return test;

			i = j + 1;
		} while( j != std::string::npos );

		return empty;
	}

	using script::ScriptMethod;
	using script::ScriptUtil;

	ScriptMethod<ScriptableAnalyzer> ScriptableAnalyzer::sm_methods[] = {
		ScriptMethod<ScriptableAnalyzer>( "breakSystem", &ScriptableAnalyzer::script_breakSystem ),
		ScriptMethod<ScriptableAnalyzer>( "register", &ScriptableAnalyzer::script_register ),
		ScriptMethod<ScriptableAnalyzer>( "ioregister", &ScriptableAnalyzer::script_ioregister ),
	};

	ScriptableAnalyzer::ScriptableAnalyzer(Core *core, ScriptEngine *vm,
											const char *scriptname)
			: Analyzer(core), script::Scriptable(vm), scriptname(scriptname) {

		std::string path = locateScript(scriptname);
		if( path == "" )
			throw ScriptException(
					util::format("Unable to locate script %s") % scriptname );

		methodBase = ScriptUtil<ScriptableAnalyzer,Scriptable>::addMethods( this,
						sm_methods, sizeof(sm_methods)/sizeof(sm_methods[0]) );

		try {
			compileFile( path.c_str() );
			registerClass(scriptname);
			setNumber("pcBytes", core->pcBytes());
		} catch( script::ScriptException & ex ) {
			throw ScriptException( ex.message() );
		}
	}

	ScriptableAnalyzer::~ScriptableAnalyzer() {
		try {
			const char *method = "finish";
			if( hasMethod(method) ) {
				pushMethod(method);
				Scriptable::call(0, 0);
			}

			unregisterClass(scriptname);
		} catch( script::ScriptException & ex ) {
			/*
			 * Throwing exceptions in destructors is a bad idea
			 * see http://www.kolpackov.net/projects/c++/eh/dtor-1.xhtml
			 * for an interesting overview of the problems
			 */
			//throw ScriptException( ex.message() );
			std::cerr << "~ScriptableAnalyzer: " << ex.message() << std::endl;
		}
	}

	void ScriptableAnalyzer::callHelper(const char *method) {
		try {
			if( hasMethod(method) ) {
				pushMethod(method);
				Scriptable::call(0, 0);
			}
		} catch( script::ScriptException & ex ) {
			throw ScriptException( ex.message() );
		}
	}

	void ScriptableAnalyzer::callHelperb(const char *method, bool b) {
		try {
			if( hasMethod(method) ) {
				pushMethod(method);
				vm->pushBoolean(b);
				Scriptable::call(1, 0);
			}
		} catch( script::ScriptException & ex ) {
			throw ScriptException( ex.message() );
		}
	}

	void ScriptableAnalyzer::callHelper(const char *method, float x) {
		try {
			if( hasMethod(method) ) {
				pushMethod(method);
				vm->pushNumber(x);
				Scriptable::call(1, 0);
			}
		} catch( script::ScriptException & ex ) {
			throw ScriptException( ex.message() );
		}
	}

	void ScriptableAnalyzer::callHelperb(const char *method, float x, bool b) {
		try {
			if( hasMethod(method) ) {
				pushMethod(method);
				vm->pushNumber(x);
				vm->pushBoolean(b);
				Scriptable::call(2, 0);
			}
		} catch( script::ScriptException & ex ) {
			throw ScriptException( ex.message() );
		}
	}

	void ScriptableAnalyzer::callHelper(const char *method, float x, float y) {
		try {
			if( hasMethod(method) ) {
				pushMethod(method);
				vm->pushNumber(x);
				vm->pushNumber(y);
				Scriptable::call(2, 0);
			}
		} catch( script::ScriptException & ex ) {
			throw ScriptException( ex.message() );
		}
	}

	int ScriptableAnalyzer::methodCall(int i) {
	    return ScriptUtil<ScriptableAnalyzer,Scriptable>::methodCall(this, i, methodBase,
	                    sm_methods, sizeof(sm_methods)/sizeof(sm_methods[0]) );
	}

	void ScriptableAnalyzer::reset(unsigned int type) {
		const char *methodName = "reset";
		callHelper(methodName, type);
	}

	void ScriptableAnalyzer::trace(dword address) {
		const char *methodName = "trace";
		callHelper(methodName, address);
	}

	void ScriptableAnalyzer::step(lword ticks) {
		const char *methodName = "step";
		try {
			// Store tick count
			setNumber("ticks", ticks);

			if( hasMethod(methodName) ) {
				pushMethod(methodName);
				vm->pushNumber(ticks);
				Scriptable::call(1, 0);
			}
		} catch( script::ScriptException & ex ) {
			throw ScriptException( ex.message() );
		}
	}

	void ScriptableAnalyzer::readRegister(unsigned int r, byte val) {
		const char *methodName = "readRegister";
		callHelper(methodName, r, val);
	}

	void ScriptableAnalyzer::writeRegister(unsigned int r, byte val) {
		const char *methodName = "writeRegister";
		callHelper(methodName, r, val);
	}

	void ScriptableAnalyzer::readByte(unsigned int addr, byte val) {
		const char *methodName = "readByte";
		callHelper(methodName, addr, val);
	}

	void ScriptableAnalyzer::writeByte(unsigned int addr, byte val) {
		const char *methodName = "writeByte";
		callHelper(methodName, addr, val);
	}

	void ScriptableAnalyzer::readFlash(unsigned int addr, byte val) {
		const char *methodName = "readFlash";
		callHelper(methodName, addr, val);
	}

	void ScriptableAnalyzer::writeFlash(unsigned int addr, word val) {
		const char *methodName = "writeFlash";
		callHelper(methodName, addr, val);
	}

	void ScriptableAnalyzer::fetchOperand(word val) {
		const char *methodName = "fetchOperand";
		callHelper(methodName, val);
	}

	void ScriptableAnalyzer::push(byte val) {
		const char *methodName = "push";
		callHelper(methodName, val);
	}

	void ScriptableAnalyzer::pop(byte val) {
		const char *methodName = "pop";
		callHelper(methodName, val);
	}

	void ScriptableAnalyzer::jump(dword address, bool push) {
		const char *methodName = "jump";
		callHelperb(methodName, address, push);
	}

	void ScriptableAnalyzer::skip() {
		const char *methodName = "skip";
		callHelper(methodName);
	}

	void ScriptableAnalyzer::call(dword address, bool push) {
		const char *methodName = "call";
		callHelperb(methodName, address, push);
	}

	void ScriptableAnalyzer::ret(bool interrupt) {
		const char *methodName = "ret";
		callHelperb(methodName, interrupt);
	}

	void ScriptableAnalyzer::sleep(unsigned int mode) {
		const char *methodName = "sleep";
		callHelper(methodName, mode);
	}

	void ScriptableAnalyzer::systemBreak() {
		const char *methodName = "systemBreak";
		callHelper(methodName);
	}

	void ScriptableAnalyzer::interrupt(unsigned int vector, unsigned int addr) {
		const char *methodName = "interrupt";
		callHelper(methodName, vector, addr);
	}

	int ScriptableAnalyzer::script_breakSystem(const char *funcName) {
		core->systemBreak();
		return 0;
	}

	int ScriptableAnalyzer::script_register(const char *funcName) {
	    if ( vm->top() != 1 || vm->getType(1) != script::VM::Number )
	        throw script::ScriptException("expects a register index");

	    try {
	    	int r = (int)vm->toNumber(1);
	    	byte val = core->getR( r );
	    	vm->pushNumber( val );
	    	return 1;
	    } catch( util::Exception & ex ) {
	    	throw script::ScriptException( ex.message() );
	    }
	}

	int ScriptableAnalyzer::script_ioregister(const char *funcName) {
	    if ( vm->top() != 1 || vm->getType(1) != script::VM::Number )
	        throw script::ScriptException("expects a io register index");

	    try {
	    	int r = (int)vm->toNumber(1);
	    	byte val = core->getIoreg( r );
	    	vm->pushNumber( val );
	    	return 1;
	    } catch( util::Exception & ex ) {
	    	throw script::ScriptException( ex.message() );
	    }
	}

}
