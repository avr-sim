/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Spi.h"
#include "Registers.h"
#include "Bus.h"
#include <cstring>

enum {
    SPR0 = 1<<0,               /* Clock Rate Select 0   */
    SPR1 = 1<<1,               /* Clock Rate Select 1   */
    CPHA = 1<<2,               /* Clock Phase           */
    CPOL = 1<<3,               /* Clock Polarity        */
    MSTR = 1<<4,               /* Master / Slave Select */
    DORD = 1<<5,               /* Data Order            */
    SPE =  1<<6,               /* SPI Enable            */
    SPIE = 1<<7                /* SPI Interrupt Enable  */
};

enum {
	SPI2X = 1<<0,              /* Double SPI Speed */
    WCOL = 1<<6,               /* Write Collision flag */
    SPIF = 1<<7                /* SPI Interrupt flag   */
};

namespace avr {

	Spi::Spi(Bus & bus, unsigned int stcVec) : Hardware(bus), stcVec(stcVec) {
		bus.claimInterrupt(stcVec, this);
	}

	Spi::~Spi() {
	}

	bool Spi::attachReg(const char *name, IORegister *reg) {
		if( strcmp(name, "spdr") == 0 )
			spdr = reg;
		else if( strcmp(name, "spcr") == 0 )
			spcr = reg;
		else if( strcmp(name, "spsr") == 0 )
			spsr = reg;
		else
			return false;

		reg->registerHW(this);
		return true;
	}

	bool Spi::finishBuild() {
		return ( (spdr != 0) && (spcr != 0) && (spsr != 0) );
	}

	void Spi::setSpcr(unsigned char val) {
		if( ((val & SPE) != 0) && ((spsr_old & SPE) == 0) )
			bus.setBreakDelta(clkDiv, this);

		setClkDiv();
		spsr_old = val;
	}

	void Spi::setSpdr(unsigned char val) {
	    spdrWrite = val;

	    if( ((*spcr & (SPE|MSTR)) == (SPE|MSTR)) ) {
	    	// Enabled and master
            if( state != READY ) {
            	 //Write Collision
                spsr->set( *spsr | WCOL);
            } else {
            	spsr->set( *spsr & ~(SPIF|WCOL) );
            	state = START_MASTER;
            }
	    }
	}

	void Spi::setClkDiv() {
        switch( *spcr & (SPR1|SPR0) ) {
            case 0: clkDiv = 2; break;
            case SPR0: clkDiv = 8; break;
            case SPR1: clkDiv = 32; break;
            case SPR1|SPR0: clkDiv = 64; break;
        }

	    if( (*spsr & SPI2X) != 0 )
	    	clkDiv *= 2;
	}

	void Spi::regChanged( IORegister *reg ) {
		if( reg == spdr ) {
			setSpdr( *spdr );
		} else if( reg == spcr ) {
			setSpcr( *spcr );
		} else if( reg == spsr ) {
			setClkDiv();
		}
	}

	void Spi::step() {
		if( state == START_MASTER ) {
			bitCnt = 0;
			state = MASTER;
		}

		if( state == MASTER ) {
			if( ++bitCnt == 7 ) {
				spsr->set( *spsr | SPIF );
				if( (*spcr & SPIE) != 0 )
					bus.raiseInterrupt(stcVec);

				state = READY;
			}
		}

		bus.setBreakDelta(clkDiv, this);
	}

	void Spi::reset() {
		clkDiv = 0;
		bitCnt = 0;
		spdrWrite = 0;
		spdrRead = 0;
		spsr_old = *spsr;

		state = READY;
	}

	void Spi::beforeInvokeInterrupt(unsigned int vector) {
		if( vector == stcVec )
			spsr->set( *spsr & ~SPIF );
	}

}
