/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Memory.h"
#include <memory.h>
#include <fstream>

#include "Format.h"
#include "AccessViolation.h"

namespace avr {
	Memory::Memory(unsigned int size) : siz(size) {
		mem = new unsigned char[size];
	}

	Memory::~Memory() {
		delete [] mem;
	}

	void Memory::write(unsigned int offset, unsigned char *block, unsigned int size /*= 1*/) {
		if( offset + size > siz )
			throw AccessViolation("Memory::write: writing too much to memory");

		memcpy( mem + offset, block, size );
	}

	void Memory::fill(unsigned char val) {
		memset( mem, val, siz );
	}

	const unsigned char *Memory::read(unsigned int offset, unsigned int size /*= 1*/) const {
		if( offset + size > siz )
			throw AccessViolation("Memory::read: reads too far");

		return mem + offset;
	}

	/**
	 * Reads \e size bytes of raw data from memory starting at
	 * offset \e offset. It returns a pointer to this data.
	 *
	 * \exception RuntimeException { When the data requested is
	 * 		not available, this exception is thrown }
	 */
	byte Memory::readByte(unsigned int offset) const {
		if( offset >= siz )
			throw AccessViolation("Memory::read: reads too far");

		return mem[ offset ];
	}

	word Memory::readWord(unsigned int offset) const {
		if( offset >= siz )
			throw AccessViolation("Memory::read: reads too far");

		return *((word *)(mem + offset));
	}

	void Memory::dump(const char *fname) const {
		std::ofstream fout( fname, std::ios::out | std::ios::binary );
		if( ! fout.is_open() )
			throw AccessViolation( util::format("Memory::dump: failed to open file: %s") % fname );

		fout.write( reinterpret_cast<const char *>(mem), siz );
	}

}
