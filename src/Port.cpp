/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Port.h"
#include "Registers.h"
#include <cstring>

namespace avr {

	Port::Port(Bus & bus, const std::string & name, unsigned int mask)
		: Hardware(bus), name(name), mask(mask) {
	}

	Port::~Port() {
	}

	bool Port::attachReg(const char *name, IORegister *reg) {
		if( strcmp(name, "port") == 0 )
			port = reg;
		else if( strcmp(name, "pin") == 0 )
			pin = reg;
		else if( strcmp(name, "ddr") == 0 )
			ddr = reg;
		else
			return false;

		reg->registerHW(this);
		return true;
	}

	void Port::regChanged( IORegister *reg ) {
		if( reg == port )
		    ;
		else if( reg == pin )
			;
		else if( reg == ddr )
			;
	}

	void Port::step() {

	}

}
