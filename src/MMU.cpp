/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "MMU.h"
#include "Registers.h"
#include "SRam.h"
#include "AccessViolation.h"

namespace avr {

	MMU::MMU(unsigned int ioSpaceSize, unsigned int ramSize, ERam *eram)
		: regs(ioSpaceSize), sram(ramSize), eram(eram) {

		static const char * Rnames[32] = {
			"r0", "r1", "r2", "r3", "r4", "r5",
			"r6", "r7", "r8", "r9", "r10", "r11",
			"r12", "r13", "r14", "r15", "r16", "r17",
			"r18", "r19", "r20", "r21", "r22", "r23",
			"r24", "r25", "r26", "r27", "r28", "r29",
			"r30", "r31"
		};
		for(int i = 0; i < 32; ++i)
			R[i].init(i, Rnames[i]);
	}

	MMU::~MMU() {
//		if( eram != 0 )
//			delete eram;
	}

	Register & MMU::reg(int i) {
		if( (unsigned int)i >= registerSpaceSize )
			throw AccessViolation("Tried to access not existing register");

		return R[i];
	}

	const Register & MMU::reg(int i) const {
		if( (unsigned int)i >= registerSpaceSize )
			throw AccessViolation("Tried to access not existing register");

		return R[i];
	}

	const std::string & MMU::registerName(byte addr) const {
		if( addr < registerSpaceSize )
			return R[addr].getName();
		else
			return regs.getIoreg(addr - registerSpaceSize).getName();
	}

	const unsigned char *MMU::readRam(unsigned int offset, unsigned int size) const {
		if( offset < registerSpaceSize + regs.size() )
			throw AccessViolation("readRam: Tried to read to register area");

		offset -= registerSpaceSize + regs.size();
		return sram.read(offset, size);
	}

	void MMU::writeRam(unsigned char *data, unsigned int offset, unsigned int size) {
		if( offset < registerSpaceSize + regs.size() )
			throw AccessViolation("writeRam: Tried to write to register area");

		offset -= registerSpaceSize + regs.size();
		sram.write(offset, data, size);
	}

	byte MMU::readByte(unsigned int offset) const {
		if( offset < registerSpaceSize )
			return R[offset];

		offset -= registerSpaceSize;
		if( offset < regs.size() )
			return regs.readByte(offset);

		offset -= regs.size();
		if( offset < sram.size() )
			return sram.readByte(offset);

		offset -= sram.size();
//		if( (eram != 0) && (offset < eram->size()) )
//			return eram->readByte(offset);

		throw AccessViolation();
	}

	word MMU::readWord(unsigned int offset) const {
		if( offset < regs.size() + registerSpaceSize )
			throw AccessViolation();

		offset -= regs.size() + registerSpaceSize;
		if( offset < sram.size() )
			return sram.readByte(offset);

		offset -= sram.size();
//		if( (eram != 0) && (offset < eram->size()) )
//			return eram->readByte(offset);

		throw AccessViolation();
	}

	void MMU::writeByte(unsigned int offset, byte val) {
		if( offset < registerSpaceSize )
			R[offset] = val;

		offset -= registerSpaceSize;
		if( offset < regs.size() )
			return regs.writeByte(offset, val);

		offset -= regs.size();
		if( offset < sram.size() )
			return sram.writeByte(offset, val);

		offset -= sram.size();
//		if( (eram != 0) && (offset < eram->size()) )
//			return eram->writeByte(offset, val);

		throw AccessViolation();
	}

	void MMU::writeWord(unsigned int offset, word val) {
		if( offset < regs.size() + registerSpaceSize )
			throw AccessViolation();

		offset -= regs.size() + registerSpaceSize;
		if( offset < sram.size() )
			return sram.writeWord(offset, val);

		offset -= sram.size();
//		if( (eram != 0) && (offset < eram->size()) )
//			return eram->writeByte(offset, val);

		throw AccessViolation();
	}

	void MMU::addIOReg(unsigned int address,
			const std::string & name, byte initial) {
		IORegister *r =
			new IORegister(address, name, initial);
		regs.addReg( address - registerSpaceSize, r );
	}

}
