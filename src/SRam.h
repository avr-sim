#ifndef AVR_SRAM_H
#define AVR_SRAM_H

#include "Memory.h"

namespace avr {

	/**
	 * @author Tom Haber
	 * @date Apr 23, 2008
	 * @brief Static ram inside the AVR chip.
	 *
	 * The static ram inside the chip, provides an interface
	 * for reading and writing. This interface is used by the MMU.
	 */
	class SRam : public Memory {
		public:
			SRam(unsigned int size);

		public:
			/**
			 * Reads a single bytes of raw data from memory at
			 * offset \e offset.
			 *
			 * \exception AccessViolation { When the data requested is
			 * 		not available, this exception is thrown }
			 */
			void writeByte(unsigned int offset, byte val);

			/**
			 * Reads a word of raw data from memory starting at
			 * offset \e offset.
			 *
			 * \exception AccessViolation { When the data requested is
			 * 		not available, this exception is thrown }
			 */
			void writeWord(unsigned int offset, word val);
	};

	inline SRam::SRam(unsigned int size) : Memory(size) {
		fill(0);
	}

	inline void SRam::writeByte(unsigned int offset, byte val) {
		write(offset, &val, 1);
	}

	inline void SRam::writeWord(unsigned int offset, word val) {
		write(offset, (unsigned char *)&val, 2);
	}

}

#endif /*AVR_SRAM_H*/
