/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#ifdef HAVE_LIBBFD
#include "BfdProgram.h"
#include <bfd.h>
#include <iostream>
#include <cstdlib>
#include "Format.h"
#include "RuntimeException.h"

namespace avr {

	void BfdProgram::load(const char *filename) {
		if( abfd != 0 )
			bfd_close(abfd);

		bfd_init();
		abfd = bfd_openr(filename, NULL);

		if (abfd==0)
			throw util::RuntimeException( util::format("Could not open file: %s") % filename );

		bfd_check_format(abfd, bfd_object);
		sec = abfd->sections;

		loadSymbols();
	}

	BfdProgram::~BfdProgram() {
		if( abfd != 0 )
			bfd_close(abfd);
	}

	void BfdProgram::loadSymbols() {
		//reading out the symbols
		int storage_needed = bfd_get_symtab_upper_bound(abfd);
		if( storage_needed < 0 )
			throw util::RuntimeException( "Failed to read symbols from bfd" );

		if( storage_needed == 0 )
			return;

		asymbol **symTable = (asymbol **)malloc(storage_needed);

		int numSymbols = bfd_canonicalize_symtab(abfd, symTable);
		if( numSymbols < 0)
			throw util::RuntimeException( "Failed to read symbols from bfd" );

		for(int i = 0; i < numSymbols; ++i) {
			// if no section data, skip
			if( symTable[i]->section == 0 )
				continue;

			// Skip local symbols
			if( (symTable[i]->flags & BSF_LOCAL) != 0 )
				continue;

			unsigned int lma = symTable[i]->section->lma;
			unsigned int vma = symTable[i]->section->vma;

			if( vma < 0x7fffff ) { //range of flash space
				addSymbolFlash( symTable[i]->value + lma, symTable[i]->name );
			} else if( vma < 0x80ffff ) { //range of ram
				unsigned int offset = vma - 0x800000;
				addSymbolRam( symTable[i]->value + offset, symTable[i]->name );
				addSymbolFlash( symTable[i]->value + lma, symTable[i]->name );
			} else if (vma<0x81ffff) { //range of eeprom
				unsigned int offset = vma - 0x810000;
				addSymbolEeprom( symTable[i]->value + offset, symTable[i]->name );
			} else {
				//throw util::RuntimeException(
				//		util::format( "Unknown symbol address range found!: %s (%lu)")
				//			% symTable[i]->name % vma );
				std::cerr << "Unknown symbol address range found!:"
							<< symTable[i]->name << " (" << vma << ")"
							<< std::endl;
			}
		}

		free(symTable);
	}

	bool BfdProgram::readNextSection(Section & s) {
		if( sec == 0 )
			return false;

		bool found = false;
		do {
			//only read flash bytes and data
			if( ((sec->flags&SEC_LOAD) != 0) && (sec->vma < 0x80ffff) ) {
				int size = sec->size;
				s.init( Section::FLASH, sec->lma, size );
				bfd_get_section_contents(abfd, sec, s.data(), 0, size);
				found = true;
			} else if( ((sec->flags&SEC_LOAD) != 0) && (sec->vma >= 0x810000) ) {
				int size = sec->size;
				s.init( Section::EEPROM, sec->vma - 0x810000, size );
				bfd_get_section_contents(abfd, sec, s.data(), 0, size);
				found = true;
			}

			sec = sec->next;
		} while( ! found && (sec != 0) );

		return found;
	}

}
#endif
