/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Exception.h"
#include <iostream>

#ifdef __GNUC__
#	include <execinfo.h>
#	include <malloc.h>
#   define GNU_SOURCE
#	include <dlfcn.h>
#	include <cxxabi.h>
#endif

#ifdef _WIN32
#	include <SymbolResolve.h>
#endif

#ifdef _DEBUG
#	ifndef DEBUG
#		define DEBUG
#	endif
#endif

namespace util {
	LowLevelException::LowLevelException() {
		msg = "";
		fillInStackTrace();
	}

	LowLevelException::~LowLevelException() {

	}

	LowLevelException::LowLevelException(const LowLevelException & ex) {
#if ! defined(DISABLESTACKTRACE)
		msg = ex.msg;
		trace = ex.trace;
		for(short i = 0; i < trace; ++i) {
			callersAddr[i] = ex.callersAddr[i];
		}
#endif
	}

	#if defined(_MSC_VER) && defined(_M_IX86) && ! defined(DISABLESTACKTRACE)
	static void *getCaller(int index) {
		void *caller = 0;
		__asm
		{
			mov ebx, ebp
			mov ecx, index
			inc ecx
			xor eax, eax
	Exception_getCaller_next:
			mov eax, [ebx+4]
			mov ebx, [ebx]
			dec ecx
			jnz Exception_getCaller_next
			mov caller, eax
		}

		return caller;
	}
	#endif

	/**
	  Creates the stacktrace buffer.
	*/
	void LowLevelException::fillInStackTrace() {
	#if ! defined(DISABLESTACKTRACE)
	#if defined(_MSC_VER) && defined(_M_IX86)
		trace = 1;
		void *addr;
		do {
			addr = getCaller(trace);
			callersAddr[trace++] = addr;
		} while( (addr != 0) && (trace < MAXTRACESIZE) );
	#elif defined(__GNUC__)
		trace = backtrace(callersAddr, MAXTRACESIZE);
	#else
		trace = 0;
	#endif
	#endif
	}

	void LowLevelException::printStackTrace() const {
		printStackTrace( std::cout );
	}

	/**
		Prints this Exception and its backtrace to the
		standard error stream. This method prints a stack trace
		for this exception on the error output stream
		The first line of output contains the result of the getMessage()
		method for this object. Remaining lines represent
		a list of the function calls that are currently active
	*/
	void LowLevelException::printStackTrace(std::ostream & str) const {
	#if ! defined(DISABLESTACKTRACE)
	#if defined(_MSC_VER) && defined(_M_IX86)
		for(unsigned short i = 2; i < trace; ++i) {
			str << '(' << (trace - i) << ")  "
				<< " [" << std::hex << callersAddr[i] << std::dec << "]: ";

#ifndef NO_DBGHELP
			DWORD64 addr = DWORD64(callersAddr[i]);
			PSYMBOL_INFO sym = SymbolResolve::getSymbolFromAddress( addr );
			str << ((sym != 0) ? sym->Name : "???") << std::endl;

			IMAGEHLP_LINE64 *im = SymbolResolve::getSourceFromAddress( addr );
			if( im != 0 )
				str << "\t" << im->FileName << ":" << im->LineNumber << std::endl;
#else
			str << "???" << std::endl;
#endif
		}
	 #elif defined(__GNUC__)
		Dl_info info;
		for(unsigned short i = 2; i < trace; ++i) {
			str << '(' << (trace - i) << ")  ";
			if( (dladdr(callersAddr[i], &info) != 0) && (info.dli_sname != 0) )  {
				const char *name =__cxxabiv1::__cxa_demangle(info.dli_sname, 0, 0, 0);
				if( name != 0 ) {
					str << name;
					free( (void *)name );
				} else {
					str << info.dli_sname;
				}
			}

			str << " [" << std::hex << callersAddr[i] << std::dec << ']' << std::endl;
		}

		/* backtrace_symbols might be more portable but does not do name
		   demangling.
		 */
	#endif
	#endif
	}
}

