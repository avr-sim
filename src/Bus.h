#ifndef AVR_BUS_H
#define AVR_BUS_H

#include <list>
#include <vector>
#include <string>
#include "Clock.h"

namespace avr {

	class Hardware;

	/**
	 * @author Tom Haber
	 * @date Apr 27, 2008
	 * @brief Manages hardware and interrupts
	 *
	 * The bus contains the list of all hardware and serves as
	 * a clock for the hardware (working on CPU ticks).
	 * Hardware can register callbacks within a number of ticks.
	 *
	 * It also encapsulates the functionality related to
	 * handling the state of interrupts.
	 */
	class Bus : public sim::Clock<Hardware> {
		public:
			Bus();
			~Bus();

		public:
			/**
			 * Reset the bus and all hardware on it.
			 */
			void reset();

			/**
			 * Adds a bit of hardware to the bus
			 */
			void addHardware(Hardware *hw);

			/**
			 * Check whether the hardware is holding the CPU.
			 * \warning call this only once per CPU tick!
			 */
			bool isHoldingCPU();

		public:
			static const unsigned int intVectorsSize = 28;

			/**
			 * Post the interrupt with vector \e vector
			 */
			void raiseInterrupt(unsigned int vector);

			/**
			 * Unpost the interrupt with vector \e vector
			 */
			void clearInterrupt(unsigned int vector);

		public:
			/**
			 * Returns the pending interrupt with highest priority.
			 */
			unsigned int pendingInterrupt() const;

			/**
			 * Checks whether an interrupt is pending.
			 */
			bool isInterruptPending() const;

			/**
			 * Returns the memory address associated with the vector.
			 */
			unsigned int interruptVectorAddress(unsigned int vector) const;

			/**
			 * Notifies the hardware associated with this interrupt
			 * that the handler is about to be invoked.
			 */
			void beforeInvokeInterrupt(unsigned int vector);

		public:
			/**
			 * Add an interrupt vector.
			 */
			void addInterrupt(unsigned int vector, unsigned int address,
								const char *name);

			/**
			 * Hardware can claim an interrupt, this means that it is
			 * responsible for raising an interrupt. It will from then
			 * on be notified before an interrupt vector is executed.
			 */
			void claimInterrupt(unsigned int vector, Hardware *hw);

		private:
			std::list<Hardware*> hardware;

		private:
			struct IntVect {
				IntVect() : hw(0) {}
				IntVect(unsigned int addr, const std::string & name)
					: address(addr), name(name), hw(0) {}
				unsigned int address;
				std::string name;
				Hardware *hw;
			};
			typedef std::vector< IntVect > InterruptTable;

		private:
			InterruptTable intVectors;
			unsigned int irqPending;
	};

	inline void Bus::addHardware(Hardware *hw) {
		hardware.push_back( hw );
	}

	inline void Bus::raiseInterrupt(unsigned int vector) {
		irqPending |= (1<<vector);
	}

	inline void Bus::clearInterrupt(unsigned int vector) {
		irqPending &= ~(1<<vector);
	}

	inline bool Bus::isInterruptPending() const {
		return irqPending != 0;
	}

	inline unsigned int Bus::pendingInterrupt() const {
		unsigned int vector;
		for(vector = 0; vector < Bus::intVectorsSize; ++vector)
			if( (irqPending & (1<<vector)) != 0 )
				break;

		return vector;
	}

	inline unsigned int Bus::interruptVectorAddress(unsigned int vector) const {
		return intVectors[vector].address;
	}

}

#endif /*AVR_BUS_H*/
