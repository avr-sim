#ifndef UTIL_EXCEPTION_H
#define UTIL_EXCEPTION_H

#include <string>

#ifndef MAXTRACESIZE
#	define MAXTRACESIZE 20
#endif

namespace util {
	/*!
	  \author Tom Haber
	  $Author: thaber $
	  $Revision: 1602 $
	  $Date: 2007-03-23 11:31:02 +0100 (Fri, 23 Mar 2007) $
	  \brief General Exception class

	  The class LowLevelException and its subclasses indicate
	  conditions that a reasonable application might want to catch.

	  Direct subclasses of LowLevelException (instead of Exception)
	  indicate severe error conditions such as segmentation violations,
	  bad memory accesses, ... It makes sense to only catch LowLevelException
	  in the main() and catch Exception elsewhere.

	  Don't forget to specify -rdynamic for stacktraces in linux
	  In windows you need a program database
	*/
	class LowLevelException {
		public:
			LowLevelException();
			LowLevelException(const LowLevelException & ex);
			virtual ~LowLevelException();

		public:
			const std::string & message() const;
			void printStackTrace() const;
			void printStackTrace(std::ostream & str) const;

		private:
			void fillInStackTrace();

		protected:
			std::string msg;
	#if ! defined(DISABLESTACKTRACE)
			void *callersAddr[MAXTRACESIZE];
			unsigned short trace;
	#endif
	};

	/*!
	  \author Tom Haber
	  $Author: thaber $
	  $Revision: 1602 $
	  $Date: 2007-03-23 11:31:02 +0100 (Fri, 23 Mar 2007) $
	  \brief General Exception class

	  The class Exception and its subclasses indicate conditions
	  that a reasonable application might want to catch.

	  Direct subclasses of LowLevelException (instead of Exception)
	  indicate severe error conditions such as segmentation violations,
	  bad memory accesses, ... It makes sense to only catch LowLevelException
	  in the main() and catch Exception elsewhere.
	 */
	class Exception : public LowLevelException {
		public:
			Exception() {}
			Exception(const char * msg) { this->msg = msg; }
			Exception(const std::string & msg) { this->msg = msg; }
	};

	/*!
		Returns the error message string of this exception.
	 */
	inline const std::string & LowLevelException::message() const {
		return msg;
	}
}
#endif
