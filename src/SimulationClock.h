#ifndef SIM_SIMULATIONCLOCK_H
#define SIM_SIMULATIONCLOCK_H

#include "Clock.h"

namespace sim {

	class SimulationObject;

	/**
	 * @author Tom Haber
	 * @date Apr 27, 2008
	 * @brief Coordinates the timing between the components of the simulation.
	 *
	 * The SimulationClock class is used to coordinate the timing
	 * between the different peripherals within a processor and
	 * in some cases, the timing between several simulated
	 * processors and modules.
	 *
	 * Simulation objects can be notified at a specific
	 * instance in time by setting a break point.
	 */
	class SimulationClock : public Clock<SimulationObject> {
		public:
			SimulationClock() : finished(false), maxTicks(0) {}

		public:
			void exec();
			void finish() { finished = true; }
			void setMaxTicks(ClockOffset ticks) { maxTicks = ticks; }

		private:
			bool finished;
			ClockOffset maxTicks;
	};

}

#endif /*SIM_SIMULATIONCLOCK_H*/
