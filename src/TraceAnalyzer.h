#ifndef AVR_TRACEANALYZER_H
#define AVR_TRACEANALYZER_H

#include "Analyzer.h"
#include "Trace.h"

namespace avr {

	/**
	 * @author Tom Haber
	 * @date May 17, 2008
	 * @brief Tracing Analyzer
	 *
	 * Provides a trace log with some of the Core state changes.
	 * This allows the user to review all instructions executed
	 * by the program and their effect on the registers at a later time.
	 */
	class TraceAnalyzer : public Analyzer {
		public:
			TraceAnalyzer(Core *core, const char *filname);
			~TraceAnalyzer();

		public:
			virtual void reset(unsigned int type);
			virtual void trace(dword address);
			virtual void step(lword ticks);

		public:
			virtual void readRegister(unsigned int r, byte val);
			virtual void writeRegister(unsigned int r, byte val);
			virtual void interrupt(unsigned int vector, unsigned int addr);

		private:
			Trace tracer;
	};

}

#endif /*AVR_TRACEANALYZER_H*/
