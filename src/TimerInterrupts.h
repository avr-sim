#ifndef AVR_TIMERINTERRUPTS_H
#define AVR_TIMERINTERRUPTS_H

#include "Hardware.h"

namespace avr {

	/**
	 * @author Tom Haber
	 * @date May 2, 2008
	 * @brief Timer interrupt hardware
	 *
	 * This class generates the interrupts for all timers.
	 */
	class TimerInterrupts : public Hardware {
		public:
			TimerInterrupts(Bus & bus, unsigned char mask,
							unsigned int bit0Vec, unsigned int bit1Vec,
							unsigned int bit2Vec, unsigned int bit3Vec,
							unsigned int bit4Vec, unsigned int bit5Vec,
							unsigned int bit6Vec, unsigned int bit7Vec);

		public:
			bool attachReg(const char *name, IORegister *reg);
			void regChanged( IORegister *reg );
			bool finishBuild();
			void beforeInvokeInterrupt(unsigned int vector);

		private:
			void checkForNewSetIrq(unsigned char tiac);
			void checkForNewClearIrq(unsigned char tiac);

		private:
			unsigned char mask;
			unsigned char tifrOld;
			unsigned char timskOld;

			unsigned int bit0Vec, bit1Vec,
						bit2Vec, bit3Vec,
						bit4Vec, bit5Vec,
						bit6Vec, bit7Vec;

		private:
			IORegister *tifr;
			IORegister *timsk;
	};

}

#endif /*AVR_TIMERINTERRUPTS_H*/
