#ifndef SIM_CLOCK_H
#define SIM_CLOCK_H

#ifdef __GNUC__
#	include <ext/slist>
#else
#	include <list>
#endif

#include <utility>
#include "Types.h"

namespace sim {
	typedef unsigned long long ClockOffset;

	/**
	 * @author Tom Haber
	 * @date Apr 27, 2008
	 * @brief Template clock class
	 *
	 * This class is used by SimulationClock and Bus to provide the
	 * timings for SimulationObjects and Hardware. It implements
	 * the basic clock functionality: stepping and breakpoints.
	 *
	 * TODO optimize, some methods have a lazy implementation
	 * TODO slist might be too slow (also has portability issues)
	 */
	template <class Type>
	class Clock {
		public:
			Clock() : value(0) {}

		public:
			void setBreak(ClockOffset cycle, Type *obj);
			void setBreakDelta(ClockOffset delta, Type *obj);
			void setDividedBreak(ClockOffset prescaler, ClockOffset delta, Type *obj);
			void clearBreak(Type *obj);
			void clearBreak(ClockOffset cycle);
			void reassignBreak(ClockOffset oldCycle, ClockOffset newCycle, Type *obj);
			void reassignBreak(Type *obj, ClockOffset newCycle);
			void reassignBreakDelta(ClockOffset delta, Type *obj);

		public:
			void step();
			void clearAll();

			ClockOffset ticks() const { return value; }
			bool isEmpty() const { return objects.empty(); }

		private:
			ClockOffset value;

			typedef std::pair<ClockOffset, Type*> Break;
#ifdef __GNUC__
			typedef __gnu_cxx::slist< Break > ClockList;
#else
			typedef std::slist< Break > ClockList;
#endif
			typedef typename ClockList::iterator ClockListIt;
			ClockList objects;
	};

	template <class Type>
	inline void Clock<Type>::setBreakDelta(ClockOffset delta, Type *obj) {
		setBreak( value + delta, obj );
	}

}

#ifndef SIM_CLOCK_INC
#	include "Clock.inc"
#endif
#endif /*SIM_CLOCK_H*/
