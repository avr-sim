#ifndef AVR_CIRCUITNODE_H
#define AVR_CIRCUITNODE_H

namespace avr {

	class CircuitNode {
		public:
			CircuitNode(float V = 5.0f, float Z = 1e3f) : V(V), Z(Z) {}
			virtual ~CircuitNode() {}
			
		public:
			float getVoltage() const { return V; }
			float getImpendace() const { return Z; }
			bool getDigital() const { return V > 0.25f; }
			
		public:
			// Digital
			void toggle();
			void high();
			void low();
			
		public:
			// Analog
			void setVoltage(float V);
			void setImpedance(float Z);
			
		private:
			float V;	// Voltage
			float Z;	// Impedance
	};
	
	inline void CircuitNode::toggle() {
		V = getDigital() ? 0.0f : 5.0f;
	}
	
	inline void CircuitNode::high() {
		V = 5.0f;
	}
	
	inline void CircuitNode::low() {
		V = 0.0f;
	}

}

#endif /*AVR_CIRCUITNODE_H*/
