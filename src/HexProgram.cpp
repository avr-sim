/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "HexProgram.h"
#include "Format.h"
#include "RuntimeException.h"

#include <string>
#include <iostream>
#include <memory.h>

namespace avr {

	void HexProgram::load(const char *filename) {
		if( f.is_open() )
			f.close();

		f.open( filename );

		if( ! f.is_open() )
			throw util::RuntimeException( util::format("Could not open file: %s") % filename );

		end = false;
		offset = 0;
	}

	HexProgram::~HexProgram() {
		if( f.is_open() )
			f.close();
	}

	/* Convert a hexidecimal digit to a 4 bit nibble. */
	static int hex2nib( char hex ) {
	    if( (hex >= 'A') && (hex <= 'F') )
	        return (10 + (hex - 'A'));

	    else if( (hex >= 'a') && (hex <= 'f') )
	        return (10 + (hex - 'a'));

	    else if( (hex >= '0') && (hex <= '9') )
	        return (hex - '0');

	    return -1;
	}

	static bool hex2bytes(const std::string & hex, size_t length,
							unsigned char bytes[]) {

		size_t off = 1;
		for(size_t i = 0; i < length; ++i) {
			int valh = hex2nib( hex[off++] );
			int vall = hex2nib( hex[off++] );
			if( (valh == -1) && (vall == -1) )
				return false;

			bytes[i] = (valh << 4) | vall;
		}

		return true;
	}

	bool HexProgram::readNextSection(Section & s) {
		if( end || f.eof() )
			return false;

		bool found = false;
		do {
			std::string line;
			std::getline(f, line);

			if( line[0] != ':' )
				continue;

			size_t length = (line.size() - 1) / 2;
			unsigned char bytes[length];
			hex2bytes(line, length, bytes);

			unsigned char crc = 0;
			for(size_t i = 0; i < length; ++i)
				crc += bytes[i];

			if( crc != 0 ) {
				std::cerr << "Invalid crc checksum" << std::endl;
				continue;
			}

			const unsigned char rec_length = bytes[0];
			if( length - 5 != rec_length ) {
				std::cerr << "Invalid record length" << std::endl;
				continue;
			}

			unsigned int addr = bytes[1] << 16 | bytes[2];

			const unsigned char rec_type = bytes[3];
			switch( rec_type ) {
				case 0: // Data record
					s.init(Section::FLASH, addr + offset, rec_length);
					memcpy( s.data(), &bytes[4], rec_length );
					found = true;
					break;

				case 1:	// End-of-file record
					end = true;
					found = true;
					break;

				case 2: // Extended 8086 Segment Record
		            if( rec_length != 2 ) {
		                std::cerr <<  "Bad Extended 8086 Segment Record" << std::endl;
		                continue;
		            }

		            offset = (bytes[4]<<16 | bytes[5]) * 16;
		            break;

				case 3:  // Start Segment Address Record
					break;

				case 4: // Extended Linear Address Record
		            if( rec_length != 2 ) {
		                std::cerr <<  "Bad Extended Linear Address Record" << std::endl;
		                continue;
		            }

		            offset = (bytes[4]<<16 | bytes[5]) * 65536;
					break;

				case 5: // Start Linear Address Record
					break;
			}

		} while( ! found && ! f.eof() );

		return found;
	}

}
