/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Trace.h"
#include "Core.h"
#include "VerboseInfo.h"
#include "DebugInterface.h"

namespace avr {

	Trace::Trace() {
		for (traceIndex = 0; traceIndex < traceBufferSize; traceIndex++)
			traceBuffer[traceIndex] = NOTHING;

		traceIndex = 0;
	}

	Trace::~Trace() {
		if( fout.is_open() )
			fout.close();
	}

	// type() - return the trace type at 'index'
	unsigned int Trace::type(unsigned int index) const {
		unsigned int traceType = get(index) & TYPE_MASK;
		unsigned int cycleType = traceType & (CYCLE_COUNTER_LO
		        | CYCLE_COUNTER_HI);
		return cycleType ? cycleType : traceType;
	}

	// When logging is enabled, the entire trace buffer will be copied to a file.
	void Trace::enableLogging(const char *fname) {
		fout.open( fname, std::ios::out | std::ios::binary );
	}

	void Trace::disableLogging() {
		fout.close();
	}

	int Trace::isCycleTrace(unsigned int index, lword *cycle) const {
		if ( (get(index) & (CYCLE_COUNTER_LO | CYCLE_COUNTER_HI)) == 0)
			return 0;

		// Cycle counter

		// A cycle counter occupies two consecutive trace buffer entries.
		// We have to determine if the current entry (pointed to by index) is
		// the high or low integer of the cycle counter.
		//
		// The upper two bits of the trace are used to decode the two 32-bit
		// integers that comprise the cycle counter. The encoding algorithm is
		// optimized for speed:
		// CYCLE_COUNTER_LO is defined as 1<<31
		// CYCLE_COUNTER_HI is defined as 1<<30
		//
		//   trace[i] = low 32 bits of cycle counter | CYCLE_COUNTER_LO
		//   trace[i+1] = upper 32 bits of "    " | CYCLE_COUNTER_HI | bit 31 of cycle counter
		//
		// The low 32-bits are always saved in the trace buffer with the msb (CYCLE_COUNTER_LO)
		// set. However, notice that this bit may've already been set prior to calling trace().
		// So we need to make sure that we don't lose it. This is done by copying it along
		// with the high 32-bits of the cycle counter into the next trace buffer location. The
		// upper 2 bits of the cycle counter are assumed to always be zero (if they're not, gpsim
		// has been running for a loooonnnggg time!). Bit 30 (CYCLE_COUNTER_HIGH) is always
		// set in the high 32 bit trace. While bit 31 gets the copy of bit 31 that was over
		// written in the low 32 bit trace.
		//
		// Here are some examples:
		//                                                upper 2 bits
		//    cycle counter    |  trace[i]    trace[i+1]    [i]   [i+1]
		//---------------------+----------------------------------------
		//         0x12345678  |  0x92345678  0x40000000    10     01
		//         0x44445555  |  0xc4445555  0x40000000    11     01
		// 0x1111222233334444  |  0xb3334444  0x51112222    10     01
		//         0x9999aaaa  |  0x9999aaaa  0xc0000000    10     11
		//         0xccccdddd  |  0xccccdddd  0xc0000000    11     11
		//         0xccccddde  |  0xccccddde  0xc0000000    11     11
		//
		// Looking at the upper two bits of trace buffer, we can make these
		// observations:
		//
		// 00 - not a cycle counter trace
		// 10 - current index points at the low int of a cycle counter
		// 01 - current index points at the high int of a cycle counter
		// 11 - if traces on either side of the current index are the same
		//      then the current index points to a low int else it points to a high int

		int j = index; // Assume that the index is pointing to the low int.
		int k = (j + 1) & traceBufferMask; // and that the next entry is the high int.

		if ( ((get(j) & CYCLE_COUNTER_LO) != 0)&& ((get(k) & CYCLE_COUNTER_HI))
		        != 0) {
			if ( (get(j) & CYCLE_COUNTER_HI) != 0) {
				// The upper two bits of the current trace are set. This means that
				// the trace is either the high 32 bits or the low 32 bits of the cycle
				// counter. This ambiguity is resolved by examining the trace buffer on
				// either side of the current index. If the entry immediately proceeding
				// this one is not a cycle counter trace, then we know that we're pointing
				// at the low 32 bits. If the proceeding entry IS a cycle counter trace then
				// we have two consecutive cycle traces (we already know that the entry
				// immediately following the current trace index is a cycle counter trace).
				// Now we know that if  have consecutive cycle traces, then they differ by one
				// count. We only need to look at the low 32 bits of these consecutive
				// traces to ascertain this.
				int i = (index - 1) & traceBufferMask; // previous index
				if ( ((get(i) & (CYCLE_COUNTER_HI | CYCLE_COUNTER_LO)) != 0)
				        &&( ((get(k) - get(i)) & 0x7fffffff) == 1))
					return 1;
			}

			// The current index points to the low int and the next entry is
			// the high int.
			// extract the ~64bit cycle counter from the trace buffer.
			if (cycle != 0) {
				*cycle = get(k) & 0x3fffffff;
				*cycle = (*cycle << 32) | ((get(j) & 0x7fffffff) | (get(k)
				        & 0x80000000));
			}

			return 2;

		}

		return 1;
	}

#define hexchar(a) std::hex << int(a) << std::dec
	void Trace::print(unsigned index, std::ostream & ostr,
					DebugInterface & dbgi, int /*verbose*/) const {
		lword cycle;
		if( isCycleTrace(index, &cycle) == 2 )
			return;

		switch( type(index) ) {
			case NOTHING:
				ostr << "empty trace cycle"<< std::endl;
				break;

			case CYCLE_COUNTER_HI:
				info(DBG, ostr) << "Cycle: " << cycle << std::endl;
				break;

			case RESET: {
					switch (get(index) & 0xff) {
						case POR_RESET:
							ostr << "Power-on reset"<< std::endl;
							break;

						case WDT_RESET:
							ostr << "WDT reset"<< std::endl;
							break;

						case JTAG_RESET:
							ostr << "JTAG reset"<< std::endl;
							break;

						case EXT_RESET:
							ostr << "External reset"<< std::endl;
							break;

						case SOFT_RESET:
							ostr << "Software initiated reset"<< std::endl;
							break;

						case BOD_RESET:
							ostr << "Brown out detection reset"<< std::endl;
							break;

						case SIM_RESET:
							ostr << "Simulation Reset"<< std::endl;
							break;

						default:
							ostr << "unknown reset"<< std::endl;
					}
				}
				break;

			case OPCODE_WRITE: {
					if (type(index-1) == OPCODE_WRITE )
						ostr << "wrote opcode: " << std::hex << (get(index)&0xffff)
						        << "to pgm memory: "<< (get(index - 1) & 0xffffff)
						        << std::dec << std::endl;
				}
				break;

			case REG_WRITE: {
					byte reg = ((get(index)&0xffff00) >> 8);
					ostr << "Registers write: address "
						<<  dbgi.registerName( reg )
						<< " value = " << hexchar(get(index)&0xff)
						<< std::endl;
				}
				break;

			case REG_READ: {
					byte reg = ((get(index)&0xffff00) >> 8);
					ostr << "Registers read: address "
						<<  dbgi.registerName( reg )
						<< " value = " << hexchar(get(index)&0xff)
						<< std::endl;
				}
				break;

			case PC_TRACE: {
					dword addr = (get(index)&0xffffff);
					ostr << "PC trace: address "
						<< std::hex << addr << std::dec << ": ";
					dbgi.trace( ostr, addr );
					ostr << std::endl;
				}
				break;

			default:
				;
		}
	}

	void Trace::printFrom(unsigned index, std::ostream & ostr, Device & dev, int verbose) const {
		DebugInterface *dbgi = dev.debugInterface();
		while( index < traceIndex ) {
			print( index, ostr, *dbgi, verbose );
			index++;
		}

		delete dbgi;
	}

	void Trace::print(std::ostream & ostr, Device & dev, int verbose) const {
		DebugInterface *dbgi = dev.debugInterface();
		unsigned int i = tbi(traceIndex-2);
		unsigned int k = tbi(traceIndex-1);

		if( isCycleTrace(i, 0) != 2 )
			return;

		unsigned int frame_start = tbi(traceIndex-2);
		unsigned int frame_end = traceIndex;

		while( inRange(k,frame_end,frame_start) ) {
			print(k, ostr, *dbgi, verbose);
			k = tbi(k-1);
		}

		delete dbgi;
	}

	void Trace::list(const char *filename, std::ostream & ostr, Device & dev, int verbose) {
		std::ifstream f( filename, std::ios::in | std::ios::binary );
		if( ! f.is_open() )
			return;

		DebugInterface *dbgi = dev.debugInterface();
		traceIndex = traceBufferSize;
		bool done = false;
		while( !done ) {
			f.read( (char*)traceBuffer, traceBufferSize * sizeof(unsigned int) );
			if( ! f ) {
				traceIndex = f.gcount() / sizeof(unsigned int);
				done = true;
			}

			for(unsigned int index = 0; index < traceIndex; ++index)
				print( index, ostr, *dbgi, verbose );
		}

		delete dbgi;
	}

}
