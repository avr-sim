#ifndef AVR_INSTRUCTIONS_H
#define AVR_INSTRUCTIONS_H

#include "Instruction.h"
#include "Types.h"

#include <iostream>

namespace avr {

	class Register;
	class Core;

	namespace op {

		class ADC : public Instruction {
		    /*
		     * Add with Carry.
		     *
		     * Opcode     : 0001 11rd dddd rrrr
		     * Usage      : ADC  Rd, Rr
		     * Operation  : Rd <- Rd + Rr + C
		     * Flags      : Z,C,N,V,S,H
		     * Num Clocks : 1
		     */

		    public:
		        ADC(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		        unsigned char Rd;
		        unsigned char Rr;

		};

		class ADD : public Instruction {
		    /*
		     * Add without Carry.
		     *
		     * Opcode     : 0000 11rd dddd rrrr
		     * Usage      : ADD  Rd, Rr
		     * Operation  : Rd <- Rd + Rr
		     * Flags      : Z,C,N,V,S,H
		     * Num Clocks : 1
		     */

		    public:
		        ADD(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		        unsigned char Rd;
		        unsigned char Rr;
		};

		class ADIW : public Instruction {
		    /*
		     * Add Immediate to Word.
		     *
		     * Opcode     : 1001 0110 KKdd KKKK
		     * Usage      : ADIW  Rd, K
		     * Operation  : Rd+1:Rd <- Rd+1:Rd + K
		     * Flags      : Z,C,N,V,S
		     * Num Clocks : 2
		     */

		    public:
		        ADIW(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		        unsigned char Rl;
		        unsigned char Rh;
		        unsigned char K;
		};

		class AND : public Instruction {
		    /*
		     * Logical AND.
		     *
		     * Opcode     : 0010 00rd dddd rrrr
		     * Usage      : AND  Rd, Rr
		     * Operation  : Rd <- Rd & Rr
		     * Flags      : Z,N,V,S
		     * Num Clocks : 1
		     */

		    public:
		        AND(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		        unsigned char Rd;
		        unsigned char Rr;
		};

		class ANDI : public Instruction {
		    /*
		     * Logical AND with Immed.
		     *
		     * Opcode     : 0111 KKKK dddd KKKK
		     * Usage      : ANDI  Rd, K
		     * Operation  : Rd <- Rd & K
		     * Flags      : Z,N,V,S
		     * Num Clocks : 1
		     */

		    public:
		        ANDI(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char K;

		};

		class ASR : public Instruction {
		    /*
		     * Arithmetic Shift Right.
		     *
		     * Opcode     : 1001 010d dddd 0101
		     * Usage      : ASR  Rd
		     * Operation  : Rd(n) <- Rd(n+1), n=0..6
		     * Flags      : Z,C,N,V,S
		     * Num Clocks : 1
		     */

		    public:
		        ASR(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class BCLR : public Instruction {
		    /*
		     * Clear a single flag or bit in SREG.
		     *
		     * Opcode     : 1001 0100 1sss 1000
		     * Usage      : BCLR
		     * Operation  : SREG(s) <- 0
		     * Flags      : SREG(s)
		     * Num Clocks : 1
		     */

		    public:
		        BCLR(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char K;
		};

		class BLD : public Instruction {
		    /* Bit load from T to Register.
		     *
		     * Opcode     : 1111 100d dddd 0bbb
		     * Usage      : BLD  Rd, b
		     * Operation  : Rd(b) <- T
		     * Flags      : None
		     * Num Clocks : 1
		     */

		    public:
		        BLD(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char Kadd, Kremove;
		};

		class BRBC : public Instruction {
		    /*
		     * Branch if Status Flag Cleared.
		     *
		     * Pass control directly to the specific bit operation.
		     *
		     * Opcode     : 1111 01kk kkkk ksss
		     * Usage      : BRBC  s, k
		     * Operation  : if (SREG(s) = 0) then PC <- PC + k + 1
		     * Flags      : None
		     * Num Clocks : 1 / 2
		     *
		     * k is an relative address represented in two's complements.
		     * (64 < k <= 64)
		     */

		    public:
		        BRBC(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char bitmask;
		    	signed char offset;
		};

		class BRBS : public Instruction	{
		    /*
		     * Branch if Status Flag Set.
		     *
		     * Pass control directly to the specific bit operation.
		     *
		     * Opcode     : 1111 00kk kkkk ksss
		     * Usage      : BRBS  s, k
		     * Operation  : if (SREG(s) = 1) then PC <- PC + k + 1
		     * Flags      : None
		     * Num Clocks : 1 / 2
		     *
		     * k is an relative address represented in two's complements.
		     * (64 < k <= 64)
		     */

		    public:
		        BRBS(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char bitmask;
		    	signed char offset;
		};

		class BSET : public Instruction {
		    /*
		     * Set a single flag or bit in SREG.
		     *
		     * Opcode     : 1001 0100 0sss 1000
		     * Usage      : BSET
		     * Operation  : SREG(s) <- 1
		     * Flags      : SREG(s)
		     * Num Clocks : 1
		     */

		    public:
		        BSET(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char K;
		};

		class BST:public Instruction
		{
		    /*
		     * Bit Store from Register to T.
		     *
		     * Opcode     : 1111 101d dddd 0bbb
		     * Usage      : BST  Rd, b
		     * Operation  : T <- Rd(b)
		     * Flags      : T
		     * Num Clocks : 1
		     */

		    public:
		        BST(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char K;

		};

		class CALL : public Instruction {
		    /*
		     * Call Subroutine.
		     *
		     * Opcode     : 1001 010k kkkk 111k kkkk kkkk kkkk kkkk
		     * Usage      : CALL  k
		     * Operation  : PC <- k
		     * Flags      : None
		     * Num Clocks : 4 / 5
		     */

		    public:
		        CALL(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		        bool is2Word() const { return true; }

		    protected:
		        unsigned char KH;
		};

		class CBI : public Instruction {
		    /*
		     * Clear Bit in I/O Register.
		     *
		     * Opcode     : 1001 1000 AAAA Abbb
		     * Usage      : CBI  A, b
		     * Operation  : I/O(A, b) <- 0
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        CBI (word opcode);
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char ioreg;
		    	unsigned char K;
		};

		class COM : public Instruction {
		    /*
		     * One's Complement.
		     *
		     * Opcode     : 1001 010d dddd 0000
		     * Usage      : COM  Rd
		     * Operation  : Rd <- $FF - Rd
		     * Flags      : Z,C,N,V,S
		     * Num Clocks : 1
		     */

		    public:
		        COM (word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class CP : public Instruction {
		    /*
		     * Compare.
		     *
		     * Opcode     : 0001 01rd dddd rrrr
		     * Usage      : CP  Rd, Rr
		     * Operation  : Rd - Rr
		     * Flags      : Z,C,N,V,S,H
		     * Num Clocks : 1
		     */

		    public:
		        CP (word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char Rr;
		};

		class CPC : public Instruction {
		    /*
		     * Compare with Carry.
		     *
		     * Opcode     : 0000 01rd dddd rrrr
		     * Usage      : CPC  Rd, Rr
		     * Operation  : Rd - Rr - C
		     * Flags      : Z,C,N,V,S,H
		     * Num Clocks : 1
		     */

		    public:
		        CPC (word opcode);
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char Rr;
		};

		class CPI : public Instruction {
		    /*
		     * Compare with Immediate.
		     *
		     * Opcode     : 0011 KKKK dddd KKKK
		     * Usage      : CPI  Rd, K
		     * Operation  : Rd - K
		     * Flags      : Z,C,N,V,S,H
		     * Num Clocks : 1
		     */

		    public:
		        CPI (word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;


		    protected:
		    	unsigned char Rd;
		    	unsigned char K;
		};

		class CPSE : public Instruction {
		    /*
		     * Compare, Skip if Equal.
		     *
		     * Opcode     : 0001 00rd dddd rrrr
		     * Usage      : CPSE  Rd, Rr
		     * Operation  : if (Rd = Rr) PC <- PC + 2 or 3
		     * Flags      : None
		     * Num Clocks : 1 / 2 / 3
		     */

		    public:
		        CPSE (word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char Rr;
		};

		class DEC : public Instruction {
		    /*
		     * Decrement.
		     *
		     * Opcode     : 1001 010d dddd 1010
		     * Usage      : DEC  Rd
		     * Operation  : Rd <- Rd - 1
		     * Flags      : Z,N,V,S
		     * Num Clocks : 1
		     */

		    public:
		        DEC (word opcode);
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class EICALL : public Instruction {
		    /*
		     * Extended Indirect Call to (Z).
		     *
		     * Opcode     : 1001 0101 0001 1001
		     * Usage      : EICALL
		     * Operation  : PC(15:0) <- Z, PC(21:16) <- EIND
		     * Flags      : None
		     * Num Clocks : 4
		     */

		    public:
		        EICALL(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char eind;
		};

		class EIJMP : public Instruction {
		    /*
		     * Extended Indirect Jmp to (Z).
		     *
		     * Opcode     : 1001 0100 0001 1001
		     * Usage      : EIJMP
		     * Operation  : PC(15:0) <- Z, PC(21:16) <- EIND
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        EIJMP (word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char eind;
		};

		class ELPM_Z : public Instruction {
		    /*
		     * Extended Load Program Memory.
		     *
		     * Opcode     : 1001 000d dddd 0110
		     * Usage      : ELPM  Rd, Z
		     * Operation  : R <- (RAMPZ:Z)
		     * Flags      : None
		     * Num Clocks : 3
		     */

		    public:
		        ELPM_Z (word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class ELPM_Z_incr:public Instruction {
		    /*
		     * Extended Ld Prg Mem and Post-Incr.
		     *
		     * Opcode     : 1001 000d dddd 0111
		     * Usage      : ELPM  Rd, Z+
		     * Operation  : Rd <- (RAMPZ:Z), Z <- Z + 1
		     * Flags      : None
		     * Num Clocks : 3
		     */

		    public:
		        ELPM_Z_incr(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class ELPM : public Instruction {
		    /*
		     * Extended Load Program Memory.
		     *
		     *
		     * Opcode     : 1001 0101 1101 1000
		     * Usage      : ELPM
		     * Operation  : R0 <- (RAMPZ:Z)
		     * Flags      : None
		     * Num Clocks : 3
		     */

		    public:
		        ELPM(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		};

		class EOR:public Instruction {
		    /*
		     * Exclusive OR.
		     *
		     * Opcode     : 0010 01rd dddd rrrr
		     * Usage      : EOR  Rd, Rr
		     * Operation  : Rd <- Rd ^ Rr
		     * Flags      : Z,N,V,S
		     * Num Clocks : 1
		     */

		    public:
		        EOR(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char Rr;
		};

		class ESPM : public Instruction {
		    /*
		     * Extended Store Program Memory.
		     *
		     * Opcode     : 1001 0101 1111 1000
		     * Usage      : ESPM
		     * Operation  : Z <- R1:R0
		     * Flags      : None
		     * Num Clocks : -
		     */

		    public:
		        ESPM(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		};

		class FMUL:public Instruction {
		    /*
		     * Fractional Mult Unsigned.
		     *
		     * Opcode     : 0000 0011 0ddd 1rrr
		     * Usage      : FMUL  Rd, Rr
		     * Operation  : R1:R0 <- (Rd * Rr)<<1 (UU)
		     * Flags      : Z,C
		     * Num Clocks : 2
		     */

		    public:
		        FMUL(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char Rr;
		};

		class FMULS : public Instruction {
		    /*
		     * Fractional Mult Signed.
		     *
		     * Opcode     : 0000 0011 1ddd 0rrr
		     * Usage      : FMULS  Rd, Rr
		     * Operation  : R1:R0 <- (Rd * Rr)<<1 (SS)
		     * Flags      : Z,C
		     * Num Clocks : 2
		     */

		    public:
		        FMULS(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char Rr;
		};

		class FMULSU : public Instruction {
		    /*
		     * Fract Mult Signed w/ Unsigned.
		     *
		     * Opcode     : 0000 0011 1ddd 1rrr
		     * Usage      : FMULSU  Rd, Rr
		     * Operation  : R1:R0 <- (Rd * Rr)<<1 (SU)
		     * Flags      : Z,C
		     * Num Clocks : 2
		     */

		    public:
		        FMULSU (word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char Rr;
		};

		class ICALL : public Instruction {
		    /*
		     * Indirect Call to (Z).
		     *
		     * Opcode     : 1001 0101 0000 1001
		     * Usage      : ICALL
		     * Operation  : PC(15:0) <- Z, PC(21:16) <- 0
		     * Flags      : None
		     * Num Clocks : 3 / 4
		     */

		    public:
		        ICALL(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		};

		class IJMP : public Instruction {
		    /*
		     * Indirect Jump to (Z).
		     *
		     * Opcode     : 1001 0100 0000 1001
		     * Usage      : IJMP
		     * Operation  : PC(15:0) <- Z, PC(21:16) <- 0
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        IJMP(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		};

		class IN : public Instruction {
		    /*
		     * In From I/O Location.
		     *
		     * Opcode     : 1011 0AAd dddd AAAA
		     * Usage      : IN  Rd, A
		     * Operation  : Rd <- I/O(A)
		     * Flags      : None
		     * Num Clocks : 1
		     */

		    public:
		        IN (word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char ioreg;
		};

		class INC : public Instruction {
		    /*
		     * Increment.
		     *
		     * Opcode     : 1001 010d dddd 0011
		     * Usage      : INC  Rd
		     * Operation  : Rd <- Rd + 1
		     * Flags      : Z,N,V,S
		     * Num Clocks : 1
		     */

		    public:
		        INC (word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class JMP : public Instruction {
		    /*
		     * Jump.
		     *
		     * Opcode     : 1001 010k kkkk 110k kkkk kkkk kkkk kkkk
		     * Usage      : JMP  k
		     * Operation  : PC <- k
		     * Flags      : None
		     * Num Clocks : 3
		     */

		    public:
		        JMP(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		        bool is2Word() const { return true; }

		    protected:
		        unsigned int K;
		};

		class LDD_Y : public Instruction {
		    /*
		     * Load Indirect with Displacement using index Y.
		     *
		     * Opcode     : 10q0 qq0d dddd 1qqq
		     * Usage      : LDD  Rd, Y+q
		     * Operation  : Rd <- (Y + q)
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        LDD_Y(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char K;
		};

		class LDD_Z : public Instruction {
		    /*
		     * Load Indirect with Displacement using index Z.
		     *
		     * Opcode     : 10q0 qq0d dddd 0qqq
		     * Usage      : LDD  Rd, Z+q
		     * Operation  : Rd <- (Z + q)
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        LDD_Z(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char K;
		};

		class LDI : public Instruction {
		    /*
		     * Load Immediate.
		     *
		     * Opcode     : 1110 KKKK dddd KKKK
		     * Usage      : LDI  Rd, K
		     * Operation  : Rd  <- K
		     * Flags      : None
		     * Num Clocks : 1
		     */

		    public:
		        LDI(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char K;
		};

		class LDS : public Instruction {
		    /*
		     * Load Direct from data space.
		     *
		     * Opcode     : 1001 000d dddd 0000 kkkk kkkk kkkk kkkk
		     * Usage      : LDS  Rd, k
		     * Operation  : Rd <- (k)
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        LDS(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		        bool is2Word() const { return true; }

		    protected:
		    	unsigned char Rd;
		};

		class LD_X : public Instruction {
		    /*
		     * Load Indirect using index X.
		     *
		     * Opcode     : 1001 000d dddd 1100
		     * Usage      : LD  Rd, X
		     * Operation  : Rd <- (X)
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        LD_X(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class LD_X_decr : public LD_X {
		    /*
		     * Load Indirect and Pre-Decrement using index X.
		     *
		     * Opcode     : 1001 000d dddd 1110
		     * Usage      : LD  Rd, -X
		     * Operation  : X <- X - 1, Rd <- (X)
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        LD_X_decr(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		};

		class LD_X_incr : public LD_X {
		    /*
		     * Load Indirect and Post-Increment using index X.
		     *
		     * Opcode     : 1001 000d dddd 1101
		     * Usage      : LD  Rd, X+
		     * Operation  : Rd <- (X), X <- X + 1
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        LD_X_incr(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		};

		class LD_Y_decr : public Instruction {
		    /*
		     * Load Indirect and PreDecrement using index Y.
		     *
		     * Opcode     : 1001 000d dddd 1010
		     * Usage      : LD  Rd, -Y
		     * Operation  : Y <- Y - 1, Rd <- (Y)
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        LD_Y_decr(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class LD_Y_incr : public Instruction {
		    /*
		     * Load Indirect and Post-Increment using index Y.
		     *
		     * Opcode     : 1001 000d dddd 1001
		     * Usage      : LD  Rd, Y+
		     * Operation  : Rd <- (Y), Y <- Y + 1
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        LD_Y_incr(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class LD_Z_incr : public Instruction {
		    /*
		     * Load Indirect and Post-Increment using index Z.
		     *
		     * Opcode     : 1001 000d dddd 0001
		     * Usage      : LD  Rd, Z+
		     * Operation  : Rd <- (Z), Z <- Z+1
		     * Flags      : None
		     * Num Clocks : 2
			 */

		    public:
		        LD_Z_incr(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class LD_Z_decr : public Instruction {
		    /*
		     * Load Indirect and Pre-Decrement using index Z.
		     *
		     * Opcode     : 1001 000d dddd 0010
		     * Usage      : LD  Rd, -Z
		     * Operation  : Z <- Z - 1, Rd <- (Z)
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        LD_Z_decr(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class LPM_Z : public Instruction {
		    /*
		     * Load Program Memory.
		     *
		     * Opcode     : 1001 000d dddd 0100
		     * Usage      : LPM  Rd, Z
		     * Operation  : Rd <- (Z)
		     * Flags      : None
		     * Num Clocks : 3
		     */

		    public:
		        LPM_Z(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class LPM : public Instruction {
		    /* Load Program Memory.
		     *
		     * This the same as LPM_Z:public Instruction
		     *
		     * Opcode     : 1001 0101 1100 1000
		     * Usage      : LPM
		     * Operation  : R0 <- (Z)
		     * Flags      : None
		     * Num Clocks : 3
		     */

		    public:
		        LPM(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		};

		class LPM_Z_incr : public Instruction {
		    /*
		     * Load Program Memory and Post-Incr.
		     *
		     * Opcode     : 1001 000d dddd 0101
		     * Usage      : LPM  Rd, Z+
		     * Operation  : Rd <- (Z), Z <- Z + 1
		     * Flags      : None
		     * Num Clocks : 3
		     */

		    public:
		        LPM_Z_incr(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class LSR : public Instruction {
		    /*
		     * Logical Shift Right.
		     *
		     * Opcode     : 1001 010d dddd 0110
		     * Usage      : LSR  Rd
		     * Operation  : Rd(n) <- Rd(n+1), Rd(7) <- 0, C <- Rd(0)
		     * Flags      : Z,C,N,V,S
		     * Num Clocks : 1
		     */

		    public:
		        LSR(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class MOV:public Instruction {
		    /* Copy Register.
		     *
		     * Opcode     : 0010 11rd dddd rrrr
		     * Usage      : MOV  Rd, Rr
		     * Operation  : Rd <- Rr
		     * Flags      : None
		     * Num Clocks : 1
		     */

		    public:
		        MOV(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char Rr;
		};

		class MOVW:public Instruction {
		    /*
		     *Copy Register Pair.
		     *
		     * Opcode     : 0000 0001 dddd rrrr
		     * Usage      : MOVW  Rd, Rr
		     * Operation  : Rd+1:Rd <- Rr+1:Rr
		     * Flags      : None
		     * Num Clocks : 1
		     */

		    public:
		        MOVW(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rdl;
		    	unsigned char Rdh;
		    	unsigned char Rrl;
		    	unsigned char Rrh;
		};

		class MUL : public Instruction {
		    /*
		     * Mult Unsigned.
		     *
		     * Opcode     : 1001 11rd dddd rrrr
		     * Usage      : MUL  Rd, Rr
		     * Operation  : R1:R0 <- Rd * Rr (UU)
		     * Flags      : Z,C
		     * Num Clocks : 2
		     */

		    public:
		        MUL(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char Rr;
		};

		class MULS : public Instruction {
		    /*
		     * Mult Signed.
		     *
		     * Opcode     : 0000 0010 dddd rrrr
		     * Usage      : MULS  Rd, Rr
		     * Operation  : R1:R0 <- Rd * Rr (SS)
		     * Flags      : Z,C
		     * Num Clocks : 2
		     */

		    public:
		        MULS(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char Rr;
		};

		class MULSU:public Instruction {
		    /*
		     * Mult Signed with Unsigned.
		     *
		     * Rd(unsigned),Rr(signed), result (signed)
		     *
		     * Opcode     : 0000 0011 0ddd 0rrr
		     * Usage      : MULSU  Rd, Rr
		     * Operation  : R1:R0 <- Rd * Rr (SU)
		     * Flags      : Z,C
		     * Num Clocks : 2
		     */

		    public:
		        MULSU(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char Rr;
		};

		class NEG : public Instruction {
		    /*
		     * Two's Complement.
		     *
		     * Opcode     : 1001 010d dddd 0001
		     * Usage      : NEG  Rd
		     * Operation  : Rd <- $00 - Rd
		     * Flags      : Z,C,N,V,S,H
		     * Num Clocks : 1
		     */

		    public:
		        NEG (word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class NOP : public Instruction {
		    /*
		     * No Operation.
		     *
		     * Opcode     : 0000 0000 0000 0000
		     * Usage      : NOP
		     * Operation  : None
		     * Flags      : None
		     * Num Clocks : 1
		     */

		    public:
		        NOP(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		};

		class OR:public Instruction {
		    /*
		     * Logical OR.
		     *
		     * Opcode     : 0010 10rd dddd rrrr
		     * Usage      : OR  Rd, Rr
		     * Operation  : Rd <- Rd or Rr
		     * Flags      : Z,N,V,S
		     * Num Clocks : 1
		     */

		    public:
		        OR(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char Rr;
		};

		class ORI : public Instruction {
		    /*
		     * Logical OR with Immed.
		     *
		     * Opcode     : 0110 KKKK dddd KKKK
		     * Usage      : ORI  Rd, K
		     * Operation  : Rd <- Rd or K
		     * Flags      : Z,N,V,S
		     * Num Clocks : 1
		     */

		    public:
		        ORI (word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char K;
		};

		class OUT : public Instruction {
		    /*
		     * Out To I/O Location.
		     *
		     * Opcode     : 1011 1AAd dddd AAAA
		     * Usage      : OUT  A Rd
		     * Operation  : I/O(A) <- Rd
		     * Flags      : None
		     * Num Clocks : 1
		     */

		    public:
		        OUT(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char ioreg;
		};

		class POP : public Instruction {
		    /*
		     * Pop Register from Stack.
		     *
		     * Opcode     : 1001 000d dddd 1111
		     * Usage      : POP  Rd
		     * Operation  : Rd <- STACK
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        POP(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		        unsigned char Rd;
		};

		class PUSH : public Instruction {
		    /*
		     * Push Register on Stack.
		     *
		     * Opcode     : 1001 001d dddd 1111
		     * Usage      : PUSH  Rd
		     * Operation  : STACK <- Rd
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        PUSH(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		        unsigned char Rd;
		};

		class RCALL : public Instruction {
		    /*
		     * Relative Call Subroutine.
		     *
		     * Opcode     : 1101 kkkk kkkk kkkk
		     * Usage      : RCALL  k
		     * Operation  : PC <- PC + k + 1
		     * Flags      : None
		     * Num Clocks : 3 / 4
		     */

		    public:
		        RCALL(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		        signed int K;
		};

		class RET : public Instruction {
		    /*
		     * Subroutine Return.
		     *
		     * Opcode     : 1001 0101 0000 1000
		     * Usage      : RET
		     * Operation  : PC <- STACK
		     * Flags      : None
		     * Num Clocks : 4 / 5
		     */

		    public:
		        RET(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		};

		class RETI : public Instruction {
		    /*
		     * Interrupt Return.
		     *
		     * Opcode     : 1001 0101 0001 1000
		     * Usage      : RETI
		     * Operation  : PC <- STACK
		     * Flags      : I
		     * Num Clocks : 4 / 5
		     */

		    public:
		        RETI(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		};

		class RJMP : public Instruction {
		    /*
		     * Relative Jump.
		     *
		     * Opcode     : 1100 kkkk kkkk kkkk
		     * Usage      : RJMP  k
		     * Operation  : PC <- PC + k + 1
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        RJMP(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		        signed int K;
		};

		class ROR : public Instruction {
		    /*
		     * Rotate Right Though Carry.
		     *
		     * Opcode     : 1001 010d dddd 0111
		     * Usage      : ROR  Rd
		     * Operation  : Rd(7) <- C, Rd(n) <- Rd(n+1), C <- Rd(0)
		     * Flags      : Z,C,N,V,S
		     * Num Clocks : 1
		     */

		    public:
		        ROR(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class SBC : public Instruction {
		    /*
		     * Subtract with Carry.
		     *
		     * Opcode     : 0000 10rd dddd rrrr
		     * Usage      : SBC  Rd, Rr
		     * Operation  : Rd <- Rd - Rr - C
		     * Flags      : Z,C,N,V,S,H
		     * Num Clocks : 1
		     */

		    public:
		        SBC(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char Rr;
		};

		class SBCI : public Instruction {
		    /*
		     * Subtract Immediate with Carry.
		     *
		     * Opcode     : 0100 KKKK dddd KKKK
		     * Usage      : SBCI  Rd, K
		     * Operation  : Rd <- Rd - K - C
		     * Flags      : Z,C,N,V,S,H
		     * Num Clocks : 1
		     */

		    public:
		        SBCI(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char K;
		};

		class SBI : public Instruction {
		    /*
		     * Set Bit in I/O Register.
		     *
		     * Opcode     : 1001 1010 AAAA Abbb
		     * Usage      : SBI  A, b
		     * Operation  : I/O(A, b) <- 1
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        SBI(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		        unsigned char ioreg;
		        unsigned char K;
		};

		class SBIC : public Instruction {
		    /*
		     * Skip if Bit in I/O Reg Cleared.
		     *
		     * Opcode     : 1001 1001 AAAA Abbb
		     * Usage      : SBIC  A, b
		     * Operation  : if (I/O(A,b) = 0) PC <- PC + 2 or 3
		     * Flags      : None
		     * Num Clocks : 1 / 2 / 3
		     */

		    public:
		        SBIC(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		        unsigned char ioreg;
		        unsigned char K;
		};

		class SBIS : public Instruction {
		    /*
		     * Skip if Bit in I/O Reg Set.
		     *
		     * Opcode     : 1001 1011 AAAA Abbb
		     * Usage      : SBIS  A, b
		     * Operation  : if (I/O(A,b) = 1) PC <- PC + 2 or 3
		     * Flags      : None
		     * Num Clocks : 1 / 2 / 3
		     */

		    public:
		        SBIS(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		        unsigned char ioreg;
		        unsigned char K;
		};

		class SBIW : public Instruction {
		    /*
		     * Subtract Immed from Word.
		     *
		     * Opcode     : 1001 0111 KKdd KKKK
		     * Usage      : SBIW  Rd, K
		     * Operation  : Rd+1:Rd <- Rd+1:Rd - K
		     * Flags      : Z,C,N,V,S
		     * Num Clocks : 2
		     */

		    public:
		        SBIW (word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		        unsigned char Rl;
		        unsigned char Rh;
		        unsigned char K;
		};

		class SBRC : public Instruction {
		    /*
		     * Skip if Bit in Reg Cleared.
		     *
		     * Opcode     : 1111 110d dddd 0bbb
		     * Usage      : SBRC  Rd, b
		     * Operation  : if (Rd(b) = 0) PC <- PC + 2 or 3
		     * Flags      : None
		     * Num Clocks : 1 / 2 / 3
		     */

		    public:
		        SBRC (word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		        unsigned char Rd;
		        unsigned char K;
		};

		class SBRS : public Instruction {
		    /*
		     * Skip if Bit in Reg Set.
		     *
		     * Opcode     : 1111 111d dddd 0bbb
		     * Usage      : SBRS  Rd, b
		     * Operation  : if (Rd(b) = 1) PC <- PC + 2 or 3
		     * Flags      : None
		     * Num Clocks : 1 / 2 / 3
		     */

		    public:
		        SBRS (word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		        unsigned char Rd;
		        unsigned char K;
		};

		class SLEEP : public Instruction {
		    /*
		     * Sleep.
		     *
		     * This is device specific and should be overridden by sub-class.
		     *
		     * Opcode     : 1001 0101 1000 1000
		     * Usage      : SLEEP
		     * Operation  : (see specific hardware specification for Sleep)
		     * Flags      : None
		     * Num Clocks : 1
		     */


		    public:
		        SLEEP(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		};

		class SPM : public Instruction {
		    /*
		     * Store Program Memory.
		     *
		     * Opcode     : 1001 0101 1110 1000
		     * Usage      : SPM
		     * Operation  : (Z) <- R1:R0
		     * Flags      : None
		     * Num Clocks : -
		     */

		    public:
		        SPM(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		};

		class STD_Y : public Instruction {
		    /*
		     * Store Indirect with Displacement.
		     *
		     * Opcode     : 10q0 qq1d dddd 1qqq
		     * Usage      : STD  Y+q, Rd
		     * Operation  : (Y + q) <- Rd
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        STD_Y(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char K;
		};

		class STD_Z : public Instruction {
		    /*
		     * Store Indirect with Displacement.
		     *
		     * Opcode     : 10q0 qq1d dddd 0qqq
		     * Usage      : STD  Z+q, Rd
		     * Operation  : (Z + q) <- Rd
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        STD_Z(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char K;
		};

		class STS : public Instruction {
		    /*
		     * Store Direct to data space.
		     *
		     * Opcode     : 1001 001d dddd 0000 kkkk kkkk kkkk kkkk
		     * Usage      : STS  k, Rd
		     * Operation  : (k) <- Rd
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        STS(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		        bool is2Word() const { return true; }

		    protected:
		    	unsigned char Rd;
		};

		class ST_X : public Instruction {
		    /*
		     * Store Indirect using index X.
		     *
		     * Opcode     : 1001 001d dddd 1100
		     * Usage      : ST  X, Rd
		     * Operation  : (X) <- Rd
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        ST_X(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class ST_X_decr : public ST_X {
		    /*
		     * Store Indirect and Pre-Decrement using index X.
		     *
		     * Opcode     : 1001 001d dddd 1110
		     * Usage      : ST  -X, Rd
		     * Operation  : X <- X - 1, (X) <- Rd
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        ST_X_decr(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		};

		class ST_X_incr : public ST_X {
		    /*
		     * Store Indirect and Post-Increment using index X.
		     *
		     * Opcode     : 1001 001d dddd 1101
		     * Usage      : ST  X+, Rd
		     * Operation  : (X) <- Rd, X <- X + 1
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        ST_X_incr(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		};

		class ST_Y_decr : public Instruction {
		    /*
		     * Store Indirect and Pre-Decrement using index Y.
		     *
		     * Opcode     : 1001 001d dddd 1010
		     * Usage      : ST  -Y, Rd
		     * Operation  : Y <- Y - 1, (Y) <- Rd
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        ST_Y_decr (word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class ST_Y_incr : public Instruction {
		    /*
		     * Store Indirect and Post-Increment using index Y.
		     *
		     * Opcode     : 1001 001d dddd 1001
		     * Usage      : ST  Y+, Rd
		     * Operation  : (Y) <- Rd, Y <- Y + 1
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        ST_Y_incr (word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class ST_Z_decr : public Instruction {
		    /*
		     * Store Indirect and Pre-Decrement using index Z.
		     *
		     * Opcode     : 1001 001d dddd 0010
		     * Usage      : ST  -Z, Rd
		     * Operation  : Z <- Z - 1, (Z) <- Rd
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        ST_Z_decr (word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class ST_Z_incr : public Instruction {
		    /*
		     * Store Indirect and Post-Increment using index Z.
		     *
		     * Opcode     : 1001 001d dddd 0001
		     * Usage      : ST  Z+, Rd
		     * Operation  : (Z) <- Rd, Z <- Z + 1
		     * Flags      : None
		     * Num Clocks : 2
		     */

		    public:
		        ST_Z_incr (word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class SUB : public Instruction {
		    /*
		     * Subtract without Carry.
		     *
		     * Opcode     : 0001 10rd dddd rrrr
		     * Usage      : SUB  Rd, Rr
		     * Operation  : Rd <- Rd - Rr
		     * Flags      : Z,C,N,V,S,H
		     * Num Clocks : 1
		     */

		    public:
		        SUB(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char Rr;
		};

		class SUBI : public Instruction {
		    /*
		     * Subtract Immediate.
		     *
		     * Opcode     : 0101 KKKK dddd KKKK
		     * Usage      : SUBI  Rd, K
		     * Operation  : Rd <- Rd - K
		     * Flags      : Z,C,N,V,S,H
		     * Num Clocks : 1
		     */

		    public:
		        SUBI(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		    	unsigned char K;
		};

		class SWAP : public Instruction {
		    /*
		     * Swap Nibbles.
		     *
		     * Opcode     : 1001 010d dddd 0010
		     * Usage      : SWAP  Rd
		     * Operation  : Rd(3..0) <--> Rd(7..4)
		     * Flags      : None
		     * Num Clocks : 1
		     */

		    public:
		        SWAP(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    protected:
		    	unsigned char Rd;
		};

		class WDR : public Instruction {
		    /*
		     * Watchdog Reset.
		     *
		     * This is device specific and must be overridden by sub-class.
		     *
		     * Opcode     : 1001 0101 1010 1000
		     * Usage      : WDR
		     * Operation  : (see specific hardware specification for WDR)
		     * Flags      : None
		     * Num Clocks : 1
		     */

		    public:
		        WDR(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		};

		class BREAK : public Instruction {
		    /*
		     * On-chip Debug system break.
		     *
		     * Opcode     : 1001 0101 1001 1000
		     * Usage      : BREAK
		     * Operation  : (see specific hardware specification for BREAK)
		     * Flags      : None
		     * Num Clocks : 1
		     */

		    public:
		        BREAK(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;
		};

		class ILLEGAL : public Instruction {
		    /*
		     * Illegal instruction
		     */

		    public:
		        ILLEGAL(word opcode);

		    public:
		        int operator()(Core *core) const;
		        int trace(Core *core, std::ostream & ostr) const;

		    private:
		    	word opcode;
		};

	}

}

#endif /*AVR_INSTRUCTIONS_H*/
