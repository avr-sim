/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "GdbServer.h"
#include "Format.h"
#include "Device.h"
#include "DebugInterface.h"
#include "AccessViolation.h"
#include "Instruction.h"

#include <iostream>
#include <stdio.h>
#include <string.h>

#ifdef WINDOWS
#   include <winsock.h>
#   pragma comment(lib, "wsock32.lib")
#else
#	include <errno.h>
#	include <signal.h>
#   include <sys/socket.h>
#   include <netinet/in.h>
#   include <fcntl.h>
#   include <netinet/tcp.h>
#	include <netdb.h>
#	include <arpa/inet.h>
#	define closesocket(a) close( (a) )
#endif

enum {
	BUF_SIZ		   = 800,		  /* Buffering size for read operations. */
    MAX_BUF        = 400,         /* Maximum size of read/write buffers. */
    MAX_READ_RETRY = 50,          /* Maximum number of retries if a read is incomplete. */

    MEM_SPACE_MASK = 0x00ff0000,  /* mask to get bits which determine memory space */
    FLASH_OFFSET   = 0x00000000,  /* Data in flash has this offset from gdb */
    SRAM_OFFSET    = 0x00800000,  /* Data in sram has this offset from gdb */
    EEPROM_OFFSET  = 0x00810000,  /* Data in eeprom has this offset from gdb */

    GDB_BLOCKING_OFF = 0,         /* Signify that a read is non-blocking. */
    GDB_BLOCKING_ON  = 1,         /* Signify that a read will block. */

    GDB_RET_NOTHING_RECEIVED = 5, /* if the read in non blocking receives nothing, we have nothing to do */
    GDB_RET_SINGLE_STEP = 4,     /* do one single step in gdb loop */
    GDB_RET_CONTINUE    = 3,     /* step until another command from gdb is received */
    GDB_RET_CTRL_C       = 2,    /* gdb has sent Ctrl-C to interrupt what is doing */
    GDB_RET_KILL_REQUEST = 1,    /* gdb has requested that sim be killed */
    GDB_RET_OK           =  0     /* continue normal processing of gdb requests */
        /* means that we should NOT execute any step!!! */
};

namespace avr {

	GdbServer::GdbServer(sim::SimulationClock & clock, int port)
			: clock(clock), current(0), sock(-1), conn(-1) {

		lastReply = 0;
		openSocket(port);
		runMode = GDB_RET_OK;
	}

	GdbServer::~GdbServer() {
		closeSocket();
	}

	void GdbServer::add(Device *dev) {
		DebugInterface *dbgi = dev->debugInterface();
		devices.push_back( dbgi );
	}

	void GdbServer::openSocket(int port) {
		if( (sock = socket(PF_INET, SOCK_STREAM, 0)) < 0 )
			throw GdbException( "Can't create socket" );

		int i = 1;
		setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &i, sizeof(i));
		//fcntl(sock, F_SETFL, fcntl(sock, F_GETFL, 0) | O_NONBLOCK);

		struct sockaddr_in address;
		address.sin_family = AF_INET;
		address.sin_port = htons(port);
		memset( &address.sin_addr, 0, sizeof(address.sin_addr) );

		if( bind( sock, (struct sockaddr *)&address, sizeof(address) ) < 0 )
			throw GdbException( "Can't bind socket" );

		if( listen(sock, 1) < 0 )
			throw GdbException( "Can't listen on socket" );

		std::cout << "Waiting on port " << port
				<< " for gdb client to connect..." << std::endl;

		blockingOn = true;
	}

	void GdbServer::closeSocket() {
		if( conn != -1 ) {
			closesocket(conn);
			conn = -1;
		}

		if( sock != -1 ) {
			closesocket(sock);
			sock = -1;
		}

	}

	void GdbServer::acceptClient() {
		struct sockaddr_in address;
	    socklen_t addrLength = sizeof(struct sockaddr *);

	    conn = accept( sock, (struct sockaddr *)&address, &addrLength );
		if( conn > 0 ) {
			/* Tell TCP not to delay small packets.  This greatly speeds up
			 interactive response. WARNING: If TCP_NODELAY is set on, then gdb
			 may timeout in mid-packet if the (gdb)packet is not sent within a
			 single (tcp)packet, thus all outgoing (gdb)packets _must_ be sent
			 with a single call to write. (see Stevens "Unix Network
			 Programming", Vol 1, 2nd Ed, page 202 for more info) */

			int i = 1;
			setsockopt(conn, IPPROTO_TCP, TCP_NODELAY, &i, sizeof (i));

			std::cout << "Connection opened by host "
						<< inet_ntoa( address.sin_addr )
						<< ", port " << ntohs( address.sin_port )
						<< std::endl;
		}
	}

	int GdbServer::readByte() {
		static char buf[BUF_SIZ];
		static int bufcnt = 0;
		static char *bufp;

		if (bufcnt-- > 0)
			return *bufp++ & 0x7f;

		bufcnt = read( conn, buf, sizeof(buf) );

		if( bufcnt <= 0 ) {
			if( bufcnt == 0 )
				throw GdbException( "Read failed: Got EOF" );

            if( errno == EAGAIN )
                return -1;

            throw GdbException( "Read failed" );
		}

		bufp = buf;
		bufcnt--;
		return *bufp++ & 0x7f;
	}

	void GdbServer::write( const void *buf, size_t count ) {
	    int res = ::write( conn, buf, count );

	    if( res < 0 )
	    	throw GdbException( util::format("write failed: %s") % strerror(errno) );

	    if( (size_t)res != count )
	    	throw GdbException( util::format("wrote only wrote %d of %d bytes") % res % count );
	}

	/* Acknowledge a packet from GDB */
	void GdbServer::sendAck() {
	    write( "+", 1 );
	}

	void GdbServer::saveLastReply( const char *reply ) {
		if( lastReply != 0 )
			delete [] lastReply;

		int len = strlen( reply );
	    lastReply = new char[len];
	    strcpy( lastReply, reply );
	}

	static char HEX_DIGIT[] = "0123456789abcdef";

	/* Send a reply to GDB. */
	void GdbServer::sendReply( const char *reply ) {
	    int cksum = 0;
	    int bytes;

	    /* Save the reply to last reply so we can resend if need be. */
	    if( reply != lastReply )
	    	saveLastReply( reply );

	    if( *reply == '\0' ) {
	        write( "$#00", 4 );
	    } else {
	    	char buf[MAX_BUF];
	        memset( buf, '\0', sizeof(buf) );

	        buf[0] = '$';
	        bytes = 1;

	        while( *reply ) {
	            cksum += (unsigned char)*reply;
	            buf[bytes] = *reply;
	            bytes++;
	            reply++;

	            /* must account for "#cc" to be added */
	            if( bytes == (MAX_BUF-3) ) {
	                /* FIXME: TRoth 2002/02/18 - splitting reply would be better */
	            	throw GdbException( "buffer overflow" );
	            }
	        }

	        buf[bytes++] = '#';
	        buf[bytes++] = HEX_DIGIT[(cksum >> 4) & 0xf];
	        buf[bytes++] = HEX_DIGIT[cksum & 0xf];

#ifdef DEBUG
	        std::cout << util::format("Send: \"%s\"") % buf << std::endl;
#endif
	        write( buf, bytes );
	    }
	}

	void GdbServer::setBlockingMode( bool blocking ) {
		if( blocking == blockingOn )
			return;

		int flags = fcntl(conn, F_GETFL, 0);
	    if( blocking ) {
	        /* turn non-blocking mode off */
	        if( fcntl( conn, F_SETFL, flags & ~O_NONBLOCK) < 0 )
	        	throw GdbException( "fcntl failed" );
	    } else {
	        /* turn non-blocking mode on */
	        if (fcntl( conn, F_SETFL, flags | O_NONBLOCK) < 0)
	        	throw GdbException( "fcntl failed" );
	    }

	    blockingOn = blocking;
	}

	/* Convert a hexidecimal digit to a 4 bit nibble. */
	static int hex2nib( char hex ) {
	    if( (hex >= 'A') && (hex <= 'F') )
	        return (10 + (hex - 'A'));

	    else if( (hex >= 'a') && (hex <= 'f') )
	        return (10 + (hex - 'a'));

	    else if( (hex >= '0') && (hex <= '9') )
	        return (hex - '0');

	    throw GdbException( "Invalid hexidecimal digit" );
	}

	static byte hex2byte( char **pkt ) {
		char *p = *pkt;
		byte val = hex2nib(*p++);
		val = (val << 4) | hex2nib(*p++);
		*pkt = p;
		return val;
	}

	static void putHex( char **pkt, byte val ) {
		char *p = *pkt;
        *p++ = HEX_DIGIT[(val >> 4) & 0xf];
        *p++ = HEX_DIGIT[val & 0xf];
		*pkt = p;
	}

	static int extractHexNum( char **pkt, char stop = '\0' ) {
	    int num = 0;
	    char *p = *pkt;
	    int max_shifts = sizeof(int)*2-1; /* max number of nibbles to shift through */

	    int i = 0;
	    while ( (*p != stop) && (*p != '\0') ) {
	        if( i > max_shifts )
	        	 throw GdbException( "number too large" );

	        num = (num << 4) | hex2nib(*p);
	        i++;
	        p++;
	    }

	    *pkt = p;
	    return num;
	}

	static void getAddrLen( char **pkt, char a_end, char l_end,
					unsigned int & addr, int & len ) {
		char *p = *pkt;

		addr = 0;
	    len  = 0;

	    /* Get the addr from the packet */
	    while( *p != a_end )
	        addr = (addr << 4) + hex2nib(*p++);
	    p++;                      /* skip over a_end */

	    /* Get the length from the packet */
	    while( *p != l_end )
	        len = (len << 4) + hex2nib(*p++);
	    p++;                      /* skip over l_end */

	    *pkt = p;
	}

	void GdbServer::queryPacket( char *pkt ) {
	    char reply[MAX_BUF];
	    memset(reply, '\0', sizeof(reply));

		if( strcmp(pkt, "C") == 0 ) {
			/*
			 * `qC'
			 * Return the current thread id.
			 * Reply:
			 * 	`QC pid'
			 */
		    snprintf( reply, sizeof(reply)-1, "%02x", current );
		} else if( strcmp(pkt, "qfThreadInfo") == 0 ) {
			/*
			 * `qfThreadInfo'
			 * `qsThreadInfo'
			 *  Obtain a list of all active thread ids from the target (OS).
			 * Since there may be too many active threads to fit into one
			 * reply packet, this query works iteratively: it may require more
			 * than one query/reply sequence to obtain the entire list of threads.
			 * The first query of the sequence will be the `qfThreadInfo' query;
			 * subsequent queries in the sequence will be the `qsThreadInfo' query.
			 */

			int n;
			n = snprintf( reply, sizeof(reply)-1, "m %02x", 0 );
			for(size_t i = 1; i < devices.size(); ++i)
				snprintf( reply + n, sizeof(reply)-n-1, ",%02x", 0 );
		} else if( strcmp(pkt, "qsThreadInfo") == 0 ) {
			reply[0] = 'l';
		} else if( strcmp(pkt, "qSupported") == 0) {
			sendReply( "PacketSize=800;qXfer:features:read+" );
		} else if( strncmp(pkt, "qXfer:features:read:target.xml:", 31) == 0 ) {
			// GDB XML target descriptions, since GDB 6.7 (2007-10-10)
			// see http://sources.redhat.com/gdb/current/onlinedocs/gdb/Target-Descriptions.html
			sendReply( "l"
					"<?xml version=\"1.0\"?>\n"
					"<!DOCTYPE target SYSTEM \"gdb-target.dtd\">\n"
					"<target version=\"1.0\">\n"
					"    <architecture>avr</architecture>\n"
					"</target>\n" );
			return;
		}

		sendReply( reply );
	}

	void GdbServer::readRegisters() {
	    /* (32 gpwr, SREG, SP, PC) * 2 hex bytes + terminator */
	    size_t  buf_sz = (32 + 1 + 2 + 4)*2 + 1;
	    char buf[ buf_sz ];

	    char *p = buf;
	    /* 32 gen purpose working registers */
	    for( int i = 0; i<32; i++ ) {
	    	byte val = devices[current]->reg(i);
	    	putHex( &p, val );
	    }

	    {
		    /* GDB thinks SREG is register number 32 */
	    	byte val = devices[current]->status();
	    	putHex( &p, val );
	    }

	    {
	    	/* GDB thinks SP is register number 33 */
	    	word sp = devices[current]->stackPointer();
	    	putHex( &p, sp & 0xff );
	    	putHex( &p, (sp >> 8) & 0xff );
	    }

	    {
	    	dword pc = devices[current]->programCounter();
	    	/* GDB thinks PC is register number 34.
	    	 * GDB stores PC in a 32 bit value (only uses 23 bits though).
	    	 */

	    	putHex( &p, pc & 0xff );
	    	putHex( &p, (pc >> 8) & 0xff );
	    	putHex( &p, (pc >> 16) & 0xff );
	    	putHex( &p, (pc >> 24) & 0xff );
	    }

	    *p = '\0';
	    sendReply(  buf );
	}

	void GdbServer::writeRegisters( char *pkt ) {
	    /* 32 gen purpose working registers */
	    for(int i = 0; i < 32; i++) {
	        byte val = hex2byte( &pkt );
	        devices[current]->setReg( i, val );
	    }

	    {
	    	/* GDB thinks SREG is register number 32 */
	    	byte val = hex2byte( &pkt );
		    devices[current]->setStatus( val );
	    }

	    {
	    	/* GDB thinks SP is register number 33 */
	        word sp = hex2byte( &pkt ) | (hex2byte( &pkt ) << 8);
	        devices[current]->setStackPointer( sp );
	    }

	    {
	    	/*
	    	 * GDB thinks PC is register number 34.
	    	 * GDB stores PC in a 32 bit value (only uses 23 bits though).
	    	 */
	    	dword pc = hex2byte( &pkt ) |
	    				hex2byte( &pkt ) << 8 |
	    				hex2byte( &pkt ) << 16 |
	    				hex2byte( &pkt ) << 24;
	    	devices[current]->setProgramCounter( pc );
	    }

	    sendReply( "OK" );
	}

	void GdbServer::readRegister( char *pkt ) {
	    char reply[MAX_BUF];
	    memset(reply, '\0', sizeof(reply));

	    int reg = extractHexNum(&pkt, '\0');
	    if( reg < 32 ) {
	    	byte val = devices[current]->reg(reg);
	    	 snprintf( reply, sizeof(reply)-1, "%02x", val );
	    } else if( reg == 32 ) {
	    	byte val = devices[current]->status();
	    	 snprintf( reply, sizeof(reply)-1, "%02x", val );
	    } else if( reg == 33 ) {
		   /* SP */
	        word sp = devices[current]->stackPointer();
	        snprintf( reply, sizeof(reply)-1, "%02x%02x",
	        		sp & 0xff, (sp >> 8) & 0xff );
	    } else if (reg == 34) {
	    	/* PC */
	        dword val = devices[current]->programCounter();
	        snprintf( reply, sizeof(reply)-1,
	                "%02x%02x" "%02x%02x",
	                val & 0xff, (val >> 8) & 0xff,
	                (val >> 16) & 0xff, (val >> 24) & 0xff );
	    } else {
	        sendReply( "E00" );
	        return;
	    }

	    sendReply( reply );
	}

	void GdbServer::writeRegister( char *pkt ) {
	    int reg = extractHexNum(&pkt, '=');
	    pkt++;                      /* skip over '=' character */

	    /* extract the low byte of value from pkt */
	    byte val = hex2byte( &pkt );
	    if( (reg >= 0) && (reg < 33) ) {
	        /* r0 to r31  */
	    	devices[current]->setReg( reg, val );
	    } else if( reg == 32 ) {
	    	/* SREG is register 32 */
	    	devices[current]->setStatus( val );
	    } else if( reg == 33 ) {
	        /* SP is 2 bytes long so extract upper byte */
	    	byte hval  = hex2byte( &pkt );
	        word sp = (hval << 8) | val;
	        devices[current]->setStackPointer( sp );
	    } else if( reg == 34 ) {
	        /* GDB thinks PC is register number 34.
	        GDB stores PC in a 32 bit value (only uses 23 bits though).
	        GDB thinks PC is bytes into flash, not words like in simulavr.

	        Must cast to dword so as not to get mysterious truncation. */

	    	dword pc = val |
	    				hex2byte( &pkt ) << 8 |
	    				hex2byte( &pkt ) << 16 |
	    				hex2byte( &pkt ) << 24;
	    	devices[current]->setProgramCounter( pc );
	    } else {
	        sendReply(  "E00" );
	        return;
	    }

	    sendReply( "OK" );
	}

	void GdbServer::readMemory( char *pkt ) {
		unsigned int addr;
		int len;
	    getAddrLen( &pkt, ',', '\0', addr, len );

	    char *buf = 0;
	    try {
	    	/* Treat registers differently from continuous memory */
	    	if( ((addr & MEM_SPACE_MASK) == SRAM_OFFSET) &&
	    			devices[current]->isRegister(addr & ~MEM_SPACE_MASK) ) {
	    		unsigned int raddr = addr & ~MEM_SPACE_MASK; /* remove the offset bits */
				buf = new char[len*2+1];
				int i;
				for(i = 0; i < len; i++) {
					byte bval = devices[current]->readRegister(raddr + i);
					buf[i*2]   = HEX_DIGIT[bval >> 4];
					buf[i*2+1] = HEX_DIGIT[bval & 0xf];
				}
				buf[i*2] = '\0';
	    	} else {
				const unsigned char *data;
				if( (addr & MEM_SPACE_MASK) == EEPROM_OFFSET ) {
					/* addressing eeprom */
					unsigned int eaddr = addr & ~MEM_SPACE_MASK; /* remove the offset bits */
					data = devices[current]->readEeprom( eaddr, len );
				} else if ( (addr & MEM_SPACE_MASK) == SRAM_OFFSET ) {
					/* addressing sram */
					unsigned int saddr = addr & ~MEM_SPACE_MASK; /* remove the offset bits */
					data = devices[current]->readRam( saddr, len );
				} else if ( (addr & MEM_SPACE_MASK) == FLASH_OFFSET ) {
					/* addressing flash */
					unsigned int faddr = addr & ~MEM_SPACE_MASK; /* remove the offset bits */
					data = devices[current]->readFlash( faddr, len );
				} else {
					throw AccessViolation( "memory space does not exist " );
				}

				buf = new char[len*2+1];
				int i;
				for(i = 0; i < len; i++) {
					byte bval = data[i];
					buf[i*2]   = HEX_DIGIT[bval >> 4];
					buf[i*2+1] = HEX_DIGIT[bval & 0xf];
				}
				buf[i*2] = '\0';
	    	}
	    } catch( util::Exception & ex ) {
	    	std::ios_base::fmtflags f = std::cerr.flags();
	        std::cerr << ex.message() << " reading address "
				<< std::hex << addr << " length " << std::dec << len << std::endl;
	        std::cerr.flags(f);

	        /*
	         * If gdb requested a read from invalid memory,
	         * always reply with 0's
	         * TODO find out why gdb requests these addresses
	         */
	        if ( (addr & MEM_SPACE_MASK) == SRAM_OFFSET ) {
	        	buf = new char[len*2+1];
	        	int i;
				for(i = 0; i < 2*len; i++)
					buf[i] = '0';
				buf[i] = '\0';
	        } else {
				char reply[10];
				snprintf( reply, sizeof(reply), "E%02x", EIO );
				sendReply( reply );
				return;
	        }
	    } catch( std::exception & ex ) {
	        std::cerr << ex.what() << std::endl;

	        char reply[10];
	        snprintf( reply, sizeof(reply), "E%02x", EIO );
	        sendReply( reply );
	        return;
	    }

	    sendReply( buf );

    	if( buf != 0 )
    		delete [] buf;
	}

	void GdbServer::writeMemory( char *pkt ) {
	    unsigned int addr;
		int len;
	    getAddrLen( &pkt, ',', ':', addr, len );

	    try {
	    	unsigned char *data = new unsigned char[len];
	        for(int i = 0; i < len; ++i) {
	        	byte val = hex2byte( &pkt );
	            data[i] = val;
	        }

		    if( (addr & MEM_SPACE_MASK) == EEPROM_OFFSET ) {
		        /* addressing eeprom */
		        addr = addr & ~MEM_SPACE_MASK; /* remove the offset bits */
		        devices[current]->writeEeprom(data, addr, len);
		    } else if ( (addr & MEM_SPACE_MASK) == SRAM_OFFSET ) {
		        /* addressing sram */
		        addr = addr & ~MEM_SPACE_MASK; /* remove the offset bits */
		        devices[current]->writeRam(data, addr, len);
		    } else if ( (addr & MEM_SPACE_MASK) == FLASH_OFFSET ) {
		        /* addressing flash */
		        addr = addr & ~MEM_SPACE_MASK; /* remove the offset bits */
		        devices[current]->writeFlash(data, addr, len);
		    } else {
		    	throw AccessViolation( "memory space does not exist " );
		    }
	    } catch( util::Exception & ex ) {
	        std::cerr << ex.message() << std::endl;

	        char reply[10];
	        snprintf( reply, sizeof(reply), "E%02x", EIO );
	        sendReply( reply );
	        return;
	    } catch( std::exception & ex ) {
	        std::cerr << ex.what() << std::endl;

	        char reply[10];
	        snprintf( reply, sizeof(reply), "E%02x", EIO );
	        sendReply( reply );
	        return;
	    }

	    sendReply( "OK" );
	}

	int GdbServer::getSignal( char *pkt ) {
	    int signo = hex2byte( &pkt );

	    /*
	     * Process signals send via remote protocol from gdb.
	     * Signals really don't make sense to the program running
	     * in the simulator, so we are using them sort of as
	     * an 'out of band' data.
	     */

	    switch (signo) {
	        case SIGHUP:
	            /* Gdb user issuing the 'signal SIGHUP' command tells sim to reset
	            itself. We reply with a SIGTRAP the same as we do when gdb
	            makes first connection with simulator. */
	            devices[current]->reset();
	            sendReply( "S05" );
	    }

	    return signo;

	}

	/* Format of breakpoint commands (both insert and remove):

	"z<t>,<addr>,<length>"  -  remove break/watch point
	"Z<t>,<add>r,<length>"  -  insert break/watch point

	In both cases t can be the following:
	t = '0'  -  software breakpoint
	t = '1'  -  hardware breakpoint
	t = '2'  -  write watch point
	t = '3'  -  read watch point
	t = '4'  -  access watch point

	addr is address.
	length is in bytes

	For a software breakpoint, length specifies the size of the instruction to
	be patched. For hardware breakpoints and watchpoints, length specifies the
	memory region to be monitored. To avoid potential problems, the operations
	should be implemented in an idempotent way. -- GDB 5.0 manual.
	*/
	void GdbServer::breakPoint( char *pkt ) {
	    char z = *(pkt-1);          /* get char parser already looked at */
	    char t = *pkt++;
	    pkt++;                      /* skip over first ',' */

	    unsigned int addr;
		int len;
	    getAddrLen( &pkt, ',', '\0', addr, len );

	    try {
		    switch( t ) {
		        case '0': /* software breakpoint */
		        case '1': /* hardware breakpoint */
		            if( z == 'z' )
		            	devices[current]->removeBreak( addr );
		            else
		            	devices[current]->insertBreak( addr );
		            break;

		        case '2':               /* write watchpoint */
		        	sendReply( "" );
		            return;

		        case '3':               /* read watchpoint */
		        	sendReply( "" );
		            return;

		        case '4':               /* access watchpoint */
		        	sendReply( "" );
		            return;             /* unsupported yet */
		    }

		    sendReply( "OK" );
	    } catch( util::Exception & ex ) {
	        std::cerr << ex.message() << std::endl;
	        sendReply( "E01" );
	    }
	}

	int GdbServer::parsePacket( char *pkt ) {
	    switch( *pkt++ ) {
	        case '?':               /* last signal */
	            sendReply( "S05" ); /* signal # 5 is SIGTRAP */
	            break;

	        case 'g':               /* read registers */
	            readRegisters(  );
	            break;

	        case 'G':               /* write registers */
	            writeRegisters(  pkt );
	            break;

	        case 'p':               /* read a single register */
	            readRegister(  pkt );
	            break;

	        case 'P':               /* write single register */
	            writeRegister(  pkt );
	            break;

	        case 'm':               /* read memory */
	            readMemory(  pkt );
	            break;

	        case 'M':               /* write memory */
	            writeMemory(  pkt );
	            break;

	        case 'D':               /* detach the debugger */
	        case 'k':               /* kill request */
	            sendReply( "OK" );
	            return GDB_RET_KILL_REQUEST;

	        case 'c':               /* continue */
	            return GDB_RET_CONTINUE;

	        case 'C':               /* continue with signal */
	            return GDB_RET_CONTINUE;

	        case 's':               /* step */
	            return GDB_RET_SINGLE_STEP;

	        case 'S':               /* step with signal */
	            getSignal(pkt);
	            return GDB_RET_SINGLE_STEP;

	        case 'z':               /* remove break/watch point */
	        case 'Z':               /* insert break/watch point */
	            breakPoint( pkt );
	            break;

	        case 'H':				/* Set thread for subsequent operations */
	        	/*
	        	 * `H c t'
	        	 *	Set thread for subsequent operations (`m', `M', `g', `G', et.al.).
	        	 * c depends on the operation to be performed: it should be `c'
	        	 * for step and continue operations, `g' for other operations.
	        	 * The thread designator t may be `-1', meaning all the threads,
	        	 * a thread number, or `0' which means pick any thread.
	        	 */
	        	pkt++;
	        	current = strtoul(pkt, 0, 16);
	        	sendReply("OK");
	        	break;

	        case 'q':               /* query requests */
	        	queryPacket(pkt);
	            break;

	        default:
	        	sendReply( "" );
	    }

	    return GDB_RET_OK;
	}

	int GdbServer::readPacket() {
	    char pkt_buf[MAX_BUF];

        memset( pkt_buf, 0, sizeof(pkt_buf) );
        setBlockingMode( true );

        int pkt_cksum = 0, i = 0;
        int c = readByte();
        while ( (c != '#') && (i < MAX_BUF) ) {
            pkt_buf[i++] = c;
            pkt_cksum += (unsigned char)c;
            c = readByte();
        }

        int cksum;
        cksum  = hex2nib( readByte() ) << 4;
        cksum |= hex2nib( readByte() );

        /* FIXME: Should send "-" (Nak) instead of aborting when we get
        checksum errors. Leave this as an error until it is actually
        seen (I've yet to see a bad checksum - TRoth). It's not a simple
        matter of sending (Nak) since you don't want to get into an
        infinite loop of (bad cksum, nak, resend, repeat).*/

        if( (pkt_cksum & 0xff) != cksum )
        	throw GdbException(
        			util::format("Bad checksum: sent 0x%x <--> computed 0x%x") % cksum % pkt_cksum );

#ifdef DEBUG
        std::cout << util::format("Recv: \"$%s#%02x\"") % pkt_buf % cksum << std::endl;
#endif

        /* always acknowledge a well formed packet immediately */
        sendAck();

        return parsePacket( pkt_buf );
	}

	int GdbServer::preParsePacket(bool blocking) {
		setBlockingMode(blocking);

	    int c = readByte( );
	    switch (c) {
	        case '$':           /* read a packet */
	        	return readPacket();

	        case '-': // Nack
	            sendReply( lastReply );
	            break;

	        case '+': // Ack
	            break;

	        case 0x03:
	            /* Gdb sends this when the user hits C-c. This is gdb's way of
	            telling the simulator to interrupt what it is doing and return
	            control back to gdb. */
	            return GDB_RET_CTRL_C;

	        case -1:
	            /* fd is non-blocking and no data to read */
	            return GDB_RET_NOTHING_RECEIVED;

	        default:
	        	std::cout << "Unknown request from gdb: "
	        				<< std::hex << c << std::dec << std::endl;
	    }

	    return GDB_RET_OK;
	}

	void GdbServer::sendPosition(int signo) {
		/* status */
		byte status = devices[current]->status();
	    /* SP */
        word sp = devices[current]->stackPointer();
    	/* PC */
        dword val = devices[current]->programCounter();

        char reply[MAX_BUF];
	    snprintf( reply, sizeof(reply),
	    			"T%02x20:%02x;21:%02x%02x;22:%02x%02x%02x%02x;",
	    			signo,
	    			status,
	    			sp & 0xff, (sp >> 8) & 0xff,
	    			val & 0xff, (val >> 8) & 0xff,
	                (val >> 16) & 0xff, (val >> 24) & 0xff);

	    sendReply(reply);
	}

	void GdbServer::exec() {
		while( true ) {
			if( conn < 0 )
				acceptClient();

			try {
				if( lastReply != 0 ) {
					delete [] lastReply;
					lastReply = 0;
				}

				handleClient();
			} catch( GdbException & ex ) {
				std::cerr << "GdbServer " << ex.message() << std::endl;

				devices[current]->deleteAllBreakpoints();
                devices[current]->reset();

                if( conn != -1 ) {
                	closesocket(conn);
                	conn = -1;
                }
			}
		}
	}

	void GdbServer::handleClient() {
		do {
	        int gdbRet = preParsePacket( runMode != GDB_RET_CONTINUE );
	        switch( gdbRet ) {
	            case GDB_RET_NOTHING_RECEIVED:
	                break;

	            case GDB_RET_OK:
	                runMode = GDB_RET_OK;
	                break;

	            case GDB_RET_CONTINUE:
	                runMode = GDB_RET_CONTINUE;
	                break;

	            case GDB_RET_SINGLE_STEP:
	                runMode = GDB_RET_SINGLE_STEP;
	                break;

	            case GDB_RET_CTRL_C:
	                runMode = GDB_RET_CTRL_C;
	                sendPosition(SIGINT);
	                break;

	            case GDB_RET_KILL_REQUEST:
	            	devices[current]->deleteAllBreakpoints();
	                devices[current]->reset();

	                if( conn != -1 ) {
	                	closesocket(conn);
	                	conn = -1;
	                }
	                return;
	        }

		} while( (runMode != GDB_RET_SINGLE_STEP ) &&
				( runMode != GDB_RET_CONTINUE) );

		try {
			do {
				devices[current]->step();
			} while( (runMode == GDB_RET_SINGLE_STEP )
					&& ! devices[current]->stepDone() );

		    if( devices[current]->hasBreaked() ) {
		        runMode = GDB_RET_OK;
		        sendPosition(SIGTRAP);
		    }
	    } catch( IllegalInstruction & ex ) {
	    	char reply[10];
	        snprintf( reply, sizeof(reply), "S%02x", SIGILL );
		        sendReply( reply );
		        runMode = GDB_RET_OK;
		        sendPosition(SIGILL);
	    }

	    if( runMode == GDB_RET_SINGLE_STEP ) {
	        runMode = GDB_RET_OK;
	        sendPosition(SIGTRAP);
	    }
	}

}
