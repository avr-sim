#ifndef TYPES_H
#define TYPES_H

#include <inttypes.h>

namespace avr {

	typedef uint8_t byte;
	typedef int8_t sbyte;
	typedef uint16_t word;
	typedef int16_t sword;
	typedef uint32_t dword;
	typedef uint64_t lword;

	/*
	 * Define all of the different types of reset conditions:
	 */
	enum RESET_TYPE {
		POR_RESET, // Power-on reset
		WDT_RESET, // Watch Dog timer timeout reset
		JTAG_RESET, // JTAG reset
		EXT_RESET, // External reset
		SOFT_RESET, // Software initiated reset
		BOD_RESET, // Brown out detection reset
		SIM_RESET, // Simulation Reset
		OTHER_RESET //
	};
}

#endif /*TYPES_H*/
