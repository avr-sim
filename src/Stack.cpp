/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Stack.h"
#include "Registers.h"
#include <cstring>

namespace avr {

	Stack::Stack(Bus & bus, MMU & mmu, word mask)
		: Hardware(bus), mmu(mmu), mask(mask), sp(0) {
	}

	void Stack::push(unsigned char val) {
		mmu.writeByte(sp, val);
		sp--;
		setSP( sp );
	}

	unsigned char Stack::pop() {
		sp++;
		setSP( sp );

		return mmu.readByte(sp);
	}

	void Stack::setSP(word sp) {
		sp &= mask;
		spl->set( getSPL() );
		sph->set( getSPH() );
		this->sp = sp;
	}

	bool Stack::attachReg(const char *name, IORegister *reg) {
		if( strcmp(name, "sph") == 0 )
			sph = reg;
		else if( strcmp(name, "spl") == 0 )
			spl = reg;
		else
			return false;

		reg->registerHW(this);
		return true;
	}

	bool Stack::finishBuild() {
		return ( sph != 0 && spl != 0 );
	}

	void Stack::regChanged(IORegister *reg) {
		if( reg == sph )
			setSPH( (unsigned char)*sph );
		else if( reg == spl )
			setSPL( (unsigned char)*spl );
	}

}
