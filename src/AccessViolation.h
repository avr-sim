#ifndef AVR_ACCESSVIOLATION_H
#define AVR_ACCESSVIOLATION_H

#include "Exception.h"

namespace avr {

	/**
	 * @author Tom Haber
	 * @date Apr 30, 2008
	 * @brief Thrown when a bad memory location is accessed
	 *
	 * AccessViolation is thrown when a memory access is out-of-bounds.
	 */
	class AccessViolation : public util::Exception {
		public:
			AccessViolation() : Exception("Access Violation") {}
			AccessViolation(const char *msg) : Exception(msg) {}
			AccessViolation(const std::string msg) : Exception(msg) {}
	};

}

#endif /*AVR_ACCESSVIOLATION_H*/
