/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Instructions.h"
#include "Core.h"
#include "DecoderHelp.h"

#include <iostream>

#define reg(r) core->getR( (r) )
#define hexchar(a) std::hex << int(a) << std::dec
#define duoreg(Rh, Rl) core->getR(Rh) << ":" << core->getR(Rl)

static std::ostream & operator <<(std::ostream & ostr, const avr::Register & r) {
	ostr << r.getName();
	return ostr;
}

namespace avr {

	namespace op {

		int ADC::trace(Core *core, std::ostream & ostr) const {
			ostr << "ADC " << reg(Rd) << ", " << reg(Rr);
		    return 1;
		}

		int ADD::trace(Core *core, std::ostream & ostr) const {
			ostr << "ADD " << reg(Rd) << ", " << reg(Rr);
		    return 2;
		}

		int ADIW::trace(Core *core, std::ostream & ostr) const {
		    ostr << "ADIW " << duoreg(Rh, Rl) << ", " << hexchar(K);
		    return 1;
		}

		int AND::trace(Core *core, std::ostream & ostr) const {
		    ostr << "AND " << reg(Rd) << ", R" << reg(Rr);
		    return 1;
		}

		int ANDI::trace(Core *core, std::ostream & ostr) const {
		    ostr << "ANDI " << reg(Rd) << ", " << hexchar(K);
		    return 1;
		}

		int ASR::trace(Core *core, std::ostream & ostr) const {
		    ostr << "ASR " << reg(Rd);
		    return 1;
		}

		const char *opcodes_bclr[8]= {
		    "CLC",
		    "CLZ",
		    "CLN",
		    "CLV",
		    "CLS",
		    "CLH",
		    "CLT",
		    "CLI"
		};

		int BCLR::trace(Core * /*core*/, std::ostream & ostr) const {
			int i;
			for(i = 0; i < 8; ++i)
				if( K == (unsigned char)~(1<<i) )
					break;

			ostr << opcodes_bclr[i];
		    return 1;
		}

		int BLD::trace(Core *core, std::ostream & ostr) const {
		    ostr << "BLD " << reg(Rd) << ", " << hexchar(Kadd);
		    return 1;
		}

		const char *branch_opcodes_clear[8] = {
		    "BRCC",
		    "BRNE",
		    "BRPL",
		    "BRVC",
		    "BRGE",
		    "BRHC",
		    "BRTC",
		    "BRID"
		};

		int BRBC::trace(Core * /*core*/, std::ostream & ostr) const {
			int i;
			for(i = 0; i < 8; ++i)
				if( bitmask == (1<<i) )
					break;

		    ostr << branch_opcodes_clear[i] << " " << int(char(offset) * 2);
		/*
		    string sym(core->Flash->GetSymbolAtAddress(core->PC+1+p2));
		    ostr << sym << " ";
		    for (int len = sym.length(); len<30;len++) { ostr << " " ; }
		*/
		    return 1;
		}

		const char *branch_opcodes_set[8] = {
		    "BRCS",
		    "BREQ",
		    "BRMO",
		    "BRVS",
		    "BRLT",
		    "BRHS",
		    "BRTS",
		    "BRIE"
		};

		int BRBS::trace(Core * /*core*/, std::ostream & ostr) const {
			int i;
			for(i = 0; i < 8; ++i)
				if( bitmask == (1<<i) )
					break;

		    ostr << branch_opcodes_set[i] << " " << int(char(offset)*2);
		    return 1;
		}

		const char *opcodes_bset[8]= {
		    "SEC",
		    "SEZ",
		    "SEN",
		    "SEV",
		    "SES",
		    "SEH",
		    "SET",
		    "SEI"
		};

		int BSET::trace(Core * /*core*/, std::ostream & ostr) const {
			int i;
			for(i = 0; i < 8; ++i)
				if( K == (1<<i) )
					break;

		    ostr << opcodes_bset[i];
		    return 1;
		}

		int BST::trace(Core *core, std::ostream & ostr) const {
		    ostr << "BST " << reg(Rd) << ", " << hexchar(K);
		    return 1;
		}

		int CALL::trace(Core *core, std::ostream & ostr) const {
		    ostr << "CALL " << hexchar(KH);
			return core->pcBytes() + 2;
		}

		int CBI::trace(Core *core, std::ostream & ostr) const {
		    ostr << "CBI " << core->getIoreg(ioreg) << ", " << hexchar(K);
		    return 2;
		}

		int COM::trace(Core *core, std::ostream & ostr) const {
		    ostr << "COM " << reg(Rd);
		    return 1;
		}

		int CP::trace(Core *core, std::ostream & ostr) const {
		    ostr << "CP " << reg(Rd) << ", " << reg(Rr);
		    return 1;
		}

		int CPC::trace(Core *core, std::ostream & ostr) const {
		    ostr << "CPC " << reg(Rd) << ", " << reg(Rr);
		    return 1;
		}

		int CPI::trace(Core *core, std::ostream & ostr) const {
		    ostr << "CPI " << reg(Rd) << ", " << hexchar(K);
		    return 1;
		}

		int CPSE::trace(Core *core, std::ostream & ostr) const {
		    ostr << "CPSE " << reg(Rd) << ", " << reg(Rr);
		    return 1;
		}

		int DEC::trace(Core *core, std::ostream & ostr) const {
		    ostr << "DEC " << reg(Rd);
		    return 1;
		}

		int EICALL::trace(Core * /*core*/, std::ostream & ostr) const {
		    ostr << "EICALL";
		    return 4;
		}

		int EIJMP::trace(Core * /*core*/, std::ostream & ostr) const {
		    ostr << "EIJMP";
		    return 2;
		}

		int ELPM_Z::trace(Core *core, std::ostream & ostr) const {
		    ostr << "ELPM " << reg(Rd) << ", Z";
/*
		    unsigned int Z = ((core->GetRampz() & 0x3f) << 16) +
		        (((*(core->R))[31]) << 8) +
		        (*(core->R))[30];

		    unsigned int flash_addr = Z;// / 2;

		    ostr << " Flash[0x"<<hex<< (unsigned int) flash_addr<<"] ";
*/
		    return 3;
		}

		int ELPM_Z_incr::trace(Core *core, std::ostream & ostr) const {
		    ostr << "ELPM " << reg(Rd) << ", Z+";

		  /*  unsigned int Z = ((core->GetRampz() & 0x3f) << 16) +
		        (((*(core->R))[31]) << 8) +
		        (*(core->R))[30];

		    unsigned int flash_addr = Z; // / 2;

		    ostr << " Flash[0x"<<hex<< (unsigned int) flash_addr<<"] ";
*/

		    return 3;
		}

		int ELPM::trace(Core * /*core*/, std::ostream & ostr) const {
		    ostr << "ELPM";

		   /* unsigned int Z = ((core->GetRampz() & 0x3f) << 16) +
		        (((*(core->R))[31]) << 8) +
		        (*(core->R))[30];

		    unsigned int flash_addr = Z; // / 2;

		    ostr << " Flash[0x"<<hex<< (unsigned int) flash_addr<<"] ";
*/
		    return 3;
		}

		int EOR::trace(Core *core, std::ostream & ostr) const {
		    ostr << "EOR " << reg(Rd) << ", " << reg(Rr);
		    return 1;
		}

		int ESPM::trace(Core * /*core*/, std::ostream & ostr) const {
		    ostr << "ESPM";
		    return 0;
		}

		int FMUL::trace(Core *core, std::ostream & ostr) const {
		    ostr << "FMUL " << reg(Rd) << ", " << reg(Rr);
		    return 2;
		}

		int FMULS::trace(Core *core, std::ostream & ostr) const {
			ostr << "FMULS " << reg(Rd) << ", " << reg(Rr);
		    return 2;
		}

		int FMULSU::trace(Core *core, std::ostream & ostr) const {
			ostr << "FMULSU " << reg(Rd) << ", " << reg(Rr);
		    return 2;
		}

		int ICALL::trace(Core *core, std::ostream & ostr) const {
		    ostr << "ICALL";
		    return core->pcBytes() + 1;
		}

		int IJMP::trace(Core * /*core*/, std::ostream & ostr) const {
		    ostr << "IJMP";
		    return 2;
		}

		int IN::trace(Core *core, std::ostream & ostr) const {
		    ostr << "IN " << reg(Rd) << ", " << core->getIoreg(ioreg);
		    return 1;
		}

		int INC::trace(Core *core, std::ostream & ostr) const {
		    ostr << "INC " << reg(Rd);
		    return 1;
		}

		int JMP::trace(Core * /*core*/, std::ostream & ostr) const {
		    ostr << "JMP";
		    return 3;
		}

		int LDD_Y::trace(Core *core, std::ostream & ostr) const {
		    ostr << "LD " << reg(Rd) << ", Y+" << hexchar(K);
		    return 2;
		}

		int LDD_Z::trace(Core *core, std::ostream & ostr) const {
		    ostr << "LDD " << reg(Rd) << ", Z";
		    return 2;
		}

		int LDI::trace(Core *core, std::ostream & ostr) const {
		    ostr << "LDI " << reg(Rd) << ", " << hexchar(K);
		    return 1;
		}

		int LDS::trace(Core *core, std::ostream & ostr) const {
		    ostr << "LDS " << reg(Rd);
		    //<< ", " << hex << "0x" << offset << dec  << " ";
		    return 2;
		}

		int LD_X::trace(Core *core, std::ostream & ostr) const {
			ostr << "LD " << reg(Rd) << ", X ";
		    return 2;
		}

		int LD_X_decr::trace(Core *core, std::ostream & ostr) const {
		    ostr << "LD " << reg(Rd) << ", -X";
		    return 2;
		}

		int LD_X_incr::trace(Core *core, std::ostream & ostr) const {
		    ostr << "LD " << reg(Rd) << ", X+";
		    return 2;
		}

		int LD_Y_decr::trace(Core *core, std::ostream & ostr) const {
		    ostr << "LD " << reg(Rd) << ", -Y";
		    return 2;
		}

		int LD_Y_incr::trace(Core *core, std::ostream & ostr) const {
		    ostr << "LD " << reg(Rd) << ", Y+";
		    return 2;
		}

		int LD_Z_incr::trace(Core *core, std::ostream & ostr) const {
		    ostr << "LD " << reg(Rd) << ", Z+";
		    return 2;
		}

		int LD_Z_decr::trace(Core *core, std::ostream & ostr) const {
		    ostr << "LD R" << reg(Rd) << ", -Z";
		    return 2;
		}

		int LPM_Z::trace(Core *core, std::ostream & ostr) const {
		    ostr << "LPM " << reg(Rd) << ", Z";

		    /* Z is R31:R30 */
		    //int Z = ((*(core->R))[ 31] << 8) + (*(core->R))[ 30];
		    //string sym(core->Flash->GetSymbolAtAddressLpm(Z));
		    //string sym(core->Flash->GetSymbolAtAddress(Z));
		    //ostr << "FLASH[" << hex << Z << "," << sym << "] ";

		    return 3;
		}

		int LPM::trace(Core * /*core*/, std::ostream & ostr) const {
		    ostr << "LPM";

		    /* Z is R31:R30 */
		    //int Z = ((*(core->R))[ 31] << 8) + (*(core->R))[ 30];
		    //string sym(core->Flash->GetSymbolAtAddressLpm(Z));
		    //string sym(core->Flash->GetSymbolAtAddress(Z));
		    //ostr << "FLASH[" << hex << Z << "," << sym << "] ";

		    return 3;
		}

		int LPM_Z_incr::trace(Core *core, std::ostream & ostr) const {
		    ostr << "LPM " << reg(Rd) << ", Z+";

		    /* Z is R31:R30 */
		    //int Z = ((*(core->R))[ 31] << 8) + (*(core->R))[ 30];
		    //string sym(core->Flash->GetSymbolAtAddressLpm(Z));
		    //string sym(core->Flash->GetSymbolAtAddress(Z));
		    //ostr << "FLASH[" << hex << Z << "," << sym << "] ";

		    return 3;
		}

		int LSR::trace(Core *core, std::ostream & ostr) const {
		    ostr << "LSR " << reg(Rd);
		    return 1;
		}

		int MOV::trace(Core *core, std::ostream & ostr) const {
		    ostr << "MOV " << reg(Rd) << ", " << reg(Rr);
		    return 1;
		}

		int MOVW::trace(Core *core, std::ostream & ostr) const {
		    ostr << "MOVW " << duoreg(Rdh,Rdl) << ", " << duoreg(Rrh,Rrl);
		    return 1;
		}

		int MUL::trace(Core *core, std::ostream & ostr) const {
		    ostr << "MUL " << reg(Rd) << ", " << reg(Rr);
		    return 2;
		}

		int MULS::trace(Core *core, std::ostream & ostr) const {
		    ostr << "MULS " << reg(Rd) << ", " << reg(Rr);
		    return 2;
		}

		int MULSU::trace(Core *core, std::ostream & ostr) const {
		    ostr << "MULSU " << reg(Rd) << ", " << reg(Rr);
		    return 2;
		}

		int NEG::trace(Core *core, std::ostream & ostr) const {
		    ostr << "NEG " << reg(Rd);
		    return 1;
		}

		int NOP::trace(Core * /*core*/, std::ostream & ostr) const {
		    ostr << "NOP";
		    return 1;
		}

		int OR::trace(Core *core, std::ostream & ostr) const {
		    ostr << "OR " << reg(Rd) << ", " << reg(Rr);
		    return 1;
		}

		int ORI::trace(Core *core, std::ostream & ostr) const {
		    ostr << "ORI " << reg(Rd) << ", " << hexchar(K);
		    return 1;
		}

		int OUT::trace(Core *core, std::ostream & ostr) const {
		    ostr << "OUT " << core->getIoreg(ioreg) << ", " << reg(Rd);
		    return 1;
		}

		int POP::trace(Core *core, std::ostream & ostr) const {
		    ostr << "POP " << reg(Rd);
		    return 2;
		}

		int PUSH::trace(Core *core, std::ostream & ostr) const {
		    ostr << "PUSH " << reg(Rd);
		    return 2;
		}

		int RCALL::trace(Core * /*core*/, std::ostream & ostr) const {
		    ostr << "RCALL " << hexchar(K);
		    return 1;
		}

		int RET::trace(Core * /*core*/, std::ostream & ostr) const {
		    ostr << "RET";
		    return 2;
		}

		int RETI::trace(Core * /*core*/, std::ostream & ostr) const {
		    ostr << "RETI";
		    return 2;
		}

		int RJMP::trace(Core * /*core*/, std::ostream & ostr) const {
		    ostr << "RJMP " << int(char(K))*2;
		    return 2;
		}

		int ROR::trace(Core *core, std::ostream & ostr) const {
		    ostr << "ROR " << reg(Rd);
		    return 1;
		}

		int SBC::trace(Core *core, std::ostream & ostr) const {
		    ostr << "SBC " << reg(Rd) << ", " << reg(Rr);
		    return 1;
		}

		int SBCI::trace(Core *core, std::ostream & ostr) const {
		    ostr << "SBCI " << reg(Rd) << ", " << hexchar(K);
		    return 1;
		}

		int SBI::trace(Core *core, std::ostream & ostr) const {
		    ostr << "SBI " << core->getIoreg(ioreg) << ", " << hexchar(K);
		    return 2;
		}

		int SBIC::trace(Core *core, std::ostream & ostr) const {
		    ostr << "SBIC " << core->getIoreg(ioreg) << ", " << hexchar(K);
		    return 2;
		}

		int SBIS::trace(Core *core, std::ostream & ostr) const {
		    ostr << "SBIS " << core->getIoreg(ioreg) << ", " << hexchar(K);
		    return 2;
		}

		int SBIW::trace(Core *core, std::ostream & ostr) const {
		    ostr << "SBIW " << duoreg(Rh,Rl) << ", " << hexchar(K);
		    return 2;
		}

		int SBRC::trace(Core *core, std::ostream & ostr) const {
		    ostr << "SBRC " << reg(Rd) << ", " << hexchar(K);
		    return 1;
		}

		int SBRS::trace(Core *core, std::ostream & ostr) const {
		    ostr << "SBRS " << reg(Rd) << ", " << hexchar(K);
		    return 1;
		}

		int SLEEP::trace(Core * /*core*/, std::ostream & ostr) const {
		    ostr << "SLEEP";
		    return 0;
		}

		int SPM::trace(Core * /*core*/, std::ostream & ostr) const {
		    ostr << "SPM";
		    return 0;
		}

		int STD_Y::trace(Core *core, std::ostream & ostr) const {
		    ostr << "STD Y+"<< hexchar(K) <<", " << reg(Rd);
		    return 2;
		}

		int STD_Z::trace(Core *core, std::ostream & ostr) const {
		    ostr << "STD Z, " << reg(Rd);
		    return 2;
		}

		int STS::trace(Core *core, std::ostream & ostr) const {
		    ostr << "STS " << reg(Rd);
		    return 2;
		}

		int ST_X::trace(Core *core, std::ostream & ostr) const {
		    ostr << "ST X, " << reg(Rd);
		    return 2;
		}

		int ST_X_decr::trace(Core *core, std::ostream & ostr) const {
		    ostr << "ST -X, " << reg(Rd);
		    return 2;
		}

		int ST_X_incr::trace(Core *core, std::ostream & ostr) const {
		    ostr << "ST X+, " << reg(Rd);
		    return 2;
		}

		int ST_Y_decr::trace(Core *core, std::ostream & ostr) const {
		    ostr << "ST -Y, " << reg(Rd);
		    return 2;
		}

		int ST_Y_incr::trace(Core *core, std::ostream & ostr) const {
		    ostr << "ST Y+, " << reg(Rd);
		    return 2;
		}

		int ST_Z_decr::trace(Core *core, std::ostream & ostr) const {
		    ostr << "ST -Z, " << reg(Rd);
		    return 2;
		}

		int ST_Z_incr::trace(Core *core, std::ostream & ostr) const {
		    ostr << "ST Z+, " << reg(Rd);
		    return 2;
		}

		int SUB::trace(Core *core, std::ostream & ostr) const {
		    ostr << "SUB " << reg(Rd) << ", " << reg(Rr);
		    return 1;
		}

		int SUBI::trace(Core *core, std::ostream & ostr) const {
		    ostr << "SUBI " << reg(Rd) << ", " << hexchar(K);
		    return 1;
		}

		int SWAP::trace(Core *core, std::ostream & ostr) const {
		    ostr << "SWAP " << reg(Rd);
		    return 1;
		}

		int WDR::trace(Core * /*core*/, std::ostream & ostr) const {
		    ostr << "WDR";
		    return 1;
		}

		int BREAK::trace(Core * /*core*/, std::ostream & ostr) const {
		    ostr << "BREAK";
		    return 1;
		}

		int ILLEGAL::trace(Core * /*core*/, std::ostream & /*ostr*/) const {
		    return 0;
		}

	}

}
