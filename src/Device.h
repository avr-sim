#ifndef DEVICE_H_
#define DEVICE_H_

#include "SimulationObject.h"
#include "Bus.h"

#include <vector>
#include <map>

namespace avr {
	typedef unsigned long long ClockFrequency;

	class Pin;
	class ERam;
	class Core;
	class Hardware;
	class Eeprom;
	class DebugInterface;

	class Analyzer;
	class ScriptEngine;
	class Program;
	class HardwareSettings;
	class DeviceSettings;

	/**
	 * @author Tom Haber
	 * @date Apr 21, 2008
	 * @brief A facade to the complete Atmel AVR chip.
	 *
	 * This class represents a complete Atmel AVR chip and
	 * forms a facade for the outside "world".
	 * The class contains some useful construction methods
	 * used by the DeviceSettings class.
	*/
	class Device : public sim::SimulationObject {
		public:
			Device(sim::SimulationClock & clock,
					const char *devicename = 0,
					bool allowStopping = true);
			~Device();

		public:
			/**
			 * Loads the useful sections from the program file
			 * into eeprom, sram and flash.
			 */
			void load(Program & program);

			/**
			 * Instantiates and adds an analyzer to the core.
			 */
			void addAnalyzer(const char *name);

			/**
			 * Adds an analyzer to the core.
			 */
			void addAnalyzer(Analyzer *analyzer);

			/**
			 * Adds a trace analyzer to the core
			 * which outputs to the specified file.
			 */
			void trace(const char *tracefile);

			/**
			 * Performs a single cpu cycle.
			 */
			void step();

			/**
			 * Change the clock frequency of the device.
			 */
			void setClockFrequency(ClockFrequency frq);

			/**
			 * Reset the chip.
			 */
			void reset(unsigned int type);

			Pin *getPin(unsigned int id) const;
			Pin *getPin(const std::string & name) const;

		public:
			/**
			 * Returns an interface for debugging, this interface
			 * has access to some protected parts of the core.
			 */
			DebugInterface *debugInterface();

		private:
			// Construction methods for DeviceSettings
			void buildCore(unsigned int ioSpaceSize, unsigned int ramSize,
							unsigned int flashSize, unsigned int pageSize,
							unsigned int stackMask, int pcBytes = 2,
							ERam *eram = 0);
			void buildHardware(const char *hwname, HardwareSettings & hws);
			void addIOReg(unsigned int address, const std::string & name,
					unsigned char initial = 0);
			void addInterrupt(unsigned int vector, unsigned int address,
								const char *name);
			void addPin(unsigned int id, const char *name, unsigned int num);

		private:
			static const ClockFrequency defaultFrequency = 8000000;
			sim::ClockOffset frq;
			Core *core;
			bool allowStopping;

			Bus bus;
			Eeprom *eeprom;

			std::vector<Pin *> pins;
			std::map<std::string, Pin *> name2pin;

		friend class DeviceSettings;
	};

	inline Pin *Device::getPin(unsigned int id) const {
		if( id >= pins.size() )
			return 0;
		return pins[ id - 1 ];
	}

}
#endif /*DEVICE_H_*/
