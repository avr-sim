#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include "Exception.h"
#include "Format.h"
#include "Types.h"

namespace avr {

	class Core;

	/**
	 * @author Tom Haber
	 * @date Apr 21, 2008
	 * @brief Thrown on illegal instructions
	 *
	 * Thrown on illegal instructions, the opcode is available.
	 */
	class IllegalInstruction : public util::Exception {
		public:
			IllegalInstruction(word opcode)
				: Exception( util::format("Illegal Instruction: %x") % opcode ) {}
	};

	/**
	 * \author Tom Haber
	 * \date Apr 21, 2008
	 * \brief An interface for all instructions on the chip.
	 *
	 * An interface for all instructions on the chip.
	 * It has a method for executing instructions on the Core
	 * and tracing (pretty printing) them.
	 */
	class Instruction {
		public:
			Instruction() {}
			virtual ~Instruction() {}

		public:
			/**
			 * Executes an instruction on the Core.
			 */
	        virtual int operator()(Core *core) const = 0;

	        /**
			 * Pretty prints an instruction
			 */
	        virtual int trace(Core *core, std::ostream & ostr) const = 0;

	        /**
			 * Returns whether the instruction has 2 words or just 1.
			 */
	        virtual bool is2Word() const { return false; }
	};

}

#endif /*INSTRUCTION_H*/
