/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Core.h"
#include "RuntimeException.h"
#include "ImplementationException.h"
#include "Instruction.h"
#include "Bus.h"
#include "Util.h"
#include "Analyzer.h"
#include "DebugInterface.h"

namespace avr {

	Core::Core(Bus & bus, unsigned int ioSpaceSize, unsigned int ramSize,
				unsigned int flashSize, unsigned int pageSize,
				word stackMask, int pcBytes, ERam *eram /*= 0*/)
			: mmu( ioSpaceSize, ramSize, eram ),
			    flash(bus, flashSize, pageSize),
				stack(bus, mmu, stackMask), bus(bus),
				pc_bytes( pcBytes ), dbgi(0) {
	}

	Core::~Core() {
		std::for_each( analyzers.begin(), analyzers.end(), Delete<Analyzer>());
	}

	void Core::init() {
		// Get some special registers
		SReg = mmu.statusReg();

		stack.attachReg("sph", mmu.getIoreg("SPH"));
		stack.attachReg("spl", mmu.getIoreg("SPL"));
		stack.finishBuild();

		flash.attachReg("spmcsr", mmu.getIoreg("SPMCR"));
		flash.finishBuild();
	}

	static void analyzeHelper(const std::list<Analyzer*> & ans,
							void (Analyzer::*method)()) {
		MethodCall<Analyzer> call(method);
		std::for_each( ans.begin(), ans.end(), call);
	}

	template <class T>
	static void analyzeHelper(const std::list<Analyzer*> & ans,
							void (Analyzer::*method)(T), T val) {
		MethodCall1<Analyzer,T> call(method, val);
		std::for_each( ans.begin(), ans.end(), call);
	}

	template <class T1, class T2>
	static void analyzeHelper(const std::list<Analyzer*> & ans,
							void (Analyzer::*method)(T1,T2),
							T1 x, T2 y) {
		MethodCall2<Analyzer,T1,T2> call(method, x, y);
		std::for_each( ans.begin(), ans.end(), call);
	}

	byte Core::readRegister(unsigned int i) const {
		byte val = mmu.reg(i);
		analyzeHelper( analyzers, &Analyzer::readRegister, i, val );
		return val;
	}

	void Core::writeRegister(unsigned int i, byte val) {
		mmu.reg(i) = val;
		analyzeHelper( analyzers, &Analyzer::writeRegister, i, val );
	}

	byte Core::readIORegister(unsigned int r) {
		IORegister & reg = mmu.getIoreg(r);

		byte val = reg;
		analyzeHelper( analyzers, &Analyzer::readRegister,
						(unsigned int)reg.getAddress(), val );
		return val;
	}

	void Core::writeIORegister(unsigned int r, byte val) {
		IORegister & reg = mmu.getIoreg(r);
		reg = val;
		analyzeHelper( analyzers, &Analyzer::writeRegister,
						(unsigned int)reg.getAddress(), val );
	}

	byte Core::readByte(unsigned int addr) const {
		byte val = mmu.readByte(addr);
		analyzeHelper( analyzers, &Analyzer::readByte, addr, val );
		return val;
	}

	void Core::writeByte(unsigned int addr, byte val) {
		mmu.writeByte(addr, val);
		analyzeHelper( analyzers, &Analyzer::writeByte, addr, val );
	}

	byte Core::readFlash(unsigned int addr) const {
		byte val = flash.readByte(addr);
		analyzeHelper( analyzers, &Analyzer::readFlash, addr, val );
		return val;
	}

	int Core::writeFlash(unsigned int Z, word data) {
		flash.storeProgramMemory(Z, data & 0xff, (data>>8) & 0xff);
		analyzeHelper( analyzers, &Analyzer::writeFlash, Z, data );
		return 0; //XXX
	}

	void Core::push(byte val) {
		stack.push(val);
		analyzeHelper( analyzers, &Analyzer::push, val );
	}

	byte Core::pop() {
		byte val = stack.pop();
		analyzeHelper( analyzers, &Analyzer::push, val );
		return val;
	}

	void Core::jump(sbyte offset, bool push /*= false*/) {
		if( push ) {
			dword val = (PC+1);
		    for(int tt = 0; tt < pc_bytes; tt++) {
		    	stack.push( val & 0xff );
		        val >>= 8;
		    }
		}

		PC += offset;
		analyzeHelper( analyzers, &Analyzer::jump, (PC+1)<<1, push );
	}

	void Core::call(dword addr, bool push /*= true*/) {
		if( push ) {
			dword val = (PC+1);
		    for(int tt = 0; tt < pc_bytes; tt++) {
		    	stack.push( val & 0xff );
		        val >>= 8;
		    }
		}

		PC = addr;
		analyzeHelper( analyzers, &Analyzer::call, (PC+1)<<1, push );
	}

	void Core::ret(bool interrupt /*= false*/) {
	    dword val = 0;
	    for(int tt = 0; tt < pc_bytes; tt++) {
	        val = val<<8;
	        val |= stack.pop();
	    }

	    justReturnedFromInterrupt = interrupt;
	    PC = val - 1;
	    analyzeHelper( analyzers, &Analyzer::ret, interrupt );
	}

	word Core::fetchOperand() {
		PC++;
		unsigned int addr = PC<<1;
		word op = flash.readWord( addr );
		analyzeHelper( analyzers, &Analyzer::fetchOperand, op );
		return op;
	}

	int Core::skip() {
		int skip = 1;
		unsigned int addr = (PC+1)<<1;
		word opcode = flash.readWord( addr );
		if( decoder.is2WordInstruction(opcode) )
			skip = 2;

		PC += skip;
		analyzeHelper( analyzers, &Analyzer::skip );
		return skip;
	}

	void Core::reset(unsigned int type) {
		cpuCycles = 0;
		PC = 0;

		mmu.reset();
		bus.reset();

		stoppedMode = false;
		justReturnedFromInterrupt = false;
		sleepMode = SLEEP_MODE_NONE;

		analyzeHelper( analyzers, &Analyzer::reset, type );
	}

	void Core::sleep() {
		IORegister & mcucr = *mmu.getIoreg("MCUCR");
		static const byte sleepEnable = (1<<5);
		if( mcucr & sleepEnable ) {
			//const IORegister *emcucr = mmu.getIoreg("EMCUCR", false);
			sleepMode = SLEEP_MODE_IDLE;

			// TODO The bit locations for the sleep mode are different between devices...
			throw util::ImplementationException("Instruction SLEEP not fully implemented");
		}

		analyzeHelper( analyzers, &Analyzer::sleep, (unsigned int)sleepMode );
	}

	void Core::systemBreak() {
		stoppedMode = true;
		analyzeHelper( analyzers, &Analyzer::systemBreak );
	}

	bool Core::invokeInterrupt() {
		// TODO IVSEL bit in GICR

		unsigned int vector = bus.pendingInterrupt();
		unsigned int addr = bus.interruptVectorAddress(vector);

		// Wake up from sleep
		if( sleepMode != SLEEP_MODE_NONE )
			sleepMode = SLEEP_MODE_NONE;

		PC--;

		unsigned int offset = 0;

		dword val = (PC+1);
		for(int tt = 0; tt < pc_bytes; tt++) {
			stack.push( val & 0xff );
		    val >>= 8;
		}

		PC = offset + addr;

		analyzeHelper( analyzers, &Analyzer::interrupt, vector, (PC+1)<<1 );
		bus.beforeInvokeInterrupt(vector);
		return true;
	}

	bool Core::step() {
		analyzeHelper<lword>( analyzers, &Analyzer::step, bus.ticks() );

		// Are we sleeping?
        if( sleepMode != SLEEP_MODE_NONE )
            return true;

        if( cpuCycles > 0 ) {
        	// Waiting for instruction to finish
        	cpuCycles--;
        	return (cpuCycles == 0);
        }

		if( justReturnedFromInterrupt ) {
			// don't process the interrupt if we just returned from
			// an interrupt handler, because the hardware manual says
			// that at least one instruction is executed after
			// returning from an interrupt.
			justReturnedFromInterrupt = false;
		} else {
			static const unsigned char Ibit = 0x80;
			IORegister & S = getIoreg(SReg);
			if( (S & Ibit) != 0 ) {
				// check if there are any pending interrupts
				if( bus.isInterruptPending() ) {
					invokeInterrupt();
					S &= (unsigned char)~Ibit;

					cpuCycles = 0;
					return true;
				}
			}
		}

		try {
			unsigned int addr = PC<<1;
			analyzeHelper( analyzers, &Analyzer::trace, addr );

			if( (dbgi == 0) || ! dbgi->checkBreak(addr) ) {
				// Fetch instruction
				word opcode = flash.readWord( addr );
				Instruction & instr = decoder.decode(opcode);
				cpuCycles = instr(this) - 1;

				PC++;
			}
		} catch( util::ImplementationException & ex ) {
			std::cerr << ex.message() << ": continuing" << std::endl;
			PC++;
			cpuCycles = 0;
		}

		return (cpuCycles == 0);
	}
}
