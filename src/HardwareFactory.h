#ifndef AVR_HARDWAREFACTORY_H
#define AVR_HARDWAREFACTORY_H

namespace avr {

	class Bus;
	class Hardware;
	class HardwareSettings;

	/**
	 * @author Tom Haber
	 * @date Apr 26, 2008
	 * @brief Factory for the internal devices.
	 *
	 *  A factory for creating internal devices based on their name.
	 */
	class HardwareFactory {
		public:
			/**
			 * Build an internal device with name \e name and
			 * settings \e hws
			 */
			static Hardware *build(const char *name,
					HardwareSettings & hws, Bus & bus);

		private:
			static Hardware *buildEeprom(HardwareSettings & hws, Bus & bus);
			static Hardware *buildPort(HardwareSettings & hws, Bus & bus);
			static Hardware *buildTimer8(HardwareSettings & hws, Bus & bus);
			static Hardware *buildTimer16(HardwareSettings & hws, Bus & bus);
			static Hardware *buildTimerIrq(HardwareSettings & hws, Bus & bus);
			static Hardware *buildUsart(HardwareSettings & hws, Bus & bus);
			static Hardware *buildSpi(HardwareSettings & hws, Bus & bus);
			static Hardware *buildADC(HardwareSettings & hws, Bus & bus);
	};

}

#endif /*AVR_HARDWAREFACTORY_H*/
