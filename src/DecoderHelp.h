#ifndef AVR_DECODERHELP_H
#define AVR_DECODERHELP_H

namespace avr {

	enum decoder_operand_masks {
	    /** 2 bit register id  ( R24, R26, R28, R30 ) */
	    mask_Rd_2     = 0x0030,
	    /** 3 bit register id  ( R16 - R23 ) */
	    mask_Rd_3     = 0x0070,
	    /** 4 bit register id  ( R16 - R31 ) */
	    mask_Rd_4     = 0x00f0,
	    /** 5 bit register id  ( R00 - R31 ) */
	    mask_Rd_5     = 0x01f0,

	    /** 3 bit register id  ( R16 - R23 ) */
	    mask_Rr_3     = 0x0007,
	    /** 4 bit register id  ( R16 - R31 ) */
	    mask_Rr_4     = 0x000f,
	    /** 5 bit register id  ( R00 - R31 ) */
	    mask_Rr_5     = 0x020f,

	    /** for 8 bit constant */
	    mask_K_8      = 0x0F0F,
	    /** for 6 bit constant */
	    mask_K_6      = 0x00CF,

	    /** for 7 bit relative address */
	    mask_k_7      = 0x03F8,
	    /** for 12 bit relative address */
	    mask_k_12     = 0x0FFF,
	    /** for 22 bit absolute address */
	    mask_k_22     = 0x01F1,

	    /** register bit select */
	    mask_reg_bit  = 0x0007,
	    /** status register bit select */
	    mask_sreg_bit = 0x0070,
	    /** address displacement (q) */
	    mask_q_displ  = 0x2C07,

	    /** 5 bit register id  ( R00 - R31 ) */
	    mask_A_5      = 0x00F8,
	    /** 6 bit IO port id */
	    mask_A_6      = 0x060F
	};

	inline int get_rd_2( word opcode ) {
	    int reg = ((opcode & mask_Rd_2) >> 4) & 0x3;
	    return (reg * 2) + 24;
	}

	inline int get_rd_3( word opcode ) {
	    int reg = opcode & mask_Rd_3;
	    return ((reg >> 4) & 0x7) + 16;
	}

	inline int get_rd_4( word opcode ) {
	    int reg = opcode & mask_Rd_4;
	    return ((reg >> 4) & 0xf) + 16;
	}

	inline int get_rd_5( word opcode ) {
	    int reg = opcode & mask_Rd_5;
	    return ((reg >> 4) & 0x1f);
	}

	inline int get_rr_3( word opcode ) {
	    return (opcode & mask_Rr_3) + 16;
	}

	inline int get_rr_4( word opcode ) {
	    return (opcode & mask_Rr_4) + 16;
	}

	inline int get_rr_5( word opcode ) {
	    int reg = opcode & mask_Rr_5;
	    return (reg & 0xf) + ((reg >> 5) & 0x10);
	}

	inline byte get_K_8( word opcode ) {
	    int K = opcode & mask_K_8;
	    return ((K >> 4) & 0xf0) + (K & 0xf);
	}

	inline byte get_K_6( word opcode ) {
	    int K = opcode & mask_K_6;
	    return ((K >> 2) & 0x0030) + (K & 0xf);
	}

	inline int get_k_7( word opcode ) {
	    return (((opcode & mask_k_7) >> 3) & 0x7f);
	}

	inline int get_k_12( word opcode ) {
	    return (opcode & mask_k_12);
	}

	inline int get_k_22( word opcode ) {
	    /* Masks only the upper 6 bits of the address, the other 16 bits
	     * are in PC + 1. */
	    int k = opcode & mask_k_22;
	    return ((k >> 3) & 0x003e) + (k & 0x1);
	}

	inline int get_reg_bit( word opcode ) {
	    return opcode & mask_reg_bit;
	}

	inline int get_sreg_bit( word opcode ) {
	    return (opcode & mask_sreg_bit) >> 4;
	}

	inline int get_q( word opcode ) {
	    /* 00q0 qq00 0000 0qqq : Yuck! */
	    int q = opcode & mask_q_displ;
	    int qq = ( ((q >> 1) & 0x1000) + (q & 0x0c00) ) >> 7;
	    return (qq & 0x0038) + (q & 0x7);
	}

	inline int get_A_5( word opcode ) {
	    return (opcode & mask_A_5) >> 3;
	}

	inline int get_A_6( word opcode ) {
	    int A = opcode & mask_A_6;
	    return ((A >> 5) & 0x0030) + (A & 0xf);
	}

}

#endif /*AVR_DECODERHELP_H*/
