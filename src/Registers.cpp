/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Registers.h"
#include "Util.h"
#include "Hardware.h"
#include <algorithm>

#include "Format.h"
#include "RuntimeException.h"

namespace avr {

	IORegisters::IORegisters(unsigned int count) {
		regs.resize( count );
		setEach( regs.begin(), regs.end(), (IORegister *)0 );
	}

	IORegisters::~IORegisters() {
		std::for_each( regs.begin(), regs.end(), Delete<IORegister>() );
	}

	void IORegisters::addReg( word addr, IORegister *reg ) {
		if( regs[addr] != 0 )
			throw util::RuntimeException(
					util::format("Register %s shares I/O location with %s")
						% reg->getName() % regs[addr]->getName() );

		regs[ addr ] = reg;
	}

	IORegister *IORegisters::getIoreg(const std::string & name, bool required) const {
		std::vector<IORegister *>::const_iterator it;
		for(it = regs.begin(); it != regs.end(); ++it) {
			if( ((*it) != 0) && (*it)->getName() == name )
				break;
		}

		if( it == regs.end() ) {
			if( ! required )
				return 0;

			throw util::RuntimeException(
					util::format("Unable to find register %s") % name );
		}

		return (*it);
	}

	void IORegisters::reset() {
		std::for_each( regs.begin(), regs.end(),
				MethodCall<IORegister>(&IORegister::reset) );
	}

	IORegister::operator unsigned char() {
		if( hw != 0 )
			hw->regAccess( this );

		return val;
	}

    unsigned char IORegister::operator =(unsigned char val) {
    	unsigned char ret = Register::operator =(val);
    	if( hw != 0 )
    		hw->regChanged( this );
    	return ret;
    }

    void IORegister::operator |=(unsigned char val) {
    	this->val |= val;
    	if( hw != 0 )
    		hw->regChanged( this );
    }

    void IORegister::operator &=(unsigned char val) {
    	this->val &= val;
    	if( hw != 0 )
    		hw->regChanged( this );
    }

}
