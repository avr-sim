#ifndef AVR_STACK_H
#define AVR_STACK_H

#include "MMU.h"
#include "Hardware.h"

namespace avr {

	/**
	 * @author Tom Haber
	 * @date Apr 23, 2008
	 * @brief Represents the stack within the chip.
	 *
	 * A software stack implementation. Provides methods
	 * for pushing and popping.
	 */
	class Stack : public Hardware {
		public:
			Stack(Bus & bus, MMU & mmu, word mask);

		public:
			word getSP() const;
			unsigned char getSPH() const;
			unsigned char getSPL() const;

			void setSP(word sp);

		public:
			void push(unsigned char val);
			unsigned char pop();

		public:
			bool attachReg(const char *name, IORegister *reg);
			bool finishBuild();
			void regChanged(IORegister *reg);

		private:
			void setSPH(unsigned char sph);
			void setSPL(unsigned char spl);

		private:
			MMU & mmu;
			word mask;
			word sp;

		private:
			Register *sph;
			Register *spl;
	};

	inline unsigned char Stack::getSPH() const {
		return (sp>>8) & 0xff;
	}

	inline unsigned char Stack::getSPL() const {
		return (sp & 0xff);
	}

	inline word Stack::getSP() const {
		return sp;
	}

	inline void Stack::setSPH(unsigned char sph) {
		sp = sp & 0x00ff;
		sp |= (sph<<8);
	    sp &= mask;
	}

	inline void Stack::setSPL(unsigned char spl) {
		sp = sp & 0xff00;
		sp |= spl;
	    sp &= mask;
	}

}

#endif /*AVR_STACK_H*/
