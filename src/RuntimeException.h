#ifndef UTIL_RUNTIMEEXCEPTION_H
#define UTIL_RUNTIMEEXCEPTION_H

#include "Exception.h"

namespace util {
	/**
	 * @author Tom Haber
	 * @date Apr 21, 2008
	 * @brief Generic Exception for most runtime issues
	 *
	 * RuntimeException is thrown when some runtime operation
	 * causes the simulator to enter a bad state.
	 */
	class RuntimeException : public Exception {
		public:
			RuntimeException() : Exception("Runtime Exception") {}
			RuntimeException(const char *s) : Exception(s) {}
			RuntimeException(const std::string & s) : Exception(s) {}
	};
}
#endif
