#ifndef AVR_PROGRAM_H
#define AVR_PROGRAM_H

#include <string>
#include <map>

namespace avr {

	/**
	 * @author Tom Haber
	 * @date May 13, 2008
	 * @brief Abstraction of program file formats
	 *
	 * A single section of the program. Contains the data
	 * associated with a specific address range and location (Flash or Eeprom)
	 */
	class Section {
		public:
			enum Type { NONE, FLASH, EEPROM };
			Section() : type(NONE), addr(0), siz(0), d(0) {}
			Section(Type type, unsigned int address, unsigned int size);
			~Section();

		public:
			void init(Type type, unsigned int address, unsigned int size);
			void clear();

		public:
			unsigned char *data() { return d; }
			unsigned int size() const { return siz; }
			unsigned int address() const { return addr; }
			bool isFlash() const { return type == FLASH; }
			bool isEeprom() const { return type == EEPROM; }

		private:
			Type type;
			unsigned int addr;
			unsigned int siz;
			unsigned char *d;
	};

	/**
	 * @author Tom Haber
	 * @date May 23, 2008
	 * @brief Abstraction of a Program
	 *
	 * Abstraction of a Program, contains methods for
	 * loading files and reading sections.
	 * Additionally, it stores the symbols with the program
	 * and has convenient lookup functions for flash and data
	 * symbols.
	 */
	class Program {
		public:
			virtual ~Program() {}

		public:
			/**
			 * Opens and loads a file.
			 */
			virtual void load(const char * filename) = 0;

			/**
			 * Reads the next available section from the file
			 * and returns the data in \e sec.
			 *
			 * \returns false if no further sections are available.
			 */
			virtual bool readNextSection(Section & sec) = 0;

		public:
			const std::string & functionName(int addr) const;
			const std::string & dataName(int addr) const;

			int functionAddress(const std::string & name) const;
			int dataAddress(const std::string & name) const;

		protected:
			void addSymbolFlash(int addr, const char *name);
			void addSymbolEeprom(int addr, const char *name);
			void addSymbolRam(int addr, const char *name);

		private:
			typedef std::map<int, std::string> SymTable;
			SymTable flashSymbols;
			SymTable dataSymbols;

			static const std::string empty;
	};

	inline Section::Section(Type type, unsigned int address, unsigned int size) {
		init( type, address, size );
	}

	inline Section::~Section() {
		clear();
	}

	inline void Section::init(Type type, unsigned int address, unsigned int size) {
		this->type = type;
		this->addr = address;
		this->siz = size;

		this->d = new unsigned char[size];
	}

	inline void Section::clear() {
		if( d != 0 ) {
			delete [] d;
			d = 0;
		}

		addr = 0;
		siz = 0;
		type = NONE;
	}

}

#endif /*AVR_PROGRAM_H*/
