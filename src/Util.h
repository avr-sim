#ifndef UTIL_H
#define UTIL_H

#include <vector>
#include <string>
#include <algorithm>

namespace {

	template<class T> class Delete {
		public:
			inline void operator ()(T* & obj) {
				delete obj;
				obj = 0;
			}
	};

	template<class T> void trimVector(std::vector<T> & v) {
		std::vector<T>(v).swap(v);
	}

	template<typename InputIter, typename Type> const Type & setEach(
	        InputIter first, InputIter last, const Type & t) {
		for (; first != last; ++first)
			*first = t;

		return t;
	}

	inline bool startsWith(const std::string & str, const std::string & prefix,
							std::string::size_type pos = 0) {
		return str.find(prefix, pos) == 0;
	}

	inline bool endsWith(const std::string & str, const std::string & postfix) {
		size_t n = postfix.length();
		return str.find(postfix, str.length()-n-1) != std::string::npos;
	}

	// C++ memset */
	template<class T> inline void fill(T *ar, const T & val, unsigned long n) {
		for (unsigned long i = 0; i < n; ++i)
			ar[i] = val;
	}

#define SPACES " \t\r\n"

	inline const std::string & trim_right(std::string & s,
	        const std::string & t = SPACES) {
		std::string::size_type i(s.find_last_not_of(t) );
		if (i != std::string::npos )
			s.erase(s.find_last_not_of(t) + 1);

		return s;
	}

	inline const std::string & trim_left(std::string & s,
	        const std::string & t = SPACES) {
		return s.erase(0, s.find_first_not_of(t) );
	}

	inline const std::string & trim(std::string & s,
	        const std::string & t = SPACES) {
		trim_left(s, t);
		trim_right(s, t);
		return s;
	}

	inline void stolower(std::string & s) {
		std::transform(s.begin(), s.end(), s.begin(), tolower);
	}

	inline void stoupper(std::string & s) {
		std::transform(s.begin(), s.end(), s.begin(), toupper);
	}

	template<class Class>
	class MethodCall {
		public:
			typedef void (Class::*Method)(void);

		public:
			MethodCall(Method _method) {
				method = _method;
			};

		public:
			void operator()(Class *c) {
				if( c != 0 )
					(c->*method)();
			}

			void operator()(Class & c) {
				((&c)->*method)();
			}

		private:
			Method method;
	};

	template<class Class, class T>
	class MethodCall1 {
		public:
			typedef void (Class::*Method)(T);

		public:
			MethodCall1(Method _method, T _x) {
				method = _method;
				x = _x;
			};

		public:
			void operator()(Class *c) {
				if( c != 0 )
					(c->*method)(x);
			}

		private:
			Method method;
			T x;
	};

	template<class Class, class T1, class T2>
	class MethodCall2 {
		public:
			typedef void (Class::*Method)(T1,T2);

		public:
			MethodCall2(Method _method, T1 _x, T2 _y) {
				method = _method;
				x = _x;
				y = _y;
			};

		public:
			void operator()(Class *c) {
				if( c != 0 )
					(c->*method)(x, y);
			}

		private:
			Method method;
			T1 x;
			T2 y;
	};

}

#endif /*UTIL_H*/
