#ifndef AVR_MMU_H
#define AVR_MMU_H

#include "Memory.h"
#include "SRam.h"
#include "Registers.h"

namespace avr {
	class ERam;

	/**
	 * @author Tom Haber
	 * @date Apr 23, 2008
	 * @brief Memory Mapping Unit
	 *
	 * Manages address decoding for memory accesses: maps the different
	 * regions to the correct classes.
	 */
	class MMU {
		public:
			MMU(unsigned int ioSpaceSize, unsigned int ramSize, ERam *eram = 0);
			~MMU();

		public:
			void addIOReg(unsigned int address,
							const std::string & name, byte initial = 0);
			const Register & getR(int r) const { return R[r]; }

			Register & reg(int r);
			const Register & reg(int r) const;

			byte statusReg() const;

			IORegister & getIoreg(unsigned int offset);
			const IORegister & getIoreg(unsigned int offset) const;
			IORegister *getIoreg(const std::string & name);

			void reset();

		public:
			unsigned int ioregs() const { return regs.size(); }
			const std::string & registerName(byte addr) const;

		public:
			/**
			 * Checks whether an offset is associated with a register
			 */
			bool isRegister(unsigned int offset) const;

			/**
			 * Reads a single bytes of raw data from memory at
			 * offset \e offset.
			 *
			 * \exception AccessViolation { When the data requested is
			 * 		not available, this exception is thrown }
			 */
			byte readByte(unsigned int offset) const;

			/**
			 * Reads a word of raw data from memory starting at
			 * offset \e offset.
			 *
			 * \exception AccessViolation { When the data requested is
			 * 		not available, this exception is thrown }
			 */
			word readWord(unsigned int offset) const;

			/**
			 * Reads a single bytes of raw data from memory at
			 * offset \e offset.
			 *
			 * \exception AccessViolation { When the data requested is
			 * 		not available, this exception is thrown }
			 */
			void writeByte(unsigned int offset, byte val);

			/**
			 * Reads a word of raw data from memory starting at
			 * offset \e offset.
			 *
			 * \exception AccessViolation { When the data requested is
			 * 		not available, this exception is thrown }
			 */
			void writeWord(unsigned int offset, word val);

			const unsigned char *readRam(unsigned int offset, unsigned int size) const;
			void writeRam(unsigned char *data, unsigned int offset, unsigned int size);

		public:
			static const unsigned int registerSpaceSize = 32;

		private:
			Register R[registerSpaceSize];

			IORegisters regs;
			SRam sram;
			ERam *eram;
	};

	inline IORegister & MMU::getIoreg(unsigned int offset) {
		return regs.getIoreg(offset);
	}

	inline const IORegister & MMU::getIoreg(unsigned int offset) const {
		return regs.getIoreg(offset);
	}

	inline IORegister *MMU::getIoreg(const std::string & name) {
		return regs.getIoreg(name);
	}

	inline byte MMU::statusReg() const {
		return regs.getIoreg("SREG")->getAddress() - registerSpaceSize;
	}

	inline void MMU::reset() {
		for(unsigned int i = 0; i < registerSpaceSize; ++i)
			R[i] = 0;
		regs.reset();
	}

	inline bool MMU::isRegister(unsigned int offset) const {
		return  offset < registerSpaceSize + regs.size();
	}

}

#endif /*AVR_MMU_H_*/
