#ifndef AVR_ADCONVERTER_H
#define AVR_ADCONVERTER_H

#include "Hardware.h"

namespace avr {

	/**
	 * @author Tom Haber
	 * @date May 27, 2008
	 * @brief Analog-Digital Converter
	 */
	class ADC: public avr::Hardware {
		public:
			ADC(Bus & bus, unsigned int ccVec);
			~ADC();

		public:
			bool attachReg(const char *name, IORegister *reg);
			bool finishBuild();
			void regChanged( IORegister *reg );
			void step();
			void reset();
			void beforeInvokeInterrupt(unsigned int vector);

		private:
			void setAdcsr(unsigned char val);
			float refVoltage() const;

		private:
			IORegister *admux;
			IORegister *adcsr;
			IORegister *adch;
			IORegister *adcl;

		private:
			unsigned char adcsr_old;
			unsigned int ccVec;
			unsigned int prescaler;

			int clk;
			float sample;
	};

}

#endif /* AVR_ADCONVERTER_H */
