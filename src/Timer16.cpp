/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Timer16.h"
#include "Registers.h"
#include "Bus.h"
#include "ImplementationException.h"

#include <iostream>
#include <cstring>

enum {
    CS0  = 1 << 0,			/* timer/counter clock select bit 0 */
    CS1  = 1 << 1,
    CS2  = 1 << 2,
	WGM2 = 1 << 3,
	WGM3 = 1 << 4,
    ICES = 1 << 6,			/* input capture edge select */
    ICNC = 1 << 7,			/* input capture noise canceler (4 CKs) */
    mask_CS = (CS0 | CS1 | CS2),
    mask_WGMb = (WGM2 | WGM3)
};

enum {
    CS_STOP = 0x00,             /* Stop, the Timer/Counter is stopped */
    CS_CK = 0x01,               /* CK                                 */
    CS_CK_8 = 0x02,             /* CK/8                               */
    CS_CK_64 = 0x03,            /* CK/64                              */
    CS_CK_256 = 0x04,           /* CK/256                             */
    CS_CK_1024 = 0x05,          /* CK/1024                            */
    CS_EXT_FALL = 0x06,         /* External Pin Tn, falling edge      */
    CS_EXT_RISE = 0x07,         /* External Pin Tn, rising edge       */
};

enum {
	WGM0	= (1<<0),
	WGM1	= (1<<1),
	FOCB	= (1<<2),
	FOCA	= (1<<3),
	COMB0	= (1<<4),
	COMB1	= (1<<5),
	COMA0	= (1<<6),
	COMA1	= (1<<7),
	mask_WGMa = (WGM1|WGM0),
	mask_COMB = (COMB1|COMB0),
	mask_COMA = (COMA1|COMA0)
};

enum {
	MODE_NORMAL			= 0,
	MODE_PWM_8			= 1,
	MODE_PWM_9			= 2,
	MODE_PWM_10			= 3,
	MODE_CTC_OCR		= 4,
	MODE_FASTPWM_8		= 5,
	MODE_FASTPWM_9		= 6,
	MODE_FASTPWM_10		= 7,
	MODE_PWM_PNF_ICR	= 8,
	MODE_PWM_PNF_OCR	= 9,
	MODE_PWM_ICR		= 10,
	MODE_PWM_OCR		= 11,
	MODE_CTC_ICR		= 12,
	MODE_FASTPWM_ICR	= 14,
	MODE_FASTPWM_OCR	= 15
};

enum {
	COM_NORMAL	= 0,
	COM_TOGGLE	= 1,
	COM_CLEAR	= 2,
	COM_SET		= 3
};


namespace avr {

#define overflow() *tifr |= tovmask
#define compareMatch() *tifr |= ocfmask

	void OutputCompareUnit16::flush() {
		ocrBuf = (ocrh->get() << 8) | ocrl->get();
	}

	void OutputCompareUnit16::regChanged(IORegister *reg) {
		if( reg == ocrl )
			ocrBuf = (ocrh->get() << 8) | ocrl->get();
	}

	void OutputCompareUnit16::forceOutputCompare(IORegister *tifr, word tcnt) {
		compareMatchOutput( tifr, tcnt );
	}

	void OutputCompareUnit16::compareMatchOutput(IORegister *tifr, word tcnt) {
        // the non-PWM modes are NORMAL and CTC
        // under NORMAL, there is no pin action for a compare match
        // under CTC, the action is to clear the pin.
        if( tcnt == ocrBuf ) {
            switch( compareMode ) {
                case COM_NORMAL:
                	break;

                case COM_TOGGLE:
                    //outputComparePin.toggle();
                    break;

                case COM_CLEAR:
                    //outputComparePin.low();
                    break;

                case COM_SET:
                    //outputComparePin.high();
                    break;
            }

            compareMatch();
        }
	}

	Timer16::Timer16(Bus & bus, unsigned char tovmask, int compunits,
			unsigned char ocfmask[])
		: Hardware(bus), tovmask(tovmask), direction(1) {
		blockCompareMatch = false;
		timerMode = 0;
		period = 0;

		nUnits = compunits;
		for(int i = 0; i < nUnits; ++i)
			units[i].setMask( ocfmask[i] );
	}

	bool Timer16::attachReg(const char *name, IORegister *reg) {
		if( strcmp(name, "tcntl") == 0 )
			tcntl = reg;
		else if( strcmp(name, "tcnth") == 0 )
			tcnth = reg;
		else if( strcmp(name, "tccra") == 0 )
			tccra = reg;
		else if( strcmp(name, "tccrb") == 0 )
			tccrb = reg;
		else if( strcmp(name, "ocrah") == 0 || strcmp(name, "ocrh") == 0 )
			units[0].setOcrhRegister(reg);
		else if( strcmp(name, "ocrbh") == 0 )
			units[1].setOcrhRegister(reg);
		else if( strcmp(name, "ocral") == 0 || strcmp(name, "ocrl") == 0 )
			units[0].setOcrlRegister(reg);
		else if( strcmp(name, "ocrbl") == 0 )
			units[1].setOcrlRegister(reg);
		else if( strcmp(name, "tifr") == 0 ) {
			tifr = reg;
			return true;
		} else
			return false;

		reg->registerHW(this);
		return true;
	}

	bool Timer16::finishBuild() {
		bool unitsOk = true;
		for(int i = 0; i < nUnits; ++i)
			unitsOk = unitsOk && units[i].ocrSet();

		return (tcntl != 0) && (tcnth != 0)
				&& (tccra != 0) && (tccrb != 0)
				&& (tifr != 0) && unitsOk;
	}

#define getTimerMode() (((tccrb->get() & mask_WGMb) >> 1) | (tccra->get() & mask_WGMa))
#define getCOMa(val) (((val) & mask_COMA) >> 6)
#define getCOMb(val) (((val) & mask_COMA) >> 4)

	void Timer16::regChanged( IORegister *reg ) {
		if( reg == tccra ) {
			unsigned char val = tccra->get();

			unsigned char timerModeNew = getTimerMode();
			if( timerMode != timerModeNew )
				direction = +1;
			timerMode = timerModeNew;

			units[0].setCompareMode( getCOMa(val) );
			units[0].setCompareMode( getCOMb(val) );

			if( val & FOCA )
				units[0].forceOutputCompare(tifr, tcnt);
			if( val & FOCB )
				units[1].forceOutputCompare(tifr, tcnt);

		} else if( reg == tccrb ) {
			unsigned char val = tccrb->get();

			unsigned char timerModeNew = getTimerMode();
			if( timerMode != timerModeNew )
				direction = +1;
			timerMode = timerModeNew;

			setClock(val);
		} else if( reg == tcntl ) {
			tcnt |= (tcnth->get()<<8) | tcntl->get();
			blockCompareMatch = true;
		} else {
			if( (timerMode == MODE_NORMAL) || (timerMode == MODE_CTC_OCR)
					 || (timerMode == MODE_CTC_ICR) ) {
				for(int i = 0; i < nUnits; ++i)
					units[i].regChanged(reg);
			}
		}
	}

	void Timer16::setClock(unsigned char tccr) {
		byte clk = tccr & mask_CS;
		switch( clk ) {
			case CS_STOP:
				bus.clearBreak(this);
				return;

			case CS_CK:
				period = 1;
				break;

			case CS_CK_8:
				period = 8;
				break;

			case CS_CK_64:
				period = 64;
				break;

			case CS_CK_256:
				period = 256;
				break;

			case CS_CK_1024:
				period = 1024;
				break;

			case CS_EXT_FALL:
			case CS_EXT_RISE:
				throw util::ImplementationException(
						"external timer/counter sources not implemented" );
		}

		bus.setBreakDelta(period, this);
	}

	void Timer16::step() {
		static const word TOP[] = {
			0xffff, 0x00ff, 0x01ff, 0x03ff, 0, 0x0ff, 0x01ff, 0x03ff
		};
		static const word MAX = 0xffff;

		word compareI = 0;
		word compareA = units[0].value();

		word tcntSave = tcnt;
		bool tov = false;

        switch( timerMode ) {
            case MODE_NORMAL:
            	if( tcnt == TOP[timerMode] ) {
            		tov = true;
            		tcnt = 0;
            	}
            	break;

            case MODE_PWM_8:
            case MODE_PWM_9:
            case MODE_PWM_10:
            	if( (tcnt == TOP[timerMode]) && (direction == +1) ) {
            		direction = -1;
            		flushOCRn();
            	} else if( (tcnt == 0) && (direction == -1) ) {
            		direction = +1;
            		tov = true;
            	}
                break;

            case MODE_CTC_OCR:
            	if( tcnt == compareA ) {
            		tcnt = 0;
            	} else if( tcnt == MAX ) {
            		tov = true;
            	}
                break;

            case MODE_FASTPWM_8:
            case MODE_FASTPWM_9:
            case MODE_FASTPWM_10:
            	if( tcnt == TOP[timerMode] ) {
            		tcnt = 0;
            		flushOCRn();
            		tov = true;
            	}
                break;

            case MODE_PWM_PNF_ICR:
            	if( (tcnt == compareI) && (direction == +1) ) {
            		direction = -1;
            	} else if( (tcnt == 0) && (direction == -1) ) {
            		direction = +1;
            		flushOCRn();
            		tov = true;
            	}
            	break;

            case MODE_PWM_PNF_OCR:
            	if( (tcnt == compareA) && (direction == +1) ) {
            		direction = -1;
            	} else if( (tcnt == 0) && (direction == -1) ) {
            		direction = +1;
            		flushOCRn();
            		tov = true;
            	}
            	break;

            case MODE_PWM_ICR:
            	if( (tcnt == compareI) && (direction == +1) ) {
            		direction = -1;
            		flushOCRn();
            	} else if( (tcnt == 0) && (direction == -1) ) {
            		direction = +1;
            		tov = true;
            	}
                break;

            case MODE_PWM_OCR:
            	if( (tcnt == compareI) && (direction == +1) ) {
            		direction = -1;
            		flushOCRn();
            	} else if( (tcnt == 0) && (direction == -1) ) {
            		direction = +1;
            		tov = true;
            	}
                break;

            case MODE_CTC_ICR:
            	if( tcnt == compareI ) {
            		tcnt = 0;
            	} else if( tcnt == MAX ) {
            		tov = true;
            	}
            	break;

            case MODE_FASTPWM_ICR:
            	if( tcnt == compareI ) {
            		tcnt = 0;
            		tov = true;
            		flushOCRn();
            	}
            	break;

            case MODE_FASTPWM_OCR:
            	if( tcnt == compareI ) {
            		tcnt = 0;
            		tov = true;
            		flushOCRn();
            	}
            	break;
        }

		if( tov )
			overflow();
		else
			tcnt += direction;

		if( ! blockCompareMatch ) {
	        for(int cntr = 0; cntr < nUnits; cntr++ )
	        	units[cntr].compareMatchOutput(tifr, tcntSave);
		}
		blockCompareMatch = false;

		tcntl->set( tcnt & 0xff );
		tcnth->set( (tcnt >> 8) & 0xff );
		bus.setBreakDelta(period, this);
	}

	void Timer16::flushOCRn() {
		for(int cntr = 0; cntr < nUnits; cntr++ )
			units[cntr].flush();
	}
}
