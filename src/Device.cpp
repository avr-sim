/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Device.h"
#include "DeviceSettings.h"
#include "Core.h"
#include "HardwareFactory.h"
#include "Eeprom.h"
#include "DebugInterface.h"
#include "Pin.h"

#include "Analyzer.h"
#include "TraceAnalyzer.h"
#include "Program.h"
#include "Format.h"
#include "RuntimeException.h"

#include <cstring>

#ifdef DEBUG
#	include <iostream>
#endif

namespace avr {
	Device::Device(sim::SimulationClock & clock, const char *devicename, bool allowStopping)
			: sim::SimulationObject(clock), core(0), allowStopping(allowStopping) {

		if( devicename != 0 )
			avr::DeviceSettings::load( this, devicename );

		core->init();
		reset(POR_RESET);

		frq = 1000000000 / defaultFrequency;
		clock.setBreakDelta( frq, this );
	}

	Device::~Device() {
		if( core != 0 )
			delete core;

		for(size_t i = 0; i < pins.size(); ++i)
			delete pins[i];
		pins.clear();
	}

	void Device::load(Program & program) {
		Section sec;
        while( program.readNextSection(sec) ) {
        	//only read flash bytes and data
        	if( sec.isFlash() ) {
        		core->loadFlash( sec.data(), sec.address(), sec.size() );

#ifdef DEBUG
                std::cout << "Flash: " << sec.size() << " bytes at " << sec.address() << std::endl;
#endif
            }

        	if( sec.isEeprom() ) {
            	eeprom->write( sec.address(), sec.data(), sec.size() );

#ifdef DEBUG
                std::cout << "Eeprom: " << sec.size() << " bytes at " << sec.address() << std::endl;
#endif
            }
        }
	}

	void Device::setClockFrequency(ClockFrequency frq) {
		this->frq = 1000000000 / frq;
		clock.reassignBreak( this, this->frq );
	}

	void Device::buildCore(unsigned int ioSpaceSize, unsigned int ramSize,
							unsigned int flashSize, unsigned int pageSize,
							unsigned int stackMask, int pcBytes /*= 2*/,
							ERam *eram /*= 0*/) {
		core = new Core(bus, ioSpaceSize, ramSize, flashSize,
						pageSize, stackMask, pcBytes, eram);
	}

	void Device::buildHardware(const char *hwname, HardwareSettings & hws) {
		Hardware *hw = HardwareFactory::build(hwname, hws, bus);

		std::string name, binding;
		while( hws.getBinding(name, binding) ) {
#ifdef DEBUG
			std::cout << "Binding \"" << name << "\" to \"" << binding << "\"" << std::endl;
#endif

			if( ! hw->attachReg( name.c_str(), core->getIoreg( binding ) ) )
				throw util::RuntimeException(
					util::format("%s::attachReg: unknown register %s") % hwname % name );
		}

		if( ! hw->finishBuild() )
			throw util::RuntimeException(
				util::format("Failed to build hardware module %s: missing register") % hwname );

		bus.addHardware( hw );

		// Special handling for eeprom
		if( strcmp(hwname, "eeprom") == 0 )
			eeprom = static_cast<Eeprom*>( hw );
	}

	void Device::addIOReg(unsigned int address,
			const std::string & name, unsigned char intial) {
		core->addIOReg(address, name, intial);
	}

	void Device::addInterrupt(unsigned int vector, unsigned int address,
								const char *name) {
		bus.addInterrupt( vector, address, name );
	}

	void Device::addPin(unsigned int id, const char *name, unsigned int num) {
#ifdef DEBUG
		std::cout << "Pin " << id << " => " << name << std::endl;
#endif

		if( pins.size() == 0 )
			pins.resize( num );

		Pin *pin = new Pin();
		pins[ id - 1 ] = pin;

		// Multiple names?
		if( name[0] == '[' ) {
			static const std::string delimiters(":");
			std::string names( name + 1, strlen(name) - 2 );

			// skip delimiters at beginning.
			std::string::size_type lastPos = names.find_first_not_of(delimiters, 0);

			// find first "non-delimiter".
			std::string::size_type pos = names.find_first_of(delimiters, lastPos);

			while( std::string::npos != pos || std::string::npos != lastPos ) {
				// found a name, add it to the map.
				std::string n = names.substr(lastPos, pos - lastPos);
				name2pin.insert( std::make_pair(n, pin) );

				// skip delimiters.  Note the "not_of"
				lastPos = names.find_first_not_of(delimiters, pos);

				// find next "non-delimiter"
				pos = names.find_first_of(delimiters, lastPos);
			}
		} else {
			name2pin.insert( std::make_pair(name, pin) );
		}
	}

	Pin *Device::getPin(const std::string & name) const {
		std::map<std::string, Pin *>::const_iterator it;
		if( (it = name2pin.find( name )) != name2pin.end() )
			return it->second;

		throw util::RuntimeException(
			util::format("No pin with name %s on this device") % name );
	}

	void Device::addAnalyzer(const char *name) {
		core->addAnalyzer( AnalyzerFactory::newAnalyzer(core, name) );
	}

	void Device::addAnalyzer(Analyzer *analyzer) {
		core->addAnalyzer( analyzer );
	}

	void Device::trace(const char *tracefile) {
		core->addAnalyzer( new TraceAnalyzer(core, tracefile) );
	}

	void Device::reset(unsigned int type) {
		core->reset(type);
		bus.reset();
	}

	void Device::step() {
		if( ! bus.isHoldingCPU() )
			core->step();

		bus.step();

		if( ! (allowStopping && core->isStopped()) )
			clock.setBreakDelta( frq, this );
	}

	DebugInterface *Device::debugInterface() {
		return new DebugInterface(*this, *core);
	}

}
