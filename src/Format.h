#ifndef UTIL_FORMAT_H
#define UTIL_FORMAT_H

#include <sstream>
#include <vector>

#include "FormatExceptions.h"
#include "FormatGroup.h"

namespace util {
	/**
	  @author Tom Haber
	  @brief C++ formatting

	  How it works
		When you call format(s), where s is the format-string,
		it constructs an object, which parses the format string
		and looks for all directives in it and prepares internal
		structures for the next step.

        Then, either immediately, as in
			cout << format("%2% %1%") % 36 % 77 )
		or later on, as in
			format fmter("%2% %1%");
			fmter % 36; fmter % 77;

		you feed variables into the formatter. Those variables
		are dumped into an internal stream, which state is set
		according to the given formatting options in the format-string,
		and the format object stores the string results for the last step.

		Once all arguments have been fed you can dump the format object
		to a stream, or get its string value by using the str() member
		function. The result string stays accessible in the format object
		until another argument is passed, at which time it is reinitialized.

		// fmter was previously created and fed arguments, it can print the result :
		cout << fmter ;

		// You can take the string result :
		string s  = fmter.str();

		// possibly several times :
		s = fmter.str( );

		// You can also do all steps at once :
		cout << boost::format("%2% %1%") % 36 % 77;

		Optionally, after step 3, you can re-use a format object and
		restart at step2 : fmter % 18 % 39; to format new variables
		with the same format-string, saving the expensive processing
		involved at step 1. All in all, the format class translates a
		format-string (with eventually printf-like directives) into
		operations on an internal stream, and finally returns the result
		of the formatting, as a string, or directly into an output stream.
	 */
	class format {
		private:
			// set of params that define the format state of a stream
			struct stream_format_state {
				std::streamsize width;
				std::streamsize precision;
				char fill;
				std::ios_base::fmtflags flags;
				stream_format_state() : width(-1), precision(-1), fill(0),
										flags(std::ios_base::dec)  {}

				stream_format_state( std::ios & os) { set_by_stream(os); }

				//- applies format_state to the stream
				void apply_on(std::ios & os) const;

				//- modifies state by applying manipulator.
				template<class T> void apply_manip(T manipulator);

				//- sets to default state.
				void reset();
				//- sets to os's state.
				void set_by_stream(const std::ios & os);
			};


#ifdef _MSC_VER
	// error C2248: 'util::format::format_item' : cannot access private struct declared in class 'util::format
		public:
#endif
			// format_item : stores all parameters that can be defined by directives in the format-string
			struct format_item {
				enum pad_values { zeropad = 1, spacepad =2, centered=4, tabulation = 8 };
				enum arg_values {
					argN_no_posit   = -1, // non-positional directive. argN will be set later.
					argN_tabulation = -2, // tabulation directive. (no argument read)
					argN_ignored    = -3  // ignored directive. (no argument read)
				};

				int argN;           //- argument number (starts at 0,  eg : %1 => argN=0)

				//  negative values are used for items that don't process an argument
				std::string res; //- result of the formatting of this item
				std::string appendix; //- piece of string between this item and the next

				stream_format_state ref_state;// set by parsing the format_string, is only affected by modify_item
				stream_format_state state;  // always same as ref_state, _unless_ modified by manipulators 'group(..)'

				// non-stream format-state parameters
				signed int truncate;        //- is >=0 for directives like %.5s (take 5 chars from the string)

				unsigned int pad_scheme;    //- several possible padding schemes can mix. see pad_values

				format_item() : argN(argN_no_posit), truncate(-1), pad_scheme(0)  {}
				void compute_states();      // sets states  according to truncate and pad_scheme.
			};

		public:
			format(const char *str);
			format(const std::string & s);
			format(const format & x);

		public:
			format & operator =(const format & x);
			format & clear(); // empty the string buffers (except bound arguments, see clear_binds() )

			// pass arguments through those operators :
			template<class T> format & operator %(const T & x) {
				return feed<const T &>(x);
			}

			// system for binding arguments :
			template<class T>  format & bind_arg(int argN, const T& val) {
				return bind_arg_body(argN, val);
			}

			format & clear_bind(int argN);
			format & clear_binds();

			// modify the params of a directive, by applying a manipulator :
			template<class T> format & modify_item(int itemN, const T & manipulator) {
				return modify_item_body(itemN, manipulator);
			}

		public:
			enum format_error_bits {
				bad_format_string_bit = 1,
				too_few_args_bit = 2,
				too_many_args_bit = 4,
				out_of_range_bit = 8,
				all_error_bits = 255,
				no_error_bits = 0
			};

			// Choosing which errors will throw exceptions :
			unsigned char exceptions() const { return exceptions_flag; }
			unsigned char exceptions(unsigned char newexcept) {
				unsigned char swp = exceptions_flag;
				exceptions_flag = newexcept;
				return swp;
			}

		public:
			std::string str() const;
            operator std::string () const;
			friend std::ostream & operator<< (std::ostream & ostr, const format & f);

		private:
			template<class T> format & feed(T);
			template<class T> void distribute(T);
			template<class T> format & modify_item_body(int, const T&);
			template<class T> format & bind_arg_body(int, const T&);

			private:
				// flag bits, used for style_
				enum style_values  {
					ordered = 1,        // set only if all directives are  positional directives
					special_needs = 4
				};

				// parse the format string :
				void parse(const std::string &);

			private:
				int style; // style of format-string :  positional or not, etc
				int cur_arg; // keep track of which argument will come
				int num_args; // number of expected arguments
				mutable bool dumped; // true only after call to str() or <<

				std::vector<format_item> items; // vector of directives (aka items)
				std::string prefix; // piece of string to insert before first item
				std::vector<bool> bound; // stores which arguments were bound

				//   size = num_args OR zero
				std::ostringstream oss; // the internal stream.
				stream_format_state state0; // reference state for oss_
				unsigned char exceptions_flag;

			template<class T> friend void put(T x, const format::format_item & specs,
							   std::string & res, std::ostringstream & oss);
			friend bool parse_printf_directive(const std::string & buf,
									   std::string::size_type *pos_p,
									   format::format_item *fpar, std::ios & os,
									   unsigned char exceptions);
	};

	// set the state of this stream according to our params
	inline void format::stream_format_state::apply_on(std::ios & os) const {
		if( width != -1 )
			os.width(width);

		if( precision != -1 )
			os.precision(precision);

		if( fill != 0 )
			os.fill(fill);

		os.flags(flags);
	}

	// set our params according to the state of this stream
	inline void format::stream_format_state::set_by_stream(const std::ios & os) {
		flags = os.flags();
		width = os.width();
		precision = os.precision();
		fill = os.fill();
	}

	// modify our params according to the manipulator
	template <class T>
	inline void format::stream_format_state::apply_manip(T manipulator) {
		std::stringstream ss;
		apply_on( ss );
		ss << manipulator;
		set_by_stream( ss );
	}

	// set our params to standard's default state
	inline void format::stream_format_state::reset() {
		width=-1; precision=-1; fill=0;
		flags = std::ios_base::dec;
	}

	// reflect pad_scheme   on  state and ref_state
	// because some pad_schemes has complex consequences on several state params.
	inline void format::format_item::compute_states() {
		if( pad_scheme & zeropad ) {
			if( ref_state.flags & std::ios_base::left ) {
				pad_scheme = pad_scheme & (~zeropad); // ignore zeropad in left alignment
			}else {
				ref_state.fill='0';
				ref_state.flags |= std::ios_base::internal;
			}
		}

		state = ref_state;
	}

	// bind one argument to a fixed value
	// this is persistent over clear() calls, thus also over str() and <<
	template <class T>
	format & format::bind_arg_body(int argN, const T & val) {
		if(dumped) clear(); // needed, because we will modify cur_arg..

		if(argN<1 || argN > num_args)  {
			if( exceptions() & out_of_range_bit )
				throw out_of_range(); // arg not in range.
			else return *this;

		}

		if(bound.size()==0)
			bound.assign(num_args,false);

		int o_cur_arg = cur_arg;
		cur_arg = argN-1; // arrays begin at 0

		bound[cur_arg]=false; // if already set, we unset and re-sets..
		operator%(val); // put val at the right place, because cur_arg is set

		// Now re-position cur_arg before leaving :
		cur_arg = o_cur_arg;
		bound[argN-1]=true;

		// hum, now this arg is bound, so move to next free arg
		if( cur_arg == argN-1 ) {
			while( (cur_arg < num_args) && bound[cur_arg] ) ++cur_arg;
		}

		return *this;
	}

	// applies a manipulator to the format_item describing a given directive.
	// this is a permanent change, clear or clear_binds won't cancel that.
	template <class T>
	format & format::modify_item_body(int itemN, const T& manipulator) {
		if(itemN<1 || itemN >= static_cast<signed int>(items.size() )) {
			if( exceptions() & out_of_range_bit )
				throw out_of_range(); // item not in range.
			else return *this;
		}

		items[itemN-1].ref_state.apply_manip( manipulator );
		items[itemN-1].state = items[itemN-1].ref_state;

		return *this;
	}

	template<class T>
	inline void put_head(std::ostream &, const T& ) {
	}

	template<class T>
	inline void put_head(std::ostream & os, const format_interal::group1<T>& x ) {
		os << format_interal::group_head(x.a1_); // send the first N-1 items, not the last
	}

	template<class T>
	inline void put_last(std::ostream & os, const T& x ) {
		os << x ;
	}

	template<class T>
	inline void put_last(std::ostream & os, const format_interal::group1<T> & x ) {
		os << format_interal::group_last(x.a1_); // this selects the last element
	}

	void do_pad(std::string & s, std::streamsize w, const char c,
				std::ios_base::fmtflags f, bool center);
	void empty_buf(std::ostringstream & os);

	// does the actual conversion of x, with given params, into a string
	// using the *supplied* stringstream. (the stream state is important)
	template<class T> void put(T x, const format::format_item & specs,
							   std::string & res, std::ostringstream & oss) {

		format::stream_format_state prev_state(oss);
		specs.state.apply_on(oss);

		// in case x is a group, apply the manip part of it,

		// in order to find width

		put_head( oss, x );
		empty_buf( oss );

		const std::streamsize w = oss.width();
		const std::ios_base::fmtflags fl=oss.flags();

		const bool internal = (fl & std::ios_base::internal) != 0;

		const bool two_stepped_padding = internal
			&&  ! ( specs.pad_scheme & format::format_item::spacepad )
			&& specs.truncate < 0 ;

		if(! two_stepped_padding ) {
			if(w>0) // handle simple padding via do_pad, not natively in stream
				oss.width(0);

			put_last( oss, x);
			res = oss.str();

			if( specs.truncate >= 0 && static_cast<unsigned int>(specs.truncate) < res.size() )
				res.erase(specs.truncate);

			// complex pads :
			if( specs.pad_scheme & format::format_item::spacepad ) {
				if( res.size()==0 ||   ( res[0]!='+' && res[0]!='-'  )) {
					res.insert(res.begin(), 1, ' '); // insert 1 space at  pos 0
				}
			}

			// need do_pad
			if(w > 0) {
				do_pad(res, w, oss.fill(), fl, (specs.pad_scheme & format::format_item::centered) !=0 );
			}

		} else {
			// 2-stepped padding
			put_last( oss, x); // oss_.width() may result in padding.
			res = oss.str();

			if( specs.truncate >= 0	)
				res.erase(specs.truncate);

			if( res.size() - w > 0 ) {
				//   length w exceeded
				// either it was multi-output with first output padding up all width..
				// either it was one big arg and we are fine.
				empty_buf( oss );
				oss.width(0);
				put_last(oss, x );

				std::string tmp = oss.str();  // minimal-length output
				std::streamsize d;
				if( (d=w - tmp.size()) <=0 ) {
					// minimal length is already >= w, so no padding  (cool!)
					res.swap(tmp);
				} else {
					// hum..  we need to pad (it was necessarily multi-output)
					typedef typename std::string::size_type size_type;

					size_type i = 0;
					while( i<tmp.size() && tmp[i] == res[i] ) // find where we should pad.
						++i;

					tmp.insert(i, static_cast<size_type>( d ), oss.fill());
					res.swap( tmp );
				}
			}
		}

		prev_state.apply_on(oss);
		empty_buf( oss );
		oss.clear();
	}

	// call put(x, ..) on every occurence of the current argument :
	template<class T>
	void format::distribute(T x) {
		if( cur_arg >= num_args ) {
			if( exceptions() & too_many_args_bit )
				throw too_many_args(); // too many variables have been supplied !

			else return;
		}

		for(unsigned long i=0; i < items.size(); ++i) {
			if(items[i].argN == cur_arg) {
				put<T>(x, items[i], items[i].res, oss );
			}
		}
	}

	template<class T>
	format & format::feed(T x) {
		if( dumped ) clear();

		distribute<T>(x);
		++cur_arg;

		if( bound.size() != 0 ) {
			while( cur_arg < num_args && bound[cur_arg] )
				++cur_arg;
		}

		// this arg is finished, reset the stream's format state
		state0.apply_on(oss);
		return *this;
	}

	inline format::operator std::string () const {
		return str();
	}
}
#endif
