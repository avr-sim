#ifndef UTIL_IMPLEMENTATIONEXCEPTION_H
#define UTIL_IMPLEMENTATIONEXCEPTION_H

#include "Exception.h"

namespace util {
	/**
	 * @author Tom Haber
	 * @date Apr 26, 2008
	 * @brief Thrown on implementation issues: missing or broken features
	 *
	 * ImplementationException is thrown when some feature is used
	 * where the implementation is missing or broken.
	 *
	 * The stepping function of the Core catches this exception,
	 * prints a warning and ignores it. This way the simulation
	 * doesn't halt because of implementation issues. Obviously
	 * the result of the simulation might be wrong.
	 */
	class ImplementationException : public Exception {
		public:
			ImplementationException() : Exception("Implementation Exception") {}
			ImplementationException(const char *s) : Exception(s) {}
			ImplementationException(const std::string & s) : Exception(s) {}
	};
}
#endif
