/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Usart.h"
#include "Bus.h"
#include "Registers.h"
#include <cstring>

enum {
    MPCM = 1<<0,               /* Multi-processor Communication Mode  */
    U2X = 1<<1,                /* Double X-mission Speed   */
    UPE = 1<<2,				   /* Parity Error			 */
    DOR = 1<<3,                /* Data OverRun          */
    FE = 1<<4,                 /* Framing Error         */
    UDRE = 1<<5,               /* Data Register Empty   */
    TXC = 1<<6,                /* Transmit Complete     */
    RXC = 1<<7,                /* Receive Complete      */
};

enum {
    TXB8 = 1<<0,               /* Transmit Data Bit 8  */
    RXB8 = 1<<1,               /* Receive  Data Bit 8  */
    UCSZ2 = 1<<2,              /* Character Size */
    TXEN = 1<<3,               /* Transmitter Enable   */
    RXEN = 1<<4,               /* Receiver    Enable   */
    UDRIE = 1<<5,              /* Data Register Interrupt Enable */
    TXCIE = 1<<6,              /* TX Complete   Interrupt Enable */
    RXCIE = 1<<7,              /* RX Complete   Interrupt Enable */
};

enum {
	UCPO = 1<<0,               /* Clock Polarity */
	UCSZ0 = 1<<1,              /* Character Size */
	UCSZ1 = 1<<2,              /* Character Size */
	USBS = 1<<3,               /* Stop Bit Select */
	UPM0 = 1<<4,               /* Parity Mode */
	UPM1 = 1<<5,               /* Parity Mode */
	UMSEL = 1<<6,              /* USART Mode Select */
	URSEL = 1<<7               /* Register Select */
};

#define samplerate() ( ((ucsrc & UMSEL) == 0) ? (((ucsra_old & U2X) != 0) ? 8 : 16) : 2 )

namespace avr {

	Usart::Usart(Bus & bus, const std::string & name, unsigned int udreVec,
				unsigned int rxVec, unsigned int txVec)
		: Hardware(bus), name(name), udreVec(udreVec), rxVec(rxVec), txVec(txVec) {

		ucsra_old = ucsrb_old = 0;
		ucsrc = 0;
		baudRate = 1;
		frameLength = 0;

		txState = TX_FINISHED;
		txShift = 0;
		txBitCount = 0;
		txRate = 0;

		rxState = RX_FINISHED;
		rxShift = 0;
		rxBitCount = 0;
		rxSample = 0;

		bus.claimInterrupt(txVec, this);
	}

	Usart::~Usart() {
	}

	bool Usart::attachReg(const char *name, IORegister *reg) {
		if( strcmp(name, "udr") == 0 )
			udr = reg;
		else if( strcmp(name, "ucsra") == 0 )
			ucsra = reg;
		else if( strcmp(name, "ucsrb") == 0 )
			ucsrb = reg;
		else if( strcmp(name, "ubrrl") == 0 )
			ubrrl = reg;
		else if( strcmp(name, "ubrrh") == 0 )
			ubrrh = reg;
		else
			return false;

		reg->registerHW(this);
		return true;
	}

	bool Usart::finishBuild() {
		return ( (udr != 0) &&
				(ucsra != 0) && (ucsrb != 0) &&
				(ubrrl != 0) && (ubrrh != 0) );
	}

	void Usart::setUdr(unsigned char udr) {
		udrWrite = udr;
		this->udr->set(udrRead);

	    if( (*ucsra & UDRE) != 0 ) {
	    	// if the data register was empty, we clear the UDRE flag now
	    	ucsra->set( *ucsra & ~UDRE );

	    	// If interrupt was raised, clear it.
	        if( (*ucsrb & UDRIE) != 0 )
	        	bus.clearInterrupt( udreVec );
	    }
	}

	void Usart::setUcsra(unsigned char val) {
		// The TXC Flag bit can be cleared by writing a one to its bit location.
		if( (val & TXC) != 0 )
			ucsra->set( *ucsra & ~TXC );

		// Handle interrupts
        const unsigned char irqold = ucsrb_old & ucsra_old;
        const unsigned char irqnew = ucsrb_old & *ucsra;

        unsigned char changed = irqold ^ irqnew;
        unsigned char setnew = changed & irqnew;
        unsigned char clearnew = changed & (~irqnew);

        checkForNewSetIrq(setnew);
        checkForNewClearIrq(clearnew);

        ucsra_old = *ucsra;
	}

	void Usart::setUcsrb(unsigned char val) {
		// RXEN or TXEN on and previously off?
        if( ( (val & (RXEN|TXEN)) != 0 ) &&
        		( (ucsrb_old & (RXEN|TXEN)) == 0 ) ) {
        	bus.setBreakDelta(baudRate, this);
        }

        setFramelength();

		// Handle interrupts
        const unsigned char irqold = ucsrb_old & ucsra_old;
        const unsigned char irqnew = *ucsrb & ucsra_old;

        unsigned char changed = irqold ^ irqnew;
        unsigned char setnew = changed & irqnew;
        unsigned char clearnew = changed & (~irqnew);

        checkForNewSetIrq(setnew);
        checkForNewClearIrq(clearnew);

        ucsrb_old = *ucsrb;
	}

	void Usart::setFramelength() {
	    if( (ucsrb_old & UCSZ2) != 0 ) {
	        frameLength=9;
	    } else {
	        switch( ucsrc & (UCSZ1|UCSZ0) ) {
	            case 0:
	                frameLength=5;
	                break;

	            case UCSZ0:
	                frameLength=6;
	                break;

	            case UCSZ1:
	                frameLength=7;
	                break;

	            case UCSZ0|UCSZ1:
	                frameLength=8;
	                break;
	        }
	    }
	}

	void Usart::regChanged( IORegister *reg ) {
		if( reg == udr ) {
			setUdr( *udr );
		} else if( reg == ubrrl ) {
			baudRate = ((*ubrrh << 8) | *ubrrl) + 1;
			bus.reassignBreakDelta(baudRate, this);
		} else if( reg == ubrrh ) {
			if( (*ubrrh & URSEL) != 0 ) {
				ucsrc = *ubrrh;
				setFramelength();
			} else {
				baudRate = ((ubrrh->get() << 8) | ubrrl->get()) + 1;
				bus.reassignBreakDelta(baudRate, this);
			}
		} else if( reg == ucsra ) {
			setUcsra( *ucsra );
		} else if( reg == ucsrb ) {
			setUcsrb( *ucsrb );
		}
	}

	void Usart::step() {
		if( (*ucsrb & RXEN) != 0 )
			receiveCycle();

		if( (*ucsrb & TXEN) != 0 ) {
			if( ++txRate == samplerate() ) {
				transmitCycle();
				txRate = 0;
			}
		}

		bus.setBreakDelta(baudRate, this);
	}

	void Usart::receiveCycle() {
		switch( rxState ) {
			case RX_STARTBIT:
				break;

			case RX_DATABIT:
				break;

			case RX_PARITY:
				break;

			case RX_STOPBIT:
				break;

			case RX_STOPBIT2:
				break;

			case RX_FINISHED:
			default:
				;
		}

		// Handle interrupts
        const unsigned char irqold = ucsrb_old & ucsra_old;
        const unsigned char irqnew = ucsrb_old & *ucsra;

        unsigned char changed = irqold ^ irqnew;
        unsigned char setnew = changed & irqnew;
        unsigned char clearnew = changed & (~irqnew);

        checkForNewSetIrq(setnew);
        checkForNewClearIrq(clearnew);

        ucsra_old = *ucsra;
	}

	void Usart::transmitCycle() {
		if( (txState == TX_FINISHED) && ((*ucsra & UDRE) == 0) ) {
			// Shift data in
			txShift = udrWrite;
			if( (*ucsrb & TXB8) != 0 )
				txShift |= 0x100;	// Set bit 8

			ucsra->set( (*ucsra & ~TXC) | UDRE );
			txState = TX_STARTBIT;
        }

		switch( txState ) {
			case TX_STARTBIT:
				txBitCount = 0;
				txState = TX_DATABIT;
				break;

			case TX_DATABIT:
				txBitCount++;

				if( txBitCount >= frameLength ) {
					if( (ucsrc & (UPM0|UPM1)) != 0 )
						txState = TX_PARITY;
					else
						txState = TX_STOPBIT;
				}
				break;

			case TX_PARITY:
                if( (ucsrc & UPM0) != 0 )
                    ;//even parity to send
                else
                    ;//odd parity to send

                txState = TX_STOPBIT;
				break;

			case TX_STOPBIT:
				if( (ucsrc & USBS) != 0 )
					txState = TX_STOPBIT2;

			case TX_STOPBIT2:
				if( (*ucsra & UDRE) == 0 ) {
					// Shift data in
					txShift = udrWrite;
					if( (*ucsrb & TXB8) != 0 )
						txShift |= 0x100;	// Set bit 8

					ucsra->set( (*ucsra & ~TXC) | UDRE );
					txState = TX_STARTBIT;
		        } else {
		        	txState = TX_COMPLETE;
		        }
				break;

			case TX_COMPLETE:
				// Transmission finished and no further data
				ucsra->set( *ucsra | TXC );
				txState = TX_FINISHED;
				break;

			case TX_FINISHED:
			default:
				;
		}

		// Handle interrupts
        const unsigned char irqold = ucsrb_old & ucsra_old;
        const unsigned char irqnew = ucsrb_old & *ucsra;

        unsigned char changed = irqold ^ irqnew;
        unsigned char setnew = changed & irqnew;
        unsigned char clearnew = changed & (~irqnew);

        checkForNewSetIrq(setnew);
        checkForNewClearIrq(clearnew);

        ucsra_old = *ucsra;
	}

	void Usart::reset() {
		baudRate = 1;
		frameLength = 0;

		// Fetch register values
		setUcsra( *ucsra );
		setUcsrb( *ucsrb );
		if( (*ubrrh & URSEL) != 0 ) {
			ucsrc = *ubrrh;
			setFramelength();
		}

		txState = TX_FINISHED;
		txShift = 0;
		txBitCount = 0;
		txRate = 0;

		rxState = RX_FINISHED;
		rxShift = 0;
		rxBitCount = 0;
		rxSample = 0;
	}

	void Usart::beforeInvokeInterrupt(unsigned int vector) {
		if( vector == txVec )
			ucsra->set( *ucsra & ~TXC );
	}

	void Usart::checkForNewSetIrq(unsigned char val) {
		if( val & UDRE ) { bus.raiseInterrupt( udreVec ); }
		if( val & RXC ) { bus.raiseInterrupt( rxVec ); }
		if( val & TXC ) { bus.raiseInterrupt( txVec ); }
	}

	void Usart::checkForNewClearIrq(unsigned char val) {
		if( val & UDRE ) { bus.clearInterrupt( udreVec ); }
		if( val & RXC ) { bus.clearInterrupt( rxVec ); }
		if( val & TXC ) { bus.clearInterrupt( txVec ); }
	}

}
