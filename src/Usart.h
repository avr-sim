#ifndef AVR_USART_H
#define AVR_USART_H

#include "Hardware.h"
#include <string>

namespace avr {

	class Usart : public avr::Hardware {
		public:
			Usart(Bus & bus, const std::string & name, unsigned int udreVec,
					unsigned int rxVec, unsigned int txVec);
			~Usart();

		public:
			/**
			 * Attach a register with name \e name to the hardware.
			 */
			bool attachReg(const char *name, IORegister *reg);

			/**
			 * Finishes the construction of the hardware.
			 * This should verify the registers and parameters
			 * @returns true if build was successful.
			 */
			bool finishBuild();

			/**
			 * An attached register changed state.
			 */
			void regChanged( IORegister *reg );

			/**
			 * Perform a single step.
			 */
			void step();

			/**
			 * Reset the internal hardware.
			 */
			void reset();

			/**
			 * Called just before an interrupt handler is invoked.
			 */
			void beforeInvokeInterrupt(unsigned int vector);

		private:
			void receiveCycle();
			void transmitCycle();
			void checkForNewSetIrq(unsigned char val);
			void checkForNewClearIrq(unsigned char val);
			void setUdr(unsigned char udr);
			void setUcsra(unsigned char ucsra);
			void setUcsrb(unsigned char ucsrb);
			void setFramelength();

		private:
			IORegister *udr;
			IORegister *ucsra;
			IORegister *ucsrb;
			IORegister *ubrrl;
			IORegister *ubrrh;

		private:
			std::string name;
			unsigned int udreVec;
			unsigned int rxVec;
			unsigned int txVec;

			unsigned char udrWrite;
			unsigned char udrRead;
			int baudRate;
			int frameLength;

			enum {
				TX_STARTBIT,
				TX_DATABIT,
				TX_PARITY,
				TX_STOPBIT,
				TX_STOPBIT2,
				TX_COMPLETE,
				TX_FINISHED
			} txState;
			int txShift;
			int txBitCount;
			int txRate;

			enum {
				RX_STARTBIT,
				RX_DATABIT,
				RX_PARITY,
				RX_STOPBIT,
				RX_STOPBIT2,
				RX_FINISHED
			} rxState;
			int rxShift;
			int rxBitCount;
			int rxSample;

			unsigned char ucsra_old;
			unsigned char ucsrb_old;
			unsigned char ucsrc;
	};

}

#endif /*AVR_USART_H*/
