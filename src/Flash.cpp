/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Flash.h"
#include "Instruction.h"
#include "Registers.h"
#include "Bus.h"
#include "AccessViolation.h"
#include "RuntimeException.h"

#include <algorithm>
#include <string.h>

enum {
    NONE = 0,
    PGERASE = 1 << 1 | 1,
    RWWSRE  = 1 << 4 | 1,
    BLBSET  = 1 << 3 | 1,
    FILL    = 1,
    PGWRITE = 1 << 2 | 1,
    BUSY = 1<<6
};

#define SPMCSR_LOWERBITS 0x1f
#define BUFFERSIZE 1<<pagebits
#define DEFAULT_VALUE 0xff
#define setBusy() spmcsr->set( *spmcsr | BUSY );

// TODO these are cycles for 8Mhz!
#define ERASE_CYCLES 36000
#define WRITE_CYCLES 36000
#define RESET_CYCLES 4

namespace avr {

	static unsigned int log2Int(unsigned int v) {
        // find the log base 2 of 32-bit v

        static const int MultiplyDeBruijnBitPosition[32] = {
            0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8,
            31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9
        };

        v |= v >> 1; // first round down to power of 2
        v |= v >> 2;
        v |= v >> 4;
        v |= v >> 8;
        v |= v >> 16;
        v = (v >> 1) + 1;

        return MultiplyDeBruijnBitPosition[(v * 0x077CB531UL) >> 27];
    }

	Flash::Flash(Bus & bus, unsigned int size, int pagesize)
		: Hardware(bus), Memory(size) {

		pagebits = log2Int(pagesize);

		Memory::fill(DEFAULT_VALUE);
		pagemask = (0xffffffff >> (31 - (pagebits)));

		buffer = new unsigned char[BUFFERSIZE];
		memset(buffer, DEFAULT_VALUE, BUFFERSIZE);

		spmcsr = 0;
	}

	Flash::~Flash() {
		if( buffer != 0 )
			delete [] buffer;
	}

	void Flash::fill(unsigned int offset, byte val, unsigned int size) {
		if( offset + size > siz )
			throw AccessViolation("Flash::fill: writing too much to memory");

		memset( mem + offset, val, size );
	}

	bool Flash::attachReg(const char *name, IORegister *reg) {
		if( strcmp(name, "spmcsr") == 0 )
			spmcsr = reg;
		else
			return false;

		reg->registerHW(this);
		return true;
	}

	bool Flash::finishBuild() {
		return ( spmcsr != 0 );
	}

	void Flash::regChanged( IORegister *reg ) {
		if( reg == spmcsr ) {
			bus.setBreakDelta(RESET_CYCLES, this);
		}
	}

	void Flash::storeProgramMemory(dword Z, byte r0, byte r1) {
        pageoffset = (Z & pagemask);
        pagenum = Z >> pagebits;

        state = *spmcsr;
        switch ( state ) {
            case PGERASE:
            	setBusy();
                bus.reassignBreakDelta(ERASE_CYCLES, this);
                break;

            case RWWSRE:
                resetRWW();
                bus.clearBreak(this);
                break;

            case BLBSET:
            	bus.clearBreak(this);
                break;

            case FILL:
            	if( pageoffset >= BUFFERSIZE )
            		throw AccessViolation("Tried to write beyond RWW buffer boundaries");

            	buffer[pageoffset] = r0;
                buffer[pageoffset+1] = r1;
                spmcsr->set( *spmcsr & ~SPMCSR_LOWERBITS );
                bus.clearBreak(this);
                break;

            case PGWRITE:
            	setBusy();
                bus.reassignBreakDelta(WRITE_CYCLES, this);
                break;
        }
	}

	void Flash::resetRWW() {
		if( (*spmcsr & BUSY) == 0 )
			spmcsr->set( *spmcsr & ~(BUSY|SPMCSR_LOWERBITS) );
	}

	void Flash::pageErase() {
        int size = BUFFERSIZE;
        int addr = pagenum * size;
     	fill( addr, DEFAULT_VALUE, size );
	}

	void Flash::pageWrite() {
        int size = BUFFERSIZE;
        int addr = pagenum * size;
        write( addr, buffer, size );
	}

	void Flash::step() {
        switch ( state ) {
        	case NONE:
        		break;

            case PGERASE:
                pageErase();
                break;

            case PGWRITE:
                pageWrite();
                break;

			default:
				throw util::RuntimeException("Flash::step: invalid state");
        }

		memset(buffer, DEFAULT_VALUE, BUFFERSIZE);
     	spmcsr->set( *spmcsr & ~(SPMCSR_LOWERBITS) );
	}

}

