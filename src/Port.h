#ifndef AVR_PORT_H
#define AVR_PORT_H

#include "Hardware.h"
#include <string>

namespace avr {

	class Register;

	/**
	 * @author Tom Haber
	 * @date Apr 26, 2008
	 * @brief IO Ports
	 *
	 * This class represents the Input/Output ports on the device.
	 */
	class Port : public Hardware {
		public:
			Port(Bus & bus, const std::string & name, unsigned int mask);
			~Port();

		public:
			bool attachReg(const char *name, IORegister *reg);
			void regChanged( IORegister *reg );
			void step();

		private:
			std::string name;
			unsigned int mask;

		private:
			Register *port;
			Register *pin;
			Register *ddr;
	};

}

#endif /*AVR_PORT_H*/
