/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ADC.h"
#include "Bus.h"
#include "Registers.h"

#include <cstring>

#define INIT_CYCLES ((int)(13.5*2))
#define RUN_CYCLES ((int)(1.5*2))
#define CONV_CYCLES ((int)(13*2) - RUN_CYCLES)
#define INT_CYCLES CONV_CYCLES

enum {
	MUX0 = 1<<0,
	MUX1 = 1<<1,
	MUX2 = 1<<2,
	MUX3 = 1<<3,
	MUX4 = 1<<4,
	ADLAR = 1<<5,
	REFS0 = 1<<6,
	REFS1 = 1<<7
};

 enum {
    ADPS0 = 1<<0,              /* Clock Rate Select 0   */
    ADPS1 = 1<<1,              /* Clock Rate Select 1   */
    ADPS2 = 1<<2,              /* Clock Rate Select 1   */
    ADIE = 1<<3,               /* ADC Interrupt Enable  */
    ADIF = 1<<4,               /* ADC Interrupt Flag    */
    ADFR = 1<<5,               /* Free Run Select       */
    ADSC = 1<<6,               /* ADC Start Conversion  */
    ADEN = 1<<7,               /* ADC Enable            */
};

namespace avr {

	ADC::ADC(Bus & bus, unsigned int ccVec) : Hardware(bus), ccVec(ccVec) {
		bus.claimInterrupt(ccVec, this);
	}

	ADC::~ADC() {
	}

	void ADC::reset() {
		adcsr_old = *adcsr;
		prescaler = 0;
		clk = 0;
		sample = 0.0f;
	}

	bool ADC::attachReg(const char *name, IORegister *reg) {
		if( strcmp(name, "admux") == 0 )
			admux = reg;
		else if( strcmp(name, "adcsr") == 0 ) {
			adcsr = reg;
		} else if( strcmp(name, "adch") == 0 )
			adch = reg;
		else if( strcmp(name, "adcl") == 0 )
			adcl = reg;
		else
			return false;

		reg->registerHW(this);
		return true;
	}

	bool ADC::finishBuild() {
		return ((admux != 0) && (adcsr != 0) && (adch != 0) && (adcl != 0) );
	}

	void ADC::setAdcsr(unsigned char val) {
	    unsigned char old = adcsr_old & (ADIF|ADSC);
	    if( (val & ADIF) != 0 )	// Clear interrupt flag if written.
	    	old &= ~ADIF;

	    val = old | (val & ~ADIF);

	    // Check interrupt
	    if( (val & (ADIE|ADIF)) == (ADIE|ADIF) )
	    	bus.raiseInterrupt(ccVec);
	    else
	    	bus.clearInterrupt(ccVec);

	    // We trigger the adc at twice the speed to allow
	    // starting with sampling at half a cycle.
        switch ( val & ( ADPS2|ADPS1|ADPS0)) {
            case 0:
            case ADPS0:
            	prescaler = 1;
                break;

            case ADPS1:
            	prescaler = 2;
                break;

            case (ADPS1|ADPS0):
            	prescaler = 4;
                break;

            case (ADPS2):
            	prescaler = 8;
                break;

            case (ADPS2|ADPS0):
            	prescaler = 16;
                break;

            case (ADPS2|ADPS1):
            	prescaler = 32;
                break;

            case (ADPS2|ADPS1|ADPS0):
            	prescaler = 64;
                break;
        }

	    if( ((val & ADSC) != 0) && ((adcsr_old & ADSC) == 0) ) {
			bus.setBreakDelta( INIT_CYCLES * prescaler, this );
			clk = 0;
			sample = 0.0f;
	    }

	    adcsr->set( val );
	    adcsr_old = val;
	}

	float ADC::refVoltage() const {
		switch( *admux & (REFS0|REFS1) ) {
			case 0:
				return 5.0f; // aref voltage

			case REFS0:
				return 5.0f; // avcc

			case REFS1|REFS0:
				return 2.56f;

			default:
				return 0.0f;
		}
	}

	void ADC::regChanged( IORegister *reg ) {
		if( reg == admux )
			;
		else if( reg == adcsr )
			setAdcsr( *adcsr );
	}

	void ADC::step() {
		if( clk++ == CONV_CYCLES ) {
			// TODO gain, differential
			unsigned int adc = (unsigned int)((sample / INT_CYCLES) * (1<<10));
			// TODO adlar register locking
            adch->set( adc >> 8 );
            adcl->set( adc & 0xff );

            adcsr_old |= ADIF;

            if( (adcsr_old & (ADIE|ADIF)) == (ADIE|ADIF) )
				bus.raiseInterrupt(ccVec);

            if( (adcsr_old & ADFR) != 0 ) {
            	// Start again if free running mode
                clk = 0;
                sample = 0.0f;
                bus.setBreakDelta( RUN_CYCLES*prescaler, this );
            } else {
            	// Clear start conversion bit
                adcsr_old &= ~ADSC;
            }

            adcsr->set( adcsr_old );
		} else {
			int chan = *admux & (MUX2|MUX1|MUX0);
			float V = 0.0f; //adpin[chan].voltage();
			float Vref = refVoltage();

			if( V > Vref ) V = Vref;
			sample += (V / Vref);

			bus.setBreakDelta( prescaler, this );
		}
	}

	void ADC::beforeInvokeInterrupt(unsigned int vector) {
		if( vector == ccVec )
			adcsr->set( *adcsr & ~ADIF );
	}

}
