#ifndef AVR_DECODER_H
#define AVR_DECODER_H

#include <vector>
#include "Types.h"

namespace avr {

	class Instruction;

	/**
	 * @author Tom Haber
	 * @date Apr 23, 2008
	 * @brief Instruction Decoder
	 *
	 * The decoder can decode instruction opcodes and returns
	 * Instruction classes capable of executing the instruction.
	 *
	 * For performance reasons, it contains a big table with all
	 * possible opcodes and accompanying Instruction instance.
	 */
	class Decoder {
		public:
			Decoder();
			~Decoder();

		public:
			Instruction & decode( word opcode ) const;
			bool is2WordInstruction( word opcode ) const;

		private:
			Instruction *lookupOpcode( word opcode );

		private:
			std::vector<Instruction *> lut;
	};

	inline Instruction & Decoder::decode( word opcode ) const {
		return *lut[opcode];
	}

}

#endif /*AVR_DECODER_H*/
