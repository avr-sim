#ifndef AVR_ANALYZER_H
#define AVR_ANALYZER_H

#include "Types.h"

namespace avr {

	class Core;

	/**
	 * @author Tom Haber
	 * @date May 17, 2008
	 * @brief Abstraction for analyzers
	 *
	 * Analyzers can be inserted into the core and all
	 * changes in the state of the core are reported to it.
	 */
	class Analyzer {
		public:
			Analyzer(Core *core) : core(core) {}
			virtual ~Analyzer() {}

		public:
			virtual void reset(unsigned int /*type*/) {}
			virtual void trace(dword /*address*/) {}
			virtual void step(lword /*ticks*/) {}

		public:
			virtual void readRegister(unsigned int /*r*/, byte /*val*/) {}
			virtual void writeRegister(unsigned int /*r*/, byte /*val*/) {}
			virtual void readByte(unsigned int /*addr*/, byte /*val*/) {}
			virtual void writeByte(unsigned int /*addr*/, byte /*val*/) {}
			virtual void readFlash(unsigned int /*addr*/, byte /*val*/) {}
			virtual void writeFlash(unsigned int /*addr*/, word /*data*/) {}
			virtual void fetchOperand(word /*val*/) {}

		public:
			virtual void push(byte /*val*/) {}
			virtual void pop(byte /*val*/) {}
			virtual void jump(dword /*address*/, bool /*push*/) {}
			virtual void skip() {}
			virtual void call(dword /*address*/, bool /*push*/) {}
			virtual void ret(bool /*interrupt*/) {}

		public:
			virtual void sleep(unsigned int /*mode*/) {}
			virtual void systemBreak() {}
			virtual void interrupt(unsigned int /*vector*/, unsigned int /*addr*/) {}

		protected:
			Core *core;
	};

	class ScriptEngine;

	/**
	 * @author Tom Haber
	 * @date May 23, 2008
	 * @brief Factory for analyzers
	 *
	 * A factory for creating analyzers based on their name.
	 */
	class AnalyzerFactory {
		public:
			/**
			 * Build a new analyzer with name \e name
			 */
			static Analyzer *newAnalyzer(Core *core, const char *name);

#ifdef ENABLE_SCRIPTING
		public:
			/**
			 * Set the scriptengine to use when creating scripting analysers
			 */
			static void setScriptEngine(ScriptEngine *vm);

		private:
			static ScriptEngine *vm;
#endif
	};

}

#endif /*AVR_ANALYZER_H*/
