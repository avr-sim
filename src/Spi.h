#ifndef AVR_SPI_H
#define AVR_SPI_H

#include "Hardware.h"

namespace avr {

	class Spi : public avr::Hardware {
		public:
			Spi(Bus & bus, unsigned int stcVec);
			~Spi();

		public:
			/**
			 * Attach a register with name \e name to the hardware.
			 */
			bool attachReg(const char *name, IORegister *reg);

			/**
			 * Finishes the construction of the hardware.
			 * This should verify the registers and parameters
			 * @returns true if build was successful.
			 */
			bool finishBuild();

			/**
			 * An attached register changed state.
			 */
			void regChanged( IORegister *reg );

			/**
			 * Perform a single step.
			 */
			void step();

			/**
			 * Reset the internal hardware.
			 */
			void reset();

			/**
			 * Called just before an interrupt handler is invoked.
			 */
			void beforeInvokeInterrupt(unsigned int vector);

		private:
			void setSpcr(unsigned char val);
			void setSpdr(unsigned char val);
			void setClkDiv();

		private:
			IORegister *spcr;
			IORegister *spsr;
			IORegister *spdr;

		private:
			unsigned int stcVec;

			int clkDiv;
			int bitCnt;
			unsigned char spdrWrite;
			unsigned char spdrRead;
			unsigned spsr_old;

			enum {
				READY,
				SLAVE,
				START_MASTER,
				MASTER
			} state;
	};

}

#endif /*AVR_SPI_H*/
