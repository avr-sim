#ifndef AVR_EEPROM_H
#define AVR_EEPROM_H

#include "Memory.h"
#include "Hardware.h"

namespace avr {

	/**
	 * @author Tom Haber
	 * @date Apr 21, 2008
	 * @brief Electrically Erasable Programmable Read-Only Memory
	 *
	 * This class represents the Eeprom memory in the chip.
	 */
	class Eeprom : public Hardware, public Memory {
		public:
			Eeprom(Bus & bus, unsigned int size, unsigned int rdyVec);
			~Eeprom();

		public:
			void writeToAddress(unsigned int addr, unsigned char val);
			unsigned char readFromAddress(unsigned int addr);

		public:
			bool attachReg(const char *name, IORegister *reg);
			void regChanged( IORegister *reg );
			void step();

		private:
			void setEECR( unsigned char eecr );

		private:
			unsigned int eear;
			unsigned int rdyVec;
			unsigned char oldEecr;

			enum {
				READY,
				READ,
				WRITE,
				WRITE_ENABLED
			} state;

			static const unsigned int writeEnableCycles = 4;
			static const unsigned int readCycles = 4;
			static const unsigned int writeCycles = 2;

		private:
			IORegister *eearl;
			IORegister *eearh;
			IORegister *eecr;
			IORegister *eedr;
	};

}

#endif /*AVR_EEPROM_H*/
