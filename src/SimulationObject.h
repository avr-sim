#ifndef SIM_SIMULATIONOBJECT_H
#define SIM_SIMULATIONOBJECT_H

#include "SimulationClock.h"

namespace sim {

	/**
	 * @author Tom Haber
	 * @date Apr 27, 2008
	 * @brief An object in the simulation
	 *
	 * Represents a member of the simulation, this can be
	 * either a peripheral or an AVR chip. It contains a
	 * stepping function and can negotiate with the clock
	 * for a specific timing.
	 */
	class SimulationObject {
		public:
			SimulationObject(SimulationClock & clock) : clock(clock) {}
			virtual ~SimulationObject() {};

		public:
			/**
			 * Perform a single simulation step.
			 */
			virtual void step() = 0;

		protected:
			SimulationClock & clock;
	};

}

#endif /*SIM_SIMULATIONOBJECT_H*/
