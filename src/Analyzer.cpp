/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Analyzer.h"
#include "ScriptableAnalyzer.h"
#include "TraceAnalyzer.h"

#include "RuntimeException.h"
#include "Format.h"
#include <cstring>

namespace avr {

#ifdef ENABLE_SCRIPTING
	ScriptEngine *AnalyzerFactory::vm = 0;

	void AnalyzerFactory::setScriptEngine(ScriptEngine *vm) {
		AnalyzerFactory::vm = vm;
	}
#endif

	Analyzer *AnalyzerFactory::newAnalyzer(Core *core, const char *name) {
		const char *tracePrefix = "trace:";
		const char *scriptPrefix = "script:";
		if( strncmp( name, tracePrefix, strlen(tracePrefix) ) == 0 ) {
			// Its a trace
			const char *filename = strchr(name,':') + 1;
			return new TraceAnalyzer(core, filename);
#ifdef ENABLE_SCRIPTING
		} else if( strncmp( name, scriptPrefix, strlen(scriptPrefix) ) == 0 ) {
			// Its a script
			const char *script = strchr(name,':') + 1;
			return new ScriptableAnalyzer(core, vm, script);
#endif
		} else {
			throw util::RuntimeException(
					util::format("Failed to create analyzer %s") % name );
		}
	}

}
