#ifndef AVR_MEMORY_H
#define AVR_MEMORY_H

#include "Types.h"

namespace avr {

	/**
	 * @author Tom Haber
	 * @date 21 April 2008
	 * @brief Abstraction of all types of memory.
	 *
	 * This class is an abstraction of all types of memory
	 * available on the chip: SRAM, ERAM, Flash.
	 *
	 * It allows read and write operations and maintains
	 * the raw data associated with the memory.
	 */
	class Memory {
		public:
			Memory(unsigned int size);
			virtual ~Memory();

		public:
			/**
			 * Reads \e size bytes of raw data from memory starting at
			 * offset \e offset. It returns a pointer to this data.
			 *
			 * \exception AccessViolation { When the data requested is
			 * 		not available, this exception is thrown }
			 */
			const unsigned char *read(unsigned int offset, unsigned int size = 1) const;

			/**
			 * Reads a single bytes of raw data from memory at
			 * offset \e offset.
			 *
			 * \exception AccessViolation { When the data requested is
			 * 		not available, this exception is thrown }
			 */
			byte readByte(unsigned int offset) const;

			/**
			 * Reads a word of raw data from memory starting at
			 * offset \e offset.
			 *
			 * \exception AccessViolation { When the data requested is
			 * 		not available, this exception is thrown }
			 */
			word readWord(unsigned int offset) const;

			/**
			 * Write \e size bytes of raw data to memory starting at
			 * offset \e offset. The data to be written is passed via
			 * the \e block variable.
			 *
			 * \exception AccessViolation { When the write exceeds
			 * 		amount of memory, this exception is thrown }
			 */
			void write(unsigned int offset, unsigned char *block, unsigned int size = 1);

			/**
			 * Fill the entire memory chunk with value \e val.
			 */
			void fill(unsigned char val);

			/**
			 * \returns the size of the memory chunk.
			 */
			unsigned int size() const;

			/**
			 * Dump the memory content to disk
			 */
			void dump(const char *fname) const;

		protected:
			unsigned char *mem;
			unsigned int siz;
	};

	inline unsigned int Memory::size() const {
		return siz;
	}
}

#endif /*AVR_MEMORY_H*/
