/*
 *	avr-sim: An atmel AVR simulator
 *  Copyright (C) 2008  Tom Haber
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Format.h"
#include <ctype.h>
#include <iostream>

namespace util {
	void empty_buf(std::ostringstream & os) {
		static const std::string emptyStr;
		os.str(emptyStr);
	}

	// applies centered / left / right  padding  to the string s.
	// Effects : string s is padded.
	void do_pad(std::string & s, std::streamsize w, const char c,
				std::ios_base::fmtflags f, bool center) {

		std::streamsize n=w-s.size();
		if(n<=0)
			return;

		if( center ){
			s.reserve(w); // allocate once for the 2 inserts
			const std::streamsize n1 = n /2, n0 = n - n1;
			s.insert(s.begin(), n0, c);
			s.append(n1, c);
		} else {
			if(f & std::ios_base::left)
				s.append(n, c);
			else
				s.insert(s.begin(), n, c);
		}
	}

	format::format(const char *str)
		: style(0), cur_arg(0), num_args(0), dumped(false),
		items(), oss(), exceptions_flag(all_error_bits) {

		state0.set_by_stream(oss);

		std::string emptyStr;
		if( str == 0 )
			str = emptyStr.c_str();

		parse( str );
	}

	format::format(const std::string & s)
		: style(0), cur_arg(0), num_args(0), dumped(false),
		items(), oss(), exceptions_flag(all_error_bits) {

		state0.set_by_stream(oss);
		parse(s);
	}

	format::format(const format & x)
		: style(x.style), cur_arg(x.cur_arg), num_args(x.num_args), dumped(false),
		items(x.items), prefix(x.prefix), bound(x.bound),
		oss(),   // <- we obviously can't copy x.oss
		state0(x.state0), exceptions_flag(x.exceptions_flag){

		state0.apply_on(oss);
	}

	format & format::operator =(const format & x) {
		if( this == &x )
			return *this;

		state0 = x.state0;
		state0.apply_on(oss);

		// plus all the other (trivial) assignments :
		exceptions_flag = x.exceptions_flag;
		items = x.items;
		prefix = x.prefix;
		bound=x.bound;
		style=x.style;
		cur_arg=x.cur_arg;
		num_args=x.num_args;
		dumped=x.dumped;
		return *this;
	}

	// empty the string buffers (except bound arguments, see clear_binds() )
	// and make the format object ready for formatting a new set of arguments
	format & format::clear() {
		for(unsigned long i=0; i<items.size(); ++i) {
			items[i].state = items[i].ref_state;

			// clear converted strings only if the corresponding argument is not  bound :
			if( bound.size()==0 || !bound[ items[i].argN ] ) items[i].res.resize(0);
		}

		cur_arg=0; dumped=false;

		// maybe first arg is bound:
		if(bound.size() != 0)
			while( (cur_arg < num_args) && bound[cur_arg] ) ++cur_arg;

		return *this;
	}

	// cancel all bindings, and clear()
	format & format::clear_binds() {
		bound.resize(0);
		clear();

		return *this;
	}

	// cancel the binding of ONE argument, and clear()
	format & format::clear_bind(int argN) {
		if(argN<1 || argN > num_args || bound.size()==0 || !bound[argN-1] ) {
			if( exceptions() & out_of_range_bit )
				throw out_of_range(); // arg not in range.
			else
				return *this;
		}

		bound[argN-1]=false;
		clear();

		return *this;
	}

	std::string format::str() const {
		dumped=true;

		if( items.size()==0 )
			return prefix;

		if( cur_arg < num_args )
			if( exceptions() & too_few_args_bit )
				throw too_few_args(); // not enough variables have been supplied !

		unsigned long sz = prefix.size();
		unsigned long i;
		for(i=0; i < items.size(); ++i)
			sz += items[i].res.size() + items[i].appendix.size();

		std::string res;
		res.reserve(sz);
		res += prefix;

		for(i=0; i < items.size(); ++i) {
			const format_item & item = items[i];
			res += item.res;
			if( item.argN == format_item::argN_tabulation) {
				std::streamsize n = item.state.width - res.size();

				if( n > 0 )
					res.append( n, item.state.fill );
			}

			res += item.appendix;
		}

		return res;
	}

	// Input : char string, with starting index
	//         a basic_ios& merely to call its widen/narrow member function in the desired locale.
	// Effects : reads s[start:] and converts digits into an integral n, of type Res
	// Returns : n
	template<class Res>
	inline Res str2int(const std::string & s,
					   std::string::size_type start,
					   std::ios & os, const Res = Res(0) ) {
		Res n = 0;

		while( start<s.size() && isdigit(s[start]) ) {
			char cur_ch = os.narrow( s[start], 0);
			n *= 10;
			n += cur_ch - '0'; // 22.2.1.1.2 of the C++ standard
			++start;
		}

		return n;
	}

	// skip printf's "asterisk-fields" directives in the format-string buf
	// Input : char string, with starting index *pos_p
	//         a basic_ios& merely to call its widen/narrow member function in the desired locale.
	// Effects : advance *pos_p by skipping printf's asterisk fields.
	// Returns : nothing
	static void skip_asterisk(const std::string & buf,
							  std::string::size_type *pos_p,
							  std::ios &os) {
		using namespace std;
		if(*pos_p >= buf.size() ) return;
		if(buf[ *pos_p]==os.widen('*')) {
			++ (*pos_p);
			while (*pos_p < buf.size() && isdigit(buf[*pos_p])) ++(*pos_p);
			if(buf[*pos_p]==os.widen('$')) ++(*pos_p);
		}
	}

	// Input   : a 'printf-directive' in the format-string, starting at buf[ *pos_p ]
	//           a basic_ios& merely to call its widen/narrow member function in the desired locale.
	//           a bitset'exceptions' telling whether to throw exceptions on errors.
	// Returns : true if parse somehow succeeded (possibly ignoring errors if exceptions disabled)
	//           false if it failed so bad that the directive should be printed verbatim
	// Effects : - *pos_p is incremented so that buf[*pos_p] is the first char after the directive
	//           - *fpar is set with the parameters read in the directive
	bool parse_printf_directive(const std::string & buf,
									   std::string::size_type *pos_p,
									   format::format_item *fpar, std::ios & os,
									   unsigned char exceptions) {
		std::string::size_type & i1 = *pos_p, i0;
		fpar->argN = format::format_item::argN_no_posit;  // if no positional-directive

		bool in_brackets=false;
		if(buf[i1]==os.widen('|')) {
			in_brackets=true;
			if( ++i1 >= buf.size() ) {
				if(exceptions & format::bad_format_string_bit)
					throw bad_format_string();

				return false;
			}
		}

		// the flag '0' would be picked as a digit for argument order, but here it's a flag :
		if(buf[i1]==os.widen('0'))
			goto parse_flags;

		// handle argument order (%2$d)  or possibly width specification: %2d
		i0 = i1;  // save position before digits
		while( i1 < buf.size() && isdigit(buf[i1]) )
			++i1;

		if( i1 != i0 ) {
			if( i1 >= buf.size() ) {
				if( exceptions & format::bad_format_string_bit )
					throw bad_format_string();

				return false;
			}

			int n=str2int(buf,i0, os, int(0) );
			// %N% case : this is already the end of the directive

			if( buf[i1] == os.widen('%') ) {
				fpar->argN = n-1;
				++i1;

				if( in_brackets && (exceptions & format::bad_format_string_bit) )
					throw bad_format_string();

				// but don't return.  maybe "%" was used in lieu of '$', so we go on.
				else return true;

			}

			if ( buf[i1]==os.widen('$') ) {
				fpar->argN = n-1;
				++i1;
			} else {
				// non-positionnal directive
				fpar->ref_state.width = n;
				fpar->argN = format::format_item::argN_no_posit;
				goto parse_precision;
			}
		}

parse_flags:
		// handle flags

		while ( i1 <buf.size() ) {
			// as long as char is one of + - = # 0 l h   or ' '
			// misc switches

			switch( os.narrow(buf[i1], 0) ) {
			case '\'' : break; // no effect yet. (painful to implement)
			case 'l':
			case 'h':  // short/long modifier : for printf-comaptibility (no action needed)
				break;

			case '-':
				fpar->ref_state.flags |= std::ios_base::left;
				break;

			case '=':
				fpar->pad_scheme |= format::format_item::centered;
				break;

			case ' ':
				fpar->pad_scheme |= format::format_item::spacepad;
				break;

			case '+':
				fpar->ref_state.flags |= std::ios_base::showpos;
				break;

			case '0':
				fpar->pad_scheme |= format::format_item::zeropad;
				// need to know alignment before really setting flags,
				// so just add 'zeropad' flag for now, it will be processed later.
				break;

			case '#':
				fpar->ref_state.flags |= std::ios_base::showpoint | std::ios_base::showbase;
				break;

			default:
				goto parse_width;
			}

			++i1;
		} // loop on flag.

		if( i1>=buf.size()) {
			if( exceptions & format::bad_format_string_bit )
				throw bad_format_string();

			return true;
		}

parse_width:
		// handle width spec
		skip_asterisk(buf, &i1, os); // skips 'asterisk fields' :  *, or *N$
		i0 = i1;  // save position before digits
		while (i1<buf.size() && isdigit(buf[i1]))
			i1++;

		if( i1!=i0 ) {
			fpar->ref_state.width = str2int( buf,i0, os, std::streamsize(0) );
		}

parse_precision:
		if( i1>=buf.size()) {
			if( exceptions & format::bad_format_string_bit )
				throw bad_format_string();
			return true;
		}

		// handle precision spec
		if (buf[i1]==os.widen('.')) {
			++i1;
			skip_asterisk(buf, &i1, os);
			i0 = i1;  // save position before digits
			while (i1<buf.size() && isdigit(buf[i1]))
				++i1;

			if(i1==i0)
				fpar->ref_state.precision = 0;
			else
				fpar->ref_state.precision = str2int(buf,i0, os, std::streamsize(0) );
		}

		// handle  formatting-type flags :

		while( i1<buf.size() &&
			( buf[i1]==os.widen('l') || buf[i1]==os.widen('L') || buf[i1]==os.widen('h')) )
			++i1;

		if( i1>=buf.size()) {
			if( exceptions & format::bad_format_string_bit )
				throw bad_format_string();

			return true;
		}

		if( in_brackets && buf[i1]==os.widen('|') ) {
			++i1;
			return true;
		}

		switch (os.narrow(buf[i1], 0) ) {
		case 'X':
			fpar->ref_state.flags |= std::ios_base::uppercase;

		case 'p': // pointer => set hex.
		case 'x':
			fpar->ref_state.flags &= ~std::ios_base::basefield;
			fpar->ref_state.flags |= std::ios_base::hex;
			break;

		case 'o':
			fpar->ref_state.flags &= ~std::ios_base::basefield;
			fpar->ref_state.flags |=  std::ios_base::oct;
			break;

		case 'E':
			fpar->ref_state.flags |=  std::ios_base::uppercase;
		case 'e':
			fpar->ref_state.flags &= ~std::ios_base::floatfield;
			fpar->ref_state.flags |=  std::ios_base::scientific;
			fpar->ref_state.flags &= ~std::ios_base::basefield;
			fpar->ref_state.flags |=  std::ios_base::dec;
			break;

		case 'f':
			fpar->ref_state.flags &= ~std::ios_base::floatfield;
			fpar->ref_state.flags |=  std::ios_base::fixed;
		case 'u':
		case 'd':
		case 'i':
			fpar->ref_state.flags &= ~std::ios_base::basefield;
			fpar->ref_state.flags |=  std::ios_base::dec;
			break;

		case 'T':
			++i1;
			if( i1 >= buf.size()) {
				if( exceptions & format::bad_format_string_bit )
					throw bad_format_string();
			} else {
				fpar->ref_state.fill = buf[i1];
			}

			fpar->pad_scheme |= format::format_item::tabulation;
			fpar->argN = format::format_item::argN_tabulation;
			break;

		case 't':
			fpar->ref_state.fill = os.widen(' ');
			fpar->pad_scheme |= format::format_item::tabulation;
			fpar->argN = format::format_item::argN_tabulation;
			break;

		case 'G':
			fpar->ref_state.flags |= std::ios_base::uppercase;
			break;

		case 'g': // 'g' conversion is default for floats.
			fpar->ref_state.flags &= ~std::ios_base::basefield;
			fpar->ref_state.flags |=  std::ios_base::dec;
			// CLEAR all floatield flags, so stream will CHOOSE
			fpar->ref_state.flags &= ~std::ios_base::floatfield;
			break;

		case 'C':
		case 'c':
			fpar->truncate = 1;
			break;

		case 'S':
		case 's':
			fpar->truncate = fpar->ref_state.precision;
			fpar->ref_state.precision = -1;
			break;

		case 'n' :
			fpar->argN = format::format_item::argN_ignored;
			break;

		default:
			if( exceptions & format::bad_format_string_bit )
				throw bad_format_string();
      }

	  ++i1;

	  if( in_brackets ) {
		  if( i1<buf.size() && buf[i1]==os.widen('|') ) {
			  ++i1;
			  return true;
          } else if( exceptions & format::bad_format_string_bit )
			  throw bad_format_string();
      }

	  return true;
  }

	// parse the format-string
	void format::parse(const std::string & buf) {
		using namespace std;
		const char arg_mark = oss.widen('%');
		bool ordered_args=true;
		int max_argN=-1;
		string::size_type i1=0;
		int num_items=0;

		// A: find upper_bound on num_items and allocates arrays
		i1=0;
		while( (i1=buf.find(arg_mark,i1)) != string::npos ) {
			if( i1+1 >= buf.size() ) {
				if( exceptions() & bad_format_string_bit)
					throw bad_format_string(); // must not end in "bla bla %"

				else break; // stop there, ignore last '%'
			}

			if(buf[i1+1] == buf[i1] ) { i1+=2; continue; } // escaped "%%" / "##"
			++i1;

			// in case of %N% directives, dont count it double (wastes allocations..) :
			while(i1 < buf.size() && isdigit(buf[i1])) ++i1;
			if( i1 < buf.size() && buf[i1] == arg_mark ) ++ i1;

			++num_items;

		}

		items.assign( num_items, format_item() );

		// B: Now the real parsing of the format string :
		num_items=0;
		i1 = 0;
		string::size_type i0 = i1;
		bool special_things=false;
		int cur_it=0;
		while( (i1=buf.find(arg_mark,i1)) != string::npos ) {
			string & piece = (cur_it==0) ? prefix : items[cur_it-1].appendix;

			// escaped mark, '%%'
			if( buf[i1+1] == buf[i1] ) {
				piece += buf.substr(i0, i1-i0) + buf[i1];
				i1+=2; i0=i1;
				continue;
			}

			if(i1!=i0) piece += buf.substr(i0, i1-i0);
			++i1;

			bool parse_ok;
			parse_ok = parse_printf_directive(buf, &i1, &items[cur_it], oss, exceptions());
			if( ! parse_ok ) continue; // the directive will be printed verbatim

			i0=i1;
			items[cur_it].compute_states(); // process complex options, like zeropad, into stream params.

			int argN=items[cur_it].argN;

			if( argN == format_item::argN_ignored )
				continue;

			if(argN ==format_item::argN_no_posit)
				ordered_args=false;
			else if(argN == format_item::argN_tabulation)
				special_things=true;
			else if(argN > max_argN)
				max_argN = argN;

			++num_items;
			++cur_it;
		} // loop on %'s

		// store the final piece of string
		string & piece = (cur_it==0) ? prefix : items[cur_it-1].appendix;
		piece += buf.substr(i0);

		if( !ordered_args) {
			// dont mix positional with non-positionnal directives
			if(max_argN >= 0 ) {
				if( exceptions() & bad_format_string_bit )
					throw bad_format_string();
				// else do nothing. => positional arguments are processed as non-positional
			}

			// set things like it would have been with positional directives :
			int non_ordered_items = 0;
			for(int i=0; i< num_items; ++i)
				if(items[i].argN == format_item::argN_no_posit) {
					items[i].argN = non_ordered_items;
					++non_ordered_items;
				}
				max_argN = non_ordered_items-1;
		}

		// C: set some member data :
		items.resize(num_items);

		if(special_things)
			style |= special_needs;

		num_args = max_argN + 1;

		if(ordered_args)
			style |=  ordered;
		else
			style &= ~ordered;
	}

	// effect: "return os << str(f);" but we can try to do it faster
	std::ostream & operator <<(std::ostream & os, const format & f) {
		if( f.items.size() == 0 ) {
			os << f.prefix;
		} else {
			if( f.cur_arg < f.num_args )
				if( f.exceptions() & format::too_few_args_bit )
					throw too_few_args(); // not enough variables have been supplied !

				if( f.style & format::special_needs) {
					os << f.str();
				} else {
					// else we dont have to count chars output, so we dump directly to os :
					os << f.prefix;
					for(unsigned long i=0; i<f.items.size(); ++i) {

						const format::format_item & item = f.items[i];
						os << item.res;
						os << item.appendix;
					}
				}
		}

		f.dumped=true;
		return os;

	}
}
