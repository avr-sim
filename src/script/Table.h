#ifndef SCRIPT_TABLE_H
#define SCRIPT_TABLE_H

#include <string>

struct lua_State;
namespace script {
	class VM;

	/**
	  \class Table
	  \author Tom Haber
	  \brief Lua Table

	  Wrapper for a lua table.
	 */
	class Table {
		public:
			/** Creates a table to specified script VM. */
			Table(VM* vm);
			Table(const Table & other);
			~Table();

		public:
			Table &	operator =(const Table & other);

		public:
			//* Remove a specific table entry.
			void remove(int index);
			//* Remove a specific table entry.
			void remove(const char *name);
			void remove(const std::string & name);

			void setString(int index, const char *v);
			void setString(int index, const std::string & v);
			void setNumber(int index, float v);
			void setTable(int index, const Table & v);
			void setBoolean(int index, bool v);
			void setLightUserData(int index, void *ud);
			void setString(const char *name, const std::string & v);
			void setNumber(const char *name, float v);
			void setTable(const char *name, const Table & v);
			void setBoolean(const char *name, bool v);
			void setLightUserData(const char *name, void *ud);

			const char *getString(const char *name) const;
			const char *getString(int index) const;
			float getNumber(const char *name) const;
			float getNumber(int index) const;
			Table getTable(const char *name) const;
			Table getTable(int index) const;
			bool getBoolean(const char *name) const;
			bool getBoolean(int index) const;

			bool isNil(const char *name) const;
			bool isNil(int index) const;

			/** Pushes member by name to script VM stack. */
			void pushMember(int index) const;
			/** Pushes member by index to script VM stack. */
			void pushMember(const char *name) const;

			/**
			  Returns number of elements in the table.
			  This number is either value of 'n' or largest
			  numerical index with non-nil value.
			 */
			int	size() const;

			/** Return Lua table reference. */
			int	luaRef() const;

		private:
			Table() {}
			friend class VM;

		private:
			lua_State *lua;
			int	ref;
	};

	inline int Table::luaRef() const {
		return ref;
	}
}
#endif

