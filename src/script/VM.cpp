#include "VM.h"
#include "ScriptException.h"
#include <iostream>
#include <assert.h>

extern "C" {
#include <lua50/lua.h>
#include <lua50/lauxlib.h>
#include <lua50/lualib.h>
}

namespace script {
#ifdef LUA_DEBUG
	static void LineHook(lua_State *L, lua_Debug *ar) {
		std::cerr << ar->currentline << std::endl;
	}

	static void CallHook(lua_State *L, lua_Debug *ar) {
		std::cerr << ar->source << ' ' <<  ar->name << std::endl;
	}
#endif

	VM::VM() {
		assert( (None == LUA_TNONE) && (Nil == LUA_TNIL) &&
			(Boolean == LUA_TBOOLEAN) && (LightUserData == LUA_TLIGHTUSERDATA) &&
			(Number == LUA_TNUMBER) && (String == LUA_TSTRING) &&
			(Table == LUA_TTABLE) && (Function == LUA_TFUNCTION) &&
			(UserData == LUA_TUSERDATA) && (Thread == LUA_TTHREAD) );

		lua = lua_open();
		if( lua == 0 )
			throw ScriptException("Failed to initialize Lua virtual machine");

		lua_pushstring(lua, "_ERRORMESSAGE");
		lua_pushlightuserdata(lua, this);
		lua_pushcclosure(lua, printError, 1);
		lua_settable(lua, LUA_GLOBALSINDEX);

		lua_register(lua, "trace", printMessage);

		static const luaL_reg lualibs[] = {
			{"base", luaopen_base},
			{"table", luaopen_table},
			{"io", luaopen_io},
			{"string", luaopen_string},
			{"math", luaopen_math},
			{"debug", luaopen_debug},
//			{"loadlib", luaopen_loadlib},
			{NULL, NULL}
		};

		const luaL_reg *lib = lualibs;
		for(; lib->func; lib++) {
			lib->func(lua);  /* open library */
			lua_settop(lua, 0);  /* discard any results */
		}

#ifdef LUA_DEBUG
		lua_sethook(lua, CallHook, LUA_MASKCALL, 0);
		lua_sethook(lua, LineHook, LUA_MASKLINE, 0);
#endif
	}

	VM::~VM() {
		if( lua != 0 ) {
			lua_settop(lua, 0);
			lua_close(lua);
			lua = 0;
		}
	}

	void VM::compileFile(const char *name) {
		if( lua_dofile(lua, name) )
			throw ScriptException("Failed to compile script file");
	}

	void VM::compileSource(const std::string & src) {
		if( lua_dostring(lua, src.c_str()) )
			throw ScriptException("Failed to compile script string");
	}

	void VM::compileSource(const char *buffer, int size) {

	}

	void VM::gc() {
		lua_setgcthreshold(lua, 0);  // collected garbage
	}

	bool VM::next(int index) {
		if( ! lua_istable(lua, index) )
			throw ScriptException("Tried to use next() on a non-table object");

		return (0 != lua_next(lua, index));
	}

	void VM::pop(int n /*= 1*/) {
		lua_pop(lua, n);
	}

	void VM::setTable(int index) {
		if( lua_istable(lua, index) == 0 )
			throw ScriptException("Tried to use object as table");

		lua_settable(lua, index);
	}

	void VM::setTableRaw(int index) {
		if( lua_istable(lua,index) == 0 )
			throw ScriptException("Tried to use object as table");

		lua_rawset(lua, index);
	}

	void VM::setTableRawI(int index, int n) {
		if( lua_istable(lua, index) == 0 )
			throw ScriptException("Tried to use object as table");

		lua_rawseti(lua, index, n);
	}

	void VM::call(int args, int results) {
		if( lua_isfunction(lua, -args - 1) == 0 )
			throw ScriptException("Called invalid script function\n");

		lua_pushliteral(lua, "_ERRORMESSAGE");
		lua_gettable(lua, LUA_GLOBALSINDEX);   // get traceback function
		lua_insert(lua, 1);

		int errnr = lua_pcall(lua, args, results, 1);
		if( errnr != 0 ) {
			if( err.empty() ) {
				const char *msg = lua_tostring(lua, -1);
				if( msg == 0 )
					msg = "(error with no message)";

				lua_pop(lua, 1);
				throw ScriptException(msg);
			} else {
				throw ScriptException(err);
			}
		}

		lua_remove(lua, 1);
	}

	void VM::registerClass(const char *name, const script::Table & table) {
		lua_pushstring(lua, name);
		lua_getref(lua, table.ref);
		lua_settable(lua, LUA_GLOBALSINDEX);
	}

	void VM::unregisterClass(const char *name) {
		lua_pushstring(lua, name);
		lua_pushnil(lua);
		lua_settable(lua, LUA_GLOBALSINDEX);
	}

	void VM::getGlobal(const std::string & name) {
		lua_getglobal(lua, name.c_str());
	}

	void VM::getGlobal(const char *name) {
		lua_getglobal(lua, name);
	}

	void VM::setGlobal(const std::string & name) {
		lua_setglobal(lua, name.c_str());
	}

	void VM::setGlobal(const char *name) {
		lua_setglobal(lua, name);
	}

	void VM::pushValue(int index) {
		lua_pushvalue(lua, index);
	}

	void VM::pushNil() {
		lua_pushnil(lua);
	}

	void VM::pushNumber(float x) {
		lua_pushnumber(lua, x);
	}

	void VM::pushBoolean(bool b) {
		lua_pushboolean(lua, b);
	}

	void VM::pushString(const std::string & str) {
		lua_pushstring(lua, str.c_str());
	}

	void VM::pushString(const char *str) {
		lua_pushstring(lua, str);
	}

	void VM::pushTable(const script::Table & tab) {
		assert( lua == tab.lua );

		if( tab.ref >= 0 )
			lua_getref(lua, tab.ref);
		else
			lua_pushnil(lua);
	}

	void VM::pushCFunction(CFunction f) {
		lua_pushcclosure(lua, f, 0);
	}

	void VM::pushCClosure(CFunction f, int n) {
		lua_pushcclosure(lua, f, n);
	}
	void VM::remove( int index ) {
		lua_remove(lua, index);
	}

	int VM::getType(int index) const {
		return lua_type(lua, index);
	}

	const char *VM::getTypeName(int type) const {
		return lua_typename(lua, type);
	}

	bool VM::isType(int index, VM::Type type) const {
		return ( lua_type(lua, index) == type );
	}

	bool VM::isCFunction(int index) const {
		return ( lua_iscfunction(lua, index) != 0 );
	}

	bool VM::isFunction(int index) const {
		return ( lua_type(lua, index) == LUA_TFUNCTION );
	}

	bool VM::isNil(int index) const {
		return ( lua_type(lua, index) == LUA_TNIL );
	}

	bool VM::isNumber(int index) const {
		return ( lua_type(lua, index) == LUA_TNUMBER );
	}

	bool VM::isString(int index) const {
		return ( lua_type(lua, index) == LUA_TSTRING );
	}

	bool VM::isTable(int index) const {
		return ( lua_type(lua, index ) == LUA_TTABLE );
	}

	bool VM::isUserData(int index) const {
		return ( lua_type(lua, index) == LUA_TUSERDATA );
	}

	bool VM::isBoolean(int index) const {
		return ( lua_type(lua, index) == LUA_TBOOLEAN );
	}

	bool VM::isEqual(int index1, int index2) const {
		return ( lua_equal(lua, index1, index2) != 0 );
	}

	bool VM::isLess(int index1, int index2) const {
		return ( lua_lessthan(lua, index1, index2) != 0 );
	}

	int VM::stackSpace() const {
		//return luaL_stackspace(lua);
		return 0;
	}

	bool VM::toBoolean(int index) const {
		if( lua_type(lua, index ) != LUA_TBOOLEAN )
			throw ScriptException("Tried to use non-boolean as boolean");

		return lua_toboolean(lua, index);
	}

	float VM::toNumber(int index) const {
		if( lua_type(lua, index ) != LUA_TNUMBER )
			throw ScriptException("Tried to use non-number as number");

		return lua_tonumber(lua, index);
	}

	const char *VM::toString(int index) const {
		if( ! lua_isstring(lua, index) )
			throw ScriptException("Tried to use non-string as string");

		return lua_tostring(lua, index);
	}

	script::Table VM::toTable(int index) const {
		if ( ! lua_istable(lua, index) )
			throw ScriptException("Tried to use non-table as table");
		lua_pushvalue(lua, index);

		script::Table tab;
		tab.lua = lua;
		tab.ref = lua_ref(lua, true);
		return tab;
	}

	VM::CFunction VM::toCFunction(int index) const {
		if( lua_iscfunction(lua, index) == 0 )
			throw ScriptException("Tried to use non C-function as C-function");

		return lua_tocfunction(lua, index);
	}

	void *VM::toUserData(int index) const {
		if( lua_isuserdata(lua, index) == 0 )
			throw ScriptException("Tried to use object as user data");

		return lua_touserdata(lua, index);
	}

	int VM::top() const {
		return lua_gettop(lua);
	}

	void VM::stackTrace(int level, std::string & trace) const {
		lua_Debug ar;
//		memset(&ar, 0, sizeof(ar));
		if( ! lua_getstack(lua, 1 + level, &ar) ) {
			trace = "";
			return;
		}

		if( ! lua_getinfo(lua, "Snl", &ar) ) {
			trace = "";
			return;
		}

		char str[512];
		sprintf(str, "at %s(%s:%i)", ar.name, ar.source, ar.currentline);
		trace.assign(str);
	}

	int VM::printError(lua_State* lua) {
		assert( lua_isstring(lua, -1) );
		assert( lua_isuserdata(lua, lua_upvalueindex(1)) );

		const char *msg = lua_tostring(lua, -1);
		VM *vm = (VM *)lua_touserdata(lua, lua_upvalueindex(1));
		vm->err = msg;

		// get stack trace
		const short MAX_LEVEL = 20;
		std::string trace[MAX_LEVEL];
		short level = 0;
		for( ; level < MAX_LEVEL ; ++level ) {
			vm->stackTrace(level, trace[level]);
			if( trace[level] == "" )
				break;

			if( level == 0 )
				vm->err = vm->err + "\nStack trace:";
			vm->err = vm->err + "\n\t  " + trace[level];
		}

		// debug output
/*		std::cerr << msg << std::endl;
		if( level > 0 ) {
			std::cerr <<  "Stack trace:";
			for ( int i = 0 ; i < level ; ++i )
				std::cerr << trace[i] << std::endl;
		}
*/
		return 0;
	}

	int VM::printMessage( lua_State* lua ) {
		assert( lua_isstring(lua, 1) );

		const char *msg = lua_tostring(lua, 1);

		// get caller
		lua_Debug ar;
//		memset(&ar, 0, sizeof(ar));
		lua_getstack(lua, 1, &ar);
		lua_getinfo(lua, "Snl", &ar);

		// debug output
		std::cerr << "script: " << msg << " at "
				  << ar.source << ar.currentline << std::endl;

		return 0;
	}

	void VM::getRef(int ref) {
		lua_getref(lua, ref);
	}

	int VM::ref(bool lock) {
		return lua_ref(lua, lock);
	}

	void VM::unref(int ref) {
		lua_unref(lua, ref );
	}
}

