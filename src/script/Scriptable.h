#ifndef SCRIPT_SCRIPTABLE_H
#define SCRIPT_SCRIPTABLE_H


#include "Table.h"

namespace script {
	class VM;


	/**
	   @author Tom Haber
	   @brief Scriptable object base

	   Base class for C++ objects which can interact with scripts.
	   For each C++ Scriptable object there is a Lua entity, table.
	   Both Lua and C++ methods can be added to the table so that
	   C++ methods can call Lua methods and vice versa.
	 */
	class Scriptable {
		public:
			/**
			  Initializes scriptable object in specified environment.

			  @param vm Script execution environment.
			 */
			Scriptable(VM *vm);
			virtual ~Scriptable();

		public:
			/**
			  Compiles script from ASCII-text file to executable code.

			  @exception ScriptException
			 */
			void compileFile(const char *name);

			/**
			  Compiles script from zero-terminated string to executable code.

			  @exception ScriptException
			 */
			void compileSource(const std::string & src);

			/**
			  Compiles script from buffer to executable code.

			  @exception ScriptException
			 */
			void compileSource(const char *buffer, int size);

		public:
			void registerClass(const char *name);
			void unregisterClass(const char *name);

			/**
			  Adds a (C++) member function to be usable from scripts.
			  When the function is called from a script, virtual methodCall()
			  is executed and unique function identifier is passed as parameter.

			  @return Unique function identifier (index).
			  @see methodCall
			 */
			int	addMethod(const std::string & name);

			/**
			  Pushes a script object member function followed by
			  script object table to the script stack.
			  To call a script function, first push the function
			  with this (pushMethod), then push arguments (pushParam)
			  and finally call(nargs, nresults) method.

			  @exception ScriptException
			  @see call
			  @see pushParam
			 */
			void pushMethod(const char *name) const;
			void pushMethod(const std::string & name) const;

			/**
			  Pushes the script object table to the script stack.
			 */
			void pushTable() const;

			/**
			  Calls a script function using this object as global environment.
			  Method and parameters must be pushed to script stack before the call.
			  Specified number of results is left to the stack after execution.

			  @exception ScriptException
			  @see pushMethod
			  @see pushParam
			 */
			void call(int nargs, int nresults) const;

			/**
			  When a (C++) member function is called from a script,
			  this function is executed and unique function identifier
			  is passed as parameter. Derived classes must override
			  this if they add new scriptable functions.

			  @param vm Script virtual machine executing the method.
			  @param i Unique function identifier (index).
			  @return Number of arguments returned in the script stack.
			  @exception ScriptException
			  @see addMethod
			 */
			virtual int methodCall(int i);

			/**
			  Returns true if the object has a script function with
			  specified name.
			 */
			bool hasMethod(const std::string & name) const;
			bool hasMethod(const char *name) const;

			/**
			  Checks from script stack that the tags of the parameters passed
			  to the (C++) method match specified sequence of tags.

			  @param tags Array of type tags to check.
			  @param n Maximum number of parameters.
			  @param opt Number of optional parameters (at the end).
			  @return true if the parameter tags match, false otherwise.
			 */
			bool hasParams(const int *tags, int n, int opt = 0) const;

			/** Returns script execution environment of the object. */
			VM *vmPtr() const;

			/**
			  Returns number of (C++) member functions there is in the
			  script object.
			 */
			int	nrMethods() const;

			/** Prints debug info about functions in the object table. */
//			void printFunctions(const std::string & objectType) const;

			/**
			  Returns C++ 'this' ptr from Lua table.

			  @exception ScriptException
			 */
//			static Scriptable *getThisPtr(VM *vm, int stackIndex);

		public:
			void setString(int index, const char *v);
			void setString(int index, const std::string & v);
			void setNumber(int index, float v);
			void setTable(int index, const Table & v);
			void setBoolean(int index, bool v);
			void setString(const char *name, const std::string & v);
			void setNumber(const char *name, float v);
			void setTable(const char *name, const Table & v);
			void setBoolean(const char *name, bool v);
			void setLightUserData(int index, void *ud);
			void setLightUserData(const char *name, void *ud);

			const char *getString(const char *name) const;
			const char *getString(int index) const;
			float getNumber(const char *name) const;
			float getNumber(int index) const;
			Table getTable(const char *name) const;
			Table getTable(int index) const;
			bool getBoolean(const char *name) const;
			bool getBoolean(int index) const;

		protected:
			VM *vm;

		private:
			int methods;
			Table table;

		private:
			Scriptable( const Scriptable& );
			Scriptable & operator=( const Scriptable& );
	};

	inline VM *Scriptable::vmPtr() const {
		return vm;
	}

	inline int Scriptable::nrMethods() const {
		return methods;
	}

	inline void Scriptable::pushMethod(const std::string & name) const {
		pushMethod( name.c_str() );
	}

	inline bool Scriptable::hasMethod(const std::string & name) const {
		return hasMethod( name.c_str() );
	}

	inline void Scriptable::setString(int index, const char *v) {
		table.setString(index, v);
	}

	inline void Scriptable::setString(int index, const std::string & v) {
		table.setString(index, v);
	}

	inline void Scriptable::setNumber(int index, float v) {
		table.setNumber(index, v);
	}

	inline void Scriptable::setTable(int index, const Table & v) {
		table.setTable(index, v);
	}

	inline void Scriptable::setBoolean(int index, bool v) {
		table.setBoolean(index, v);
	}

	inline void Scriptable::setString(const char *name, const std::string & v) {
		table.setString(name, v);
	}

	inline void Scriptable::setNumber(const char *name, float v) {
		table.setNumber(name, v);
	}

	inline void Scriptable::setTable(const char *name, const Table & v) {
		table.setTable(name, v);
	}

	inline void Scriptable::setBoolean(const char *name, bool v) {
		table.setBoolean(name, v);
	}

	inline void Scriptable::setLightUserData(int index, void *ud) {
		table.setLightUserData(index, ud);
	}

	inline void Scriptable::setLightUserData(const char *name, void *ud) {
		table.setLightUserData(name, ud);
	}

	inline const char *Scriptable::getString(const char *name) const {
		return table.getString(name);
	}

	inline const char *Scriptable::getString(int index) const {
		return table.getString(index);
	}

	inline float Scriptable::getNumber(const char *name) const {
		return table.getNumber(name);
	}

	inline float Scriptable::getNumber(int index) const {
		return table.getNumber(index);
	}

	inline Table Scriptable::getTable(const char *name) const {
		return table.getTable(name);
	}

	inline Table Scriptable::getTable(int index) const {
		return table.getTable(index);
	}

	inline bool Scriptable::getBoolean(const char *name) const {
		return table.getBoolean(name);
	}

	inline bool Scriptable::getBoolean(int index) const {
		return table.getBoolean(index);
	}
}
#endif

