#include "Scriptable.h"
#include "VM.h"
#include "ScriptException.h"
#include "RestoreStack.h"

#include <algorithm>
#include <vector>
#include <iostream>

extern "C" {
#include <lua50/lua.h>
}

namespace script {
	class This {
		public:
			This(VM *vm, Table & tab) : vm(vm) {
				  vm->getGlobal("this");
				  old = vm->ref(true);

				  vm->pushTable(tab);
				  vm->setGlobal("this");
			  }

			  ~This() {
				  vm->getRef(old);
				  vm->setGlobal("this");
				  vm->unref(old);
			  }

		private:
			VM *vm;
			int old;
	};

	/** Member function call dispatcher. */
	static int callMethod(lua_State *lua) {
		try {
			// check that 'this' table is passed to the function
			int type = lua_type(lua, 1);
			if( type != LUA_TTABLE )
				throw ScriptException("Missing 'this' table when calling C++ function from Lua");

			// get C++ this ptr from Lua 'this' table index 0
			lua_rawgeti(lua, 1, 0);
			type = lua_type(lua, -1);
			if( type != LUA_TLIGHTUSERDATA )
				throw ScriptException("Invalid 'this' table when calling C++ function from Lua");

			Scriptable* scriptable = (Scriptable*)lua_touserdata(lua, -1);

			// get index of called method
			int methodIndex = (int)lua_tonumber(lua, lua_upvalueindex(1));

			// remove 'this' table,
			lua_remove(lua, 1);
			lua_pop(lua, 1);

			// call the function, only arguments in the Lua stack
			return scriptable->methodCall(methodIndex);
		} catch( ScriptException  & e ) {
			lua_pushstring(lua, e.message().c_str() );
			lua_error(lua);
			return 0;
		}
	}

#if 0
	/**
	  Gets scriptable C++ functions of a table.

	  @param tableIndex Index of the table in VM stack.
	 */
	static void getFunctions(VM *vm, std::vector<std::string> & funcNames,
														int tableIndex) {
		vm->pushNil();

		while( vm->next(tableIndex) ) {
			if( vm->isFunction(-1) && vm->isCFunction(-1) ) {
				// ignore 'init'
				const char *funcName = vm->toString(-2);
				if( strcmp("init", funcName) != 0 )
					funcNames.push_back( std::string(funcName) );
			}

			vm->pop();
		}
	}
#endif

	Scriptable::Scriptable(VM *vm) : vm(vm), methods(0), table(vm) {
		// set C++ this ptr to index 0
		table.setLightUserData(0, this);
	}

	Scriptable::~Scriptable() {
		table.remove(0);
	}

	void Scriptable::compileFile(const char *name) {
		This th(vm, table);
		vm->compileFile(name);
	}

	void Scriptable::compileSource(const std::string & src) {
		This th(vm, table);
		vm->compileSource(src);
	}

	void Scriptable::compileSource(const char *buffer, int size) {
		This th(vm, table);
		vm->compileSource(buffer, size);
	}

	void Scriptable::registerClass(const char *name) {
		vm->registerClass(name, table);
	}

	void Scriptable::unregisterClass(const char *name) {
		vm->unregisterClass(name);
	}

	int Scriptable::addMethod(const std::string & name) {
		const int methodIndex = methods++;
		vm->pushTable(table);
		vm->pushString(name);
		vm->pushNumber( (float)methodIndex );
		vm->pushCClosure(callMethod, 1);
		vm->setTable(-3);
		vm->pop();
		return methodIndex;
	}

	void Scriptable::pushMethod(const char *name) const {
		// get method to stack top
		table.pushMember(name);
		vm->pushTable(table);
	}

	void Scriptable::pushTable() const {
		vm->pushTable(table);
	}

	bool Scriptable::hasMethod(const char *name) const {
		table.pushMember(name);
		bool ret = vm->isFunction(-1);
		vm->pop();
		return ret;
	}

	bool Scriptable::hasParams(const int types[], int n, int opt) const {
		int params = vm->top();
		if( params < n - opt )
			return false;

		int count = n;
		if( count > params && count >= n-opt )
			count = params;

		for( int i = 0 ; i < count ; ++i ) {
			if( vm->getType(i + 1) != types[i] )
				return false;
		}

		return true;
	}

	void Scriptable::call(int nargs, int nresults) const {
		int top = vm->top();

		int elems = 2 + nargs;
		if( ! vm->isFunction(-elems) ) {
			vm->pop(elems);
			throw ScriptException("Function not exist in script");
		}

		vm->call(1 + nargs, nresults );	// this + args

		// Check stack
		if( top + nresults - (nargs + 2) != vm->top() )
			throw ScriptException("Scriptable::call => Bad stack");
	}

	int Scriptable::methodCall(int) {
		return 0;
	}
}

