#include "Table.h"
#include "VM.h"
#include "ScriptException.h"
#include "RestoreStack.h"

extern "C" {
#include <lua50/lua.h>
#include <lua50/lauxlib.h>
}

namespace script {
	Table::Table(VM *vm) {
		lua = vm->luaState();
		lua_newtable(lua);
		ref = lua_ref(lua, true);
	}

	Table::Table(const Table & other) : lua(other.lua) {
		lua_getref(lua, other.ref);
		ref = lua_ref(lua, true);
	}

	Table::~Table() {
		lua_unref(lua, ref);
	}

	Table & Table::operator =(const Table & other) {
		lua_unref(lua, ref);

		lua_getref(lua, other.ref);
		lua = other.lua;
		ref = lua_ref(other.lua, true);

		return *this;
	}

	void Table::remove(int index) {
		lua_getref(lua, ref);
		lua_pushnil(lua);
		lua_rawseti(lua, -2, index);
		lua_pop(lua, 1);
	}

	void Table::remove(const std::string & name) {
		lua_getref(lua, ref);
		lua_pushstring(lua, name.c_str());
		lua_pushnil(lua);
		lua_rawset(lua, -3);
		lua_pop(lua, 1);
	}

	void Table::remove(const char *name) {
		lua_getref(lua, ref);
		lua_pushstring(lua, name);
		lua_pushnil(lua);
		lua_rawset(lua, -3);
		lua_pop(lua, 1);
	}

	void Table::setString(int index, const char *v) {
		lua_getref(lua, ref);
		lua_pushstring(lua, v);
		lua_rawseti(lua, -2, index);
		lua_pop(lua, 1);
	}

	void Table::setString(int index, const std::string & v) {
		RestoreStack rs(lua);
		lua_getref(lua, ref);
		lua_pushstring(lua, v.c_str());
		lua_rawseti(lua, -2, index);
		lua_pop(lua, 1);
	}

	void Table::setNumber(int index, float v) {
		lua_getref(lua, ref);
		lua_pushnumber(lua, v);
		lua_rawseti(lua, -2, index);
		lua_pop(lua, 1);
	}

	void Table::setBoolean(int index, bool v) {
		lua_getref(lua, ref);
		lua_pushboolean(lua, v);
		lua_rawseti(lua, -2, index);
		lua_pop(lua, 1);
	}

	void Table::setTable(int index, const Table & v) {
		lua_getref(lua, ref);
		lua_getref(lua, v.luaRef() );
		lua_rawseti(lua, -2, index);
		lua_pop(lua, 1);
	}

	void Table::setLightUserData(int index, void *ud) {
		lua_getref(lua, ref);
		lua_pushlightuserdata(lua, ud);
		lua_rawseti(lua, -2, index);
		lua_pop(lua, 1);
	}

	void Table::setString(const char *name, const std::string & v) {
		lua_getref(lua, ref);
		lua_pushstring(lua, name);
		lua_pushstring(lua, v.c_str());
		lua_rawset(lua, -3);
		lua_pop(lua, 1);
	}

	void Table::setNumber(const char *name, float v) {
		lua_getref(lua, ref);
		lua_pushstring(lua, name);
		lua_pushnumber(lua, v);
		lua_rawset(lua, -3);
		lua_pop(lua, 1);
	}

	void Table::setBoolean(const char *name, bool v) {
		lua_getref(lua, ref);
		lua_pushstring(lua, name);
		lua_pushboolean(lua, v);
		lua_rawset(lua, -3);
		lua_pop(lua, 1);
	}

	void Table::setTable(const char *name, const Table & v) {
		lua_getref(lua, ref);
		lua_pushstring(lua, name);
		lua_getref(lua, v.luaRef() );
		lua_rawset(lua, -3);
		lua_pop(lua, 1);
	}

	void Table::setLightUserData(const char *name, void *ud) {
		lua_getref(lua, ref);
		lua_pushstring(lua, name);
		lua_pushlightuserdata(lua, ud);
		lua_rawset(lua, -3);
		lua_pop(lua, 1);
	}

	const char *Table::getString(const char *name) const {
		lua_getref(lua, ref);
		lua_pushstring(lua, name);
		lua_rawget(lua, -2);
		const char *str = lua_tostring(lua, -1);
		lua_pop(lua, 2);
		return str;
	}

	float Table::getNumber(const char *name) const {
		lua_getref(lua, ref);
		lua_pushstring(lua, name);
		lua_rawget(lua, -2);
		float x = lua_tonumber(lua, -1);
		lua_pop(lua, 2);
		return x;
	}

	Table Table::getTable(const char *name) const {
		lua_getref(lua, ref);
		lua_pushstring(lua, name);
		lua_rawget(lua, -2);
		if( lua_istable(lua, -1) == 0 )
			throw ScriptException("Tried to get table from table but type was invalid");

		Table tab;
		tab.lua = lua;
		tab.ref = lua_ref(lua, true);
		lua_pop(lua, 1);
		return tab;
	}

	const char *Table::getString(int index) const {
		lua_getref(lua, ref);
		lua_rawgeti(lua, -1, index);
		const char *str = lua_tostring(lua, -1);
		lua_pop(lua, 2);
		return str;
	}

	float Table::getNumber(int index) const {
		lua_getref(lua, ref);
		lua_rawgeti(lua, -1, index);

		if( lua_type(lua,-1) != LUA_TNUMBER )
			throw ScriptException("Tried to get number from table but type was invalid");

		float x = lua_tonumber(lua, -1);
		lua_pop(lua, 2);
		return x;
	}

	Table Table::getTable(int index) const {
		lua_getref(lua, ref);
		lua_rawgeti(lua, -1, index);
		if( lua_istable(lua,-1) == 0 )
			throw ScriptException("Tried to get table from table but type was invalid");

		Table tab;
		tab.lua = lua;
		tab.ref = lua_ref(lua, true);
		lua_pop(lua, 1);
		return tab;
	}

	bool Table::isNil(const char *name) const {
		lua_getref(lua, ref);
		lua_pushstring(lua, name);
		lua_rawget(lua, -2);
		bool isnil = ( lua_isnil(lua, -1 ) != 0 );
		lua_pop(lua, 2);
		return isnil;
	}

	bool Table::isNil(int index) const {
		lua_getref(lua, ref);
		lua_rawgeti(lua, -1, index);
		bool isnil = ( lua_isnil(lua, -1 ) != 0 );
		lua_pop(lua, 2);
		return isnil;
	}

	void Table::pushMember(int index) const {
		lua_getref(lua, ref);
		lua_rawgeti(lua, -1, index);
		lua_remove(lua, -2);
	}

	void Table::pushMember(const char *name) const {
		lua_getref(lua, ref);
		lua_pushstring(lua, name);
		lua_rawget(lua, -2);
		lua_remove(lua, -2);
	}

	int Table::size() const {
		lua_getref(lua, ref);
		int size = luaL_getn(lua, -1);
		lua_pop(lua, 1);
		return size;
	}
}

