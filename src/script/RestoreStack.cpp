#include "RestoreStack.h"
#include "VM.h"
#include <iostream>

extern "C" {
#include <lua50/lua.h>
}

namespace script {
	RestoreStack::RestoreStack( VM* vm ) :
		lua( vm->luaState() )	{
		top = lua_gettop(lua);
	}

	RestoreStack::RestoreStack( lua_State* lua ) :
		lua( lua ) {
		top = lua_gettop(lua);
	}

	RestoreStack::~RestoreStack() {
		if( lua_gettop(lua) != top ) {
			std::cerr << "Bad top " << top - lua_gettop(lua) << std::endl;
			lua_settop( lua, top );
		} else {
			std::cerr << "Good top" << std::endl;
		}
	}
}

