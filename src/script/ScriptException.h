#ifndef SCRIPT_SCRIPTEXCEPTION_H
#define SCRIPT_SCRIPTEXCEPTION_H

//#include <Exception.h>
#include <string>

namespace script {
	//! Thrown if the application or script performs invalid script operation.
	class ScriptException /*: public Exception*/ {
		public:
//			ScriptException() : Exception(msg) {}
//			ScriptException(const std::string & msg) : Exception(msg) {}
//			ScriptException(const char *msg) : Exception(msg) {}
			ScriptException() : msg(msg) {}
			ScriptException(const std::string & msg) : msg(msg) {}
			ScriptException(const char *msg) : msg(msg) {}

		public:
			const std::string & message() { return msg; }

		private:
			std::string msg;
	};
}
#endif

