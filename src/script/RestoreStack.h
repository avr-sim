#ifndef _SCRIPT_RESTORESTACK_H
#define _SCRIPT_RESTORESTACK_H

struct lua_State;
namespace script {
	class VM;

	/**
	 * @author Tom Haber
	 * @date May 23, 2008
	 * @brief
	 *
	 * Restores script VM stack top at the end of the scope.
	 * @warning This is a convenience class for debugging and
	 * should not be used for anything else.
	 */
	class RestoreStack {
		public:
			RestoreStack( VM* vm );
			RestoreStack( lua_State* lua );
			~RestoreStack();

		private:
			lua_State*	lua;
			int			top;

		private:
			RestoreStack( const RestoreStack& );
			RestoreStack& operator=( const RestoreStack& );
	};
}
#endif

