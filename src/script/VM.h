#ifndef SCRIPT_VM_H
#define SCRIPT_VM_H

#include <string>
#include "Table.h"

struct lua_State;
namespace script {

	/**
	 * @author Tom Haber
	 * @date May 23, 2008
	 * @brief Script virtual machine for lua
	 *
	 * Script virtual machine for lua. Contains script execution
	 * environment like globals, stack, etc.
	 */
	class VM {
		public:
			/** C function prototype. */
			typedef int (*CFunction)(lua_State *);

			enum Type {
				None = -1,
				Nil = 0,
				Boolean = 1,
				LightUserData = 2,
				Number = 3,
				String = 4,
				Table = 5,
				Function = 6,
				UserData = 7,
				Thread = 8
			};

		public:
			/**
			  Initializes virtual machine.

			  @exception ScriptException
			 */
			VM();
			~VM();

		public:
			/**
			  Compiles script from ASCII-text file to executable code.

			  @exception ScriptException
			 */
			void compileFile(const char *name);

			/**
			  Compiles script from zero-terminated string to executable code.

			  @exception ScriptException
			 */
			void compileSource(const std::string & src);

			/**
			  Compiles script from buffer to executable code.

			  @exception ScriptException
			 */
			void compileSource(const char *buffer, int size);

			/** Force garbage collection. */
			void gc();

			/**
			  Returns specified stack trace level.
			  Level 0 is current function.
			 */
			void stackTrace(int level, std::string & trace) const;

			/**
			  Prints error message to debug output and stderr.
			  Function accepts single parameter, message to be printed.
			  The function expects VM* as closure.
			  printMessage is set to '_ERRORMESSAGE' in
			  environment initialization.
			 */
			static int printError(lua_State *lua);

			/**
			 * Prints message to debug output.
			 * Function accepts single parameter, message to be printed.
			 * printMessage is set to 'trace' in environment initialization.
			 */
			static int printMessage(lua_State* lua);

		public:
			/** Pushes global variable value to the stack. */
			void getGlobal(const std::string & name);
			void getGlobal(const char *name);

			/** Sets global variable value and pops value from the stack. */
			void setGlobal(const std::string & name);
			void setGlobal(const char *name);

			/**
			  Iterates table. Pops key and pushes key-value pair.
			  Use nil key to start the iteration.
			  If no more elements then the function returns false
			  and does not push anything.

			  @param index Index of the table in stack.
			  @return If no more elements then false.
			 */
			bool next(int index);

			/** Pops n items from the stack. */
			void pop(int n = 1);

			/**
			  Sets value at specified key in a table.
			  Key-value pair is popped from the stack.

			  @param index Index of the table in stack.
			 */
			void setTable(int index);

			/**
			  Sets value at specified key in a table.
			  Key-value pair is popped from the stack.
			  Metadata method is not invoked.

			  @param index Index of the table in stack.
			 */
			void setTableRaw(int index);

			/**
			  Sets value at nth key in a table.
			  Value is popped from the stack. Metadata method is not invoked.

			  @param index Index of the table in stack.
			 */
			void setTableRawI(int index, int n);

			void call(int args, int results);
			void registerClass(const char *name, const script::Table & table);
			void unregisterClass(const char *name);

		public:
			/** Pushes a copy of an item in the stack. */
			void pushValue(int index);
			/** Pushes nil to the stack. */
			void pushNil();
			/** Pushes number to the stack. */
			void pushNumber(float x);
			/** Pushes boolean to the stack. */
			void pushBoolean(bool b);
			/** Pushes string to the stack. */
			void pushString(const std::string & str);
			/** Pushes string to the stack. */
			void pushString(const char *str);
			/** Pushes table to the stack. */
			void pushTable(const script::Table & tab);

			/**
			  Pushes C function to the stack.
			  Same as C function closure with zero parameters.
			 */
			void pushCFunction(CFunction f);
			/** Pushes C function closure to the stack with n parameters. */
			void pushCClosure(CFunction f, int n);

			/** Removes an item from the stack. */
			void remove(int index);
			/** Returns type of an item in the stack. */
			int getType(int index) const;
			/** Returns name of the type returned by getType(). */
			const char *getTypeName(int type) const;

			bool isType(int index, Type type) const;
			/** Returns true if an item in the stack is a C function. */
			bool isCFunction(int index) const;
			/** Returns true if an item in the stack is a function. */
			bool isFunction(int index) const;
			/** Returns true if an item in the stack is a nil value. */
			bool isNil(int index) const;
			/** Returns true if an item in the stack is a number. */
			bool isNumber(int index) const;
			/** Returns true if an item in the stack is a string. */
			bool isString(int index) const;
			/** Returns true if an item in the stack is a table. */
			bool isTable(int index) const;
			/** Returns true if an item in the stack is user data. */
			bool isUserData(int index) const;
			/** Returns true if an item in the stack is 1 or nil. */
			bool isBoolean(int index) const;

			/** Compares two items in the stack. */
			bool isEqual(int index1, int index2) const;
			/** Compares two items in the stack. */
			bool isLess(int index1, int index2) const;

			/** Returns available stack space in bytes. */
			int stackSpace() const;
			/** Returns stack top index. */
			int top() const;

			/** Returns boolean from the stack. */
			bool toBoolean(int index) const;

			/** Returns number from the stack. */
			float toNumber(int index) const;
			/** Returns string from the stack. */
			const char *toString(int index) const;
			/** Returns table reference from the stack. */
			script::Table toTable(int index) const;
			/** Returns C function from the stack. */
			CFunction toCFunction(int index) const;
			/** Returns user data from the stack. */
			void *toUserData(int index) const;

			lua_State *luaState() const;

		public:
			/** Pushes referred object to the stack. */
			void getRef(int ref);

			/** Releases a reference. */
			void unref(int ref);

			/**
			  Pops a value from the stack and creates reference to it.
			  Locked item can't be garbage collected.
			 */
			int ref(bool lock);

		private:
			lua_State *lua;
			std::string	err;

		private:
			VM(const VM &);
			VM & operator=(const VM &);
	};

	inline lua_State *VM::luaState() const {
		return lua;
	}
}
#endif

