#ifndef SCRIPT_SCRIPTUTIL_H
#define SCRIPT_SCRIPTUTIL_H

#include "Scriptable.h"
#include "VM.h"
#include <string>

namespace script {
	/**
	 * @author Tom Haber
	 * @date May 23, 2008
	 * @brief Method type of scriptable object
	 *
	 * Method type of scriptable object. Convenience class
	 * for representing scripting methods on a Scriptable class.
	 */
	template <class T>
	class ScriptMethod {
		public:
			/** Method prototype. */
			typedef int (T::*FuncType)(const char *funcName);

		public:
			/**
			  Creates method with specified name and function address.
			  Note that the name is *not* copied.
			 */
			ScriptMethod(const char *name, FuncType func)
									: name(name), func(func) {}

		public:
			/** Returns name of the method. */
			const char *methodName() const { return name; }

			/** Returns function address of the method. */
			FuncType function() const { return func; }

		private:
			const char *name;
			FuncType func;
	};

	/**
	 * @author Tom Haber
	 * @date May 23, 2008
	 * @brief Helper functions for scripting.
	 *
	 * Helper functions for scripting.
	 * @param T Derived scriptable class.
	 * @param B Base scriptable class.
	 */
	template <class T, class B>
	class ScriptUtil {
		public:
			/**
			  Adds n C++ methods to be usable from the script.

			  @return Base index for the added methods.
			 */
			static int addMethods(T *obj, ScriptMethod<T> *methods, int n) {
				int methodBase = 0;

				if( n > 0 ) {
					methodBase = obj->addMethod( methods[0].methodName() );
					for(int i = 1; i < n ; ++i)
						obj->addMethod( methods[i].methodName() );
				}

				return methodBase;
			}

			/**
			  Calls C++ method. If the index is out of range then base
			  class (B) methodCall is called.

			  @param obj The object calling this method.
			  @param vm Script virtual machine requesting the method execution.
			  @param i Index of the method to be executed.
			  @param methodBase Base index for methods of this class.
			  @param methods An array of methods of this class.
			  @param n Number of methods in the array.
			  @return Number of return values in the script stack.
			 */
			static int methodCall(T *obj, int i, int methodBase,
										ScriptMethod<T>* methods, int n ) {
				int index = i - methodBase;
				if( index >= 0 && index < n )
					return ( obj->*methods[index].function() )(methods[index].methodName() );
				else
					return obj->B::methodCall(i);
			}
	};
}
#endif

