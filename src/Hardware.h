#ifndef AVR_HARDWARE_H
#define AVR_HARDWARE_H

namespace avr {

	class Bus;
	class IORegister;

	/**
	 * @author Tom Haber
	 * @date Apr 26, 2008
	 * @brief Abstraction of all internal devices
	 *
	 * This is an abstraction of all the internal devices inside
	 * an AVR chip. These encompass timers, usart, spi, etc
	 *
	 * Hardware can negotiate its timing with the Bus,
	 * \e step is called based on this timing.
	 */
	class Hardware {
		public:
			Hardware(Bus & bus) : bus(bus), holdCycles(0) {}
			virtual ~Hardware() {}

		public:
			/**
			 * Attach a register with name \e name to the hardware.
			 */
			virtual bool attachReg(const char *name, IORegister *reg) = 0;

			/**
			 * Finishes the construction of the hardware.
			 * This should verify the registers and parameters
			 * @returns true if build was successful.
			 */
			virtual bool finishBuild() { return true; }

			/**
			 * An attached register changed state.
			 */
			virtual void regChanged( IORegister *reg ) = 0;

			/**
			 * An attached register is accessed.
			 */
			virtual void regAccess( IORegister * /*reg*/ ) {}

			/**
			 * Perform a single step.
			 */
			virtual void step() {}

			/**
			 * Reset the internal hardware.
			 */
			virtual void reset() {}

			/**
			 * Called just before an interrupt handler is invoked.
			 */
			virtual void beforeInvokeInterrupt(unsigned int /*vector*/) {}

		public:
			/**
			 * Are we holding the CPU?
			 * @warning only call this once per CPU cycle.
			 */
			bool isHoldingCPU();

		protected:
			/**
			 * Set the number of cycles to hold the CPU.
			 */
			void setHoldCycles(unsigned int cycles);

		protected:
			Bus & bus;
			unsigned int holdCycles;
	};

	inline void Hardware::setHoldCycles(unsigned int cycles) {
		holdCycles = cycles;
	}

	inline bool Hardware::isHoldingCPU() {
		if( holdCycles != 0 )
			holdCycles--;

		return (holdCycles != 0);
	}

}

#endif /*AVR_HARDWARE_H*/
